package com.sinvie.emergency_base_model;

import android.app.Application;


//import com.sinvie.emergency_base.SinvieOptions;


/**
 * @author: JonsonBob
 * @date: 2021/9/7
 * @Des: 类描述
 */
public class BaseApplication extends Application {

    //程序包名
    private static final String FRONT_PKG = "com.sinvie.emergency_base_model";

    @Override
    public void onCreate() {
        super.onCreate();

//        SinvieOptions.getInstance().init(this,FRONT_PKG);

    }
}
