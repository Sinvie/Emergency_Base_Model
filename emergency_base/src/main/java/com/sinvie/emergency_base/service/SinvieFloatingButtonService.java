package com.sinvie.emergency_base.service;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.huawei.ecterminalsdk.base.TsdkCallStreamInfo;
import com.huawei.ecterminalsdk.models.common.util.TsdkLogUtil;
import com.sinvie.emergency_conf.CallService.SinvieCallMgr;
import com.sinvie.emergency_conf.CommonService.util.SinvieLogUtil;
import com.sinvie.emergency_conf.CommonService.util.SinvieStringUtil;
import com.sinvie.emergency_conf.ConferenceService.SinvieMeetingMgr;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.ui.conference.SinvieConfManagerActivity;

import java.util.List;

/**
 * Created by lh on 2020/04/07.
 */
public class SinvieFloatingButtonService extends Service {

    private static final String TAG = SinvieFloatingButtonService.class.getSimpleName();

    // 定义浮动窗口布局 Scheduling restart of crashed
    private LinearLayout mFloatLayout;
    private WindowManager.LayoutParams wmParams;
    // 创建浮动窗口设置布局参数的对象
    private WindowManager mWindowManager;

    private RelativeLayout mFloatView;
    private TextView mShow;

    private TsdkCallStreamInfo mQosInfo;
    private Handler mHandler;
    private Runnable mRunnable;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate() {
        super.onCreate();
        wmParams = new WindowManager.LayoutParams();
        // 通过getApplication获取的是WindowManagerImpl.CompatModeWrapper
        mWindowManager = (WindowManager) getApplication().getSystemService(getApplication().WINDOW_SERVICE);
        // 设置window type
        if (Build.VERSION.SDK_INT >= 26) {
            wmParams.type = 2038;
        } else {
            wmParams.type = WindowManager.LayoutParams.TYPE_PHONE;
        }
        // 设置图片格式，效果为背景透明
        wmParams.format = PixelFormat.RGBA_8888;
        // 设置浮动窗口不可聚焦（实现操作除浮动窗口外的其他可见窗口的操作）
        wmParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        // 调整悬浮窗显示的停靠位置为左侧底部
        wmParams.gravity = Gravity.LEFT | Gravity.BOTTOM;
        // 以屏幕左上角为原点，设置x、y初始值，相对于gravity
        wmParams.x = 0;
        wmParams.y = 150;
        // 设置悬浮窗口长宽数据
        wmParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        wmParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        if (mHandler != null) {
            mHandler = null;
        }
        if (mRunnable != null) {
            mRunnable = null;
        }
        mHandler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {
                // 辅流发送的质量统计显示
                int mCallID = SinvieMeetingMgr.getInstance().getCurrentConferenceCallID();
                mQosInfo = SinvieCallMgr.getInstance().getCallQuality(mCallID);
                SinvieLogUtil.d(TAG,mCallID +"");
                if (mQosInfo != null) {
                    if (mShow != null) {
                        if (SinvieStringUtil.isNotEmpty(mQosInfo.getDataStreamInfo().getEncodeName().trim())) {
                            mShow.setText(mQosInfo.getDataStreamInfo().getEncodeName() + System.lineSeparator() +
                                    mQosInfo.getDataStreamInfo().getSendBitRate() + "Kbps" + System.lineSeparator() +
                                    mQosInfo.getDataStreamInfo().getEncoderSize() + System.lineSeparator() +
                                    mQosInfo.getDataStreamInfo().getSendFrameRate() + "fps" + System.lineSeparator() +
                                    mQosInfo.getDataStreamInfo().getSendLossFraction() + "%" + System.lineSeparator() +
                                    mQosInfo.getDataStreamInfo().getSendDelay() + "ms" + System.lineSeparator() +
                                    mQosInfo.getDataStreamInfo().getSendJitter() + "ms" + System.lineSeparator()
                            );
                        }
                    }
                }
                if (mHandler != null) {
                    mHandler.postDelayed(this, 2000);
                }
            }
        };
        if (mHandler != null) {
            mHandler.postDelayed(mRunnable, 2000);
        }

        // 创建悬浮按钮
        createFloatView();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressWarnings("static-access")
    @SuppressLint("InflateParams")
    private void createFloatView() {

        // 获取浮动窗口视图所在布局
        mFloatLayout = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.view_sinvie_layout_floating_window, null);
        // 添加mFloatLayout
        mWindowManager.addView(mFloatLayout, wmParams);
        // 浮动窗口按钮
        mFloatView = (RelativeLayout) mFloatLayout.findViewById(R.id.rlyt_float_stop_share);
        mShow = (TextView) mFloatLayout.findViewById(R.id.tv_show);
        // 设置监听浮动窗口的触摸移动监听
        FloatingOnTouchListener floatingOnTouchListener = new FloatingOnTouchListener();
        mFloatView.setOnTouchListener(floatingOnTouchListener);
        // 设置监听浮动窗口的点击监听
        mFloatView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // app切换到前台
                mFloatView.setVisibility(View.GONE);
                ActivityManager activityManager = (ActivityManager) SinvieFloatingButtonService.this.getSystemService(Context.ACTIVITY_SERVICE);
                List<ActivityManager.RunningTaskInfo> taskInfoList = activityManager.getRunningTasks(20);
                for (ActivityManager.RunningTaskInfo taskInfo : taskInfoList) {
                    if (taskInfo.topActivity.getPackageName().equals(SinvieFloatingButtonService.this.getPackageName())) {
                        TsdkLogUtil.i("FloatingButtonService", "find task to front");
                        activityManager.moveTaskToFront(taskInfo.id, 0);
                        return;
                    }
                }
                TsdkLogUtil.i("FloatingButtonService", "find task failed,intent start activity");
                Intent resultIntent = new Intent(SinvieFloatingButtonService.this, SinvieConfManagerActivity.class);
                resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                SinvieFloatingButtonService.this.startActivity(resultIntent);
            }
        });
    }

    private class FloatingOnTouchListener implements View.OnTouchListener {
        private float rawX;
        private float rawY;

        @SuppressLint("ClickableViewAccessibility")
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    rawX = event.getRawX();
                    rawY = event.getRawY();
                    break;
                case MotionEvent.ACTION_MOVE:
                    // getRawX是触摸位置相对于屏幕的坐标，getX是相对于按钮的坐标
                    int distanceX = (int) (event.getRawX() - rawX);
                    int distanceY = (int) (event.getRawY() - rawY);
                    wmParams.x = wmParams.x + distanceX;
                    wmParams.y = wmParams.y - distanceY;
                    // 刷新
                    mWindowManager.updateViewLayout(mFloatLayout, wmParams);
                    rawX = event.getRawX();
                    rawY = event.getRawY();
                    break;
                case MotionEvent.ACTION_UP:
                    break;
            }
            return false;
        }
    }

    @Override
    public void onDestroy() {
        if (mFloatLayout != null) {
            // 移除悬浮窗口
            if (mWindowManager != null) {
                mWindowManager.removeView(mFloatLayout);
            }
        }

        if (mHandler != null) {
            if (mRunnable != null) {
                mHandler.removeCallbacks(mRunnable);
                mRunnable = null;
                mHandler = null;
            }
        }
        super.onDestroy();
    }
}


