package com.sinvie.emergency_base.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.utils.widget.ImageFilterView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Group;
import androidx.core.content.ContextCompat;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.utils.SinvieAudioRecordUtils;
import com.sinvie.emergency_base.widget.popup.SinvieSentVoicePopup;

import java.io.File;

/**
 * @author: JonsonBob
 * @date: 2021/8/20
 * @Des: 类描述
 */
public class SinvieSendVoiceBtn extends LinearLayout {
    private static final String TAG = SinvieSendVoiceBtn.class.getSimpleName();

    private Context mContext;
    private ConstraintLayout mClSendVoice;
    private Group mGroupSendNormal;
    private Group mGroupSendAfter;
    private ImageFilterView mIvSendVoiceStart;
    private TextView mTvSendVoice;
    private ImageFilterView mIvSendVoiceEnd;
    private ImageFilterView mIvSendVoiceStatus;
    private TextView mTvSendVoiceTime;

    /**
     * 倒计时 popupWindow
     */
    private SinvieSentVoicePopup mSinvieSentVoicePopup;

    /**
     * 录音
     */
    private SinvieAudioRecordUtils mSinvieAudioRecordUtils;

    private String mAudioLength ;

    public SinvieSendVoiceBtn(Context context) {
        super(context);
        initView(context);
    }

    public SinvieSendVoiceBtn(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public SinvieSendVoiceBtn(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    public SinvieSendVoiceBtn(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context);
    }
    private void initView(Context context) {
        mContext = context;
        inflate(context, R.layout.view_sinvie_send_voice_btn, this);

        mClSendVoice = findViewById(R.id.cl_send_voice);
        mGroupSendNormal = findViewById(R.id.group_send_normal);
        mGroupSendAfter = findViewById(R.id.group_send_after);
        mIvSendVoiceStart = findViewById(R.id.iv_send_voice_start);
        mTvSendVoice = findViewById(R.id.tv_send_voice);
        mIvSendVoiceEnd = findViewById(R.id.iv_send_voice_end);
        mIvSendVoiceStatus = findViewById(R.id.iv_send_voice_status);
        mTvSendVoiceTime = findViewById(R.id.tv_send_voice_time);
        setResetBtn();

        mSinvieSentVoicePopup = new SinvieSentVoicePopup(mContext);

        initAudio();

        mSinvieSentVoicePopup.setOnSendVoiceListener(new SinvieSentVoicePopup.OnSendVoiceListener() {
            @Override
            public void onCountDownFinishListener() {
                //倒计时停止
                mAudioLength = "60";
            }

            @Override
            public void onCountDownDismissListener(String time) {
                //主动停止
                mAudioLength = time;
            }
        });

    }

    private void initAudio() {
        mSinvieAudioRecordUtils = new SinvieAudioRecordUtils();

        mSinvieAudioRecordUtils.setOnRecorderListener(new SinvieAudioRecordUtils.OnRecorderListener() {
            @Override
            public void onStartPlayer() {//播放开始监听
//                isplay = true;
            }

            @Override
            public void onFinishPlayer() {//播放完成监听
//                isplay = false;
            }

            @Override
            public void onStopPlayer() {//播放暂停监听
//                isplay = false;
            }

            @Override
            public void onFailurePlayer() {//播放失败监听
//                isplay = false;
            }

            @Override
            public void onStartRecorder() {//录音开始监听
//                countDownTimer.start();
//                isrecord = true;

                //popup提示消息
                mSinvieSentVoicePopup.setPopupTips(mContext.getString(R.string.sinvie_report_sent_voice_press_tips));
                //重设popup 背景
                mSinvieSentVoicePopup.setPopupBg(R.drawable.svg_sinvie_send_voice_popup_normal_pg);
                //显示popup
                mSinvieSentVoicePopup.show(SinvieSendVoiceBtn.this);
                //修改按钮状态
                setSendingBtn();
            }

            @Override
            public void onCancelRecorder() {//录音取消监听
//                if (audioRecord != null) {
//                    audioRecord.createFileName(false);
//                }
//                isrecord = false;
                setResetBtn();
            }

            @Override
            public void onStopRecorder(File file) {//录音结束监听
//                countDownTimer.onFinish();
//                countDownTimer.cancel();
//                isrecord = false;
                //关闭dialog
                mSinvieSentVoicePopup.dismiss();
                //显示录音长度
                setAfterSendBtn(mAudioLength);
            }

            @Override
            public void onFailureRecorder() {//录音失败监听
//                if (audioRecord != null) {
//                    audioRecord.onCancelRecorder();
//                }
//                isrecord = false;
                setResetBtn();
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //按下位置
        float startY = 0;
        //抬起位置
        float endY = 0;
        //是否发送
        boolean send = false;
        //touch 监听
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                //按下 开始录音 记录位置
                startY = event.getY();
                //开始录音
//                mAudioRecordUtils.OnRecorder(true);
//                // popup提示消息
                mSinvieSentVoicePopup.setPopupTips(mContext.getString(R.string.sinvie_report_sent_voice_press_tips));
                //重设popup 背景
                mSinvieSentVoicePopup.setPopupBg(R.drawable.svg_sinvie_send_voice_popup_normal_pg);
                //显示popup
                mSinvieSentVoicePopup.show(SinvieSendVoiceBtn.this);
                //修改按钮状态
                setSendingBtn();
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                //抬起 停止录音 记录位置
                endY = event.getY();
                //结束录音
//                mAudioRecordUtils.OnRecorder(false);
//                //popup关闭
                mSinvieSentVoicePopup.dismiss();
                //显示录音长度
                setAfterSendBtn(mAudioLength);
                break;
            case MotionEvent.ACTION_MOVE:
                //滑动到一定的距离取消录音
                float moveY = event.getY();
                if (moveY < -100) {
                    //改变popup 提示
                    mSinvieSentVoicePopup.setPopupTips(getContext().getString(R.string.sinvie_report_sent_voice_move_tips));
                    //改变popup 背景
                    mSinvieSentVoicePopup.setPopupBg(R.drawable.svg_sinvie_send_voice_popup_warning_pg);

                }else{
                    //改变popup 提示
                    mSinvieSentVoicePopup.setPopupTips(getContext().getString(R.string.sinvie_report_sent_voice_press_tips));
                    //改变popup 背景
                    mSinvieSentVoicePopup.setPopupBg(R.drawable.svg_sinvie_send_voice_popup_normal_pg);
                }
                break;
        }
        return true;
    }

    /**
     * 重置按钮
     */
    public void setResetBtn(){
        //隐藏两侧图片
        mGroupSendNormal.setVisibility(GONE);
        //隐藏播放后图片
        mGroupSendAfter.setVisibility(GONE);
        //显示中间文字
        mTvSendVoice.setVisibility(VISIBLE);
        //改变中间文字颜色为黑色
        mTvSendVoice.setTextColor(ContextCompat.getColor(mContext,R.color.sinvie_theme_text_black_color));
        //设置中间文字 按住说话
        mTvSendVoice.setText(R.string.sinvie_hold_to_speak);
        //改变背景为灰色
        mClSendVoice.setBackgroundResource(R.drawable.bg_sinvie_corners_dark_white_20);
    }

    /**
     * 正在录音状态
     */
    private void setSendingBtn(){
        //显示两侧图片
        mGroupSendNormal.setVisibility(VISIBLE);
        //隐藏播放后图片
        mGroupSendAfter.setVisibility(GONE);
        //显示中间文字
        mTvSendVoice.setVisibility(VISIBLE);
        //改变中间文字颜色为蓝色
        mTvSendVoice.setTextColor(ContextCompat.getColor(mContext,R.color.sinvie_theme_text_blue_color));
        //设置中间文字 松开发送
        mTvSendVoice.setText(R.string.sinvie_let_go_to_send);
        //改变背景为灰色
        mClSendVoice.setBackgroundResource(R.drawable.bg_sinvie_corners_dark_white_20);
    }

    /**
     * 录音结束状态
     */
    private void setAfterSendBtn(String time){
        //隐藏两侧图片
        mGroupSendNormal.setVisibility(GONE);
        //隐藏中间文字
        mTvSendVoice.setVisibility(GONE);
        //显示播放后图片
        mGroupSendAfter.setVisibility(VISIBLE);
        //设置时间长度
        mTvSendVoiceTime.setText(time);
        //改变背景为蓝色
        mClSendVoice.setBackgroundResource(R.drawable.bg_sinvie_corners_dark_blue_20);
    }

    /**
     * 清除录音文件
     */
    public void clearVoiceData(){
        mSinvieAudioRecordUtils.onDeleteRecorder();
    }

    /**
     * 关闭录音
     */
    public void cancelVoiceRecorder(){
        mSinvieAudioRecordUtils.onCancelRecorder();
    }
}
