package com.sinvie.emergency_base.widget.dialog;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.constraintlayout.utils.widget.ImageFilterView;
import androidx.fragment.app.DialogFragment;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieReportTypeBean;
import com.sinvie.emergency_widget.selector.SinvieDataProvider;
import com.sinvie.emergency_widget.selector.ISinvieSelectAble;
import com.sinvie.emergency_widget.selector.SinvieSelectedListener;
import com.sinvie.emergency_widget.selector.widget.SinvieSelectorLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * 无限选择底部弹窗
 */
public class SinvieSelectorDialog extends DialogFragment {

    private static final String TAG = SinvieSelectorDialog.class.getSimpleName();

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";

    private ArrayList<SinvieReportTypeBean> mReportTypeList;
    private String mDialogTitle;
    private ArrayList<SinvieReportTypeBean> mSelectedReportTypeList;

    private ImageFilterView mIvDialogClose;
    private TextView mTvDialogTitle;
    private TextView mTvDialogRight;
    private SinvieSelectorLayout mLlSelector;

    private onSelectorListener mListener;

    public SinvieSelectorDialog() {
        // Required empty public constructor
    }

    /**
     * 选择监听
     */
    public void setListener(onSelectorListener listener) {
        mListener = listener;
    }


    public static SinvieSelectorDialog newInstance(ArrayList<SinvieReportTypeBean> arrayList, String title, ArrayList<SinvieReportTypeBean> selectedReportTypeList) {
        SinvieSelectorDialog fragment = new SinvieSelectorDialog();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM1, arrayList);
        args.putString(ARG_PARAM2, title);
        args.putParcelableArrayList(ARG_PARAM3, selectedReportTypeList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mReportTypeList = getArguments().getParcelableArrayList(ARG_PARAM1);
            mDialogTitle = getArguments().getString(ARG_PARAM2);
            mSelectedReportTypeList = getArguments().getParcelableArrayList(ARG_PARAM3);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // 下面这些设置必须在此方法(onStart())中才有效
        Window window = getDialog().getWindow();
        // 如果不设置这句代码, 那么弹框就会与四边都有一定的距离
        window.setBackgroundDrawableResource(android.R.color.transparent);
        // 设置动画
        window.setWindowAnimations(R.style.sinvie_bottomSheet_animation);

        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.BOTTOM;
        // 如果不设置宽度,那么即使你在布局中设置宽度为 match_parent 也不会起作用
        params.width = getResources().getDisplayMetrics().widthPixels;
        params.height = (int) ((getResources().getDisplayMetrics().heightPixels) * 0.5);
        window.setAttributes(params);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.view_sinvie_dialog_selector, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        mIvDialogClose = view.findViewById(R.id.iv_title_corner_20_close);
        mTvDialogTitle = view.findViewById(R.id.tv_title_corner_20_title);
        mTvDialogRight = view.findViewById(R.id.tv_title_corner_20_right);
        mLlSelector = view.findViewById(R.id.ll_selector);

        mTvDialogTitle.setText(mDialogTitle);
        mIvDialogClose.setVisibility(View.VISIBLE);
        mTvDialogRight.setVisibility(View.VISIBLE);

        mLlSelector.setDataProvider(mDataProvider);
        mLlSelector.setSelectedListener(mSelectedListener);

        mTvDialogRight.setOnClickListener(v -> dismiss());
        mIvDialogClose.setOnClickListener(v -> dismiss());

        if (mSelectedReportTypeList != null && !mSelectedReportTypeList.isEmpty()) {
            //设置默认选择
            setSelectAblesList(0, mReportTypeList);
        }
    }

    private final SinvieDataProvider mDataProvider = new SinvieDataProvider() {
        @Override
        public void provideData(int currentDeep, int preId, ISinvieSelectAble selectAble, DataReceiver receiver) {
            //判断子类是否为空 为空就停止选择
            if (selectAble == null) {
                //如果等级是在第一级 那就重新赋值 否则退出选择
                if (currentDeep == 0) {
                    receiver.send(getData(mReportTypeList));
                } else {
                    //如果是最后层级，返回null代表勾选完成
                    receiver.send(null);
                }
            } else {
                if (selectAble.getArg() == null) {
                    //如果是最后层级，返回null代表勾选完成
                    receiver.send(null);
                }
                //先判断是否属于list
                if (selectAble.getArg() instanceof List<?>) {
                    ArrayList<SinvieReportTypeBean> dataBean = new ArrayList<>();
                    for (Object reportTypeBean : (List<?>) selectAble.getArg()) {
                        if (reportTypeBean instanceof SinvieReportTypeBean) {
                            dataBean.add((SinvieReportTypeBean) reportTypeBean);
                        }
                    }
                    boolean hasNext = dataBean.isEmpty();
                    if (hasNext) {
                        dataBean.add(new SinvieReportTypeBean());
                        //即使是最后的等级也要填充数据 即使是空的数据
//                        receiver.send(getData(dataBean));
                        //如果是最后层级，返回null代表勾选完成
                        receiver.send(null);
                    } else {
                        //从网络或者数据库获取数据
                        receiver.send(getData(dataBean));
                    }
                }
            }

        }
    };

    private final SinvieSelectedListener mSelectedListener = new SinvieSelectedListener() {
        @Override
        public void onAddressSelected(ArrayList<ISinvieSelectAble> selectAbles) {
            mListener.onSelectorListener(selectAbles);
//            dismiss();

        }
    };

    /**
     * 和总数据进行每一层的数据对比
     *
     * @param index          当前层数
     * @param reportTypeList 当前层数所有类型
     */
    public void setSelectAblesList(int index, ArrayList<SinvieReportTypeBean> reportTypeList) {
        //每次获取当前层所选中的 item 数据
        SinvieReportTypeBean bean = mSelectedReportTypeList.get(index);
        if (mLlSelector == null || bean == null || reportTypeList == null) {
            return;
        }
        //遍历当前层数据
        for (int i = 0; i < reportTypeList.size(); i++) {
            //找到所对应的事件 id 记录该id 对应的 position
            if (bean.getId() == reportTypeList.get(i).getId()) {
                mLlSelector.setDefaultSelector(i);
                index += 1;
                if (index < mSelectedReportTypeList.size()) {
                    setSelectAblesList(index, (ArrayList<SinvieReportTypeBean>) reportTypeList.get(i).getChildren());
                } else {
                    return;
                }
            }
        }

    }

    /**
     * 数据转换
     */
    public static ArrayList<ISinvieSelectAble> getData(ArrayList<SinvieReportTypeBean> beanList) {

        ArrayList<ISinvieSelectAble> data = new ArrayList<>();
        for (SinvieReportTypeBean sinvieReportTypeBean : beanList) {
            data.add(new ISinvieSelectAble() {
                @Override
                public String getName() {
                    return sinvieReportTypeBean.getName();
                }

                @Override
                public int getId() {
                    return sinvieReportTypeBean.getId();
                }

                @Override
                public Object getArg() {
                    return sinvieReportTypeBean.getChildren();
                }
            });
        }
        return data;
    }

    public interface onSelectorListener {
        /**
         * 选择返回监听
         */
        void onSelectorListener(ArrayList<ISinvieSelectAble> selectAbles);
    }

}