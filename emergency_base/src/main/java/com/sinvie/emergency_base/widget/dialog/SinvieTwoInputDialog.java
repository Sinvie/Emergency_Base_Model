package com.sinvie.emergency_base.widget.dialog;

import android.content.Context;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.sinvie.emergency_base.R;


public class SinvieTwoInputDialog extends SinvieBaseDialog {
    private TextView titleTV;
    private EditText inputET1;
    private EditText inputET2;
    private ImageView clearNameIV;
    private ImageView clearNumberIV;


    public SinvieTwoInputDialog(Context context) {
        super(context);
        setContentView(R.layout.view_sinvie_layout_three_input);
        setLeftButtonListener(null);
        setCanceledOnTouchOutside(false);
        titleTV = (TextView) findViewById(R.id.dialog_title);
        titleTV.setText(R.string.sinvie_add_member);
        inputET1 = (EditText) findViewById(R.id.dialog_edittext);
        inputET2 = (EditText) findViewById(R.id.dialog_edittext1);

        clearNameIV = (ImageView) findViewById(R.id.ivClearText);
        clearNumberIV = (ImageView) findViewById(R.id.ivClearText1);
        clearNameIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputET1.setText("");
            }
        });
        clearNumberIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputET2.setText("");
            }
        });
    }

    public void setEditTextOneCiphertext() {
        TransformationMethod method = PasswordTransformationMethod.getInstance();
        inputET1.setTransformationMethod(method);
    }

    public void setEditTextTwoCiphertext() {
        TransformationMethod method = PasswordTransformationMethod.getInstance();
        inputET2.setTransformationMethod(method);
    }

    public EditText getEditTextOne() {
        return inputET1;
    }

    public EditText getEditTextTwo() {
        return inputET2;
    }

    public String getInput1() {
        return inputET1.getText() == null ? "" : inputET1.getText().toString().trim();
    }

    public String getInput2() {
        return inputET2.getText() == null ? "" : inputET2.getText().toString().trim();
    }

    public void setHint1(int hint1) {
        inputET1.setHint(hint1);
    }

    public void setHint2(int hint2) {
        inputET2.setHint(hint2);
    }
}
