package com.sinvie.emergency_base.widget.dialog;

import android.content.Context;

import com.sinvie.emergency_base.R;


/**
 * This class is about confirmation dialog box, left and right two buttons
 * 确认对话框，左右两个按钮。
 * 该对话框，不带标题，已经弃用，请使用ConfirmTitleDialog替代。
 */
public class SinvieConfirmDialog extends SinvieBaseDialog {
    public SinvieConfirmDialog(Context context, String message) {
        super(context);
        setContentView(R.layout.view_sinvie_dialog_confirm_title);
        setMessage(message);
        setCanceledOnTouchOutside(false);
        setLeftButtonListener(null);
        setRightButtonListener(null);
    }

    public SinvieConfirmDialog(Context context, int resId) {
        this(context, context.getString(resId));
    }

}
