package com.sinvie.emergency_base.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.VideoView;

/**
 * @author: JonsonBob
 * @date: 2021/8/9
 * @Des: 类描述
 */
public class SinvieCustomVideoView extends VideoView {

    private PlayPauseListener mListener;

    public SinvieCustomVideoView(Context context) {
        super(context);
    }

    public SinvieCustomVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SinvieCustomVideoView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setPlayPauseListener(PlayPauseListener listener) {
        mListener = listener;
    }

    @Override
    public void pause() {
        super.pause();
        if (mListener != null) {
            mListener.onPause();
        }
    }

    @Override
    public void start() {
        super.start();
        if (mListener != null) {
            mListener.onPlay();
        }
    }

   public interface PlayPauseListener {
        void onPlay();
        void onPause();
    }

}
