package com.sinvie.emergency_base.widget.popup;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.widget.PopupWindowCompat;

import com.sinvie.emergency_base.R;

/**
 * @author: JonsonBob
 * @date: 2021/8/21
 * @Des: 类描述
 */
public class SinvieSentVoicePopup extends PopupWindow {

    private static final String TAG= SinvieSentVoicePopup.class.getSimpleName();

    private static Context mContext;
    private TextView mPopupCountDown, mPopupTips;
    private ConstraintLayout mPopupBg;
    private CountDownTimer mCountDownTimer;

    private long mTime = 0;
    /**
     * 倒计时间隔
     */
    private int MIN_INTERVAL = 1000;
    /**
     * 倒计时总长
     */
    private int COUNT_DOWN_TIME = 60 * MIN_INTERVAL;

    private OnSendVoiceListener mOnSendVoiceListener;

    public SinvieSentVoicePopup(Context context) {
        super(context);
        initView(context);
    }

    public SinvieSentVoicePopup(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public SinvieSentVoicePopup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    public SinvieSentVoicePopup(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context);
    }

    private void initView(Context context) {
        mContext = context;

        setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        setOutsideTouchable(true);
        setFocusable(true);
        setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View contentView = LayoutInflater.from(context).inflate(R.layout.view_sinvie_send_voice_popup, null, false);
        setContentView(contentView);

        mPopupCountDown = contentView.findViewById(R.id.tv_popup_sent_voice_time);
        mPopupTips = contentView.findViewById(R.id.tv_popup_sent_voice_status);
        mPopupBg = contentView.findViewById(R.id.cl_popup_send_voice_bg);
        initCountDown();
    }

    @SuppressWarnings("ResourceType")
    private static int makeDropDownMeasureSpec(int measureSpec) {
        int mode;
        if (measureSpec == ViewGroup.LayoutParams.WRAP_CONTENT) {
            mode = View.MeasureSpec.UNSPECIFIED;
        } else {
            mode = View.MeasureSpec.EXACTLY;
        }
        return View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(measureSpec), mode);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
            mCountDownTimer = null;
        }
        if (mOnSendVoiceListener!= null) {
            mOnSendVoiceListener.onCountDownDismissListener(mTime+"");
        }
    }

    /**
     * 录音时长
     *
     * @param time 时长 (s)
     */
    private void setPopupCountDown(String time, int colorId) {
        if (mPopupCountDown != null) {
            mPopupCountDown.setText(time);
            mPopupCountDown.setTextColor(ContextCompat.getColor(mContext, colorId));
        }
    }

    /**
     * 录音提示
     *
     * @param tips 提示
     */
    public void setPopupTips(String tips) {
        if (mPopupTips != null) {
            mPopupTips.setText(tips);
        }
    }

    /**
     * 录音背景
     *
     * @param bgId 资源文件id
     */
    public void setPopupBg(int bgId) {
        if (mPopupBg != null) {
            mPopupBg.setBackgroundResource(bgId);
        }
    }

    /**
     * 显示 popupWindow
     *
     * @param window   popupWindow
     * @param showView 所绑定的view
     */
    private void showAsDropDown(PopupWindow window, View showView) {
        View contentView = window.getContentView();
        //需要先测量，PopupWindow还未弹出时，宽高为0
        contentView.measure(makeDropDownMeasureSpec(window.getWidth()), makeDropDownMeasureSpec(window.getHeight()));

        //计算显示位置 居上 居中
        int offsetX = Math.abs(window.getContentView().getMeasuredWidth() - showView.getWidth()) / 2;
        int offsetY = -(window.getContentView().getMeasuredHeight() + showView.getHeight());
        PopupWindowCompat.showAsDropDown(window, showView, offsetX, offsetY, Gravity.START);
    }

    /**
     * 外放接口
     *
     * @param showView
     */
    public void show(View showView) {
        //显示 popupWindow
        showAsDropDown(this, showView);
        //显示倒计时
        startCountDown();
    }

    public void startCountDown() {

        if (mCountDownTimer == null) {
            initCountDown();
        }
        //开始显示倒计时
        mCountDownTimer.start();
    }

    /**
     * 初始化 倒计时
     */
    private void initCountDown() {

        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
            mCountDownTimer = null;
        }

        mCountDownTimer = new CountDownTimer(COUNT_DOWN_TIME, MIN_INTERVAL) {
            @Override
            public void onTick(long millisUntilFinished) {
                mTime =  millisUntilFinished  / MIN_INTERVAL;
                //显示倒计时
                if (mTime > 10) {
                    setPopupCountDown(mTime + " s", R.color.sinvie_white);
                } else {
                    setPopupCountDown(mTime + " s", R.color.sinvie_theme_reject_color);
                }
            }

            @Override
            public void onFinish() {
                //倒计时完毕
                if (mOnSendVoiceListener != null) {
                    mOnSendVoiceListener.onCountDownFinishListener();
                }
            }
        };
    }

    public void setOnSendVoiceListener(OnSendVoiceListener onSendVoiceListener) {
        mOnSendVoiceListener = onSendVoiceListener;
    }

    public interface OnSendVoiceListener {
        /**
         * 倒计时结束
         */
        void onCountDownFinishListener();

        void onCountDownDismissListener(String time);
    }
}
