package com.sinvie.emergency_base.widget.dialog;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.utils.SinvieCommonUtil;

/**
 * This class is about editable dialog box
 * 可编辑对话框
 */
public class SinvieEditDialog extends SinvieBaseDialog {
    private TextView tvSelect;
    private EditText editText;
    private ImageView ivClearText;
    // private Context context;

    public SinvieEditDialog(Context context, String title) {
        super(context);
        setContentView(R.layout.view_sinvie_dialog_editable);
        setTitle(title);
        setLeftButtonListener(null);
        // this.context = context;
        tvSelect = (TextView) findViewById(R.id.tv_select);
        editText = (EditText) findViewById(R.id.dialog_edittext);
        ivClearText = (ImageView) findViewById(R.id.ivClearText);
        setCanceledOnTouchOutside(false);
        ivClearText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                editText.setText("");
            }
        });
    }

    public SinvieEditDialog(Context context, int titleResId) {
        this(context, context.getString(titleResId));
    }

    public String getText() {
        return editText.getText() == null ? "" : editText.getText().toString().trim();
    }

    public void setTextStyle(String initText, int maxLength, int inputType) {
        SinvieCommonUtil.processEditTextWithNumber(editText, initText, maxLength, inputType);
        initEditText(initText);
    }

    private void initEditText(String initText) {
        //根据初始文本，设置按钮初始状态
        if (TextUtils.isEmpty(initText)) {
            getRightButton().setTextColor(SinvieLocContext.getResources()
                    .getColor(R.color.sinvie_textThirdly));
            getRightButton().setEnabled(false);
        }
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s.toString().trim())) {
                    getRightButton().setTextColor(SinvieLocContext.getResources()
                            .getColor(R.color.sinvie_textThirdly));
                    getRightButton().setEnabled(false);
                    ivClearText.setVisibility(View.GONE);

                } else {
                    getRightButton().setTextColor(SinvieLocContext.getResources()
                            .getColor(R.color.sinvie_dialog_button_color));
                    getRightButton().setEnabled(true);
                    ivClearText.setVisibility(View.VISIBLE);
                }
            }

        });
    }

    public void setText(String text) {
        editText.setText(text);
    }

    public EditText getEditText() {
        return editText;
    }

    public TextView getSelectTextView() {
        return tvSelect;
    }
}
