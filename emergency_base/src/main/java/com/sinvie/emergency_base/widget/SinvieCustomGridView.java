package com.sinvie.emergency_base.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * @author: JonsonBob
 * @date: 2021/7/11
 * @Des: 自定义 gridview 重写onMeasure方法 使其高度不会只显示item一行
 */
public class SinvieCustomGridView extends GridView {
    public SinvieCustomGridView(Context context) {
        super(context);
    }

    public SinvieCustomGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SinvieCustomGridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SinvieCustomGridView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    //不出现滚动条
    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }

}
