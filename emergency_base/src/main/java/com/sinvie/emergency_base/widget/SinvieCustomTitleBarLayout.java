package com.sinvie.emergency_base.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.utils.SinvieScreenUtil;
import com.sinvie.emergency_live.tuikit.base.ISinvieTitleBarLayout;

import org.jetbrains.annotations.NotNull;

/**
 * @author: JonsonBob
 * @date: 2021/7/1
 * @Des: 自定义 view 头部
 */
public class SinvieCustomTitleBarLayout extends LinearLayout implements ISinvieTitleBarLayout {
    private static final String TAG = SinvieCustomTitleBarLayout.class.getSimpleName();

    private LinearLayout mLeftGroup;
    private LinearLayout mRightGroup;
    private TextView mLeftTitle;
    private TextView mCenterTitle;
    private TextView mRightTitle;
    private ImageView mLeftIcon;
    private ImageView mRightIcon;
    private ImageView mRightSecondIcon;

    private RelativeLayout mTitleLayout;

    public SinvieCustomTitleBarLayout(@NonNull @NotNull Context context) {
        super(context);
        initialize(context);
    }

    public SinvieCustomTitleBarLayout(@NonNull @NotNull Context context,
                                      @Nullable @org.jetbrains.annotations.Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    public SinvieCustomTitleBarLayout(@NonNull @NotNull Context context,
                                      @Nullable @org.jetbrains.annotations.Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context);
    }

    private void initialize(Context context) {
        inflate(context, R.layout.view_sinvie_custom_title_bar, this);
        mTitleLayout = findViewById(R.id.custom_title_layout);
        mLeftGroup = findViewById(R.id.custom_title_left_group);
        mRightGroup = findViewById(R.id.custom_title_right_group);
        mLeftTitle = findViewById(R.id.custom_title_left_text);
        mRightTitle = findViewById(R.id.custom_title_right_text);
        mCenterTitle = findViewById(R.id.custom_title);
        mLeftIcon = findViewById(R.id.custom_title_left_icon);
        mRightIcon = findViewById(R.id.custom_title_right_icon);
        mRightSecondIcon = findViewById(R.id.custom_title_right_second_icon);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mTitleLayout.getLayoutParams();
        params.height = SinvieScreenUtil.getPxByDp(context,50);
        mTitleLayout.setLayoutParams(params);
        setBackgroundColor(context.getResources().getColor(R.color.sinvie_status_bar_color));
    }

    @Override
    public void setOnLeftClickListener(OnClickListener listener) {
        mLeftGroup.setOnClickListener(listener);
    }

    @Override
    public void setOnRightClickListener(OnClickListener listener) {
        mRightGroup.setOnClickListener(listener);
    }


    @Override
    public void setTitle(String title, POSITION position) {
        switch (position) {
            case LEFT:
                mLeftTitle.setText(title);
                break;
            case RIGHT:
                mRightTitle.setText(title);
                break;
            case MIDDLE:
                mCenterTitle.setText(title);
                break;
        }
    }

    @Override
    public LinearLayout getLeftGroup() {
        return mLeftGroup;
    }

    @Override
    public LinearLayout getRightGroup() {
        return mRightGroup;
    }

    @Override
    public ImageView getLeftIcon() {
        return mLeftIcon;
    }

    @Override
    public void setLeftIcon(int resId) {
        mLeftIcon.setImageResource(resId);
    }

    @Override
    public ImageView getRightIcon() {
        return mRightIcon;
    }

    @Override
    public void setRightIcon(int resId) {
        mRightIcon.setImageResource(resId);
    }

    public ImageView getRightSecondIcon() {
        return mRightSecondIcon;
    }

    public void setRightSecondIcon(int resId) {
        mRightSecondIcon.setImageResource(resId);
        mRightSecondIcon.setVisibility(VISIBLE);
    }

    @Override
    public TextView getLeftTitle() {
        return mLeftTitle;
    }

    @Override
    public TextView getMiddleTitle() {
        return mCenterTitle;
    }

    @Override
    public TextView getRightTitle() {
        return mRightTitle;
    }
}
