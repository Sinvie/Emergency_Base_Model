package com.sinvie.emergency_base.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_conf.CommonService.util.SinvieLogUtil;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.widget.adapter.SinvieListViewDialogAdapter;

import java.util.List;

/**
 * This class is about simple list dialog.
 */
public class SimpleListDialog extends Dialog
{
    ListView listView;

    protected SimpleListDialog(Context context)
    {
        super(context, R.style.Sinve_Theme_dialog);

        setContentView(R.layout.view_sinvie_dialog_whitestyle_lv);
        listView = (ListView) findViewById(R.id.dialog_listview);

        setCanceledOnTouchOutside(true);
        setOnDismissListener(null);
    }

    /**
     * 简单的数据显示类
     */
    public SimpleListDialog(Context context, List<Object> data)
    {
        this(context);
        listView.setPadding(0, 0, 0, 0);
        listView.setDividerHeight(1);
        ViewGroup baseLayout = (ViewGroup) findViewById(R.id.base_layout);
        baseLayout.setBackgroundDrawable(SinvieLocContext.getContext().getResources().getDrawable(R.drawable.bg_sinvie_popup_window));
        SinvieListViewDialogAdapter adapter = new SinvieListViewDialogAdapter(context, data);
        listView.setAdapter(adapter);
    }

    /**
     * 设置listview的item点击事件
     */
    public void setOnItemClickListener(AdapterView.OnItemClickListener listener)
    {
        if (listener == null)
        {
            SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "listener is null, please check!");
            return;
        }

        listView.setOnItemClickListener(listener);
    }

}
