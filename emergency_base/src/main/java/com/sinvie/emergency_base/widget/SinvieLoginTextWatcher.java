package com.sinvie.emergency_base.widget;

import android.text.Editable;
import android.text.TextWatcher;

import com.sinvie.emergency_base.common.SinvieUIConstants;

/**
 * @author: JonsonBob
 * @date: 2021/6/30
 * @Des: 自定义 TextWatcher
 */
public abstract class SinvieLoginTextWatcher implements TextWatcher {

    /**
     * login type 0:手机验证码登录, 1:账号密码登录
     */
    private int mLoginType = 0;

    /**
     * 输入的类型 0:手机号,1:验证码,2:账号,3:密码
     */
    private int mInputType = 0;


    public SinvieLoginTextWatcher(int loginType, int inputType) {
        mLoginType = loginType;
        mInputType = inputType;
    }

    public abstract void checkInputCondition(boolean open);

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (s != null) {
            if (mLoginType == 0) {
                //手机验证码登录
                if (mInputType == 0) {
                    //手机号 等于11位 输出true
                    checkInputCondition(s.length() == 11);
                } else {
                    //验证码 等于;6位 输出true
                    checkInputCondition(s.length() == SinvieUIConstants.LOGIN_CODE_LENGTH);
                }
            } else {
                //账号密码登录
                checkInputCondition(s.length() > 0);
            }
        } else {
            checkInputCondition(false);
        }
    }
}
