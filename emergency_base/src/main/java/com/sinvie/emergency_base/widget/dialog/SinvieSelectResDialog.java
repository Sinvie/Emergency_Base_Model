package com.sinvie.emergency_base.widget.dialog;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.utils.widget.ImageFilterView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.widget.SinvieWorkbenchItemDecoration;

import org.jetbrains.annotations.NotNull;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SinvieSelectResDialog#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SinvieSelectResDialog extends DialogFragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private Context mContext;
    private ConstraintLayout mLlSelectResDialogTop;
    private ImageFilterView mIvSelectPicDialogClose;
    private TextView mTvSureSelectResDialog;
    private RecyclerView mRvSelectResDialogClose;

    private OnSelectPicTypeItemClickListener mTypeItemClickListener;

    public static SinvieSelectResDialog newInstance(String param1, String param2) {
        SinvieSelectResDialog fragment = new SinvieSelectResDialog();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public SinvieSelectResDialog() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull @NotNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    public void setItemClickListener(OnSelectPicTypeItemClickListener itemClickListener) {
        mTypeItemClickListener = itemClickListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        // 下面这些设置必须在此方法(onStart())中才有效

        Window window = getDialog().getWindow();
        // 如果不设置这句代码, 那么弹框就会与四边都有一定的距离
        window.setBackgroundDrawableResource(android.R.color.transparent);
        // 设置动画
        window.setWindowAnimations(R.style.sinvie_bottomSheet_animation);

        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.BOTTOM;
        // 如果不设置宽度,那么即使你在布局中设置宽度为 match_parent 也不会起作用
        params.width = getResources().getDisplayMetrics().widthPixels;
        window.setAttributes(params);
    }

    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // 去掉默认的标题
//        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sinvie_select_res_dialog, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        mLlSelectResDialogTop = view.findViewById(R.id.ll_select_res_dialog_top);
        mIvSelectPicDialogClose = view.findViewById(R.id.iv_select_pic_dialog_close);
        mTvSureSelectResDialog = view.findViewById(R.id.tv_sure_select_res_dialog);
        mRvSelectResDialogClose = view.findViewById(R.id.rv_select_res_dialog_close);

        SelectResAdapter adapter = new SelectResAdapter(mContext);
        adapter.setItemClickListener(mItemClickListener);
        mRvSelectResDialogClose.setLayoutManager(new LinearLayoutManager(mContext));
        mRvSelectResDialogClose.addItemDecoration(new SinvieWorkbenchItemDecoration(mContext,
                R.drawable.icon_sinvie_item_decoration_1));
        mRvSelectResDialogClose.setAdapter(adapter);

    }


    private OnSelectPicTypeItemClickListener mItemClickListener =
            new OnSelectPicTypeItemClickListener() {
                @Override
                public void onSelectPicTypeItemClickListener(int position) {
                    mTypeItemClickListener.onSelectPicTypeItemClickListener(position);
                    dismiss();
                }
            };

    public static class SelectResAdapter extends RecyclerView.Adapter<SelectResAdapter.SelectResViewHolder> {

        private final int[] mSelectResTitle = {R.string.sinvie_select_res_type_pic,
                R.string.sinvie_select_res_type_camera, R.string.sinvie_select_res_type_video};
        private Context mContext;
        private OnSelectPicTypeItemClickListener mItemClickListener;

        public SelectResAdapter(Context context) {
            mContext = context;
        }

        public void setItemClickListener(OnSelectPicTypeItemClickListener itemClickListener) {
            mItemClickListener = itemClickListener;
        }

        @NonNull
        @NotNull
        @Override
        public SelectResViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent,
                                                      int viewType) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_sinvie_select_res_dialog,
                    parent, false);
            return new SelectResViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull @NotNull SelectResViewHolder holder, int position) {
            holder.bind(mSelectResTitle[position]);
            holder.itemView.setOnClickListener(v ->
                    mItemClickListener.onSelectPicTypeItemClickListener(position));
        }

        @Override
        public int getItemCount() {
            return mSelectResTitle.length;
        }

        public class SelectResViewHolder extends RecyclerView.ViewHolder {

            private TextView mTvItemSelectResDialog;

            public SelectResViewHolder(@NonNull @NotNull View itemView) {
                super(itemView);
                mTvItemSelectResDialog = itemView.findViewById(R.id.tv_item_select_res_dialog);
            }

            public void bind(int title) {
                mTvItemSelectResDialog.setText(mContext.getString(title));
            }
        }
    }

    public interface OnSelectPicTypeItemClickListener {
        void onSelectPicTypeItemClickListener(int position);
    }


}