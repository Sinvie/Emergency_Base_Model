package com.sinvie.emergency_base.widget.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.utils.SinvieClickUtils;

/**
 * @param
 * @author JonsonBob
 * @description 删除 事报 续报 的确定弹窗
 * @return
 * @time 2021/7/15 14:03
 */
public class SinvieTipDialog extends DialogFragment implements View.OnClickListener {

    private static final String TITLE = "title";
    private static final String CONTENT = "content";
    private static final String LEFT = "left";
    private static final String RIGHT = "right";

    private TextView mTvTipsDialogTitle;
    private TextView mTvTipsDialogContent;
    private TextView mTvTipsDialogLeft;
    private TextView mTvTipsDialogRight;

    private TipDialogLeftClickListener mLeftClickListener;
    private TipDialogRightClickListener mRightClickListener;

    private String mTitle;
    private String mContent;
    private String mLeft;
    private String mRight;

    public SinvieTipDialog() {
    }

    public static SinvieTipDialog newInstance(String title, String content, String left, String right) {
        SinvieTipDialog fragment = new SinvieTipDialog();
        Bundle args = new Bundle();
        args.putString(TITLE,title);
        args.putString(CONTENT,content);
        args.putString(LEFT,left);
        args.putString(RIGHT,right);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, getTheme());
        if (getArguments() != null) {
            mTitle = getArguments().getString(TITLE);
            mContent = getArguments().getString(CONTENT);
            mLeft = getArguments().getString(LEFT);
            mRight = getArguments().getString(RIGHT);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().getWindow().setLayout(-1, -2);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //底部弹出动画
//        WindowManager.LayoutParams params = getDialog().getWindow()
//                .getAttributes();
//        params.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
//        params.windowAnimations = R.style.bottomSheet_animation;
//        getDialog().getWindow().setAttributes(params);
//        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        getDialog().setCanceledOnTouchOutside(true);
//        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sinvie_report_type_dialog, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        mTvTipsDialogTitle = view.findViewById(R.id.tv_tips_dialog_title);
        mTvTipsDialogContent = view.findViewById(R.id.tv_tips_dialog_content);
        mTvTipsDialogLeft = view.findViewById(R.id.tv_tips_dialog_left);
        mTvTipsDialogRight = view.findViewById(R.id.tv_tips_dialog_right);

        mTvTipsDialogLeft.setOnClickListener(this);
        mTvTipsDialogRight.setOnClickListener(this);

        setText(mTvTipsDialogTitle, mTitle);
        setText(mTvTipsDialogContent, mContent);
        setText(mTvTipsDialogLeft, mLeft);
        setText(mTvTipsDialogRight, mRight);
    }

    @Override
    public void onClick(View v) {
        //如果快速点击 就退出
        if (SinvieClickUtils.isFastClick(v.getId())) {
            return;
        }
        if (v.getId() == R.id.tv_tips_dialog_left) {
            if (mLeftClickListener != null) {
                mLeftClickListener.onTipDialogLeftClickListener(this);
            } else {
                dismiss();
            }
        } else if (v.getId() == R.id.tv_tips_dialog_right) {
            if (mRightClickListener != null) {
                mRightClickListener.onTipDialogRightClickListener(this);
            } else {
                dismiss();
            }
        }
    }

    /**
     * 左边监听
     *
     * @param listener 左边监听
     * @return
     */
    public void setOnLeftClickListener(TipDialogLeftClickListener listener) {
        mLeftClickListener = listener;
    }

    /**
     * 右边监听
     *
     * @param listener 右边监听(默认关闭dialog)
     * @return
     */
    public void setOnRightClickListener(TipDialogRightClickListener listener) {
        mRightClickListener = listener;
    }


    private void setText(TextView textView, String str) {
        if (textView != null) {
            textView.setText(str);
        }
    }

    private void setTextColor(TextView textView, int color) {
        if (textView != null) {
            textView.setTextColor(ContextCompat.getColor(SinvieLocContext.getContext(), color));
        }
    }

    public interface TipDialogLeftClickListener {
        void onTipDialogLeftClickListener(SinvieTipDialog dialog);
    }

    public interface TipDialogRightClickListener {
        void onTipDialogRightClickListener(SinvieTipDialog dialog);
    }
}