package com.sinvie.emergency_base.widget.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;


import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_widget.pinentryedittext.SinviePinEntryEditText;

import java.util.Objects;

/**
 * @param
 * @author JonsonBob
 * @description 直播房间锁 dialog
 * @return
 * @time 2021/7/3 12:07
 */
public class SinviePwdDialog extends DialogFragment implements View.OnClickListener {

    private ImageView mIvPwdDialogClose;
    private TextView mTvDialogTitle;
    private TextView mTvDialogTips;
    private SinviePinEntryEditText mPinEntryEditText;
    private TextView mTvSubmitLiveRoomPwd;
    private onLiveRoomPwdListener mOnLiveRoomPwdListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sinvie_live_room_pwd, container, false);
        //取消 dialog title
        Objects.requireNonNull(Objects.requireNonNull(getDialog()).getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        initView(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        //Android中 DialogFragment宽度占满高度自适应，在Fragment的onResume()
        // 方法中设置window的宽高即可，第一个参数是宽度-1代表默认占满；第二个参数高度-2默认自适应高度（这两个参数也可以直接设置固定宽高）。
        Objects.requireNonNull(Objects.requireNonNull(getDialog()).getWindow()).setLayout(-1, -2);
    }

    public void setOnLiveRoomPwdListener(onLiveRoomPwdListener onLiveRoomPwdListener) {
        mOnLiveRoomPwdListener = onLiveRoomPwdListener;
    }

    private void initView(View view) {
        mIvPwdDialogClose = view.findViewById(R.id.iv_pwd_dialog_close);
        mTvDialogTitle = view.findViewById(R.id.tv_dialog_title);
        mTvDialogTips = view.findViewById(R.id.tv_dialog_tips);
        mPinEntryEditText = view.findViewById(R.id.pinEntryEditText);
        mTvSubmitLiveRoomPwd = view.findViewById(R.id.tv_submit_live_room_pwd);

        mIvPwdDialogClose.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_pwd_dialog_close) {
            dismiss();
        } else if (v.getId() == R.id.tv_submit_live_room_pwd) {
            if (mPinEntryEditText.getText() != null && mPinEntryEditText.getText().toString().length() == SinvieUIConstants.LIVE_ROOM_PWD_LENGTH) {
                mOnLiveRoomPwdListener.onLiveRoomPwdListener(mPinEntryEditText.getText().toString());
                dismiss();
            }else {
                //toast
                Toast.makeText(getActivity(),R.string.sinvie_pwd_dialog_pwd_empty_error,Toast.LENGTH_SHORT).show();
            }
        }
    }

    public interface onLiveRoomPwdListener {
        void onLiveRoomPwdListener(String pwd);
    }
}