package com.sinvie.emergency_base.http.api

import com.sinvie.emergency_base.bean.*
import com.sinvie.emergency_base.bean.electronic.*
import com.sinvie.emergency_base.http.SinvieNetContact
import com.sinvie.emergency_base.http.base.SinvieBaseEvenRep
import com.sinvie.emergency_base.http.base.SinvieBaseRep
import io.reactivex.rxjava3.core.Observable
import okhttp3.RequestBody
import retrofit2.http.*

/**
 * @author: JonsonBob
 * @date: 2021/7/5
 * @Des:  所有 api请求接口
 */
interface SinvieBaseApiService {

    /**
     * 验证码登录接口
     */
    @POST("/oauth/h5login")
    fun doLogin(@Body requestBody: RequestBody): Observable<SinvieCodeLoginBean>

    /**
     * 密码登录接口
     */
    @POST("/oauth/login")
    fun doPwdLogin(@Body requestBody: RequestBody):Observable<SinvieLoginBean>
    /**
     * 获取手机验证码
     */
    @GET("/ser/mail/mailcontactor/getMobileVerificationCode")
    fun getMobileCode(
        @Query(SinvieNetContact.REQUEST_PARAM_MOBILE) mobile: String,
        @Query(SinvieNetContact.REQUEST_PARAM_LOGIN_TYPE) type: String
    ): Observable<SinvieBaseRep<String>>

    /**
     * 获取 userSig
     */
    @GET("/event/im/userSig")
    fun getUserSig(@Query("userId") token: String): Observable<SinvieBaseRep<String>>

    /**
     * 获取个人信息
     */
    @GET("/ser/mail/mailcontactor/contactorList")
    fun getContactorData(
        @Query("pageNum") pageNum: Int,
        @Query("pageSize") pageSize: Int,
        @Query("contactIds") contactIds: String
    ): Observable<SinvieBaseEvenRep<SinvieContactorBean>>

    /**
     * 初始化直播房间 (保存直播间信息)
     */

    @POST("/event/im/appImLive")
    fun saveRoomInfo(@Body requestBody: RequestBody): Observable<SinvieBaseRep<SinvieRoomBean>>

    /**
     * 更新直播房间信息
     */
    @POST("/event/im/appImLive/update")
    fun updateRoom(@Body requestBody: RequestBody): Observable<SinvieBaseRep<SinvieRoomBean>>

    /**
     * TODO获取直播间列表 (\待定)
     */
    @GET("/event/im/appImLives")
    fun getRoomList(): Observable<SinvieBaseRep<List<SinvieDataBean>>>

    @POST("/meeting/csMeetingRecord/list")
    fun getConferenceDataHistory(): Observable<SinvieBaseRep<List<SinvieConfDataHistoryBean>>>

    /**
     * 获取上报列表
     */
    @POST("/app/event/info/getEventInfoReportList")
    fun getReportList(@Body requestBody: RequestBody): Observable<SinvieBaseRep<SinvieReportBean>>

    /**
     * 获取事件上报详情
     */
    @POST("/app/event/info/getDetail")
    fun getReportDetailInfo(@Body requestBody: RequestBody): Observable<SinvieBaseRep<SinvieReportDetailBean>>

    /**
     * 获取事件类型列表 多级列表需要手动解析
     */
    @POST("/app/eos/caseClass/getTree")
    fun getEventType(): Observable<SinvieBaseRep<List<SinvieReportTypeBean>>>

    /**
     * 保存上报的事件 有id为更新 无id为新建
     */
    @POST("/app/event/info/saveEventInfoReport")
    fun saveReportInfo(@Body requestBody: RequestBody): Observable<SinvieBaseRep<String>>

    /**
     * 删除上报的事件
     */
    @POST("/app/event/info/deleteEventInfoReport")
    fun deleteReportInfo(@Body requestBody: RequestBody): Observable<SinvieBaseRep<String>>

    /**
     * 保存续报
     */
    @POST("/app/event/info/saveEventInfoSupplement")
    fun saveReportFollowUp(@Body requestBody: RequestBody): Observable<SinvieBaseRep<String>>

    /**
     * 删除续报
     */
    @POST("/app/event/info/deleteEventInfoSupplement")
    fun deleteReportFollowUp(@Body requestBody: RequestBody): Observable<SinvieBaseRep<String>>

    /**
     * 上传文件 视频
     */
    @POST("/fileupload/appAttachment/fileUploadAttachment")
    fun saveFile(@Body requestBody: RequestBody): Observable<SinvieBaseRep<SinvieSaveFileBean>>

    @POST("/solr/solr/searchMultiTypeResource")
    fun nearbySearch(@Body requestBody: RequestBody): Observable<SinvieSearchResourceResultBean<List<ResourceTypeBean>>>

    @POST
    fun getDeviceInfo(@Url url:String): Observable<SinvieBaseRep<DeviceInfoBean>>

    @POST
    fun getRiskInfo(@Url url: String): Observable<RiskRep<RiskInfo>>

    @POST
    fun getEquipLibInfo(@Url url: String): Observable<SinvieBaseRep<String>>

    @POST
    fun getTeamInfo(@Url url: String): Observable<SinvieBaseRep<String>>

    @POST("/ser/resoure/resoureProtectTarget/info")
    fun getProtectTargetInfo(@Body requestBody: RequestBody): Observable<ProtectRep<ProtectBean>>

    @POST
    fun getRefuge(@Url url: String): Observable<SinvieBaseRep<String>>

    @POST
    fun getResourceLib(@Url url: String): Observable<SinvieBaseRep<String>>

    @POST
    fun getMaterial(@Url url: String): Observable<SinvieBaseRep<String>>

    @POST("/event/reportLoaction/report")
    fun reportLocation(@Body requestBody: RequestBody): Observable<SinvieBaseRep<String>>
}