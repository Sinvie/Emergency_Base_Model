package com.sinvie.emergency_base.http.base

/**
 * @author: JonsonBob
 * @date: 2021/7/16
 * @Des: 类描述
 */
data class SinvieBaseEvenRep<T>(val list: T, val code: Int, val msg: String)