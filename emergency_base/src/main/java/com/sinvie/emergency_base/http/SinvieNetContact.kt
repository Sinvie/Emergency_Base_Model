package com.sinvie.emergency_base.http

object SinvieNetContact {
    private const val PORT = ":8055"
//    private const val PORT = ":8091" //扬州

    //    const val IP_ADDRESS = "192.168.3.203"  //竞翊服务器
//    const val IP_ADDRESS = "192.168.3.221" //华哥本地地址
//    const val IP_ADDRESS = "192.168.3.217" //凡哥本地地址
//    const val IP_ADDRESS = "192.168.3.43" //鑫哥服务器地址
//    const val IP_ADDRESS = "192.168.1.189"  //服务器地址，修改要同步修改H5配置文件SIOC-H5.config.js
//    const val IP_ADDRESS ="172.21.12.73" //扬州
//    const val IP_ADDRESS = "172.16.140.207"//HUAWEI OpenLab
//    const val IP_ADDRESS ="124.71.14.193"  //公有云
    const val IP_ADDRESS = "61.142.239.43" //南海

    private const val URL = "http://"

    const val BASE_URL = "$URL$IP_ADDRESS$PORT"
    //协作标会地址
    const val COOP_URL = "http://10.170.207.207:9966/#/"

    /**
     * ===================================登录请求 关键字============================================
     */
    const val REQUEST_PARAM_USER_NAME = "username"
    const val REQUEST_PARAM_PASSWORD = "password"
    const val REQUEST_PARAM_CODE_NUM = "codeNum"
    const val REQUEST_PARAM_TYPE = "type"

    /**
     * ===================================获取验证码 关键字==========================================
     */
    const val REQUEST_PARAM_MOBILE = "mobile"
    const val REQUEST_PARAM_LOGIN_TYPE = "loginType"

    /**
     * ===================================直播请求 关键字============================================
     */
    /**
     * 创建直播间
     */
    const val REQUEST_PARAM_USER_ID = "userId"
    const val REQUEST_PARAM_ROOM_ID = "roomId"
    const val REQUEST_PARAM_LIVE_TYPE = "liveType"
    const val REQUEST_PARAM_LIVE_ADDRESS = "liveAddress"
    const val REQUEST_PARAM_LIVE_START_TIME = "startTime"
    const val REQUEST_PARAM_LIVE_END_TIME = "endTime"
    const val REQUEST_PARAM_LIVE_STATUS = "liveStatus"

    /**
     * 更新直播间
     */
    //主键id
    const val REQUEST_PARAM_ID = "id"

    //事件id
    const val REQUEST_PARAM_EVENT_ID = "eventId"

    const val REQUEST_VALUE_CREATE_ROOM = "createRoom"
    const val REQUEST_VALUE_UPDATE_ROOM = "updateRoom"
    const val REQUEST_VALUE_DESTROY_ROOM = "destroyRoom"
    const val REQUEST_VALUE_GET_ROOM_LIST = "getRoomList"

    const val TYPE_LIVE_ROOM = "liveRoom"
    const val TYPE_VOICE_ROOM = "voiceRoom"
    const val TYPE_GROUP_LIVE = "groupLive"

    const val ROOM_TITLE = "room_title"
    const val GROUP_ID = "group_id"
    const val USE_CDN_PLAY = "use_cdn_play"
    const val PUSHER_NAME = "pusher_name"
    const val COVER_PIC = "cover_pic"
    const val PUSHER_AVATAR = "pusher_avatar"
    const val ANCHOR_ID = "anchor_id"

    const val ERROR_CODE_UNKNOWN = -1

    /**
     * =================================err code===========================================
     */
    const val FAIL_EMPTY = 10000;
    const val FAIL_CODE = 1110;

    const val ERROR_CODE = 1111;
    const val ERROR_CODE_401 = 401;
    const val ERROR_CODE_20005 = 20005
    const val ERROR_NET = 321;
    const val ERROR_FILE_SUFFIX = 322;
}