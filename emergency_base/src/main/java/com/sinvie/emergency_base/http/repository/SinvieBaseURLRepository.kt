package com.sinvie.emergency_base.http.repository

import com.google.gson.Gson
import com.sinvie.emergency_base.bean.*
import com.sinvie.emergency_base.bean.electronic.*
import com.sinvie.emergency_base.common.SinvieUIConstants
import com.sinvie.emergency_base.http.SinvieNetContact
import com.sinvie.emergency_base.http.SinvieRetrofitFactory
import com.sinvie.emergency_base.http.api.SinvieBaseApiService
import com.sinvie.emergency_base.http.base.SinvieBaseEvenRep
import com.sinvie.emergency_base.http.base.SinvieBaseRep
import com.sinvie.emergency_base.ui.login.SinvieUserInfo
import com.sinvie.emergency_base.utils.SinvieFileUtil
import io.reactivex.rxjava3.core.Observable
import okhttp3.FormBody
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File

/**
 * @author: JonsonBob
 * @date: 2021/7/5
 * @Des: 网络请求类
 */
class SinvieBaseURLRepository {

    /**
     * 手机验证码登录
     */
    fun doLogin(
        username: String, password: String, type: String
    ): Observable<SinvieCodeLoginBean> {
        val requestBody = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart(SinvieNetContact.REQUEST_PARAM_USER_NAME, username)
            .addFormDataPart(SinvieNetContact.REQUEST_PARAM_PASSWORD, password)
            .addFormDataPart(SinvieNetContact.REQUEST_PARAM_TYPE, type)
            .build()
        return SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java)
            .doLogin(requestBody)
    }

    /**
     * 密码登录
     */

    fun doLogin(account: String, pwd: String): Observable<SinvieLoginBean> {
        val formBody: RequestBody = FormBody.Builder()
            .add(SinvieNetContact.REQUEST_PARAM_USER_NAME, account)
            .add(SinvieNetContact.REQUEST_PARAM_PASSWORD, pwd)
            .add(SinvieNetContact.REQUEST_PARAM_TYPE, "APP")
            .build()
        return SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java).doPwdLogin(formBody)
    }

    /**
     * 获取手机验证码
     */
    fun getMobileCode(mobile: String): Observable<SinvieBaseRep<String>> {
        val type = "2"
        return SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java)
            .getMobileCode(mobile, type)
    }

    /**
     * 获取userSig
     */
    fun getUserSig(): Observable<SinvieBaseRep<String>> =
        SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java)
            .getUserSig(SinvieUserInfo.getInstance().userId)

    /**
     * 获取 通讯录 的 信息
     */
    fun getContactData(
        pageIndex: Int,
        contactorId: String
    ): Observable<SinvieBaseEvenRep<SinvieContactorBean>> {

        return SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java)
            .getContactorData(pageIndex, SinvieUIConstants.PAGE_SIZE, contactorId)
    }

    /**
     * 初始化直播间(保存直播间信息)
     */
    fun saveRoom(requestBody: RequestBody): Observable<SinvieBaseRep<SinvieRoomBean>> =
        SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java).saveRoomInfo(requestBody)

    /**
     * 更新直播间信息
     */
    fun updateRoom(requestBody: RequestBody): Observable<SinvieBaseRep<SinvieRoomBean>> =
        SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java).updateRoom(requestBody)

    /**
     * 获取直播间列表
     */
    fun getRoomList(): Observable<SinvieBaseRep<List<SinvieDataBean>>> =
        SinvieRetrofitFactory.instance.create(
            SinvieBaseApiService::class.java
        ).getRoomList();

    /**
     *上传文件 图片 视频
     */
    fun saveFile(
        filePath: String,
        mediaType: String,
        type: String
    ): Observable<SinvieBaseRep<SinvieSaveFileBean>> {
        var requestBody = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart(
                "file",
                SinvieFileUtil.getResourceName(filePath),
                File(filePath).asRequestBody(mediaType.toMediaTypeOrNull())
            )
            .addFormDataPart("type", type)
            .build()

        return SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java).saveFile(requestBody)
    }

    /**
     * 获取上报事件列表
     */
    fun getReportList(pageIndex: Int, keyword: String?): Observable<SinvieBaseRep<SinvieReportBean>> {

        val requestBody = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("page", "$pageIndex")
            .addFormDataPart("size", "${SinvieUIConstants.PAGE_SIZE}")
        if (keyword != null) {
            requestBody.addFormDataPart("keyword", keyword)
        }
        return SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java)
            .getReportList(requestBody.build())
    }

    /**
     * 获取事件续报
     */
    fun getReportDetailInfo(id: String): Observable<SinvieBaseRep<SinvieReportDetailBean>> {
        val requestBody = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("id", id).build()
        return SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java)
            .getReportDetailInfo(requestBody)
    }

    /**
     * 获取事件类型
     */
    fun getEventType(): Observable<SinvieBaseRep<List<SinvieReportTypeBean>>> =
        SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java).getEventType()

    /**
     * 删除上报事件
     */
    fun deleteReportEvent(id: String): Observable<SinvieBaseRep<String>> =
        SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java).deleteReportInfo(
            MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart("id", id).build()
        )

    /**
     * 保存上报事件 使用json
     */
    fun saveReportEvent(sinvieReportEventInfo: SinvieReportEventInfo): Observable<SinvieBaseRep<String>> {
        val requestBody = Gson().toJson(sinvieReportEventInfo)
            .toRequestBody("application/json;charset=utf-8".toMediaType())
        return SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java)
            .saveReportInfo(requestBody);

    }

    /**
     * 删除续报事件
     */
    fun deleteReportFollowUp(id: String): Observable<SinvieBaseRep<String>> =
        SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java).deleteReportFollowUp(
            MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart("id", id).build()
        )

    /**
     * 保存续报事件 使用json
     */
    fun saveReportFollowUp(reportEventInfo: HashMap<String, String>): Observable<SinvieBaseRep<String>> {
        val requestBody = Gson().toJson(reportEventInfo)
            .toRequestBody("application/json;charset=utf-8".toMediaType())
        return SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java)
            .saveReportFollowUp(requestBody);

    }
}

fun nearbySearch(searchParam: NearbySearchParam): Observable<SinvieSearchResourceResultBean<List<ResourceTypeBean>>> {
    val requestBody = Gson().toJson(searchParam)
        .toRequestBody("application/json;charset=utf-8".toMediaType())

    return SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java).nearbySearch(requestBody)
}

fun getDeviceInfo(id: String): Observable<SinvieBaseRep<DeviceInfoBean>> {
    val url = "${SinvieNetContact.BASE_URL}/device/device/appdevice/info/$id"
    return SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java).getDeviceInfo(url)
}

fun getRiskInfo(id: String): Observable<RiskRep<RiskInfo>> {
    val url = "${SinvieNetContact.BASE_URL}/ser/risk/riskdanger/info/$id"
    return SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java).getRiskInfo(url)
}

fun getEquipLibInfo(id: String): Observable<SinvieBaseRep<String>> {
    val url = "${SinvieNetContact.BASE_URL}/ser/resoure/resoureequipmentgroup/info/$id"
    return SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java).getEquipLibInfo(url)
}

fun getTeamInfo(id: String): Observable<SinvieBaseRep<String>> {
    val url = "${SinvieNetContact.BASE_URL}/ser/resoure/resoureteam/info/$id"
    return SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java).getTeamInfo(url)
}

fun getProtectTargetInfo(id: String): Observable<ProtectRep<ProtectBean>> {
    val requestBody = MultipartBody.Builder()
        .setType(MultipartBody.FORM)
        .addFormDataPart("id", id)
    return SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java).getProtectTargetInfo(requestBody.build())
}

fun getRefuge(id: String): Observable<SinvieBaseRep<String>> {
    val url = "${SinvieNetContact.BASE_URL}/ser/resoure/resourearea/info/$id"
    return SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java).getRefuge(url)
}

fun getResourceLib(id: String): Observable<SinvieBaseRep<String>> {
    val url = "${SinvieNetContact.BASE_URL}/ser/resoure/resourearticlestorehouse/info/$id"
    return SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java).getResourceLib(url)
}

fun getMaterial(id: String): Observable<SinvieBaseRep<String>> {
    val url = "${SinvieNetContact.BASE_URL}/ser/resoure/resourearticle/info/$id"
    return SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java).getMaterial(url)
}

fun reportLocation(sinvieAddrInfo: SinvieAddrReportBean): Observable<SinvieBaseRep<String>> {
    val requestBody =
        Gson().toJson(sinvieAddrInfo).toRequestBody("application/json;charset=utf-8".toMediaType())
    return SinvieRetrofitFactory.instance.create(SinvieBaseApiService::class.java).reportLocation(requestBody)
}