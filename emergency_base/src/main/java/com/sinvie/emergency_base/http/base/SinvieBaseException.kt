package   com.sinvie.emergency_base.http.base

data class SinvieBaseException(val code:Int, val msg:String): Throwable() {
}