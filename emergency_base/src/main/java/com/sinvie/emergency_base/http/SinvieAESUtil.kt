package com.sinvie.emergency_base.http

import android.annotation.SuppressLint
import java.util.*
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

object SinvieAESUtil {
    //key为16位
    private const val KEY = "_aes_secret_key_"
    private const val IV = "_aes_secret_iv__"

    /**
     * 加密方法
     *
     * @param data 要加密的数据
     * @param key  加密key, 如果为空使用默认的key
     * @param iv   加密iv，如果为空使用默认的iv加密
     * @return 加密的结果
     * @throws Exception
     */
    @SuppressLint("NewApi")
    @JvmOverloads
    @Throws(Exception::class)
    fun encrypt(data: String, key: String = KEY, iv: String = IV): String? {
        return try {
            val cipher = Cipher.getInstance("AES/CBC/NoPadding") //"算法/模式/补码方式"NoPadding PkcsPadding
            val blockSize: Int = cipher.blockSize
            val dataBytes = data.toByteArray()
            var plaintextLength = dataBytes.size
            if (plaintextLength % blockSize != 0) {
                plaintextLength += (blockSize - plaintextLength % blockSize)
            }
            val plaintext = ByteArray(plaintextLength)
            System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.size)
            val keyspec = SecretKeySpec(key.toByteArray(), "AES")
            val ivspec = IvParameterSpec(iv.toByteArray())
            cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec)
            val encrypted: ByteArray = cipher.doFinal(plaintext)
            Base64.getEncoder().encodeToString(encrypted)
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    /**
     * 解密方法
     *
     * @param data 要解密的数据
     * @param key  解密key, 如果为空使用默认的key
     * @param iv   解密iv，如果为空使用默认的iv加密
     * @return 解密的结果
     * @throws Exception
     */
    @SuppressLint("NewApi")
    @JvmOverloads
    @Throws(Exception::class)
    fun desEncrypt(data: String?, key: String = KEY, iv: String = IV): String? {
        return try {
            val encrypted1: ByteArray = Base64.getDecoder().decode(data)
            val cipher: Cipher = Cipher.getInstance("AES/CBC/NoPadding")
            val keyspec = SecretKeySpec(key.toByteArray(), "AES")
            val ivspec = IvParameterSpec(iv.toByteArray())
            cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec)
            val original: ByteArray = cipher.doFinal(encrypted1)
            String(original)
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    /**
     * 测试
     */
    @Throws(Exception::class)
    @JvmStatic
    fun main(args: Array<String>) {
        val test = "123456c"
        //        String test = new String(test1.getBytes(), "UTF-8");
        var data: String? = null
        val key = KEY
        val iv = IV
        // /g2wzfqvMOeazgtsUVbq1kmJawROa6mcRAzwG1/GeJ4=
        data = encrypt(test, key, iv)
        println("数据：$test")
        println("加密：$data")
//        val jiemi = desEncrypt("IGHWM9nAHRLV26VYpeIiuA==", key, iv)!!.trim { it <= ' ' }
        val jiemi = desEncrypt(data, key, iv)!!.trim { it <= ' ' }
        println("解密：$jiemi")
    }
}