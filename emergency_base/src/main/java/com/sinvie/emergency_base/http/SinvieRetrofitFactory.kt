package com.sinvie.emergency_base.http

import android.os.Environment
import com.sinvie.emergency_base.http.interceptor.SinvieHeaderInterceptor
import com.sinvie.emergency_base.http.interceptor.SinvieDemoLogInterceptor
import com.sinvie.emergency_base.http.interceptor.SinvieNetCacheInterceptor
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit


/**
 * create by Admin in 2021/6/8
 *
 * @Description : retrofit封装
 **/
class SinvieRetrofitFactory {


    private lateinit var retrofit: Retrofit

    companion object {
        //单例
        val instance by lazy(mode = LazyThreadSafetyMode.SYNCHRONIZED) {
            SinvieRetrofitFactory()
        }
    }

    /**
     * 组装retrofit对象
     */
    init {
        retrofit = Retrofit.Builder()
            .baseUrl(SinvieNetContact.BASE_URL)
            .client(getClient())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .build()
    }

    /**
     * 组装client
     */
    private fun getClient(): OkHttpClient {
        val file = File(Environment.getExternalStorageDirectory(), "cache")
        val cacheSize = 10 * 1024 * 1024L

        return OkHttpClient.Builder()
            .cache(Cache(file, cacheSize))
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .connectTimeout(10, TimeUnit.SECONDS)
            //addNetworkInterceptor() 添加网络拦截器
            // 能够操作中间过程的响应,如重定向和重试.
            // 当网络短路而返回缓存响应时不被调用.
            // 只观察在网络上传输的数据.
            // 携带请求来访问连接.
            .addNetworkInterceptor(SinvieNetCacheInterceptor())
            //addInterceptor() 添加应用拦截器
            // 不需要担心中间过程的响应,如重定向和重试.
            // 总是只调用一次,即使HTTP响应是从缓存中获取.
            // 观察应用程序的初衷. 不关心OkHttp注入的头信息如: If-None-Match.
            // 允许短路而不调用 Chain.proceed(),即中止调用.
            // 允许重试,使 Chain.proceed()调用多次.
            .addInterceptor(SinvieHeaderInterceptor())
//            .addInterceptor(SinvieDemoLogInterceptor())
            .addInterceptor(getLogInterceptor())
            .build()

    }

    /**
     * log 拦截器
     */
    private fun getLogInterceptor(): Interceptor {
        val logInterceptor = HttpLoggingInterceptor()
        logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        return logInterceptor
    }

    /**
     * service
     */
    fun <T> create(service: Class<T>): T {
        return retrofit.create(service)
    }

}