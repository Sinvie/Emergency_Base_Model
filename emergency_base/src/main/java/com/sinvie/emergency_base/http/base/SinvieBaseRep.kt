package com.sinvie.emergency_base.http.base

/**
 * create by Admin in 2021/6/8
 *
 * @Description : 接口返回格式
 **/
data class SinvieBaseRep<T>(val data: T, val code: Int, val msg: String)