package com.sinvie.emergency_base.http.interceptor

import com.sinvie.emergency_base.ui.login.SinvieUserInfo
import okhttp3.Interceptor
import okhttp3.Response

/**
 * @author: JonsonBob
 * @date: 2021/7/6
 * @Des: 类描述
 */
class SinvieHeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        SinvieUserInfo.getInstance().token?.let {
            val request = chain.request().newBuilder()
                .addHeader("token", SinvieUserInfo.getInstance().token)
                .build()
            return chain.proceed(request)
        }
        return chain.proceed(chain.request().newBuilder().addHeader("token","").build())
    }
}