@file:JvmName("SinvieCommonNetUtils")

package com.sinvie.emergency_base.http.common

import android.util.Log
import com.sinvie.emergency_conf.CommonService.util.SinvieLogUtil
import com.sinvie.emergency_base.bean.SinvieCodeLoginBean
import com.sinvie.emergency_base.bean.SinvieLoginBean
import com.sinvie.emergency_base.bean.electronic.ProtectRep
import com.sinvie.emergency_base.bean.electronic.RiskRep
import com.sinvie.emergency_base.http.base.SinvieBaseEvenRep
import com.sinvie.emergency_base.http.base.SinvieBaseException
import com.sinvie.emergency_base.http.base.SinvieBaseObserver
import com.sinvie.emergency_base.http.base.SinvieBaseRep
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

/*
*  Observable扩展
*/
fun <T> Observable<T>.execute(observerSinvie: SinvieBaseObserver<T>) {
    this.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(observerSinvie)
}

/*
*  返回数据转换
*/
fun <T> Observable<SinvieBaseRep<T>>.covert(): Observable<T> {
    return this.flatMap { t ->
        SinvieLogUtil.d("CommonNetUtils --> ", "${t.code} :  ${t.msg}")
        if (t.code != 0) {
            Observable.error(SinvieBaseException(t.code, t.msg))
        } else {
            Observable.just(t.data)
        }
    }
}

//验证码登录返回
fun Observable<SinvieCodeLoginBean>.covertCodeLogin():Observable<SinvieCodeLoginBean>{
    return this.flatMap { t ->
        SinvieLogUtil.d("CommonNetUtils --> ", "${t.code} :  ${t.msg}")
        if (t.code != 0) {
            Observable.error(SinvieBaseException(t.code, t.msg))
        } else {
            Observable.just(t)
        }
    }
}

//密码登录返回
fun Observable<SinvieLoginBean>.covertPwdLogin():Observable<SinvieLoginBean>{
    return this.flatMap { t ->
        SinvieLogUtil.d("CommonNetUtils --> ", "${t.code} :  ${t.msg}")
        if (t.code != 0) {
            Observable.error(SinvieBaseException(t.code, t.msg))
        } else {
            Observable.just(t)
        }
    }
}

//event 事件返回统一封装
fun <T> Observable<SinvieBaseEvenRep<T>>.covertEvent(): Observable<T> {

    return this.flatMap { t ->
        SinvieLogUtil.d("CommonNetUtils --> ", "${t.code} :  ${t.msg}")
        if (t.code != 0) {
            Observable.error(SinvieBaseException(t.code, t.msg))
        } else {
            Observable.just(t.list)
        }
    }
}

fun <T> Observable<RiskRep<T>>.convertRisk(): Observable<T> {
    return this.flatMap { t ->
        Log.d("Risk", "${t.code} :  ${t.msg}")
        if (t.msg != "success") {
            Observable.error(t.msg?.let { SinvieBaseException(t.code, it) })
        } else {
            Observable.just(t.riskDanger)
        }
    }
}

fun <T> Observable<ProtectRep<T>>.convertProtect(): Observable<T> {
    return this.flatMap { t ->
        Log.d("Protect", "${t.code} :  ${t.msg}")
        if (t.msg != "success") {
            Observable.error(t.msg?.let { SinvieBaseException(t.code, it) })
        } else {
            Observable.just(t.ResoureProtectTargetEntity)
        }
    }
}