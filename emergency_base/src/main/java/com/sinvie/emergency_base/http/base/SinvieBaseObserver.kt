package com.sinvie.emergency_base.http.base

import com.sinvie.emergency_base.http.SinvieNetContact
import com.sinvie.emergency_base.http.common.SinvieNetExceptionUtils
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable

/**
 * create by Admin in 2021/6/8
 *
 * @Description : 返回数据统一处理
 **/
open abstract class SinvieBaseObserver<T> : Observer<T> {
    override fun onSubscribe(d: Disposable?) {

    }

    override fun onNext(t: T?) {
        onSuccess(t!!)
    }

    override fun onError(e: Throwable?) {
        if (e is SinvieBaseException) {
//            println(e.msg)
            onFailure(e.code, e.msg)
        } else {
            val errorMsg = SinvieNetExceptionUtils.exceptionHandler(e!!)
            //println(errorMsg)
            onFailure(SinvieNetContact.ERROR_CODE, errorMsg)
        }
        //println("loading结束")
    }

    override fun onComplete() {
        // println("loading结束")
    }

    abstract fun onFailure(errCode: Int, errMsg: String);


    abstract fun onSuccess(t: T?)
}