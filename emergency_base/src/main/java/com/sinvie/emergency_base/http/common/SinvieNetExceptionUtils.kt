package com.sinvie.emergency_base.http.common

import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext
import com.sinvie.emergency_conf.CommonService.util.SinvieLogUtil
import com.sinvie.emergency_base.R
import com.sinvie.emergency_base.http.SinvieNetContact
import org.json.JSONException
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.text.ParseException

/**
 * create by Admin in 2021/6/9
 *
 * @Description : 异常信息封装
 **/
object SinvieNetExceptionUtils {

    fun exceptionHandler(e: Throwable): String {
        SinvieLogUtil.d("NetExceptionUtils ", "-> ${e.message}")
        var errorMsg = "未知错误"
        when (e) {
            is UnknownHostException -> {
                errorMsg = "网络不可用"
            }
            is SocketTimeoutException -> {
                errorMsg = "请求网络超时"
            }
            is HttpException -> {
                val httpException = e as HttpException
                errorMsg = covertStatusCode(httpException)
            }
            is ParseException,
            is JSONException -> {
                errorMsg = "数据解析错误"
            }
        }
        return errorMsg
    }

    private fun covertStatusCode(httpException: HttpException): String {
        var msg = ""
        when (httpException.code()) {
            in 500 until 600 -> {
                msg = "服务器处理请求错误"
            }
            in 402 until 500 -> {
                msg = "服务器无法处理请求"
            }
            SinvieNetContact.ERROR_CODE_401 -> {
                msg = SinvieLocContext.getContext().getString(R.string.sinvie_errCode_401)
            }
            SinvieNetContact.ERROR_CODE_20005->{
                msg = SinvieLocContext.getContext().getString(R.string.sinvie_errCode_20005)
            }
            in 300 until 400 -> {
                msg = "请求被重定向到其他界面"
            }
            else -> {
                msg = httpException.message()
            }
        }
        SinvieLogUtil.d(SinvieNetExceptionUtils::class.java.simpleName, "Code = ${httpException.code()} ")
        return msg
    }
}