package com.sinvie.emergency_base.http.nearby

import android.util.Log
import com.sinvie.emergency_base.bean.electronic.SinvieSearchResourceResultBean
import com.sinvie.emergency_base.http.base.SinvieBaseException
import io.reactivex.rxjava3.core.Observable

fun <T> Observable<SinvieSearchResourceResultBean<T>>.convert(): Observable<T> {
    return this.flatMap { t ->
        Log.d("SearchResourceResult", "${t.code} :  ${t.message}")
        if (!t.success) {
            Observable.error(t.message?.let { SinvieBaseException(t.code, it) })
        } else {
            Observable.just(t.data)
        }
    }
}
