package com.sinvie.emergency_base.common;


/**
 * This class is about UI module constants, unified management Interface constants
 * UI层常量类，统一管理界面常量
 */
public final class SinvieUIConstants {

    public static final String DEMO_TAG = "EmergencyLive-";

    public static final String CONF_ID = "conf_id";
    public static final String CONF_TO_CALL = "conf_to_call";
    public static final String BUNDLE_KEY = "bundle_key";
    public static final String CALL_INFO = "call_info";
    public static final String CHAT_TYPE = "CHAT_TYPE";
    public static final String PERSONAL_CONTACT = "personal_contact";
    public static final String BUNDLE = "bundle";
    public static final String VIDEO_SYSTEM_PATH = "video_system_path";
    public static final String CONST_GROUP = "const_group";
    public static final String MEDIA_RESOURCE = "media_resource";
    public static final String GROUP_MEMBER = "group_member";
    public static final String GROUP_OPERATE_MODE = "group_operate";
    public static final String GROUP_OPERATE_ADD = "group_add_member";
    public static final String GROUP_OPERATE_DELETE = "group_delete";

    public static final String CONTACT_POSITION = "contact_position";

    public static final String IS_VIDEO_CONF = "is_video_conf";
    public static final String IS_DATE_CONF = "is_date_conf";
    public static final String IS_START_SHARE_CONF = "is_start_share_conf";
    public static final String IS_AUX_DATA_SHARE = "is_aux_data_share";
    public static final String LDAP_CONTACTS_INFO = "ldap_contacts_info";
    public static final String FILE_TYPE = "file_type";
    public static final String SELECT_PATH = "select_path";

    public static final String CERT_ISSUER = "Issuer:";

    public static final String SELECT_NUMBER = "select_number";
    public static final String SELECT_DISPLAY_NAME = "select_display_name:";
    public static final String SELECT_URI = "select_uri";
    public static final String SELECT_ORGANIZATION_NAME = "select_organization_name";
    public static final String SELECT_ATTENDEE_TYPE = "select_attendee_type";
    public static final String SELECT_TP_SPEED = "select_tp_speed";
    public static final String SELECT_EMAIL = "select_email";
    public static final String SELECT_MOBILE = "select_mobile";

    public static final String SETTINGS_IS_AUTO_ADJUST = "is_auto_adjust";

    public static final String ATTENDEE_TYPE_CONF_ROOM = "1";
    public static final String ATTENDEE_TYPE_ATTENDEES = "5";

    public static final int MEDIA_TYPE_V3_SVC = 0;
    public static final int MEDIA_TYPE_V3_AVC = 1;
    public static final int MEDIA_TYPE_V3_VOICE = 2;

    public static final String INPUT_TYPE_IP = "0123456789.";
    public static final int PORT_LENGTH = 4;

    /**
     * Obtaining user avatars and info
     */
    public static final int ENTERPRISE_HEAD_SELF = 21;
    public static final int ENTERPRISE_HEAD_SYS = 22;
    public static final int ENTERPRISE_HEAD_DEF = 23;
    public static final int ENTERPRISE_HEAD_NULL = 24;
    public static final int ENTERPRISE_SEARCH_SUCCESS = 0;
    public static final int ENTERPRISE_SEARCH_NULL = 1;
    public static final int ENTERPRISE_SEARCH_FAILED = 2;
    public static final int ENTERPRISE_SELF_TERMINAL = 25;
    public static final int ENTERPRISE_SEARCH_DEPT = 105;

    public static final int SET_SYSTEM_HEAD_PHOTO_CODE = 101;
    public static final int LOAD_ALL_HEAD_ICON = 100;
    public static final int PRO_LOAD_HEADICON = 101;
    public static final int LOAD_SELF_HEADIMAGE = 102;

    public static final int SMC_TYPE_2_0 = 2;
    public static final int SMC_TYPE_3_0 = 4;

    public static final String TIME_ZONE_ID = "58";
    public static final String TIME_OFFSET = "28800000";
    public static final String TIME_ZONE_NAME = "Asia/Shanghai";

    public static final int DISPLAY_NAME_LENGTH = 64;
    public static final int PASSWORD_EXPIRE_NORMAL = 255;

    /**
     * Avatar Image source
     */
    public static final int SYSTEM_PICTURE = 0;
    public static final int ALBUM_PICTURE = 1;

    /**
     * Online status
     */
    public static final int STATUS_ON_LINE = 0;
    public static final int STATUS_BUSY = 1;
    public static final int STATUS_XA = 2;
    public static final int STATUS_DND = 3;

    /**
     * Im constants
     */
    public static final String IM_CHECK_CONTACT_GROUP_NAME = "check_contact_group_name";
    public static final String IM_RETURN_CONTACT_GROUP_NAME = "return_contact_group_name";
    public static final String IM_RETURN_CONTACT_GROUP_ID = "return_contact_group_id";
    public static final String IM_RETURN_CONTACT_GROUP_ALL = "return_contact_group_all";
    public static final String IM_ALL_CONTACT_NUM = "im_all_contact_num";
    public static final String IM_CONTACT_INFO = "im_contact_info";
    public static final String IM_CHAT_GROUP_INFO = "im_chat_group_info";

    public static final int IM_TEAM_CHECKED = 7;
    public static final int IM_REQUEST_CODE_CONTACT_GROUP = 8;
    public static final int IM_RESULT_CODE_CONTACT_GROUP = 9;
    public static final int IM_CHANGE_INFO_FAILED = 10;
    public static final int IM_CHANGE_INFO = 11;
    public static final int IM_REQUEST_CODE_CHAT_GROUP_CREATE = 12;
    public static final int IM_RESULT_CODE_CHAT_GROUP_CREATE = 13;
    public static final int IM_REQUEST_CODE_CHAT_GROUP_DELETE = 14;
    public static final int IM_RESULT_CODE_CHAT_GROUP_DELETE = 15;

    /**
     * im room intent
     */
    public static final String ROOM_TITLE = "room_title";
    public static final String GROUP_ID = "group_id";
    public static final String USE_CDN_PLAY = "use_cdn_play";
    public static final String PUSHER_NAME = "pusher_name";
    public static final String COVER_PIC = "cover_pic";
    public static final String PUSHER_AVATAR = "pusher_avatar";
    public static final String ANCHOR_ID = "anchor_id";

    /**
     * permission request
     */
    public static final int PERMISSION_PACKAGE_REQUEST = 996;
    /**
     * login type
     */
    public static final String LOGIN_TYPE = "login_type";
    public static final int LOGIN_CODE_LENGTH = 4;

    /**
     * main tab type
     */
    public static final int TAB_CONVERSATION_TYPE = 0;
    public static final int TAB_TASK_TYPE = 1;
    //    public static final int TAB_LIVE_TYPE = 2;
    public static final int TAB_WORKBENCH = 2;
    public static final int TAB_CONTACT_TYPE = 3;
    public static final int TAB_MYSELF_TYPE = 4;

    /**
     * 直播类型
     */
    public static final int LIVE_ROOM_TASK = 1;
    public static final int LIVE_ROOM_SITE = 0;

    /**
     * 直播状态
     */
    public static final int LIVE_STATUS_START = 1;
    public static final int LIVE_STATUS_STOP = 0;

    /**
     * live room pwd length
     */
    public static final int LIVE_ROOM_PWD_LENGTH = 4;

    /**
     * live room menu
     */
    public static final int LIVE_ROOM_MENU_CLOSE = 0;
    public static final int LIVE_ROOM_MENU_SWITCH_CAMERA = 1;
    public static final int LIVE_ROOM_MENU_SCENES_HORIZONTAL = 2;
    public static final int LIVE_ROOM_MENU_LOCK = 3;
    public static final int LIVE_ROOM_MENU_NOT_LOCK = 4;

    //腾讯 实时音视频错误码 https://cloud.tencent.com/document/product/647/38307
    public static final int LIVE_ROOM_DESTROY_ERROR = -100013;

    /**
     * ************************************华为会议******************************************
     */
    public static final String CONFERENCE_LOGIN_FAIL_MSG = "Login_Fail_Msg";
    public static final String CONFERENCE_LOGIN_FAIL_CODE = "Login_Fail_Code";

    /**
     * ==================================== app ============================================
     */

    //事件上报

    public static final int ADD_NEW_REPORT_REQUEST = 200;
    public static final int ADD_NEW_REPORT_RESULT = 201;

    public static final int REPORT_DETAIL_REQUEST = 300;
    public static final int REPORT_DETAIL_DEL_RESULT = 301;
    public static final int REPORT_DETAIL_EDIT_RESULT = 302;

    public static final int ADD_REPORT_DETAIL_REQUEST = 400;
    public static final int ADD_REPORT_DETAIL_RESULT = 401;

    public static final String REPORT_INFO_BEAN = "report_info";
    public static final String REPORT_DETAIL_INFO_BEAN = "report_detail_info";
    public static final String REPORT_INFO_TITLE = "report_title";

    public static final String REPORT_INTENT_TYPE = "intent_type";
    public static final int REPORT_TYPE_ADD = 0;
    public static final int REPORT_TYPE_EDIT = 1;

    public static final int SELECT_ADDRESS_REQUEST = 500;
    public static final int SELECT_ADDRESS_RESULT = 501;
    //选择 地址的经纬度
    public static final String ADDRESS_TYPE_LATITUDE = "latitude";
    public static final String ADDRESS_TYPE_LONGITUDE = "longitude";
    public static final String ADDRESS_TYPE_ADDRESS = "address";

    /**
     * page size
     */
    public static final int PAGE_SIZE = 10;

    /**
     * 视频播放
     */
    public static final String PLAY_VIDEO_URL = "play_video_url";
}
