package com.sinvie.emergency_base;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.hjq.permissions.XXPermissions;
import com.scwang.smart.refresh.footer.ClassicsFooter;
import com.scwang.smart.refresh.header.ClassicsHeader;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.sinvie.emergency_base.helper.SinvieConfigHelper;
import com.sinvie.emergency_base.helper.SinvieHelloChatController;
import com.sinvie.emergency_base.signature.SinvieGenerateUserSig;
import com.sinvie.emergency_base.ui.main.SinvieSplashActivity;
import com.sinvie.emergency_base.ui.scene.SinvieSceneManager;
import com.sinvie.emergency_base.utils.SinvieAppFrontBackHelper;
import com.sinvie.emergency_base.utils.SinvieBrandUtil;
import com.sinvie.emergency_base.utils.SinvieCrashUtil;
import com.sinvie.emergency_base.utils.SinvieDemoLog;
import com.sinvie.emergency_base.utils.SinvieMessageNotification;
import com.sinvie.emergency_base.utils.SinviePrivateConstants;
import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieConfBroadcastConstants;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcast;
import com.sinvie.emergency_live.tuikit.SinvieTUIKit;
import com.sinvie.emergency_live.tuikit.base.SinvieIMEventListener;
import com.sinvie.emergency_live.tuikit.base.SinvieTUIListenerManager;
import com.sinvie.emergency_live.tuikit.modules.conversation.SinvieConversationManagerKit;
import com.sinvie.emergency_live.tuikit.utils.SinvieBackgroundTasks;
import com.sinvie.emergency_live.tuikit.utils.SinvieTUIKitUtils;
import com.sinvie.emergency_live.tuikit_live.live.SinvieTUIKitLive;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.crashreport.CrashReport;
import com.tencent.imsdk.v2.V2TIMCallback;
import com.tencent.imsdk.v2.V2TIMManager;
import com.tencent.imsdk.v2.V2TIMMessage;
import com.xiasuhuei321.loadingdialog.manager.StyleManager;
import com.xiasuhuei321.loadingdialog.view.LoadingDialog;

import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/8/18
 * @Des: 类描述
 */
public class SinvieOptions {

    private static final String TAG = SinvieOptions.class.getSimpleName();
    public static boolean isSceneEnable = false;
    private static Application mApplication;

    /**
     * ----------------------------------------------------静态内部类 单列模式 end -----------------------------------------------------------------
     */


    //static 代码段可以防止内存泄露
    static {
        //设置全局的Header构建器
        SmartRefreshLayout.setDefaultRefreshHeaderCreator((context, layout) -> {
            layout.setPrimaryColorsId(R.color.sinvie_theme_bg, R.color.sinvie_theme_text_black_color);
            //全局设置主题颜色
            return new ClassicsHeader(context);//.setTimeFormat(new DynamicTimeFormat("更新于
            // %s"));//指定为经典Header，默认是 贝塞尔雷达Header
        });
        //设置全局的Footer构建器
        SmartRefreshLayout.setDefaultRefreshFooterCreator((context, layout) -> {
            //指定为经典Footer，默认是 BallPulseFooter
            return new ClassicsFooter(context).setDrawableSize(20);
        });
    }

    private final String licenseUrl = "";
    private final String licenseKey = "";
    /**
     * IM 离线推送
     */
    private final SinvieIMEventListener mIMEventListener = new SinvieIMEventListener() {
        @Override
        public void onNewMessage(V2TIMMessage v2TIMMessage) {
            super.onNewMessage(v2TIMMessage);
            String imSdkVersion = V2TIMManager.getInstance().getVersion();
            // IMSDK 5.0.1及以后版本 doBackground 之后同时会离线推送
            if (SinvieTUIKitUtils.compareVersion(imSdkVersion, "5.0.1") < 0) {
                SinvieMessageNotification notification = SinvieMessageNotification.getInstance();
                notification.notify(v2TIMMessage);
            }
        }
    };
    /**
     * IM 设置华为离线推送
     */
    private final SinvieConversationManagerKit.MessageUnreadWatcher mUnreadWatcher = count -> {
        // 华为离线推送角标
//        SinvieHUAWEIHmsMessageService.updateBadge(SinvieLocContext.getContext(), count);
    };

    /**
     * 封装Activity生命周期返回
     */
    private final SinvieAppFrontBackHelper.OnAppStatusListener mOnAppStatusListener = new SinvieAppFrontBackHelper.OnAppStatusListener() {
        @Override
        public void onCreate(Activity activity) {
            // 重启整个程序
            Intent intent = new Intent(activity, SinvieSplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mApplication.startActivity(intent);
        }

        @Override
        public void onFront(boolean isChangingConfiguration) {
            if (!isChangingConfiguration) { // 应用切到前台
                SinvieDemoLog.i(TAG, "application enter foreground");
                V2TIMManager.getOfflinePushManager().doForeground(new V2TIMCallback() {
                    @Override
                    public void onError(int code, String desc) {
                        SinvieDemoLog.e(TAG, "doForeground err = " + code + ", desc = " + desc);
                    }

                    @Override
                    public void onSuccess() {
                        SinvieDemoLog.i(TAG, "doForeground success");
                    }
                });
                SinvieTUIKit.removeIMEventListener(mIMEventListener);
                SinvieConversationManagerKit.getInstance().removeUnreadWatcher(mUnreadWatcher);
                SinvieMessageNotification.getInstance().cancelTimeout();

                //华为广播
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.CUSTOM_BROADCAST_CONSTANTS_FRONT, 1);
            }
        }

        @Override
        public void onBack() {
            // 应用切到后台
            SinvieDemoLog.i(TAG, "application enter background");
            int unReadCount = SinvieConversationManagerKit.getInstance().getUnreadTotal();
            V2TIMManager.getOfflinePushManager().doBackground(unReadCount,
                    new V2TIMCallback() {
                        @Override
                        public void onError(int code, String desc) {
                            SinvieDemoLog.e(TAG,
                                    "doBackground err = " + code + ", desc = " + desc);
                        }

                        @Override
                        public void onSuccess() {
                            SinvieDemoLog.i(TAG, "doBackground success");
                        }
                    });
            // 应用退到后台，消息转化为系统通知
            SinvieTUIKit.addIMEventListener(mIMEventListener);
            SinvieConversationManagerKit.getInstance().addUnreadWatcher(mUnreadWatcher);
            //华为广播
            SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.CUSTOM_BROADCAST_CONSTANTS_BACK, 0);
        }
    };

    /**
     * ----------------------------------------------------静态内部类 单列模式 start -----------------------------------------------------------------
     */

    private SinvieOptions() {

    }

    public static SinvieOptions getInstance() {
        return SingletonHolder.sIntance;
    }

    /**
     * TODO IM 注册自定义监听器 如果有新的自定义消息 都需要增加新的自定义消息监听
     */
    private static void registerCustomListeners() {
        SinvieTUIListenerManager.getInstance().addChatListener(new SinvieHelloChatController());
        SinvieTUIListenerManager.getInstance().addConversationListener(new SinvieHelloChatController.HelloConversationController());
    }

    /**
     * 判断是否为前台流程
     *
     * @param context
     * @param frontPkg
     * @return
     */
    private static boolean isFrontProcess(Context context, String frontPkg) {
        ActivityManager manager =
                (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> infos = manager.getRunningAppProcesses();
        if (infos == null || infos.isEmpty()) {
            return false;
        }
        final int pid = android.os.Process.myPid();
        for (ActivityManager.RunningAppProcessInfo info : infos) {
            if (info.pid == pid) {
                SinvieDemoLog.i(TAG, "processName-->" + info.processName);
                return frontPkg.equals(info.processName);
            }
        }
        return false;
    }

    /**
     * 初始化
     *
     * @param application app application
     */
    public void init(Application application, String front_pkg) {
        mApplication = application;

        //初始化上下文工具类
        SinvieLocContext.init(mApplication);

        //初始化崩溃工具类
        if (!isFrontProcess(mApplication, front_pkg)) {

            //处理程序崩溃 缓存在本地 EmergencyLive 文件夹下
            SinvieCrashUtil.getInstance().init(mApplication);
            Log.i("EmergencyLive", "onCreate: PUSH Process.");
            return;
        }
        Bugly.init(mApplication, "d6e4b716eb", true);

        XXPermissions.setScopedStorage(true);

        //初始化腾讯控件
        initTencentIM();

        CrashReport.initCrashReport(mApplication, "42ae14e566", true);

        //初始化 loadingDialog
        initLoadingDialog();
    }

    /**
     * *************************************** loading dialog*************************************
     */

    private void initLoadingDialog() {
        StyleManager s = new StyleManager();

        //在这里调用方法设置s的属性
        //code here...
        s.Anim(false).repeatTime(0).contentSize(-1).intercept(true);

        LoadingDialog.initStyle(s);

    }

    /******************************************* **腾讯初始化*** *******************************/
    public void initTencentIM() {
        //初始化 tuikit
        SinvieTUIKit.init(mApplication, SinvieGenerateUserSig.SDKAPPID, new SinvieConfigHelper().getConfigs());
        SinvieTUIKitLive.init(mApplication);
        //注册自定义消息
        registerCustomListeners();
        //注册平台推送
        registerPushManager();
        //注册Activity生命周期返回
        new SinvieAppFrontBackHelper().register(mApplication, mOnAppStatusListener);
        //初始化 直播板块
        initSceneManager();

    }

    /**
     * 初始化各个平台推送
     */
    private void registerPushManager() {
//        //初始化推送管理
//        HeytapPushManager.init(mApplication, true);
//        //注册各个平台的离线推送
//        if (SinvieBrandUtil.isBrandXiaoMi()) {
//            // 小米离线推送
//            MiPushClient.registerPush(mApplication, SinviePrivateConstants.XM_PUSH_APPID,
//                    SinviePrivateConstants.XM_PUSH_APPKEY);
//        } else if (SinvieBrandUtil.isBrandHuawei()) {
//            // 华为离线推送，设置是否接收Push通知栏消息调用示例
//            HmsMessaging.getInstance(mApplication).turnOnPush().addOnCompleteListener(task -> {
//                if (task.isSuccessful()) {
//                    SinvieDemoLog.i(TAG, "huawei turnOnPush Complete");
//                } else {
//                    SinvieDemoLog.e(TAG,
//                            "huawei turnOnPush failed: ret=" + task.getException().getMessage());
//                }
//            });
//        } else if (MzSystemUtils.isBrandMeizu(mApplication)) {
//            // 魅族离线推送
//            PushManager.register(mApplication, SinviePrivateConstants.MZ_PUSH_APPID,
//                    SinviePrivateConstants.MZ_PUSH_APPKEY);
//        } else if (SinvieBrandUtil.isBrandVivo()) {
//            // vivo离线推送
//            PushClient.getInstance(mApplication.getApplicationContext()).initialize();
//        } else if (HeytapPushManager.isSupportPush()) {
//            // oppo离线推送，因为需要登录成功后向我们后台设置token，所以注册放在MainActivity中做
//
//        }
    }

    /**
     * IM 初始化 实时音视频 之所以这么写 方便把音视频 从聊天 剥离
     */
    private void initSceneManager() {
        SinvieSceneManager.init(mApplication, licenseUrl, licenseKey);
        isSceneEnable = true;
    }

    /**
     * 静态内部类
     */
    private static class SingletonHolder {

        private static final SinvieOptions sIntance = new SinvieOptions();

    }

    public static Context getAppContext() {
        return mApplication;
    }
}
