package com.sinvie.emergency_base.utils;

import java.io.Closeable;
import java.io.IOException;


public class SinvieCloseables {
    private static final String TAG = SinvieCloseables.class.getSimpleName();
    public static void closeCloseable(Closeable closeable) {
        if (closeable == null) {
            return;
        }

        try {
            closeable.close();
        } catch (IOException e) {
            SinvieDemoLog.e(TAG, e.getMessage());
        }
    }
}
