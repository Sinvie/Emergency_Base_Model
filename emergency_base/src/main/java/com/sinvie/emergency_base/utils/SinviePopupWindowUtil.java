package com.sinvie.emergency_base.utils;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

public class SinviePopupWindowUtil {
    private static final SinviePopupWindowUtil mInstance = new SinviePopupWindowUtil();

    private SinviePopupWindowUtil() {
    }

    public static SinviePopupWindowUtil getInstance() {
        return mInstance;
    }

    /**
     * 生成PopupWindow对象
     *
     * @return PopupWindow
     */
    public synchronized PopupWindow generatePopupWindow(View view) {
        final PopupWindow popupWindow = new PopupWindow(view,
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popupWindow.setFocusable(true);
        view.setFocusableInTouchMode(true);
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_MENU && popupWindow.isShowing()) {
                    dismissPopupWindow(popupWindow);
                    return true;
                }
                return false;
            }
        });

        return popupWindow;
    }

    public void dismissPopupWindow(PopupWindow popupWindow) {
        if (null != popupWindow) {
            popupWindow.dismiss();
        }
    }

}
