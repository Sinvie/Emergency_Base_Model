package com.sinvie.emergency_base.utils.amap

import android.view.View
import com.amap.api.location.AMapLocation
import com.amap.api.maps.model.BitmapDescriptorFactory
import com.amap.api.maps.model.LatLng
import com.amap.api.maps.model.Marker
import com.amap.api.maps.model.MarkerOptions
import com.amap.api.navi.model.NaviLatLng
import com.amap.api.services.core.LatLonPoint
import java.text.DecimalFormat
import java.util.*

fun View.setMark(position: LatLng, move: Boolean): MarkerOptions {
    return MarkerOptions().icon(BitmapDescriptorFactory.fromView(this))
        .position(position)
        .draggable(move)
}

/**
 * this 图片资源ID
 */
fun Int.setMark(position: LatLng, move: Boolean): MarkerOptions {
    return MarkerOptions().icon(BitmapDescriptorFactory.fromResource(this))
        .position(position)
        .draggable(move)
}

fun Marker.setIconRes(resId: Int) {
    this.setIcon(BitmapDescriptorFactory.fromResource(resId))
}

fun Int.getFriendlyTime(): String {
    if (this > 3600) {
        val hour = this / 3600
        val minute = this % 3600 / 60
        return hour.toString() + "小时" + minute + "分钟"
    }
    if (this >= 60) {
        val minute = this / 60
        return minute.toString() + "分钟"
    }
    return this.toString() + "秒"
}

fun Int.getFriendlyLength(): String {
    if (this > 10000) {// 10 km
        val dis = this / 1000
        return dis.toString() + "千米"
    }
    if (this > 1000) {
        val dis = this.toFloat() / 1000
        val fnum = DecimalFormat("##0.0")
        val dstr = fnum.format(dis.toDouble())
        return dstr.toString() + "千米"
    }
    if (this > 100) {
        val dis = this / 50 * 50
        return dis.toString() + "米"
    }
    var dis = this / 10 * 10
    if (dis == 0) {
        dis = 10
    }
    return dis.toString() + "米"
}

fun LatLonPoint.convertToLatLng(): LatLng = LatLng(this.latitude, this.longitude)

fun List<LatLonPoint>.convertArrList(): List<LatLng> {
    val lineShapes = ArrayList<LatLng>()
    for (point in this) {
        val latLngTemp: LatLng = point.convertToLatLng()
        lineShapes.add(latLngTemp)
    }
    return lineShapes
}

fun AMapLocation.convertToLng(): LatLng = LatLng(this.latitude, this.longitude)

fun LatLonPoint.toNaviLatLng(): NaviLatLng = NaviLatLng(this.latitude, this.longitude)