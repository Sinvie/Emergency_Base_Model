package com.sinvie.emergency_base.utils.amap

import android.content.Context
import com.amap.api.maps.AMap
import com.amap.api.maps.model.BitmapDescriptor
import com.amap.api.maps.model.LatLng
import com.amap.api.maps.model.MarkerOptions
import com.amap.api.maps.model.PolylineOptions
import com.amap.api.services.core.LatLonPoint
import com.amap.api.services.route.RidePath
import com.amap.api.services.route.RideStep

/**
 * 骑行路线图层类。在高德地图API里，如果要显示步行路线规划，可以用此类来创建骑行路线图层。如不满足需求，也可以自己创建自定义的骑行路线图层。
 *
 * 通过此构造函数创建骑行路线图层。
 * @param context 当前activity。
 * @param amap 地图对象。
 * @param path 骑行路线规划的一个方案。详见搜索服务模块的路径查询包（com.amap.api.services.route）中的类 **[WalkStep](../../../../../../Search/com/amap/api/services/route/WalkStep.html)**。
 * @param start 起点。详见搜索服务模块的核心基础包（com.amap.api.services.core）中的类**[LatLonPoint](../../../../../../Search/com/amap/api/services/core/LatLonPoint.html)**。
 * @param end 终点。详见搜索服务模块的核心基础包（com.amap.api.services.core）中的类**[LatLonPoint](../../../../../../Search/com/amap/api/services/core/LatLonPoint.html)**。
 *
*/
class SinvieRideRouteOverlay(
    context: Context?, amap: AMap?, path: RidePath,
    start: LatLonPoint, end: LatLonPoint
) : SinvieRouteOverlay(context!!) {
    private var mPolylineOptions: PolylineOptions? = null
    private var walkStationDescriptor: BitmapDescriptor? = null
    private val ridePath: RidePath

    /**
     * 添加骑行路线到地图中。
     * @since V3.5.0
     */
    fun addToMap() {
        initPolylineOptions()
        try {
            val ridePaths = ridePath.steps
            for (i in ridePaths.indices) {
                val rideStep = ridePaths[i]
                val latLng = rideStep.polyline[0].convertToLatLng()
                addRideStationMarkers(rideStep, latLng)
                addRidePolyLines(rideStep)
            }
            addStartAndEndMarker()
            addPolyLine(mPolylineOptions)
        } catch (e: Throwable) {
            e.printStackTrace()
        }
    }

    /**
     * @param rideStep
     */
    private fun addRidePolyLines(rideStep: RideStep) {
        mPolylineOptions!!.addAll(rideStep.polyline.convertArrList())
    }

    /**
     * @param rideStep
     * @param position
     */
    private fun addRideStationMarkers(rideStep: RideStep, position: LatLng) {
        addStationMarker(
            MarkerOptions()
                .position(position)
                .title(
                    """
                方向:${rideStep.action}
                道路:${rideStep.road}
                """.trimIndent()
                )
                .snippet(rideStep.instruction).visible(nodeIconVisible)
                .anchor(0.5f, 0.5f).icon(walkStationDescriptor)
        )
    }

    /**
     * 初始化线段属性
     */
    private fun initPolylineOptions() {
//        if (walkStationDescriptor == null) {
//            walkStationDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.amap_ride)
//        }
        mPolylineOptions = null
        mPolylineOptions = PolylineOptions()
        mPolylineOptions!!.color(driveColor).width(routeWidth)
    }

    init {
        mAMap = amap
        ridePath = path
        startPoint = start.convertToLatLng()
        endPoint = end.convertToLatLng()
    }
}
