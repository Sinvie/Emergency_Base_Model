package com.sinvie.emergency_base.utils;

import com.google.gson.Gson;
import com.sinvie.emergency_base.bean.SinvieAppAttachmentsBean;
import com.sinvie.emergency_base.bean.SinvieGridImgBean;
import com.sinvie.emergency_base.http.SinvieNetContact;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/13
 * @Des: 类描述
 */
public class SinvieChangeBeanUtil {
    private static final String TAG = SinvieChangeBeanUtil.class.getSimpleName();

    /**
     * ReportBean 切换 GridImgBean
     *
     * @param beans 原来的数据 list bean
     * @return 返回的 数据 list bean
     */
    public static List<SinvieGridImgBean> getGridImgBeanList(List<SinvieAppAttachmentsBean> beans) {
        List<SinvieGridImgBean> beanList = new ArrayList<>();
        if (beans == null) {
            return beanList;
        }
        for ( SinvieAppAttachmentsBean bean : beans) {
            SinvieDemoLog.i(TAG,new Gson().toJson(bean));
            SinvieGridImgBean imgBean = new SinvieGridImgBean();
            //组装url
            imgBean.setUrl(SinvieNetContact.BASE_URL + bean.getUrl());
            //判断格式
            int suffixFormat = SinvieFileUtil.getSuffixFormat(bean.getUrl());
            if (suffixFormat == SinvieFileUtil.SUFFIX_FORMAT_VIDEO) {
                imgBean.setGridType(0);
            } else if (suffixFormat == SinvieFileUtil.SUFFIX_FORMAT_IMG) {
                imgBean.setGridType(1);
            } else {
                imgBean.setGridType(-1);
            }
            beanList.add(imgBean);
        }
        return beanList;
    }

}
