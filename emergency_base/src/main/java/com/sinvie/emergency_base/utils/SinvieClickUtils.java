package com.sinvie.emergency_base.utils;

import java.util.HashMap;

/**
 * @description 防止多次点击事件工具类
 * @param
 * @return
 * @author JonsonBob
 * @time 2021/6/21 14:22
 */
public class SinvieClickUtils {

    // 两次点击按钮之间的点击间隔不能少于2000毫秒
    private static final int MIN_CLICK_DELAY_TIME = 1000;
    private static HashMap<Integer, Long> sLastClickTimeMap = new HashMap<>();

    /**
     * 判断按钮是否是快速点击
     * @param viewId 按钮id
     * @return
     */
    public static boolean isFastClick(int viewId) {
        boolean flag = false;
        long curClickTime = System.currentTimeMillis();
        long lastClickTime = getLastClickTime(viewId);
        if ((curClickTime - lastClickTime) < MIN_CLICK_DELAY_TIME) {
            flag = true;
        }else {
            sLastClickTimeMap.put(viewId, curClickTime);
        }
        return flag;
    }

    public static void clear() {
        sLastClickTimeMap.clear();
    }

    private static Long getLastClickTime(int viewId) {
        Long lastClickTime = sLastClickTimeMap.get(viewId);
        if (lastClickTime == null) {
            lastClickTime = 0L;
        }
        return lastClickTime;
    }

}
