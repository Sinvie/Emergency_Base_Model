package com.sinvie.emergency_base.utils.network

import android.app.DownloadManager
import android.content.ContentValues
import android.content.Context
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.webkit.URLUtil
import androidx.core.content.FileProvider
import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext
import java.io.File

fun getRandomFilepath(
    context: Context,
    extension: String,
    directory: String = Environment.DIRECTORY_PICTURES
): String {
    return "${context.getExternalFilesDir(directory)?.absolutePath}/${System.currentTimeMillis()}.$extension"
}

fun getRandomUri(
    context: Context,
    extension: String
): Uri {
    return getUriFromPath(context, getRandomFilepath(context, extension))
}

fun getUriFromPath(context: Context, path: String): Uri {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        val values = ContentValues()
        values.put(MediaStore.MediaColumns.DISPLAY_NAME, path)
        values.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES)
        context.contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)!!
    } else {
        //com.sinvie.emergency_live ${BuildConfig.APPLICATION_ID}
        FileProvider.getUriForFile(context, "com.sinvie.emergency_base.provider", File(path))
    }
}

fun downloadBySystem(url: String, contentDisposition: String, mimeType: String) {
    // 指定下载地址
    val request = DownloadManager.Request(Uri.parse(url))
    // 允许媒体扫描，根据下载的文件类型被加入相册、音乐等媒体库
    request.allowScanningByMediaScanner()
    // 设置通知的显示类型，下载进行时和完成后显示通知
    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
    // 设置通知栏的标题，如果不设置，默认使用文件名
//        request.setTitle("This is title");
    // 设置通知栏的描述
//        request.setDescription("This is description");
    // 允许在计费流量下下载
    request.setAllowedOverMetered(false)
    // 允许该记录在下载管理界面可见
    request.setVisibleInDownloadsUi(false)
    // 允许漫游时下载
    request.setAllowedOverRoaming(true)
    // 允许下载的网路类型
    request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI)
    // 设置下载文件保存的路径和文件名
    val fileName = URLUtil.guessFileName(url, contentDisposition, mimeType)
    Log.d("downloadBySystem", "fileName:{$fileName}")
    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName)
    val downloadManager = SinvieLocContext.getContext().getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
    // 添加一个下载任务
    val downloadId: Long = downloadManager.enqueue(request)
    Log.d("downloadBySystem", "downloadId:{$downloadId}")
}