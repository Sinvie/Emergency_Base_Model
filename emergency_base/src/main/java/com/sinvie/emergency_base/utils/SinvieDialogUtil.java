package com.sinvie.emergency_base.utils;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.sinvie.emergency_base.R;

/**
 * This class is about dialog util.
 */
public class SinvieDialogUtil {
    private SinvieDialogUtil() {
    }

    public static AlertDialog generateDialog(Context context, int content,
                                             DialogInterface.OnClickListener positiveListener) {
        return new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.sinvie_msg_tip))
                .setMessage(context.getString(content))
                .setPositiveButton(context.getString(R.string.sinvie_conform), positiveListener)
                .setNegativeButton(context.getString(R.string.sinvie_cancel), null).create();
    }

    public static AlertDialog generateDialog(Context context, int content, int positive,
                                             int negative,
                                             DialogInterface.OnClickListener positiveListener,
                                             DialogInterface.OnClickListener negativeListener) {
        return new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.sinvie_msg_tip))
                .setMessage(context.getString(content))
                .setPositiveButton(context.getString(positive), positiveListener)
                .setNegativeButton(context.getString(negative), negativeListener).create();
    }
}
