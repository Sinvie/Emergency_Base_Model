package com.sinvie.emergency_base.utils;

import android.annotation.SuppressLint;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author: JonsonBob
 * @date: 2021/7/7
 * @Des: 类描述
 */
public class SinvieTimeUtil {

    //返回 data string
    public static final int TIME_DATA = 0;
    //返回 时间戳
    public static final int TIME_LONG = 1;

    public static String getTimeData() {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        return simpleDateFormat.format(date);
    }
    public static String getTimeInterval(long startTime) {
        int time = (int) (System.currentTimeMillis() - startTime) / 1000;
//        SystemClock.elapsedRealtime()
        String hh = new DecimalFormat("00").format(time / 3600);
        String mm = new DecimalFormat("00").format(time % 3600 / 60);
        String ss = new DecimalFormat("00").format(time % 60);
        return (hh + ":" + mm + ":" + ss);
    }

}
