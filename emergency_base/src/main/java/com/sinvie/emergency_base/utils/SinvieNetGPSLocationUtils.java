package com.sinvie.emergency_base.utils;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_conf.CommonService.util.SinvieLogUtil;
import com.sinvie.emergency_base.bean.SinvieAddrReportBean;
import com.sinvie.emergency_base.ui.login.SinvieUserInfo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.content.Context.LOCATION_SERVICE;

/**
 * @author: JonsonBob
 * @date: 2021/8/24
 * @Des: 网络定位 GPS定位
 */
public class SinvieNetGPSLocationUtils {
    private static final String TAG = SinvieNetGPSLocationUtils.class.getSimpleName();
    private static SinvieAddrReportBean mAddressBean = null;

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static SinvieAddrReportBean getLocation() {
        if (mAddressBean == null) {
            LocationManager locationManager = (LocationManager) SinvieLocContext.getContext().getSystemService(LOCATION_SERVICE);
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location == null) {
                SinvieLogUtil.e(TAG, "GPS位置信息为空");
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location == null) {
                    SinvieLogUtil.e(TAG, "网络位置信息也为空");
                } else {
                    SinvieLogUtil.d(TAG, "网络位置信息:$location");
                    mAddressBean = new SinvieAddrReportBean(
                            0,
                            Integer.parseInt(SinvieUserInfo.getInstance().getUserId()),
                            "",
                            0,
                            0,
                            location.getLongitude(),
                            location.getLatitude(),
                            getAddress(location.getLatitude(),location.getLongitude()) ,
                            "",
                            0
                    );
                }
            } else {
                SinvieLogUtil.d(TAG, "GPS位置信息:$location");
                mAddressBean = new SinvieAddrReportBean(
                        0,
                        Integer.parseInt(SinvieUserInfo.getInstance().getUserId()),
                        "",
                        0,
                        0,
                        location.getLongitude(),
                        location.getLatitude(),
                        getAddress(location.getLatitude(),location.getLongitude()) ,
                        "",
                        0
                );
            }
        }
        return mAddressBean;
    }

    private static String getAddress(double latitude, double longitude) {
        Geocoder gc = new Geocoder(SinvieLocContext.getContext(), Locale.getDefault());
        List<Address> resultList = new ArrayList<>();
        try {
            resultList = gc.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (resultList != null && resultList.size() >= 1) {
            return resultList.get(0).getFeatureName();
        } else {
            return "";
        }
    }
}
