package com.sinvie.emergency_base.utils;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

/**
 * @author: JonsonBob
 * @date: 2021/6/23
 * @Des: 应用前后台状态监听帮助类，仅在Application中使用
 */
public class SinvieAppFrontBackHelper {

    private OnAppStatusListener mOnAppStatusListener;
    /**
     * 注册状态监听，仅在Application中使用
     * @param application
     * @param listener
     */
    public void register(Application application, OnAppStatusListener listener){
        mOnAppStatusListener = listener;
        application.registerActivityLifecycleCallbacks(activityLifecycleCallbacks);
    }

    public void unRegister(Application application){
        application.unregisterActivityLifecycleCallbacks(activityLifecycleCallbacks);
    }

    private Application.ActivityLifecycleCallbacks activityLifecycleCallbacks = new Application.ActivityLifecycleCallbacks() {

        //活动的activity 数量
        private int foregroundActivities = 0;
        //正在更改配置
        private boolean isChangingConfiguration;

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            if(mOnAppStatusListener != null && savedInstanceState != null){
                mOnAppStatusListener.onCreate(activity );
            }
        }

        @Override
        public void onActivityStarted(Activity activity) {
            foregroundActivities++;
            // 数值从0变到1说明是从后台切到前台
            if (foregroundActivities == 1){
                // 从后台切到前台
                if(mOnAppStatusListener != null){
                    mOnAppStatusListener.onFront(isChangingConfiguration );
                }
            }
            isChangingConfiguration = false;
        }

        @Override
        public void onActivityResumed(Activity activity) {

        }

        @Override
        public void onActivityPaused(Activity activity) {

        }

        @Override
        public void onActivityStopped(Activity activity) {
            foregroundActivities--;
            // 数值从1到0说明是从前台切到后台
            if (foregroundActivities == 0){
                // 从前台切到后台
                if(mOnAppStatusListener != null){
                    mOnAppStatusListener.onBack();
                }
            }
            isChangingConfiguration = activity.isChangingConfigurations();
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {

        }
    };

    public interface OnAppStatusListener{
        void onCreate(Activity activity);
        void onFront(boolean isChangingConfiguration);
        void onBack();
    }
}
