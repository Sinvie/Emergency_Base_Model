package com.sinvie.emergency_base.utils.amap

import android.content.Context
import android.graphics.Color
import android.util.Log
import com.amap.api.maps.AMap
import com.amap.api.maps.model.*
import com.amap.api.services.core.LatLonPoint
import com.amap.api.services.route.DrivePath
import com.amap.api.services.route.DriveStep
import com.amap.api.services.route.TMC
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.asin
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt

/**
 * 导航路线图层类。
 */
class SinvieDrivingRouteOverlay(
    private val mContext: Context, amap: AMap, path: DrivePath?,
    start: LatLonPoint, end: LatLonPoint, throughPointList: List<LatLonPoint>?
) :
    SinvieRouteOverlay(mContext) {
    private val TAG = SinvieDrivingRouteOverlay::class.java.simpleName

    private val drivePath: DrivePath?
    private val throughPointList: List<LatLonPoint>?
    private val throughPointMarkerList = mutableListOf<Marker>()
    private var throughPointMarkerVisible = true
    private var tmcs: MutableList<TMC>? = ArrayList()
    private var mPolylineOptions: PolylineOptions? = null
    private var mPolylineOptionscolor: PolylineOptions? = null
    var isColorfulLine = true

    /**
     * 设置路线宽度
     *
     * @param mWidth 路线宽度，取值范围：大于0
     */
    override var routeWidth = 25f
    private var mLatLngsOfPath: MutableList<LatLng>? = null

    /**
     * 添加驾车路线添加到地图上显示。
     */
    fun addToMap() {
        initPolylineOptions()
        try {
            if (mAMap != null) {
                if (routeWidth == 0f || drivePath == null) {
                    return
                }
                mLatLngsOfPath = ArrayList()
                tmcs = ArrayList()
                val drivePaths = drivePath.steps
                Log.d(TAG, "steps:${drivePaths.size}")
                for (step: DriveStep in drivePaths) {
                    val latlonPoints = step.polyline
                    Log.d(TAG, "step tmcs:${step.tmCs}")
                    val tmclist = step.tmCs
                    (tmcs as ArrayList<TMC>).addAll(tmclist)
                    addDrivingStationMarkers(step, latlonPoints[0].convertToLatLng())
                    for (latlonpoint: LatLonPoint in latlonPoints) {
                        mPolylineOptions!!.add(latlonpoint.convertToLatLng())
                        (mLatLngsOfPath as ArrayList<LatLng>).add(latlonpoint.convertToLatLng())
                    }
                }
                if (startMarker != null) {
                    startMarker!!.remove()
                    startMarker = null
                }
                if (endMarker != null) {
                    endMarker!!.remove()
                    endMarker = null
                }
                addStartAndEndMarker()
                addThroughPointMarker()
                Log.d(TAG, "isColorfulLine:$isColorfulLine tmcs:$tmcs")
                if (isColorfulLine && (tmcs as ArrayList<TMC>).isNotEmpty()) {
                    colorWayUpdate(tmcs)
                    showcolorPolyline()
                } else {
                    showPolyline()
                }
            }

        } catch (e: Throwable) {
            e.printStackTrace()
        }
    }

    /**
     * 初始化线段属性
     */
    private fun initPolylineOptions() {
        mPolylineOptions = null
        mPolylineOptions = PolylineOptions()
        mPolylineOptions!!
            .color(driveColor)
//            .setCustomTexture(BitmapDescriptorFactory.fromResource(R.drawable.custtexture))
            .width(routeWidth)
    }

    private fun showPolyline() {
        addPolyLine(mPolylineOptions)
    }

    private fun showcolorPolyline() {
        Log.d(TAG, "colors list:$mPolylineOptionscolor")
        addPolyLine(mPolylineOptionscolor)
    }

    /**
     * 根据不同的路段拥堵情况展示不同的颜色
     *
     * @param tmcSection
     */
    private fun colorWayUpdate(tmcSection: List<TMC>?) {
        if (mAMap == null) {
            return
        }
        if (tmcSection == null || tmcSection.isEmpty()) {
            return
        }
        var segmentTrafficStatus: TMC
        mPolylineOptionscolor = null
        mPolylineOptionscolor = PolylineOptions()
        mPolylineOptionscolor!!.width(routeWidth)
        val colorList: MutableList<Int> = ArrayList()
        mPolylineOptionscolor!!.add(tmcSection[0].polyline[0].convertToLatLng())
        colorList.add(driveColor)
        for (i in tmcSection.indices) {
            segmentTrafficStatus = tmcSection[i]
            Log.d(TAG, "traffic status:${segmentTrafficStatus.status}")
            val color = getcolor(segmentTrafficStatus.status)
            val mPolyline = segmentTrafficStatus.polyline
            for (j in 1 until mPolyline.size) {
                mPolylineOptionscolor!!.add(mPolyline[j].convertToLatLng())
                colorList.add(color)
            }
        }
        colorList.add(driveColor)
        mPolylineOptionscolor!!.colorValues(colorList)
    }

    private fun getcolor(status: String): Int {
        when (status) {
            "畅通" -> {
                return Color.GREEN
            }
            "缓行" -> {
                return Color.YELLOW
            }
            "拥堵" -> {
                return Color.RED
            }
            "严重拥堵" -> {
                return Color.parseColor("#ff990033")
            }
            else -> {
                return Color.parseColor("#ff537edc")
            }
        }
    }

    /**
     * @param driveStep
     * @param latLng
     */
    private fun addDrivingStationMarkers(driveStep: DriveStep, latLng: LatLng) {
        addStationMarker(
            MarkerOptions()
                .position(latLng)
                .title(
                    "\u65B9\u5411:${driveStep.action}\n\u9053\u8DEF:" + driveStep.road
                )
                .snippet(driveStep.instruction).visible(nodeIconVisible)
                .anchor(0.5f, 0.5f)
//                .icon(driveBitmapDescriptor)
        )
    }

    override val latLngBounds: LatLngBounds
        get() {
            val b = LatLngBounds.builder()
            b.include(LatLng(startPoint!!.latitude, startPoint!!.longitude))
            b.include(LatLng(endPoint!!.latitude, endPoint!!.longitude))
            if (throughPointList != null && throughPointList.isNotEmpty()) {
                for (i in throughPointList.indices) {
                    b.include(
                        LatLng(
                            throughPointList[i].latitude,
                            throughPointList[i].longitude
                        )
                    )
                }
            }
            return b.build()
        }

    fun setThroughPointIconVisibility(visible: Boolean) {
        try {
            throughPointMarkerVisible = visible
            if ((throughPointMarkerList.size > 0)
            ) {
                for (i in throughPointMarkerList.indices) {
                    throughPointMarkerList[i].isVisible = visible
                }
            }
        } catch (e: Throwable) {
            e.printStackTrace()
        }
    }

    private fun addThroughPointMarker() {
        if (throughPointList != null && throughPointList.isNotEmpty()) {
            var latLonPoint: LatLonPoint?
            for (i in throughPointList.indices) {
                latLonPoint = throughPointList[i]
                throughPointMarkerList.add(
                    mAMap!!.addMarker(
                        MarkerOptions()
                            .position(LatLng(latLonPoint.latitude, latLonPoint.longitude))
                            .visible(throughPointMarkerVisible)
//                                .icon(throughPointBitDes)
                            .title("\u9014\u7ECF\u70B9")
                    )
                )
            }
        }
    }

//    private val throughPointBitDes: BitmapDescriptor
//        get() = BitmapDescriptorFactory.fromResource(R.drawable.amap_through)

    /**
     * 去掉DriveLineOverlay上的线段和标记。
     */
    override fun removeFromMap() {
        try {
            super.removeFromMap()
            if ((throughPointMarkerList.size > 0)
            ) {
                for (i in throughPointMarkerList.indices) {
                    throughPointMarkerList[i].remove()
                }
                throughPointMarkerList.clear()
            }
        } catch (e: Throwable) {
            e.printStackTrace()
        }
    }

    companion object {
        /**
         * 获取两点间距离
         *
         * @param start
         * @param end
         * @return
         */
        fun calculateDistance(start: LatLng, end: LatLng): Int {
            val x1 = start.longitude
            val y1 = start.latitude
            val x2 = end.longitude
            val y2 = end.latitude
            return calculateDistance(x1, y1, x2, y2)
        }

        fun calculateDistance(x1: Double, y1: Double, x2: Double, y2: Double): Int {
            var x1 = x1
            var y1 = y1
            var x2 = x2
            var y2 = y2
            val NF_pi = 0.01745329251994329 // 弧度 PI/180
            x1 *= NF_pi
            y1 *= NF_pi
            x2 *= NF_pi
            y2 *= NF_pi

            val v1 = DoubleArray(3)
            v1[0] = cos(y1) * cos(x1) - cos(y2) * cos(x2)
            v1[1] = cos(y1) * sin(x1) - cos(y2) * sin(x2)
            v1[2] = sin(y1) - sin(y2)
            val dist = sqrt((v1[0] * v1[0]) + (v1[1] * v1[1]) + (v1[2] * v1[2]))
            return (asin(dist / 2) * 12742001.5798544).toInt()
        }

        //获取指定两点之间固定距离点
        fun getPointForDis(sPt: LatLng, ePt: LatLng, dis: Double): LatLng {
            val lSegLength = calculateDistance(sPt, ePt).toDouble()
            val preResult = dis / lSegLength
            return LatLng(
                (ePt.latitude - sPt.latitude) * preResult + sPt.latitude,
                (ePt.longitude - sPt.longitude) * preResult + sPt.longitude
            )
        }
    }

    /**
     * 根据给定的参数，构造一个导航路线图层类对象。
     *
     * @param amap      地图对象。
     * @param path 导航路线规划方案。
     * @param context   当前的activity对象。
     */
    init {
        mAMap = amap
        drivePath = path
        startPoint = start.convertToLatLng()
        endPoint = end.convertToLatLng()
        this.throughPointList = throughPointList
    }
}