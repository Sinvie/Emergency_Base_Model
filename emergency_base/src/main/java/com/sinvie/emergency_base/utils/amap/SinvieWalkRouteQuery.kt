package com.sinvie.emergency_base.utils.amap

import android.content.Context
import com.amap.api.maps.AMap
import com.amap.api.maps.model.BitmapDescriptor
import com.amap.api.maps.model.LatLng
import com.amap.api.maps.model.MarkerOptions
import com.amap.api.maps.model.PolylineOptions
import com.amap.api.services.core.LatLonPoint
import com.amap.api.services.route.WalkPath
import com.amap.api.services.route.WalkStep

/**
 * 步行路线图层类。在高德地图API里，如果要显示步行路线规划，可以用此类来创建步行路线图层。如不满足需求，也可以自己创建自定义的步行路线图层。
 *
 * 通过此造函数创建步行路线图层。
 * @param context 当前activity。
 * @param amap 地图对象。
 * @param path 步行路线规划的一个方案。详见搜索服务模块的路径查询包（com.amap.api.services.route）中的类 **[WalkStep](../../../../../../Search/com/amap/api/services/route/WalkStep.html)**。
 * @param start 起点。详见搜索服务模块的核心基础包（com.amap.api.services.core）中的类**[LatLonPoint](../../../../../../Search/com/amap/api/services/core/LatLonPoint.html)**。
 * @param end 终点。详见搜索服务模块的核心基础包（com.amap.api.services.core）中的类**[LatLonPoint](../../../../../../Search/com/amap/api/services/core/LatLonPoint.html)**。
 *
 * @since V2.1.0
 */
class SinvieWalkRouteOverlay(
    context: Context?, amap: AMap?, path: WalkPath,
    start: LatLonPoint, end: LatLonPoint
) : SinvieRouteOverlay(context!!) {
    private var mPolylineOptions: PolylineOptions? = null
    private var walkStationDescriptor: BitmapDescriptor? = null
    private val walkPath: WalkPath

    /**
     * 添加步行路线到地图中。
     * @since V2.1.0
     */
    fun addToMap() {
        initPolylineOptions()
        try {
            val walkPaths = walkPath.steps
            for (i in walkPaths.indices) {
                val walkStep = walkPaths[i]
                val latLng: LatLng = walkStep.polyline[0].convertToLatLng()
                addWalkStationMarkers(walkStep, latLng)
                addWalkPolyLines(walkStep)
            }
            addStartAndEndMarker()
            addPolyLine(mPolylineOptions)
        } catch (e: Throwable) {
            e.printStackTrace()
        }
    }

    /**
     * @param walkStep
     * @return
     */
    private fun getLastWalkPoint(walkStep: WalkStep): LatLonPoint {
        return walkStep.polyline[walkStep.polyline.size - 1]
    }

    /**
     * @param walkStep
     * @return
     */
    private fun getFirstWalkPoint(walkStep: WalkStep): LatLonPoint {
        return walkStep.polyline[0]
    }

    /**
     * @param walkStep
     */
    private fun addWalkPolyLines(walkStep: WalkStep) {
        mPolylineOptions!!.addAll(walkStep.polyline.convertArrList())
    }

    /**
     * @param walkStep
     * @param position
     */
    private fun addWalkStationMarkers(walkStep: WalkStep, position: LatLng) {
        addStationMarker(
            MarkerOptions()
                .position(position)
                .title(
                    """
                方向:${walkStep.action}
                道路:${walkStep.road}
                """.trimIndent()
                )
                .snippet(walkStep.instruction).visible(nodeIconVisible)
                .anchor(0.5f, 0.5f).icon(walkStationDescriptor)
        )
    }

    /**
     * 初始化线段属性
     */
    private fun initPolylineOptions() {
//        if (walkStationDescriptor == null) {
//            walkStationDescriptor = getWalkBitmapDescriptor()
//        }
        mPolylineOptions = null
        mPolylineOptions = PolylineOptions()
        mPolylineOptions!!.color(walkColor).width(routeWidth)
    }

    init {
        mAMap = amap
        walkPath = path
        startPoint = start.convertToLatLng()
        endPoint = end.convertToLatLng()
    }
}
