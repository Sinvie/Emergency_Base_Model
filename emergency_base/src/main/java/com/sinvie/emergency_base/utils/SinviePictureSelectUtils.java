package com.sinvie.emergency_base.utils;

import android.app.Activity;
import android.content.pm.ActivityInfo;

import androidx.fragment.app.Fragment;

import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.animators.AnimationType;
import com.luck.picture.lib.camera.CustomCameraView;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.language.LanguageConfig;
import com.luck.picture.lib.listener.OnResultCallbackListener;
import com.sinvie.emergency_base.R;
import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.view.OverlayView;

/**
 * @author: JonsonBob
 * @date: 2021/7/7
 * @Des: 图片选择辅助类
 */
public class SinviePictureSelectUtils {

    private static volatile SinviePictureSelectUtils instance = null;
    private PictureSelector mPictureSelector;

    private SinviePictureSelectUtils() {

    }

    public static SinviePictureSelectUtils getInstance() {
        if (instance == null) {
            synchronized (SinviePictureSelectUtils.class) {
                if (instance == null) {
                    instance = new SinviePictureSelectUtils();
                }
            }
        }
        return instance;
    }

    public SinviePictureSelectUtils init(Activity activity) {
        mPictureSelector = PictureSelector.create(activity);
        return this;
    }

    public SinviePictureSelectUtils init(Fragment fragment) {
        mPictureSelector = PictureSelector.create(fragment);
        return this;
    }

    public void startSelectPic(int num, OnResultCallbackListener<LocalMedia> listener) {
        mPictureSelector.openGallery(PictureMimeType.ofAll())//相册 媒体类型 PictureMimeType.ofAll()
                // 、ofImage()、ofVideo()
                // 、ofAudio()
                //.openCamera()//单独使用相机 媒体类型 PictureMimeType.ofImage()、oVideo()
                .theme(R.style.picture_default_style)// xml样式配制 R.style
                // .picture_default_style、picture_WeChat_style or 更多参考Demo
                .imageEngine(SinvieGlideEngine.createGlideEngine())
//                .compressEngine(ImageCompressEngine.createCompressEngine())// 图片加载引擎 需要 implements ImageEngine接口
                .isCompress(true)
//                .compressEngine() // 自定义图片压缩引擎
                .selectionMode(PictureConfig.MULTIPLE)//单选or多选 PictureConfig.SINGLE PictureConfig
                // .MULTIPLE
                .isPageStrategy(true, 20, true)//开启分页模式，默认开启另提供两个参数；pageSize
                // 每页总数；
                // isFilterInvalidFile是否过滤损坏图片
                .isSingleDirectReturn(false)//PictureConfig.SINGLE模式下是否直接返回
                .isWeChatStyle(true)//开启R.style.picture_WeChat_style样式
//                .setPictureWindowAnimationStyle()//相册启动退出动画
                .isCamera(true)//列表是否显示拍照按钮
                .isZoomAnim(true)//图片选择缩放效果
                // .PNG_Q
                .setCameraImageFormat(PictureMimeType.JPEG)// 相机图片格式后缀,默认.jpeg
                .setCameraVideoFormat(PictureMimeType.MP4)// 相机视频格式后缀,默认.mp4
                .setCameraAudioFormat(PictureMimeType.AMR)// 录音音频格式后缀,默认.amr
                .maxSelectNum(num)//最大选择数量,默认9张
                .minSelectNum(0)// 最小选择数量
                .maxVideoSelectNum(1)//视频最大选择数量
                .minVideoSelectNum(0)//视频最小选择数量
                .videoMaxSecond(60)// 查询多少秒以内的视频
                .videoMinSecond(10)// 查询多少秒以内的视频
                .imageSpanCount(4)//列表每行显示个数
                .recordVideoSecond(60)//录制视频秒数 默认60s
                .filterMinFileSize(0) // 过滤最小的文件
                .filterMaxFileSize(400) // 过滤最大的文件
//                .queryMimeTypeConditions(PictureMimeType.ofJPEG()) // 只查询什么类型的文件
                .isGif(false)//是否显示gif
                .cropImageWideHigh(3, 4)// 裁剪宽高比，设置如果大于图片本身宽高则无效
                .withAspectRatio(3, 4)//裁剪比例
                .cutOutQuality(100)// 裁剪输出质量 默认100
                .freeStyleCropMode(OverlayView.DEFAULT_FREESTYLE_CROP_MODE)// 裁剪框拖动模式
                .isCropDragSmoothToCenter(true)// 裁剪框拖动时图片自动跟随居中
                .circleDimmedLayer(false)// 是否开启圆形裁剪
                .setCircleDimmedBorderColor(0xfff)//设置圆形裁剪边框色值
                .setCircleStrokeWidth(1)//设置圆形裁剪边框粗细
                .showCropFrame(true)// 是否显示裁剪矩形边框 圆形裁剪时建议设为false
                .showCropGrid(true)//是否显示裁剪矩形网格 圆形裁剪时建议设为false
                .rotateEnabled(true)//裁剪是否可旋转图片
                .scaleEnabled(true)//裁剪是否可放大缩小图片
                .isDragFrame(false)//是否可拖动裁剪框(固定)
                .hideBottomControls(false)//显示底部uCrop工具栏
                .basicUCropConfig(new UCrop.Options())//对外提供ucrop所有的配制项
//                .compressEngine()// 自定义压缩引擎
                .minimumCompressSize(100)// 小于多少kb的图片不压缩
                .videoQuality(1)//视频录制质量 0 or 1
//                .compressQuality()//图片压缩后输出质量
                .synOrAsy(true)//开启同步or异步压缩
//                .compressSavePath()//自定义压缩图片保存地址，注意Q版本下的适配
                .isMultipleSkipCrop(true)//多图裁剪是否支持跳过
                .isMultipleRecyclerAnimation(true)// 多图裁剪底部列表显示动画效果
                .isReturnEmpty(false)//未选择数据时按确定是否可以退出
                .isAndroidQTransform(true)//Android Q版本下是否需要拷贝文件至应用沙盒内
                .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)//屏幕旋转方向
                // ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED ...
                .isOriginalImageControl(true)//开启原图选项
//                .bindCustomPlayVideoCallback()//自定义视频播放拦截
//                .bindCustomCameraInterfaceListener()//自定义拍照回调接口
//                .bindCustomPreviewCallback()// 自定义图片预览回调接口
//                .cameraFileName()//自定义拍照文件名，如果是相册内拍照则内部会自动拼上当前时间戳防止重复
//                .renameCompressFile()//自定义压缩文件名，多张压缩情况下内部会自动拼上当前时间戳防止重复
//                .renameCropFileName()//自定义裁剪文件名，多张裁剪情况下内部会自动拼上当前时间戳防止重复
                .setRecyclerAnimationMode(AnimationType.DEFAULT_ANIMATION)//列表动画效果,AnimationType
                // .ALPHA_IN_ANIMATION、SLIDE_IN_BOTTOM_ANIMATION
                .isUseCustomCamera(false)// 开启自定义相机
                .setButtonFeatures(CustomCameraView.BUTTON_STATE_BOTH)// 自定义相机按钮状态,
                // CustomCameraView.BUTTON_STATE_BOTH
                .setLanguage(LanguageConfig.CHINESE)//国际化语言 LanguageConfig.CHINESE、ENGLISH、JAPAN等
                .isWithVideoImage(true)//图片和视频是否可以同选,只在ofAll模式下有效
                .isMaxSelectEnabledMask(true)//选择条件达到阀时列表是否启用蒙层效果
                .isAutomaticTitleRecyclerTop(true)//图片列表超过一屏连续点击顶部标题栏快速回滚至顶部
//                .setOutputCameraPath()// 自定义相机输出目录只针对Android Q以下版本，具体参考Demo
                .forResult(listener);//结果回调分两种方式onActivityResult()和OnResultCallbackListener方式
    }

    public void startSelectPic(OnResultCallbackListener<LocalMedia> listener){
       mPictureSelector .openGallery(PictureMimeType.ofAll())
                .imageEngine(SinvieGlideEngine.createGlideEngine())
                .forResult(listener);
    }

    /**
     * 打开相机
     *
     * @param type 单独使用相机 媒体类型 PictureMimeType.ofImage()、ofVideo()
     */
    public void openCamera(int type, OnResultCallbackListener<LocalMedia> listener) {
        mPictureSelector.openCamera(type)
                .forResult(listener);
    }

}
