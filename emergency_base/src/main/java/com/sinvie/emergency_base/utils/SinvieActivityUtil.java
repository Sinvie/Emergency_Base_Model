package com.sinvie.emergency_base.utils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;

import com.sinvie.emergency_base.ui.SinvieIntentConstant;


public final class SinvieActivityUtil {
    private static final String TAG = SinvieActivityUtil.class.getSimpleName();
    private SinvieActivityUtil() {
    }

    public static void startActivity(Context context, String action, String[] categorys) {
        try {
            Intent intent = new Intent(action);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            for (int i = 0; i < categorys.length; i++) {
                intent.addCategory(categorys[i]);
            }
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            SinvieDemoLog.e(TAG, e.getMessage());
        }
    }

    public static void startActivity(Context context, String action) {
//        LogUtil.d(TAG,"startActivity : " + context.getClass().getSimpleName() +" : " + action);
        try {
            Intent intent = new Intent(action);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addCategory(SinvieIntentConstant.DEFAULT_CATEGORY);
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            SinvieDemoLog.e(TAG, e.getMessage());
        }
    }

    public static void startActivity(Context context, Intent intent) {
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            SinvieDemoLog.e(TAG, e.getMessage());
        }
    }

    public static void startActivityForResult(Activity activity, Intent intent, int requestCode) {
        try {
            intent.addCategory(SinvieIntentConstant.DEFAULT_CATEGORY);
            activity.startActivityForResult(intent, requestCode);
        } catch (ActivityNotFoundException e) {
            SinvieDemoLog.e(TAG, e.getMessage());
        }
    }

    public static void startActivityForResult(Activity activity, String action, int requestCode) {
        try {
            Intent intent = new Intent(action);
            intent.addCategory(SinvieIntentConstant.DEFAULT_CATEGORY);
            activity.startActivityForResult(intent, requestCode);
        } catch (ActivityNotFoundException e) {
            SinvieDemoLog.e(TAG, e.getMessage());
        }
    }
}
