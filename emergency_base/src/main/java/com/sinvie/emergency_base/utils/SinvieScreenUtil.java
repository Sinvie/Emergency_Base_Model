package com.sinvie.emergency_base.utils;

import android.content.Context;

import com.sinvie.emergency_base.SinvieOptions;
import com.sinvie.emergency_live.tuikit.SinvieTUIKit;

/**
 * @author: JonsonBob
 * @date: 2021/12/14
 * @Des: 类描述
 */
public class SinvieScreenUtil {
    public static int getPxByDp(Context context,float dp) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int)(dp * scale + 0.5F);
    }
}
