package com.sinvie.emergency_base.utils;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;

/**
 * This class is about file Tool class.
 */
public final class SinvieFileUtil {

    private static final String TAG = SinvieFileUtil.class.getSimpleName();
    private SinvieFileUtil() {
    }


    /**
     * copy file
     *
     * @param input   FileInputStream
     * @param trgPath The path to the target file to be copied
     */
    public static boolean copyFile(InputStream input, String trgPath) {
        boolean sdCardExist = Environment.getExternalStorageState()
                .equals(Environment.MEDIA_MOUNTED);
        if (!sdCardExist) {
            SinvieDemoLog.e(TAG, "sdcard is not exist");
            return false;
        }

        if (null == input || TextUtils.isEmpty(trgPath)) {
            SinvieDemoLog.e(TAG, "stream or The target path is null!");
            return false;
        }

        BufferedInputStream inBuffStream = null;
        FileOutputStream output = null;
        BufferedOutputStream outBuffStream = null;

        File trgFile = new File(trgPath);
        try {
            if (!trgFile.exists()) {
                boolean isCreateSuccess = trgFile.createNewFile();
                if (!isCreateSuccess) {
                    return false;
                }
            }
            inBuffStream = new BufferedInputStream(input);
            output = new FileOutputStream(trgFile);
            outBuffStream = new BufferedOutputStream(output);
            byte[] buffer = new byte[2 * 1024 * 1024];
            while (true) {
                int inBuflen = inBuffStream.read(buffer);
                if (-1 == inBuflen) {
                    outBuffStream.flush();
                    break;
                } else {
                    outBuffStream.write(buffer, 0, inBuflen);
                }
            }

            return true;
        } catch (FileNotFoundException e) {
            SinvieDemoLog.e(TAG, e.getMessage());
            return false;
        } catch (IOException e) {
            SinvieDemoLog.e(TAG, e.getMessage());
            return false;
        } finally {
            SinvieCloseables.closeCloseable(outBuffStream);
            SinvieCloseables.closeCloseable(output);
            SinvieCloseables.closeCloseable(inBuffStream);
            SinvieCloseables.closeCloseable(input);
        }
    }

    /**
     * is sdcard exist
     *
     * @return boolean
     */
    public static boolean isSdCardExist() {
        boolean sdCardExist = Environment.getExternalStorageState()
                .equals(Environment.MEDIA_MOUNTED);
        if (sdCardExist) {
            return true;
        }
        SinvieDemoLog.e(TAG, "sdcard is not exist");
        return false;
    }

    public static void deleteFile(File file) {
        deleteFile(file, null);
    }

    public static void deleteFile(File file, File[] exceptFiles) {
        if (file == null || !file.exists()) {
            return;
        }

        if (file.isFile() && !file.isHidden()) {
            boolean success = file.delete();

            if (!success) {
                SinvieDemoLog.e(TAG, "delete file error ");
            }

            return;
        }

        if (file.isDirectory() && !isContainFile(file, exceptFiles)) {
            File[] files = file.listFiles();
            if (null != files && 0 != files.length) {
                for (File f : files) {
                    deleteFile(f, exceptFiles);
                }
            }

            if (!file.delete()) {
                SinvieDemoLog.e(TAG, "delete file error ");
            }
        }
    }

    /**
     * Unit conversion
     *
     * @param size
     * @return String
     */
    public static String makeUpSizeShow(double size) {
        double unit = 1024.0;
        String sizeUnit = "B";
        // to KB
        if (unit < size) {
            sizeUnit = "KB";
            size = size / unit;
        }
        // to M
        if (unit < size) {
            sizeUnit = "M";
            size = size / unit;
        }
        // to .00
        DecimalFormat df = new DecimalFormat(".00");
        return df.format(size) + sizeUnit;
    }

    private static boolean isContainFile(File file, File[] files) {
        if (file == null || files == null || files.length == 0) {
            return false;
        }

        for (File f : files) {
            if (file.equals(f)) {
                return true;
            }
        }

        return false;
    }

    /**
     * 系统打开。
     *
     * @param gfFilePath
     * @return
     */
    public static int openBySystem(Context context, String gfFilePath) {

        return OpenResult.OPEN_BY_THIRDPARTY_FAIL;
    }

    private interface OpenResult {
        int OPEN_SUCCESS = 0;
        int OPEN_BY_THIRDPARTY_FAIL = 1;
    }

    public static int SUFFIX_FORMAT_ERROR = -1;
    public static int SUFFIX_FORMAT_VIDEO = 0;
    public static int SUFFIX_FORMAT_IMG = 1;

    /**
     * 根据返回的 url 判断是 img 还是 video
     *
     * @param url
     * @return
     */
    public static int getSuffixFormat(String url) {
        SinvieDemoLog.i(TAG,url);
        //从最后的 . 开始截取
        int index = url.lastIndexOf(".");
        if (index ==-1){
            index = url.lastIndexOf("-");
        }
        String lastIndexEnd = url.substring(index);
        //全部转换成小写再比较
        if (".jpg".equals(lastIndexEnd.toLowerCase())
                || lastIndexEnd.toLowerCase().equals(".png")
                ||lastIndexEnd.toLowerCase().equals(".bmp")) {
            return SUFFIX_FORMAT_IMG;
        } else if (".mp4".equals(lastIndexEnd.toLowerCase())) {
            return SUFFIX_FORMAT_VIDEO;
        } else {
//            SinvieToastUtil.toastLongMessage(LocContext.getContext().getString(R.string.file_suffix_error));
            return SUFFIX_FORMAT_ERROR;
        }
    }

    public static String getResourceName(String url){
        //从最后的 . 开始截取
        String lastIndexEnd = url.substring(url.lastIndexOf("/"));
        return lastIndexEnd.substring(1);
    }
}
