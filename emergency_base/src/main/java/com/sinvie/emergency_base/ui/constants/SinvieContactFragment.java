package com.sinvie.emergency_base.ui.constants;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.ui.base.SinvieAbsFragment;

/**
 * @description 通讯录界面
 * @param
 * @return
 * @author JonsonBob
 * @time 2021/6/25 17:57
 */
public class SinvieContactFragment extends SinvieAbsFragment {

    @Override
    public int getLayoutId() {
        return R.layout.fragment_sinvie_contact;
    }

}