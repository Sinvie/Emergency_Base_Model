package com.sinvie.emergency_base.ui.scene.view;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.ui.scene.SinvieCustomAudienceFragment;
import com.sinvie.emergency_live.tuikit_live.live.base.SinvieConstants;

/**
 * @param
 * @author JonsonBob
 * @description 实现了直播间观众界面
 * @return
 * @time 2021/7/2 14:41
 */
public class SinvieCustomAudienceLayout extends FrameLayout {
    private static final String TAG = "LiveAudienceLayout";

    private CustomLiveRoomAudienceDelegate mLiveRoomAudienceDelegate;
    private FragmentManager mFragmentManager;
    private SinvieCustomAudienceFragment mLiveRoomAudienceFragment;

    public SinvieCustomAudienceLayout(@NonNull Context context) {
        super(context);
    }

    public SinvieCustomAudienceLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.view_sinvie_live_room_audience, this);
        mLiveRoomAudienceFragment = new SinvieCustomAudienceFragment();
    }

    /**
     * 通过 roomId 初始化观众端
     *
     * @param fragmentManager 用于管理fragment，activity 请通过 getSupportFragmentManager() 传入
     * @param roomId          观众端会自动进入该房间
     * @param anchorId
     * @param useCdn          是否使用CDN进行播放
     * @param cdnURL          CDN 播放链接，您可以在直播[控制台](https://console.cloud.tencent.com/live) 配置您的播放域名。
     */
    public void initWithRoomId(FragmentManager fragmentManager, int roomId, String anchorId, boolean useCdn, String cdnURL) {
        Bundle bundle = new Bundle();
        bundle.putInt(SinvieConstants.ROOM_ID, roomId);
        bundle.putString(SinvieConstants.ANCHOR_ID, anchorId);
        bundle.putBoolean(SinvieConstants.USE_CDN, useCdn);
        bundle.putString(SinvieConstants.CDN_URL, cdnURL);
        mLiveRoomAudienceFragment.setArguments(bundle);

        mFragmentManager = fragmentManager;
        mFragmentManager.beginTransaction()
                .add(R.id.live_audience_container, mLiveRoomAudienceFragment, "tuikit-live-audience-fragment")
                .commit();
    }

    /**
     * 设置 UI 回调接口
     *
     * @param liveRoomAudienceDelegate
     */
    public void setLiveRoomAudienceDelegate(CustomLiveRoomAudienceDelegate liveRoomAudienceDelegate) {
        mLiveRoomAudienceDelegate = liveRoomAudienceDelegate;
        mLiveRoomAudienceFragment.setLiveRoomAudienceDelegate(mLiveRoomAudienceDelegate);
    }


    /**
     * 请在 Activity 的 onBackPress 函数中调用该函数，会主动结束房间
     */
    public void onBackPressed() {
        if (mLiveRoomAudienceFragment != null) {
            mLiveRoomAudienceFragment.onBackPressed();
        }
    }

    public interface CustomLiveRoomAudienceDelegate {
        /**
         * 点击界面中的关闭按钮等会回调该通知，可以在Activity中调用finish方法
         */
        void onClose();

        /**
         * UI 组件内部产生错误会通过该接口回调出来
         *
         * @param errorCode 错误码
         * @param message   错误信息
         */
        void onError(int errorCode, String message);
    }
}
