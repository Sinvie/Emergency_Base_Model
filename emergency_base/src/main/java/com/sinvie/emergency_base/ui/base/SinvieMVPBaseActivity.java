package com.sinvie.emergency_base.ui.base;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

import android.content.Context;
import android.content.Intent;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.ui.SinvieIntentConstant;
import com.sinvie.emergency_base.ui.login.SinvieUserInfo;
import com.sinvie.emergency_base.utils.SinvieActivityUtil;
import com.sinvie.emergency_base.utils.SinvieConstants;
import com.sinvie.emergency_base.utils.SinvieDemoLog;
import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_live.tuikit.SinvieTUIKit;
import com.sinvie.emergency_live.tuikit.base.SinvieIMEventListener;
import com.sinvie.emergency_live.tuikit.utils.SinvieToastUtil;

/**
 * This abstract class is about MVP base activity.
 */
public abstract class SinvieMVPBaseActivity<V, P extends SinvieMVPBasePresenter<V>> extends SinvieBaseActivity {
    private static final String TAG = SinvieMVPBaseActivity.class.getSimpleName();
    // 监听做成静态可以让每个子类重写时都注册相同的一份。
    private static SinvieIMEventListener mIMEventListener = new SinvieIMEventListener() {
        @Override
        public void onForceOffline() {
            SinvieToastUtil.toastLongMessage(SinvieLocContext.getContext().getString(R.string.sinvie_repeat_login_tip));
            logout(SinvieLocContext.getContext());
        }

        @Override
        public void onUserSigExpired() {
            SinvieToastUtil.toastLongMessage(SinvieLocContext.getContext().getString(R.string.sinvie_expired_login_tip));
            logout(SinvieLocContext.getContext());
        }
    };
    protected P mPresenter;
    protected V mView;

    public static void logout(Context context) {
        SinvieDemoLog.i(TAG, "logout");
        SinvieUserInfo.getInstance().setToken("");
        SinvieUserInfo.getInstance().setAutoLogin(false);
        Intent intent = new Intent(SinvieIntentConstant.LOGIN_FOR_DEV_ACTIVITY_ACTION);
        intent.setFlags(FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(SinvieConstants.LOGOUT, true);
        SinvieActivityUtil.startActivity(context, intent);
    }


    @Override
    public void initializeView() {
        mPresenter = createPresenter();
        mView = createView();
        mPresenter.attachView(mView);

        //im聊天增加事件监听
        SinvieTUIKit.addIMEventListener(mIMEventListener);

    }

    @Override
    protected void onStart() {
        super.onStart();
//        boolean login = UserInfo.getInstance().isAutoLogin();
//        if (!login) {
//            MVPBaseActivity.logout(LocContext.getContext());
//        }
    }

    @Override
    public void setActivityDestroy() {
        mPresenter.detachView();
    }

    protected abstract V createView();

    protected abstract P createPresenter();
}
