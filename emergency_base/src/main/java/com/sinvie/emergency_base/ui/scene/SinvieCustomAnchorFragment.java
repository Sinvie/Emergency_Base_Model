package com.sinvie.emergency_base.ui.scene;

import static com.sinvie.emergency_live.tuikit_live.live.base.SinvieConstants.MAX_LINK_MIC_SIZE;
import static com.sinvie.emergency_live.tuikit_live.live.base.SinvieConstants.REQUEST_PK_TIME_OUT;
import static com.sinvie.emergency_live.tuikit_live.live.modules.liveroom.ui.widget.SinvieFinishDetailDialog.ANCHOR_HEART_COUNT;
import static com.sinvie.emergency_live.tuikit_live.live.modules.liveroom.ui.widget.SinvieFinishDetailDialog.LIVE_TOTAL_TIME;
import static com.sinvie.emergency_live.tuikit_live.live.modules.liveroom.ui.widget.SinvieFinishDetailDialog.TOTAL_AUDIENCE_COUNT;

import android.Manifest;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.constraintlayout.widget.Group;

import com.hjq.permissions.OnPermissionCallback;
import com.hjq.permissions.XXPermissions;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnResultCallbackListener;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieRoomInfo;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.ui.base.SinvieAbsFragment;
import com.sinvie.emergency_base.ui.login.SinvieUserInfo;
import com.sinvie.emergency_base.ui.scene.view.SinvieCustomAnchorLayout;
import com.sinvie.emergency_base.ui.scene.view.SinvieCustomPreviewLayout;
import com.sinvie.emergency_base.utils.SinvieNetWorkSpeedUtils;
import com.sinvie.emergency_base.utils.SinviePictureSelectUtils;
import com.sinvie.emergency_base.utils.SinvieTimeUtil;
import com.sinvie.emergency_live.tuikit_live.liteav.audiosettingkit.SinvieAudioEffectPanel;
import com.sinvie.emergency_live.tuikit_live.liteav.audiosettingkit.SinvieCircleImageView;
import com.sinvie.emergency_live.tuikit_live.live.SinvieTUIKitLive;
import com.sinvie.emergency_live.tuikit_live.live.base.SinvieConstants;
import com.sinvie.emergency_live.tuikit_live.live.component.bottombar.SinvieBottomBarLayout;
import com.sinvie.emergency_live.tuikit_live.live.component.common.SinvieSelectMemberView;
import com.sinvie.emergency_live.tuikit_live.live.component.countdown.ISinvieCountDownTimerView;
import com.sinvie.emergency_live.tuikit_live.live.component.countdown.SinvieCountDownTimerView;
import com.sinvie.emergency_live.tuikit_live.live.component.gift.SinvieGiftAdapter;
import com.sinvie.emergency_live.tuikit_live.live.component.gift.imp.SinvieDefaultGiftAdapterImp;
import com.sinvie.emergency_live.tuikit_live.live.component.gift.imp.SinvieGiftInfo;
import com.sinvie.emergency_live.tuikit_live.live.component.gift.imp.SinvieGiftInfoDataHandler;
import com.sinvie.emergency_live.tuikit_live.live.component.input.SinvieInputTextMsgDialog;
import com.sinvie.emergency_live.tuikit_live.live.component.message.SinvieChatEntity;
import com.sinvie.emergency_live.tuikit_live.live.component.message.SinvieChatLayout;
import com.sinvie.emergency_live.tuikit_live.live.modules.liveroom.model.SinvieTRTCLiveRoom;
import com.sinvie.emergency_live.tuikit_live.live.modules.liveroom.model.SinvieTRTCLiveRoomCallback;
import com.sinvie.emergency_live.tuikit_live.live.modules.liveroom.model.SinvieTRTCLiveRoomDef;
import com.sinvie.emergency_live.tuikit_live.live.modules.liveroom.model.SinvieTRTCLiveRoomDelegate;
import com.sinvie.emergency_live.tuikit_live.live.modules.liveroom.model.impl.base.SinvieTXUserInfo;
import com.sinvie.emergency_live.tuikit_live.live.modules.liveroom.model.impl.base.SinvieTXUserListCallback;
import com.sinvie.emergency_live.tuikit_live.live.modules.liveroom.model.impl.room.impl.SinvieTXRoomService;
import com.sinvie.emergency_live.tuikit_live.live.modules.liveroom.ui.widget.SinvieExitConfirmDialog;
import com.sinvie.emergency_live.tuikit_live.live.modules.liveroom.ui.widget.SinvieFinishDetailDialog;
import com.sinvie.emergency_live.tuikit_live.live.modules.liveroom.ui.widget.SinvieLinkMicListDialog;
import com.sinvie.emergency_live.tuikit_live.live.modules.liveroom.ui.widget.SinvieLiveVideoView;
import com.sinvie.emergency_live.tuikit_live.live.modules.liveroom.ui.widget.SinvieVideoViewController;
import com.sinvie.emergency_live.tuikit_live.live.utils.SinvieTUILiveLog;
import com.sinvie.emergency_live.tuikit_live.live.utils.SinvieUIUtil;
import com.tencent.rtmp.ui.TXCloudVideoView;

import org.jetbrains.annotations.NotNull;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;


/**
 * @param
 * @author JonsonBob
 * @description 自定义直播间主播界面
 * @return
 * @time 2021/7/2 15:12
 */
public class SinvieCustomAnchorFragment extends SinvieAbsFragment {

    private static final String TAG = SinvieCustomAnchorFragment.class.getSimpleName();
    private static final int LIVE_TIME = 300;
    private static final int LIVE_TIME_Millis = 1000;
    private Handler mMainHandler = new Handler(Looper.getMainLooper());

    private ConstraintLayout mRootView;
    private TXCloudVideoView mVideoViewAnchor;
    private ImageView mImagePkLayer;
    private SinvieAudioEffectPanel mAnchorAudioPanel;
    //主播信息 / 操作 界面
    private SinvieVideoViewController mVideoViewController;
    private SinvieCustomPreviewLayout mLayoutPreview;
    private AlertDialog mErrorDialog;
    private SinvieBottomBarLayout mBottomToolBarLayout;
    private SinvieChatLayout mChatLayout;
    private SinvieTRTCLiveRoom mLiveRoom;
    private SinvieSelectMemberView mSelectMemberView;
    private TextView mBottomStopPkBtn;
    private Group mGroupAfterLive;
    private Group mGroupButtomView;
    private TextView mStatusTipsView;         //状态提示view
    private SinvieLinkMicListDialog mLinkMicListDialog;
    private AlertDialog mRoomPKRequestDialog;
    private ImageView mImageRedDot;
    private SinvieCountDownTimerView mCountDownTimerView;  //倒计时view

    //通知 LiveRoomAnchorActivity UI 变更
    private SinvieGiftAdapter mGiftAdapter;
    private SinvieGiftInfoDataHandler mGiftInfoDataHandler;
    private SinvieCustomAnchorLayout.CustomLiveRoomAnchorLayoutDelegate mLiveRoomAnchorLayoutDelegate;

    private SinvieRoomInfo mSinvieRoomInfo = new SinvieRoomInfo();
    private SinvieTRTCLiveRoomDef.LiveAnchorInfo mAnchorInfo = new SinvieTRTCLiveRoomDef.LiveAnchorInfo();
    private List<String> mAnchorUserIdList = new ArrayList<>();

    //更新观众信息
    private Runnable mGetAudienceRunnable;
    //更新直播时长
    private Runnable mGetLiveTimeRunnable;
    private MyHandler mHandler;

    private long mTotalMemberCount = 0;   // 总进房观众数量
    private long mCurrentMemberCount = 0;   // 当前观众数量
    private long mHeartCount = 0;   // 点赞数量
    private long mStartTime = 0;   // 主播开始直播时间
    private String mOwnerUserId;
    private int mRoomId;
    private String mRoomName;
    private boolean mIsEnterRoom;
    private int mCurrentStatus = SinvieTRTCLiveRoomDef.ROOM_STATUS_NONE;
    private boolean mIsPkStatus;           //当前正在pk
    private boolean mIsLinkMicStatus;      //当前正在连麦
    private SinvieTRTCLiveRoomDelegate mTRTCLiveRoomDelegate = new SinvieTRTCLiveRoomDelegate() {
        @Override
        public void onError(int code, String message) {
            SinvieTUILiveLog.e(TAG, "onError: " + code + " " + message);
            if (mLiveRoomAnchorLayoutDelegate != null) {
                mLiveRoomAnchorLayoutDelegate.onError(mSinvieRoomInfo, code, message);
            }
        }

        @Override
        public void onWarning(int code, String message) {

        }

        @Override
        public void onDebugLog(String message) {

        }

        @Override
        public void onRoomInfoChange(SinvieTRTCLiveRoomDef.TRTCLiveRoomInfo roomInfo) {
            SinvieTUILiveLog.d(TAG, "onRoomInfoChange: " + roomInfo);
            int oldStatus = mCurrentStatus;
            mCurrentStatus = roomInfo.roomStatus;
            setAnchorViewFull(mCurrentStatus != SinvieTRTCLiveRoomDef.ROOM_STATUS_PK);
            SinvieTUILiveLog.d(TAG, "onRoomInfoChange: " + mCurrentStatus);
            if (oldStatus == SinvieTRTCLiveRoomDef.ROOM_STATUS_PK
                    && mCurrentStatus != SinvieTRTCLiveRoomDef.ROOM_STATUS_PK) {
                // 上一个状态是PK，需要将界面中的元素恢复
                mBottomStopPkBtn.setVisibility(View.VISIBLE);
                mBottomStopPkBtn.setText(R.string.sinvie_live_wait_link);
                judgeRedDotShow();
                SinvieLiveVideoView videoView = mVideoViewController.getPKUserView();
                mImagePkLayer.setVisibility(View.GONE);
                mIsPkStatus = false;
            } else if (oldStatus == SinvieTRTCLiveRoomDef.ROOM_STATUS_LINK_MIC
                    && mCurrentStatus != SinvieTRTCLiveRoomDef.ROOM_STATUS_LINK_MIC) {
                mIsLinkMicStatus = false;
            }
        }

        @Override
        public void onRoomDestroy(String roomId) {

        }

        @Override
        public void onAnchorEnter(final String userId) {
            if (context == null) {
                SinvieTUILiveLog.d(TAG, "getContext is null!");
                return;
            }

            mAnchorUserIdList.add(userId);
            final SinvieLiveVideoView view = mVideoViewController.applyVideoView(userId);
            if (view == null) {
                Toast.makeText(context, R.string.sinvie_live_warning_link_user_max_limit,
                        Toast.LENGTH_SHORT).show();
                return;
            }
            if (mCurrentStatus != SinvieTRTCLiveRoomDef.ROOM_STATUS_PK) {
                view.startLoading();
            }
            mLiveRoom.startPlay(userId, view.getPlayerVideo(),
                    new SinvieTRTCLiveRoomCallback.ActionCallback() {
                        @Override
                        public void onCallback(int code, String msg) {
                            if (code == 0) {
                                Log.d(TAG, userId + "");
                                if (mCurrentStatus != SinvieTRTCLiveRoomDef.ROOM_STATUS_PK) {
                                    view.stopLoading(true);
                                }
                            }
                        }
                    });
        }

        @Override
        public void onAnchorExit(String userId) {
            mAnchorUserIdList.remove(userId);
            mLiveRoom.stopPlay(userId, null);
            mVideoViewController.recycleVideoView(userId);
        }

        @Override
        public void onAudienceEnter(SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo) {
            mTotalMemberCount++;
            SinvieChatEntity entity = new SinvieChatEntity();
            entity.setSenderName(getString(R.string.sinvie_live_notification));
            if (TextUtils.isEmpty(userInfo.userName)) {
                entity.setContent(getString(R.string.sinvie_live_user_join_live, userInfo.userId));
            } else {
                entity.setContent(getString(R.string.sinvie_live_user_join_live, userInfo.userName));
            }
            entity.setType(SinvieConstants.MEMBER_ENTER);
            updateIMMessageList(entity);
            addAudienceListLayout(userInfo);
        }

        @Override
        public void onAudienceExit(SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo) {
            SinvieChatEntity entity = new SinvieChatEntity();
            entity.setSenderName(getString(R.string.sinvie_live_notification));
            if (TextUtils.isEmpty(userInfo.userName)) {
                entity.setContent(getString(R.string.sinvie_live_user_quit_live, userInfo.userId));
            } else {
                entity.setContent(getString(R.string.sinvie_live_user_quit_live, userInfo.userName));
            }
            entity.setType(SinvieConstants.MEMBER_EXIT);
            updateIMMessageList(entity);
            removeAudienceListLayout(userInfo);
            mLinkMicListDialog.removeMemberEntity(userInfo.userId);
            judgeRedDotShow();
        }

        @Override
        public void onRequestJoinAnchor(final SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo,
                                        String reason) {
            if (context == null) {
                SinvieTUILiveLog.d(TAG, "getContext is null!");
                return;
            }

            if (mAnchorUserIdList != null && mAnchorUserIdList.size() >= MAX_LINK_MIC_SIZE) {
                mLiveRoom.responseJoinAnchor(userInfo.userId, false,
                        getString(R.string.sinvie_live_warning_link_user_max_limit));
                return;
            }
            SinvieLinkMicListDialog.MemberEntity memberEntity = new SinvieLinkMicListDialog.MemberEntity();
            memberEntity.userId = userInfo.userId;
            memberEntity.userAvatar = userInfo.avatarUrl;
            memberEntity.userName = userInfo.userName;
            mLinkMicListDialog.addMemberEntity(memberEntity);
            judgeRedDotShow();
            Toast.makeText(context, context.getString(R.string.sinvie_live_request_link_mic,
                    userInfo.userName), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onAudienceRequestJoinAnchorTimeout(String userId) {
            mLinkMicListDialog.removeMemberEntity(userId);
            judgeRedDotShow();
        }

        @Override
        public void onAudienceCancelRequestJoinAnchor(String userId) {
            mLinkMicListDialog.removeMemberEntity(userId);
            judgeRedDotShow();
        }

        @Override
        public void onKickoutJoinAnchor() {

        }

        @Override
        public void onRequestRoomPK(final SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                    .setCancelable(true)
                    .setTitle(R.string.sinvie_live_tips)
                    .setMessage(getString(R.string.sinvie_live_request_pk, userInfo.userName))
                    .setPositiveButton(R.string.sinvie_live_accept, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (context == null) {
                                SinvieTUILiveLog.d(TAG, "getContext is null!");
                                return;
                            }

                            if (mIsLinkMicStatus) {
                                Toast.makeText(context, R.string.sinvie_live_link_mic_status,
                                        Toast.LENGTH_SHORT).show();
                                return;
                            }
                            dialog.dismiss();
                            mLiveRoom.responseRoomPK(userInfo.userId, true, "");
                            mIsPkStatus = true;
                        }
                    })
                    .setNegativeButton(R.string.sinvie_live_refuse, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            mLiveRoom.responseRoomPK(userInfo.userId, false,
                                    getString(R.string.sinvie_live_anchor_refuse_pk_request));
                        }
                    });

            mMainHandler.post(() -> {
                if (mRoomPKRequestDialog != null) {
                    mRoomPKRequestDialog.dismiss();
                }
                mRoomPKRequestDialog = builder.create();
                mRoomPKRequestDialog.setCancelable(false);
                mRoomPKRequestDialog.setCanceledOnTouchOutside(false);
                mRoomPKRequestDialog.show();
            });
        }

        @Override
        public void onAnchorCancelRequestRoomPK(String userId) {
            if (mRoomPKRequestDialog != null) {
                mRoomPKRequestDialog.dismiss();
            }
        }

        @Override
        public void onAnchorRequestRoomPKTimeout(String userId) {
            if (mRoomPKRequestDialog != null) {
                mRoomPKRequestDialog.dismiss();
            }
        }

        @Override
        public void onQuitRoomPK() {
        }

        @Override
        public void onRecvRoomTextMsg(String message, SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo) {
            handleTextMsg(userInfo, message);
        }

        @Override
        public void onRecvRoomCustomMsg(String cmd, String message,
                                        SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo) {
            int type = Integer.valueOf(cmd);
            switch (type) {
                case SinvieConstants.IMCMD_PRAISE:
                    mHeartCount++;
                    handlePraiseMsg(userInfo);
                    break;
                case SinvieConstants.IMCMD_DANMU:
                    handleDanmuMsg(userInfo, message);
                    break;
                case SinvieConstants.IMCMD_GIFT:
                    handleGiftMsg(userInfo, message);
                    break;
                default:
                    break;
            }
        }
    };

    public void setLiveRoomAnchorLayoutDelegate(SinvieCustomAnchorLayout.CustomLiveRoomAnchorLayoutDelegate liveRoomAnchorLayoutDelegate) {
        mLiveRoomAnchorLayoutDelegate = liveRoomAnchorLayoutDelegate;
    }

    public void stopLive() {
        if (mIsEnterRoom) {
            destroyRoom();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIsEnterRoom = false;

        mHandler = new MyHandler(Looper.myLooper(), this);

        mLiveRoom = SinvieTRTCLiveRoom.sharedInstance(context);
        mLiveRoom.setDelegate(mTRTCLiveRoomDelegate);
        mOwnerUserId = SinvieUserInfo.getInstance().getUserId();

        // 如果当前观众页悬浮窗，关闭并退房
//        if (SinvieFloatWindowLayout.getInstance().mWindowMode == SinvieConstants.WINDOW_MODE_FLOAT) {
//            SinvieFloatWindowLayout.getInstance().closeFloatWindow();
//            mLiveRoom.exitRoom(null);
//        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        preExitRoom();
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_sinvie_custom_live_room_anchor;
    }

    @Override
    public void onDataLoad() {
        initView(mView);

        //先显示网速
        new SinvieNetWorkSpeedUtils(context, mHandler).startShowNetSpeed();

        updateAnchorInfo();

        initData();
    }

    private void preExitRoom() {

        Log.d(TAG, "preExitRoom: " + mIsEnterRoom);
        if (mIsEnterRoom) {
            showExitInfoDialog(context.getString(R.string.sinvie_live_warning_anchor_exit_room),
                    false);
        } else {
            finishRoom();
        }
    }

    private void initView(View view) {
        mRootView = view.findViewById(R.id.custom_root);
        mVideoViewAnchor = view.findViewById(R.id.custom_video_view_anchor);
        mImagePkLayer = view.findViewById(R.id.custom_iv_pk_layer);
        mAnchorAudioPanel = new SinvieAudioEffectPanel(context);
        mLayoutPreview = view.findViewById(R.id.custom_layout_preview);
        mChatLayout = view.findViewById(R.id.custom_layout_chat);
        mGroupAfterLive = view.findViewById(R.id.custom_group_after_live);
        mGroupButtomView = view.findViewById(R.id.custom_group_bottom_view);
        mStatusTipsView = view.findViewById(R.id.custom_state_tips);
        initBottomToolBar(view);
        mAnchorAudioPanel.setAudioEffectManager(mLiveRoom.getAudioEffectManager());
        mAnchorAudioPanel.initPanelDefaultBackground();
        List<SinvieLiveVideoView> tuiVideoViewList = new ArrayList<>();
        tuiVideoViewList.add(view.findViewById(R.id.custom_video_view_link_mic_1));
        tuiVideoViewList.add(view.findViewById(R.id.custom_video_view_link_mic_2));
        tuiVideoViewList.add(view.findViewById(R.id.custom_video_view_link_mic_3));
        mVideoViewController = new SinvieVideoViewController(tuiVideoViewList,
                userId -> {
                    if (userId != null) {
                        mLiveRoom.kickoutJoinAnchor(userId, null);
                    }
                });

        mBottomStopPkBtn = view.findViewById(R.id.custom_btn_bottom_stop_pk);
        mImageRedDot = view.findViewById(R.id.custom_img_badge);
        mBottomStopPkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentStatus == SinvieTRTCLiveRoomDef.ROOM_STATUS_PK) {
                    mLiveRoom.quitRoomPK(null);
                } else {
                    //排麦需求
                    mLinkMicListDialog.show();
                }
            }
        });

        //PK展示界面
        mSelectMemberView = new SinvieSelectMemberView(context);
        mSelectMemberView.setOnSelectedCallback(new SinvieSelectMemberView.onSelectedCallback() {
            @Override
            public void onSelected(int seatIndex, SinvieSelectMemberView.MemberEntity memberEntity) {
                mLiveRoom.requestRoomPK(memberEntity.roomId, memberEntity.userId,
                        REQUEST_PK_TIME_OUT, new SinvieTRTCLiveRoomCallback.ActionCallback() {
                            @Override
                            public void onCallback(int code, String msg) {
                                if (context == null) {
                                    SinvieTUILiveLog.d(TAG, "getContext is null!");
                                    return;
                                }

                                mStatusTipsView.setText("");
                                mStatusTipsView.setVisibility(View.GONE);
                                if (code == 0) {
                                    // 主播已经接受
                                    Toast.makeText(context, R.string.sinvie_live_anchor_accept_pk,
                                            Toast.LENGTH_SHORT).show();
                                } else if (code == -2) {
                                    Toast.makeText(context,
                                            R.string.sinvie_live_anchor_accept_pk_timeout,
                                            Toast.LENGTH_SHORT).show();
                                } else {
                                    // 主播拒绝
                                    Toast.makeText(context, R.string.sinvie_live_anchor_reject_pk,
                                            Toast.LENGTH_SHORT).show();
                                }
                                mSelectMemberView.dismiss();
                            }
                        });
                mSelectMemberView.dismiss();
                mStatusTipsView.setText(R.string.sinvie_live_wait_anchor_accept);
                mStatusTipsView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCancel() {
                mSelectMemberView.dismiss();
            }
        });

        mLinkMicListDialog = new SinvieLinkMicListDialog(context);
        mLinkMicListDialog.setTitle(context.getString(R.string.sinvie_live_audience_link_mic));
        mLinkMicListDialog.setOnSelectedCallback(new SinvieLinkMicListDialog.onSelectedCallback() {
            @Override
            public void onItemAgree(SinvieLinkMicListDialog.MemberEntity memberEntity) {
                if (context == null) {
                    SinvieTUILiveLog.d(TAG, "getContext is null!");
                    return;
                }

                if (mIsPkStatus) {
                    Toast.makeText(context, R.string.sinvie_live_pk_status, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (mAnchorUserIdList != null && mAnchorUserIdList.size() >= MAX_LINK_MIC_SIZE) {
                    Toast.makeText(context, R.string.sinvie_live_warning_link_user_max_limit,
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                mLiveRoom.responseJoinAnchor(memberEntity.userId, true,
                        getString(R.string.sinvie_live_anchor_accept));
                mLinkMicListDialog.removeMemberEntity(memberEntity.userId);
                mLinkMicListDialog.dismiss();
                judgeRedDotShow();
                mIsLinkMicStatus = true;
            }

            @Override
            public void onItemReject(SinvieLinkMicListDialog.MemberEntity memberEntity) {
                mLiveRoom.responseJoinAnchor(memberEntity.userId, false,
                        getString(R.string.sinvie_live_anchor_reject));
                mLinkMicListDialog.removeMemberEntity(memberEntity.userId);
                mLinkMicListDialog.dismiss();
                judgeRedDotShow();
            }

            @Override
            public void onCancel() {
                mLinkMicListDialog.dismiss();
            }
        });

        mLayoutPreview.setPreviewCallback(new SinvieCustomPreviewLayout.CustomPreviewCallback() {
            @Override
            public void onSwitchCamera() {
                //切换前后摄像头
                mLiveRoom.switchCamera();
            }

            @Override
            public void onSelectCover() {
                //选择封面
                XXPermissions.with(context).permission(LivePermissions())
                        .request((permissions, all) -> openPictureCamera());
            }

            @Override
            public void onSceneOrientation() {
                //确定屏幕方向

            }

            @Override
            public void onLockLiveRoom(boolean isLock, String pwd) {
                //房间是否锁定

            }

            @Override
            public void onStartLive(String roomName, int audioQualityType) {
                //直播类型
                mSinvieRoomInfo.setLiveType(audioQualityType);

                XXPermissions.with(context)
                        .permission(LivePermissions())
                        .request((permissions, all) -> {
                            startPreview();
                            startLiveCountDown(roomName, audioQualityType);

                        });
            }

            @Override
            public void onClose() {
                // 点击预览页的关闭按钮
                if (mStartTime != 0) {
                    //开启直播
                    preExitRoom();
                } else {
                    //没开直播
                    finishRoom();
                }
            }
        });
        XXPermissions.with(context).permission(LivePermissions())
                .request(new OnPermissionCallback() {
                    @Override
                    public void onGranted(List<String> permissions, boolean all) {
                        startPreview();
                    }
                });

        mCountDownTimerView = view.findViewById(R.id.custom_countdown_timer_view);
    }

    private void startLiveCountDown(final String roomName, final int audioQualityType) {
        //隐藏直播开始前设置界面
        mLayoutPreview.setVisibility(View.GONE);
        mCountDownTimerView.countDownAnimation(SinvieCountDownTimerView.DEFAULT_COUNTDOWN_NUMBER);
        mCountDownTimerView.setOnCountDownListener(new ISinvieCountDownTimerView.ICountDownListener() {
            @Override
            public void onCountDownComplete() {
                startLive(roomName, audioQualityType);
            }
        });
    }

    private void startLive(final String roomName, final int audioQualityType) {
        // 开始创建房间
        SinvieTRTCLiveRoomDef.TRTCCreateRoomParam roomParam = new SinvieTRTCLiveRoomDef.TRTCCreateRoomParam();
        roomParam.roomName = roomName;
        roomParam.coverUrl = mAnchorInfo.avatarUrl;
        mLiveRoom.createRoom(mRoomId, roomParam, (code, msg) -> {
            if (context == null) {
                SinvieTUILiveLog.d(TAG, "getContext is null!");
                return;
            }

            Toast.makeText(context, R.string.sinvie_live_create_room_success,
                    Toast.LENGTH_SHORT).show();
            if (code == 0) {
                mIsEnterRoom = true;
                onCreateRoomSuccess(audioQualityType);
                if (mLiveRoomAnchorLayoutDelegate != null) {
                    //直播间标题
                    mSinvieRoomInfo.setLiveRoomTitle(roomName);
                    //直播间id
                    mSinvieRoomInfo.setRoomId(mRoomId + "");
                    //主播id
                    mSinvieRoomInfo.setUserId(mOwnerUserId);
                    //事件id 暂时为空
                    //mRoomInfo.setEventId();
                    //直播定位地址
                    //mRoomInfo.setLiveAddress();
                    //直播开始时间
                    mSinvieRoomInfo.setStartTime(System.currentTimeMillis() + "");
//                        mRoomInfo.setLiveStartTime(new TimeUtil().getTimeData());
                    //直播结束时间
                    //mRoomInfo.setLiveEndTime();
                    //记录直播状态
                    mSinvieRoomInfo.setLiveStatus(SinvieUIConstants.LIVE_STATUS_START);
                    //mRoomInfo.setStreamUrl();
                    mLiveRoomAnchorLayoutDelegate.onRoomCreate(mSinvieRoomInfo);
                }
            } else {
                Toast.makeText(context, R.string.sinvie_live_create_room_fail,
                        Toast.LENGTH_SHORT).show();
                showErrorAndQuit(code, msg);
            }
        });
    }

    private void initData() {
        mGiftInfoDataHandler = new SinvieGiftInfoDataHandler();
        mGiftAdapter = new SinvieDefaultGiftAdapterImp();
        mGiftInfoDataHandler.setGiftAdapter(mGiftAdapter);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mRoomId = bundle.getInt("room_id");
        }
    }

    private void initBottomToolBar(View view) {
        mBottomToolBarLayout = view.findViewById(R.id.custom_layout_bottom_toolbar);
        mBottomToolBarLayout.setOnTextSendListener(new SinvieInputTextMsgDialog.OnTextSendDelegate() {
            @Override
            public void onTextSend(String msg, boolean tanmuOpen) {
                SinvieChatEntity entity = new SinvieChatEntity();
                entity.setSenderName(getString(R.string.sinvie_live_message_me));
                entity.setContent(msg);
                entity.setType(SinvieConstants.TEXT_TYPE);
                updateIMMessageList(entity);

                if (tanmuOpen) {
                    mLiveRoom.sendRoomCustomMsg(String.valueOf(SinvieConstants.IMCMD_DANMU), msg,
                            new SinvieTRTCLiveRoomCallback.ActionCallback() {
                                @Override
                                public void onCallback(int code, String msg) {

                                }
                            });
                } else {
                    mLiveRoom.sendRoomTextMsg(msg, new SinvieTRTCLiveRoomCallback.ActionCallback() {
                        @Override
                        public void onCallback(int code, String msg) {
                            if (code != 0) {
                                Toast.makeText(SinvieTUIKitLive.getAppContext(),
                                        R.string.sinvie_live_message_send_fail, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });

        // 初始化切换摄像头按钮
        SinvieCircleImageView buttonSwitchCam = new SinvieCircleImageView(context);
        buttonSwitchCam.setImageResource(R.drawable.ic_sinvie_live_switch_camera_on);
        buttonSwitchCam.setOnClickListener(view1 -> mLiveRoom.switchCamera());
        mBottomToolBarLayout.setRightButtonsLayout(Collections.singletonList(buttonSwitchCam));
    }

    private void updateIMMessageList(SinvieChatEntity entity) {
        mChatLayout.addMessageToList(entity);
    }

    /**
     * 处理点赞消息显示
     */
    public void handlePraiseMsg(SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo) {
        SinvieChatEntity entity = new SinvieChatEntity();

        entity.setSenderName(getString(R.string.sinvie_live_notification));
        if (TextUtils.isEmpty(userInfo.userName)) {
            entity.setContent(getString(R.string.sinvie_live_user_click_like, userInfo.userId));
        } else {
            entity.setContent(getString(R.string.sinvie_live_user_click_like, userInfo.userName));
        }
        entity.setType(SinvieConstants.MEMBER_ENTER);
        updateIMMessageList(entity);
    }

    /**
     * 处理弹幕消息的显示
     */
    public void handleDanmuMsg(SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo, String text) {
        handleTextMsg(userInfo, text);
    }

    /**
     * 处理礼物弹幕消息
     */
    private void handleGiftMsg(SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo, String giftId) {
        if (mGiftInfoDataHandler != null) {
            SinvieGiftInfo giftInfo = mGiftInfoDataHandler.getGiftInfo(giftId);
            if (giftInfo != null) {
                if (userInfo != null) {
                    giftInfo.sendUserHeadIcon = userInfo.avatarUrl;
                    if (!TextUtils.isEmpty(userInfo.userName)) {
                        giftInfo.sendUser = userInfo.userName;
                    } else {
                        giftInfo.sendUser = userInfo.userId;
                    }
                }
            }
        }
    }

    /**
     * 处理文本消息显示
     */
    public void handleTextMsg(SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo, String text) {
        SinvieChatEntity entity = new SinvieChatEntity();
        if (TextUtils.isEmpty(userInfo.userName)) {
            entity.setSenderName(userInfo.userId);
        } else {
            entity.setSenderName(userInfo.userName);
        }
        entity.setContent(text);
        entity.setType(SinvieConstants.TEXT_TYPE);
        updateIMMessageList(entity);
    }

    protected void startPreview() {
        // 打开本地预览，传入预览的 View
        mVideoViewAnchor.setVisibility(View.VISIBLE);
        mLiveRoom.startCameraPreview(true, mVideoViewAnchor, null);
    }

    private void finishRoom() {
        mLiveRoom.stopCameraPreview();
        if (mLiveRoomAnchorLayoutDelegate != null) {
            mLiveRoomAnchorLayoutDelegate.onClose();
        }
    }

    protected void destroyRoom() {
        mIsEnterRoom = false;
        mLiveRoom.stopCameraPreview();
        mLiveRoom.destroyRoom((code, msg) -> {
            if (code == 0) {
                SinvieTUILiveLog.d(TAG, "destroy room ");
            } else {
                SinvieTUILiveLog.d(TAG, "destroy room failed:" + msg);
            }
        });
        mLiveRoom.setDelegate(null);
        if (mLiveRoomAnchorLayoutDelegate != null) {
            mSinvieRoomInfo.setEndTime(System.currentTimeMillis() + "");
            mSinvieRoomInfo.setLiveStatus(SinvieUIConstants.LIVE_STATUS_STOP);
            mLiveRoomAnchorLayoutDelegate.onRoomDestroy(mSinvieRoomInfo);
        }

        // 退房后，清理AudioPanel的相关设置
        if (mAnchorAudioPanel != null) {
            mAnchorAudioPanel.unInit();
        }
    }

    /**
     * 直播房间创建成功
     *
     * @param audioQualityType 直播类型 (不是任务类型) : 音乐,原生
     */
    private void onCreateRoomSuccess(int audioQualityType) {
        // 创建房间成功
        mGroupAfterLive.setVisibility(View.VISIBLE);
        mGroupButtomView.setVisibility(View.VISIBLE);

        //显示控件
        mLayoutPreview.setVisibility(View.VISIBLE);
        //隐藏部分开播后不能调整的属性
        mLayoutPreview.startLive();
        //房间号
        mLayoutPreview.setRoomId(mRoomId + "");

        //更新时长
        updateLiveTimeInterval();

        //更新头布局时间
        updateTopToolBar();

        mLiveRoom.setAudioQuality(audioQualityType);
        // 创建房间成功，开始推流
        mLiveRoom.startPublish("", new SinvieTRTCLiveRoomCallback.ActionCallback() {
            @Override
            public void onCallback(int code, String msg) {
                if (context == null) {
                    SinvieTUILiveLog.d(TAG, "getContext is null!");
                    return;
                }

                if (code == 0) {
                    mStartTime = System.currentTimeMillis();
                    SinvieTUILiveLog.d(TAG, "start live success");
                } else {
                    Toast.makeText(context, "start publish failed:" + msg,
                            Toast.LENGTH_SHORT).show();
                    SinvieTUILiveLog.e(TAG, "start live failed:" + msg);
                }
            }
        });
    }

    // 获取主播最新info
    private void updateAnchorInfo() {

        mAnchorInfo.userId = SinvieUserInfo.getInstance().getUserId();
        mAnchorInfo.userName = SinvieUserInfo.getInstance().getName();
        mAnchorInfo.avatarUrl = SinvieUserInfo.getInstance().getAvatar();
        //走接口获取数据
        SinvieTXRoomService.getInstance().getUserInfo(Collections.singletonList(mOwnerUserId),
                new SinvieTXUserListCallback() {
                    @Override
                    public void onCallback(int code, String msg, List<SinvieTXUserInfo> list) {
                        if (code == 0) {
                            for (SinvieTXUserInfo info : list) {
                                if (info.userId.equals(mOwnerUserId)) {
                                    mAnchorInfo.userId = info.userId;
                                    mAnchorInfo.userName = info.userName;
                                    mAnchorInfo.avatarUrl = info.avatarURL;
//                                    mLayoutTopToolBar.setAnchorInfo(mAnchorInfo);
//                                    mLayoutTopToolBar.setHasFollowed(true);
                                    SinvieTUILiveLog.d(TAG, "updateAnchorInfo mAnchorInfo: " + mAnchorInfo.toString());
                                }
                            }
                        } else {
                            SinvieTUILiveLog.e(TAG, "code: " + code + " msg: " + msg);
                        }
                    }
                });
    }

    /**
     * 显示直播结果的弹窗，如：观看数量、点赞数量、直播时长数
     */
    protected void showPublishFinishDetailsDialog() {
        //确认则显示观看detail
        SinvieFinishDetailDialog dialog = new SinvieFinishDetailDialog();
        Bundle args = new Bundle();
        long second = 0;
        if (mStartTime != 0) {
            second = (System.currentTimeMillis() - mStartTime) / 1000;
        }
        args.putString(LIVE_TOTAL_TIME, SinvieUIUtil.formattedTime(second));
        args.putString(ANCHOR_HEART_COUNT, String.format(Locale.CHINA, "%d", mHeartCount));
        args.putString(TOTAL_AUDIENCE_COUNT, String.format(Locale.CHINA, "%d", mTotalMemberCount));
        dialog.setArguments(args);
        dialog.setCancelable(false);
        if (dialog.isAdded()) {
            dialog.dismiss();
        } else {
            dialog.show(getChildFragmentManager(), "");
        }
    }

    /**
     * 显示确认消息
     *
     * @param msg     消息内容
     * @param isError true错误消息（必须退出） false提示消息（可选择是否退出）
     */
    public void showExitInfoDialog(String msg, Boolean isError) {
        final SinvieExitConfirmDialog dialogFragment = new SinvieExitConfirmDialog();
        dialogFragment.setCancelable(false);
        dialogFragment.setMessage(msg);

        if (dialogFragment.isAdded()) {
            dialogFragment.dismiss();
            return;
        }

        if (isError) {
            destroyRoom();
            dialogFragment.setPositiveClickListener(() -> {
                dialogFragment.dismiss();
                showPublishFinishDetailsDialog();
            });
            dialogFragment.show(getChildFragmentManager(), "SinvieExitConfirmDialog");
            return;
        }

        dialogFragment.setPositiveClickListener(() -> {
            dialogFragment.dismiss();
            destroyRoom();
            showPublishFinishDetailsDialog();
        });

        dialogFragment.setNegativeClickListener(() -> dialogFragment.dismiss());
        dialogFragment.show(getChildFragmentManager(), "SinvieExitConfirmDialog");
    }

    /**
     * 显示错误并且退出直播的弹窗
     *
     * @param errorCode
     * @param errorMsg
     */
    protected void showErrorAndQuit(int errorCode, String errorMsg) {
        if (mErrorDialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context,
                    R.style.SinvieTUILiveDialogTheme)
                    .setTitle(R.string.sinvie_live_error)
                    .setMessage(errorMsg)
                    .setNegativeButton(R.string.sinvie_live_get_it, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mErrorDialog.dismiss();
                            destroyRoom();
                            finishRoom();
                        }
                    });
            mErrorDialog = builder.create();
        }
        if (mErrorDialog.isShowing()) {
            mErrorDialog.dismiss();
        }
        mErrorDialog.show();
    }

    private void setAnchorViewFull(boolean isFull) {
        if (isFull) {
            ConstraintSet set = new ConstraintSet();
            set.clone(mRootView);
            set.connect(mVideoViewAnchor.getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP);
            set.connect(mVideoViewAnchor.getId(), ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START);
            set.connect(mVideoViewAnchor.getId(), ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM);
            set.connect(mVideoViewAnchor.getId(), ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END);
            set.applyTo(mRootView);
        } else {
            ConstraintSet set = new ConstraintSet();
            set.clone(mRootView);
            set.connect(mVideoViewAnchor.getId(), ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START);
            set.connect(mVideoViewAnchor.getId(), ConstraintSet.END, R.id.custom_gl_vertical, ConstraintSet.END);
            set.applyTo(mRootView);
        }
    }

    //        更新主播消息
    private void updateTopToolBar() {
        updateAnchorInfo();
        updateTopAudienceInfo();
    }

    private void updateTopAudienceInfo() {
        mGetAudienceRunnable = () -> mLiveRoom.getAudienceList((code, msg, list) -> {
            if (code == 0) {
                //计算直播人数
                addAudienceListLayout(list);
            } else {
                mMainHandler.postDelayed(mGetAudienceRunnable, 2000);
            }
        });
        // 为了防止进房后立即获取列表不全，所以增加了一个延迟
        mMainHandler.postDelayed(mGetAudienceRunnable, 2000);
    }

    private void addAudienceListLayout(List<SinvieTRTCLiveRoomDef.TRTCLiveUserInfo> list) {
        mLayoutPreview.addAudienceListUser(list);
    }

    private void addAudienceListLayout(SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo) {
        mLayoutPreview.addAudienceListUser(userInfo);
    }

    private void removeAudienceListLayout(SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo) {
        mLayoutPreview.removeAudienceUser(userInfo);
    }

    /**
     * 更新直播时长
     */
    private void updateLiveTimeInterval() {

        mGetLiveTimeRunnable = () -> {
            mLayoutPreview.setLiveTime(SinvieTimeUtil.getTimeInterval(mStartTime));
            mMainHandler.postDelayed(mGetLiveTimeRunnable, LIVE_TIME_Millis);
        };
        mMainHandler.postDelayed(mGetLiveTimeRunnable, LIVE_TIME_Millis);
    }

    private void judgeRedDotShow() {
        if (mCurrentStatus == SinvieTRTCLiveRoomDef.ROOM_STATUS_PK) {
            mImageRedDot.setVisibility(View.GONE);
        } else {
            if (mLinkMicListDialog.getList().size() > 0) {
                mImageRedDot.setVisibility(View.VISIBLE);
            } else {
                mImageRedDot.setVisibility(View.GONE);
            }
        }
    }

    private String[] LivePermissions() {
        String[] permissions = {
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO,
//                Manifest.permission.MANAGE_EXTERNAL_STORAGE,
        };
        return permissions;
    }

    private void openPictureCamera() {
        SinviePictureSelectUtils.getInstance()
                .init(SinvieCustomAnchorFragment.this)
                .startSelectPic(1, new OnResultCallbackListener<LocalMedia>() {
                    @Override
                    public void onResult(List<LocalMedia> result) {
                        LocalMedia media = result.get(0);
                        String path = null;
                        if (media.isCut() && !media.isCompressed()) {
                            // 裁剪过
                            path = media.getCutPath();
                        } else if (media.isCompressed() || (media.isCut() && media.isCompressed())) {
                            // 压缩过,或者裁剪同时压缩过,以最终压缩过图片为准
                            path = media.getCompressPath();
                        } else {
                            // 原图
                            path = media.getPath();
                        }
                        mLayoutPreview.setLiveRoomCover(path);
                    }

                    @Override
                    public void onCancel() {

                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMainHandler.removeCallbacks(mGetAudienceRunnable);
        mMainHandler.removeCallbacks(mGetLiveTimeRunnable);
        mAnchorAudioPanel = null;
    }

    private class MyHandler extends Handler {

        private WeakReference<SinvieCustomAnchorFragment> mWeakReference;

        public MyHandler(@NonNull @NotNull Looper looper, SinvieCustomAnchorFragment fragment) {
            super(looper);
            mWeakReference = new WeakReference<SinvieCustomAnchorFragment>(fragment);
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if (mWeakReference.get() == null) {
                return;
            }
            if (SinvieNetWorkSpeedUtils.NET_WORK_SPEED_MSG == msg.what) {
                mWeakReference.get().mLayoutPreview.setLiveNetSpeed(msg.obj.toString());
            }
        }
    }

}