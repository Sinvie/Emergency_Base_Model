package com.sinvie.emergency_base.ui.call;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import com.huawei.ecterminalsdk.base.TsdkMobileAuidoRoute;
import com.sinvie.emergency_conf.CallService.SinvieCallConstant;
import com.sinvie.emergency_conf.CallService.SinvieCallMgr;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.utils.SinvieDialogUtil;


public class SinvieCallInActivitySinvie extends SinvieBaseMediaActivity {
    private AlertDialog mDialog;
    private MyHandler mHandler = new MyHandler();

    @SuppressLint("HandlerLeak")
    private class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            if (KeyEvent.KEYCODE_BACK == msg.what) {
                mDialog = SinvieDialogUtil.generateDialog(SinvieCallInActivitySinvie.this, R.string.sinvie_ntf_end_call,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (mIsConfCall) {
                                    finish();
                                }
                                SinvieCallMgr.getInstance().endCall(mCallID);
                            }
                        });
                mDialog.show();
            }
        }
    }


    @Override
    public void initializeData() {
        super.initializeData();
    }

    @Override
    public void initializeComposition() {
        super.initializeComposition();

        mRejectBtn.setVisibility(View.VISIBLE);
        mAudioAcceptCallArea.setVisibility(View.VISIBLE);
        mAudioAcceptCallArea.setActivated(false);

        if (mIsVideoCall == true) {
            mVideoAcceptCallArea.setVisibility(View.VISIBLE);
            mVideoAcceptCallArea.setActivated(true);
        }

        mDivertCallArea.setVisibility(View.VISIBLE);

        mCallNameTv.setText(null == mDisplayName ? "" : mDisplayName);
        mCallNumberTv.setText(null == mCallNumber ? "" : mCallNumber);

        mRejectBtn.setOnClickListener(this);
        mAudioAcceptCallArea.setOnClickListener(this);
        mVideoAcceptCallArea.setOnClickListener(this);
        mDivertCallArea.setOnClickListener(this);

        int currentAudioRoute = SinvieCallMgr.getInstance().getCurrentAudioRoute();
        if (currentAudioRoute != SinvieCallConstant.TYPE_LOUD_SPEAKER) {
            SinvieCallMgr.getInstance().setAudioRoute(TsdkMobileAuidoRoute.TSDK_E_MOBILE_AUDIO_ROUTE_LOUDSPEAKER);
        }
        mSpeakerButton.setActivated(true);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int id = v.getId();
        if (id == R.id.reject_btn) {
            if (0 != mConfToCallHandle) {
                SinvieCallMgr.getInstance().stopPlayRingingTone();
                SinvieCallMgr.getInstance().stopPlayRingBackTone();
            }
            SinvieCallMgr.getInstance().endCall(mCallID);
            finish();
        } else if (id == R.id.audio_accept_call_area) {
            SinvieCallMgr.getInstance().answerCall(mCallID, false);

        } else if (id == R.id.video_accept_call_area) {

            SinvieCallMgr.getInstance().answerCall(mCallID, mIsVideoCall);
        } else if (id == R.id.divert_call_area) {
            final EditText editText = new EditText(this);
            new AlertDialog.Builder(this)
                    .setView(editText)
                    .setTitle("Please input divert number")
                    .setPositiveButton("sure", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String number = editText.getText().toString();
                            SinvieCallMgr.getInstance().divertCall(mCallID, number);
                        }
                    })
                    .setNegativeButton("exit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        }
    }

    @Override
    public void onBackPressed() {
        mHandler.sendEmptyMessage(KeyEvent.KEYCODE_BACK);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dismissDialog(mDialog);
        mHandler.removeCallbacksAndMessages(null);
    }
}
