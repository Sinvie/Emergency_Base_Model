package com.sinvie.emergency_base.ui.scene;


import static com.sinvie.emergency_live.tuikit_live.live.base.SinvieConstants.REQUEST_LINK_MIC_TIME_OUT;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.ui.scene.view.SinvieCustomAudienceLayout;
import com.sinvie.emergency_base.utils.SinvieDemoLog;
import com.sinvie.emergency_live.tuikit_live.liteav.audiosettingkit.SinvieCircleImageView;
import com.sinvie.emergency_live.tuikit_live.liteav.demo.beauty.SinvieBeautyParams;
import com.sinvie.emergency_live.tuikit_live.live.SinvieTUIKitLive;
import com.sinvie.emergency_live.tuikit_live.live.base.SinvieBaseFragment;
import com.sinvie.emergency_live.tuikit_live.live.base.SinvieConstants;
import com.sinvie.emergency_live.tuikit_live.live.component.bottombar.SinvieBottomBarLayout;
import com.sinvie.emergency_live.tuikit_live.live.component.common.SinvieActionSheetDialog;
import com.sinvie.emergency_live.tuikit_live.live.component.floatwindow.SinvieFloatWindowLayout;
import com.sinvie.emergency_live.tuikit_live.live.component.gift.ISinvieGiftPanelView;
import com.sinvie.emergency_live.tuikit_live.live.component.gift.SinvieGiftAdapter;
import com.sinvie.emergency_live.tuikit_live.live.component.gift.SinvieGiftPanelDelegate;
import com.sinvie.emergency_live.tuikit_live.live.component.gift.imp.SinvieDefaultGiftAdapterImp;
import com.sinvie.emergency_live.tuikit_live.live.component.gift.imp.SinvieGiftInfo;
import com.sinvie.emergency_live.tuikit_live.live.component.gift.imp.SinvieGiftInfoDataHandler;
import com.sinvie.emergency_live.tuikit_live.live.component.gift.imp.SinvieGiftPanelViewImp;
import com.sinvie.emergency_live.tuikit_live.live.component.input.SinvieInputTextMsgDialog;
import com.sinvie.emergency_live.tuikit_live.live.component.message.SinvieChatEntity;
import com.sinvie.emergency_live.tuikit_live.live.component.message.SinvieChatLayout;
import com.sinvie.emergency_live.tuikit_live.live.component.report.SinvieReportController;
import com.sinvie.emergency_live.tuikit_live.live.component.topbar.SinvieTopToolbarLayout;
import com.sinvie.emergency_live.tuikit_live.live.modules.liveroom.model.SinvieTRTCLiveRoom;
import com.sinvie.emergency_live.tuikit_live.live.modules.liveroom.model.SinvieTRTCLiveRoomCallback;
import com.sinvie.emergency_live.tuikit_live.live.modules.liveroom.model.SinvieTRTCLiveRoomDef;
import com.sinvie.emergency_live.tuikit_live.live.modules.liveroom.model.SinvieTRTCLiveRoomDelegate;
import com.sinvie.emergency_live.tuikit_live.live.modules.liveroom.ui.widget.SinvieLiveVideoManagerLayout;
import com.sinvie.emergency_live.tuikit_live.live.modules.liveroom.ui.widget.SinvieLiveVideoView;
import com.sinvie.emergency_live.tuikit_live.live.utils.SinviePermissionUtils;
import com.sinvie.emergency_live.tuikit_live.live.utils.SinvieTUILiveLog;
import com.tencent.imsdk.v2.V2TIMManager;

import java.util.List;


/**
 * @param
 * @author JonsonBob
 * @description 自定义观众界面
 * @return
 * @time 2021/7/2 17:35
 */
public class SinvieCustomAudienceFragment extends SinvieBaseFragment implements SinvieTopToolbarLayout.TopToolBarDelegate {
    private static final String TAG = SinvieCustomAudienceFragment.class.getSimpleName();

    private static final long LINK_MIC_INTERVAL = 3 * 1000;    //连麦间隔控制

    private Context mContext;
    private SinvieTopToolbarLayout mLayoutTopToolBar;
    private SinvieBottomBarLayout mLayoutBottomToolBar;
    private SinvieChatLayout mLayoutChatMessage;
    private AlertDialog mDialogError;             // 错误提示的Dialog
    private SinvieTRTCLiveRoom mLiveRoom;                // 组件类
    private ConstraintLayout mRootView;
    private SinvieLiveVideoManagerLayout mLayoutVideoManager;
    private SinvieCircleImageView mButtonLink;
    private ImageView mImagePkLayer;
    private TextView mStatusTipsView;
    private Button mButtonReportUser;         //举报按钮
    private SinvieFloatWindowLayout mLayoutFloatWindow;        //悬浮窗组件

    private boolean isEnterRoom = false;    // 表示当前是否已经进房成功
    private boolean isUseCDNPlay = false;    // 表示当前是否是CDN模式
    private boolean mIsAnchorEnter = false;    // 表示主播是否已经进房
    private boolean mIsBeingLinkMic = false;    // 表示当前是否在连麦状态
    private boolean isFloatRecovery = false;    // 表示是否是从悬浮窗恢复过来的
    private int mCurrentStatus = SinvieTRTCLiveRoomDef.ROOM_STATUS_NONE;
    private int mRoomId;
    private long mLastLinkMicTime;
    private String mAnchorId;
    private String mSelfUserId;
    private String mCdnUrl;

    private SinvieGiftAdapter mGiftAdapter;
    private SinvieGiftInfoDataHandler mGiftInfoDataHandler;

    private Runnable mGetAudienceRunnable;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private Runnable mShowAnchorLeave = new Runnable() {      //如果一定时间内主播没出现
        @Override
        public void run() {
            mLayoutVideoManager.setAnchorOfflineViewVisibility(mIsAnchorEnter ? View.GONE :
                    View.VISIBLE);
            mLayoutBottomToolBar.setVisibility(mIsAnchorEnter ? View.VISIBLE : View.GONE);
            mButtonReportUser.setVisibility(mIsAnchorEnter ? View.VISIBLE : View.GONE);
        }
    };

    private SinvieTRTCLiveRoomDef.LiveAnchorInfo mAnchorInfo = new SinvieTRTCLiveRoomDef.LiveAnchorInfo();
    private SinvieCustomAudienceLayout.CustomLiveRoomAudienceDelegate mLiveRoomAudienceDelegate;

    private SinvieTRTCLiveRoomDelegate mTRTCLiveRoomDelegate = new SinvieTRTCLiveRoomDelegate() {
        @Override
        public void onError(int code, String message) {
            SinvieTUILiveLog.e(TAG, "onError: " + code + " " + message);
            if (mLiveRoomAudienceDelegate != null) {
                mLiveRoomAudienceDelegate.onError(code, message);
            }
        }

        @Override
        public void onWarning(int code, String message) {

        }

        @Override
        public void onDebugLog(String message) {

        }

        @Override
        public void onRoomInfoChange(SinvieTRTCLiveRoomDef.TRTCLiveRoomInfo roomInfo) {
            mCurrentStatus = roomInfo.roomStatus;
            // 由于CDN模式下是只播放一路画面，所以不需要做画面的设置
            if (isUseCDNPlay) {
                if (mCurrentStatus != SinvieTRTCLiveRoomDef.ROOM_STATUS_PK) {
                    mImagePkLayer.setVisibility(View.GONE);
                } else {
                    mImagePkLayer.setVisibility(View.VISIBLE);
                }
                return;
            }
            mLayoutVideoManager.updateRoomStatus(roomInfo.roomStatus);
            SinvieTUILiveLog.d(TAG, "onRoomInfoChange: " + mCurrentStatus);
        }

        @Override
        public void onRoomDestroy(String roomId) {
            if (SinvieFloatWindowLayout.getInstance().mWindowMode == SinvieConstants.WINDOW_MODE_FLOAT) {
                SinvieFloatWindowLayout.getInstance().closeFloatWindow();
                return;
            }
            showErrorAndQuit(0, getString(R.string.sinvie_live_warning_room_disband));
        }

        @Override
        public void onAnchorEnter(final String userId) {
            SinvieDemoLog.d(TAG, "onAnchorEnter userId: " + userId + ", mOwnerId -> " + mAnchorId);
            if (userId.equals(mAnchorId)) {
                // 如果是大主播的画面
                mIsAnchorEnter = true;
                mButtonReportUser.setVisibility(View.VISIBLE);
                mHandler.removeCallbacks(mShowAnchorLeave);
                mLayoutVideoManager.startAnchorVideo(userId, true,
                        (code, msg) -> {
                            if (code != 0) {
                                onAnchorExit(userId);
                            }
                        });
                initBottomToolBar();
                updateFollowView(userId);
            } else {
                mLayoutVideoManager.startAnchorVideo(userId, false, null);
            }
        }

        @Override
        public void onAnchorExit(String userId) {
            if (userId.equals(mAnchorId)) {
                mLayoutVideoManager.stopAnchorVideo(userId, true, null);
                mLayoutBottomToolBar.setVisibility(View.GONE);
                mButtonReportUser.setVisibility(View.GONE);
            } else {
                mLayoutVideoManager.stopAnchorVideo(userId, false, null);
            }
        }

        @Override
        public void onAudienceEnter(SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo) {
            //左下角显示用户加入消息
            SinvieChatEntity entity = new SinvieChatEntity();
            entity.setSenderName(mContext.getString(R.string.sinvie_live_notification));
            if (TextUtils.isEmpty(userInfo.userName)) {
                entity.setContent(mContext.getString(R.string.sinvie_live_user_join_live,
                        userInfo.userId));
            } else {
                entity.setContent(mContext.getString(R.string.sinvie_live_user_join_live,
                        userInfo.userName));
            }
            entity.setType(SinvieConstants.MEMBER_ENTER);
            updateIMMessageList(entity);
            addAudienceListLayout(userInfo);
        }

        @Override
        public void onAudienceExit(SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo) {
            SinvieChatEntity entity = new SinvieChatEntity();
            entity.setSenderName(mContext.getString(R.string.sinvie_live_notification));
            if (TextUtils.isEmpty(userInfo.userName)) {
                entity.setContent(mContext.getString(R.string.sinvie_live_user_quit_live,
                        userInfo.userId));
            } else {
                entity.setContent(mContext.getString(R.string.sinvie_live_user_quit_live,
                        userInfo.userName));
            }
            entity.setType(SinvieConstants.MEMBER_EXIT);
            updateIMMessageList(entity);
            removeAudienceListLayout(userInfo);
        }

        @Override
        public void onRequestJoinAnchor(SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo, String reason) {

        }

        @Override
        public void onAudienceRequestJoinAnchorTimeout(String userId) {

        }

        @Override
        public void onAudienceCancelRequestJoinAnchor(String userId) {

        }

        @Override
        public void onKickoutJoinAnchor() {
            stopLinkMic();
        }

        @Override
        public void onRequestRoomPK(SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo) {

        }

        @Override
        public void onAnchorCancelRequestRoomPK(String userId) {

        }

        @Override
        public void onAnchorRequestRoomPKTimeout(String userId) {

        }

        @Override
        public void onQuitRoomPK() {

        }

        @Override
        public void onRecvRoomTextMsg(String message, SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo) {
            handleTextMsg(userInfo, message);
        }

        @Override
        public void onRecvRoomCustomMsg(String cmd, String message,
                                        SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo) {
            int type = Integer.valueOf(cmd);
            switch (type) {
                case SinvieConstants.IMCMD_PRAISE:
                    handlePraiseMsg(userInfo);
                    break;
                case SinvieConstants.IMCMD_DANMU:
                    handleDanmuMsg(userInfo, message);
                    break;
                case SinvieConstants.IMCMD_GIFT:
                    handleGiftMsg(userInfo, message);
                    break;
                default:
                    break;
            }
        }
    };


    public void setLiveRoomAudienceDelegate(SinvieCustomAudienceLayout.CustomLiveRoomAudienceDelegate liveRoomAudienceDelegate) {
        mLiveRoomAudienceDelegate = liveRoomAudienceDelegate;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_sinvie_custom_live_room_audience, container, false);
        initView(rootView);
        SinviePermissionUtils.checkLivePermission(getActivity());
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (isFloatRecovery) {
            mLiveRoom.exitRoom(new SinvieTRTCLiveRoomCallback.ActionCallback() {
                @Override
                public void onCallback(int code, String msg) {
                    enterRoom();
                }
            });
        } else {
            if (isEnterRoom) {
                enterRoom();
            }
            enterRoom();
        }
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacks(mShowAnchorLeave);
        mHandler.removeCallbacks(mGetAudienceRunnable);
    }

    private void initView(View rootView) {
        // 请求恢复全屏模式
        updateLiveWindowMode(SinvieConstants.WINDOW_MODE_FULL);

        mLayoutTopToolBar = rootView.findViewById(R.id.custom_layout_top_toolbar);
        mLayoutBottomToolBar = rootView.findViewById(R.id.custom_layout_bottom_toolbar);
        mLayoutChatMessage = rootView.findViewById(R.id.custom_layout_chat);
        mRootView = rootView.findViewById(R.id.custom_root);
        mImagePkLayer = rootView.findViewById(R.id.custom_iv_pk_layer);
        mStatusTipsView = rootView.findViewById(R.id.custom_state_tips);
        mLayoutTopToolBar.setTopToolBarDelegate(this);
        mButtonReportUser = rootView.findViewById(R.id.custom_report_user);
        mLayoutVideoManager = rootView.findViewById(R.id.custom_ll_video_view);
        mLayoutVideoManager.updateVideoLayoutByWindowStatus();
        mLayoutVideoManager.setVideoManagerLayoutDelegate(new SinvieLiveVideoManagerLayout.VideoManagerLayoutDelegate() {
            @Override
            public void onClose() {
                if (mLiveRoomAudienceDelegate != null) {
                    mLiveRoomAudienceDelegate.onClose();
                }
            }
        });
        mButtonReportUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reportUser();
            }
        });
    }

    private void initData(Context context) {
        mContext = context;
        mSelfUserId = V2TIMManager.getInstance().getLoginUser();
        mLiveRoom = SinvieTRTCLiveRoom.sharedInstance(context);
        mLiveRoom.setDelegate(mTRTCLiveRoomDelegate);
        mGiftAdapter = new SinvieDefaultGiftAdapterImp();
        mGiftInfoDataHandler = new SinvieGiftInfoDataHandler();
        mGiftInfoDataHandler.setGiftAdapter(mGiftAdapter);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mRoomId = bundle.getInt(SinvieConstants.ROOM_ID);
            isUseCDNPlay = bundle.getBoolean(SinvieConstants.USE_CDN);
            mCdnUrl = bundle.getString(SinvieConstants.CDN_URL);
            mAnchorId = bundle.getString(SinvieConstants.ANCHOR_ID);
        }
    }

    private void enterRoom() {
        if (isEnterRoom) {
            return;
        }
        mLiveRoom.enterRoom(mRoomId, isUseCDNPlay, mCdnUrl,
                new SinvieTRTCLiveRoomCallback.ActionCallback() {
                    @Override
                    public void onCallback(int code, String msg) {
                        if (code == 0) {
                            isEnterRoom = true;
                            updateTopToolBar();
                        } else {
                            exitRoom();
                        }
                    }
                });
        mHandler.postDelayed(mShowAnchorLeave, 3000);
    }

    private void exitRoom() {
        if (isEnterRoom && mLiveRoom != null) {
            mLiveRoom.exitRoom(null);
            isEnterRoom = false;
        }
    }

    private void initBottomToolBar() {
        mLayoutBottomToolBar.setVisibility(View.VISIBLE);
        mLayoutBottomToolBar.setOnTextSendListener(new SinvieInputTextMsgDialog.OnTextSendDelegate() {
            @Override
            public void onTextSend(String msg, boolean tanmuOpen) {
                SinvieChatEntity entity = new SinvieChatEntity();
                entity.setSenderName(mContext.getString(R.string.sinvie_live_message_me));
                entity.setContent(msg);
                entity.setType(SinvieConstants.TEXT_TYPE);
                updateIMMessageList(entity);

                if (tanmuOpen) {

                    mLiveRoom.sendRoomCustomMsg(String.valueOf(SinvieConstants.IMCMD_DANMU), msg,
                            new SinvieTRTCLiveRoomCallback.ActionCallback() {
                                @Override
                                public void onCallback(int code, String msg) {

                                }
                            });
                } else {
                    mLiveRoom.sendRoomTextMsg(msg, new SinvieTRTCLiveRoomCallback.ActionCallback() {
                        @Override
                        public void onCallback(int code, String msg) {
                            if (code != 0) {
                                Toast.makeText(SinvieTUIKitLive.getAppContext(),
                                        R.string.sinvie_live_message_send_fail, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });

    }

    private void startLinkMic() {
        mButtonLink.setEnabled(false);
        mButtonLink.setImageResource(R.drawable.ic_sinvie_live_linkmic_off);

        mStatusTipsView.setText(R.string.sinvie_live_wait_anchor_accept);
        mStatusTipsView.setVisibility(View.VISIBLE);
        mLiveRoom.requestJoinAnchor(getString(R.string.sinvie_live_request_link_mic_anchor, mSelfUserId), REQUEST_LINK_MIC_TIME_OUT, (code, msg) -> {
            if (getActivity() == null) {
                SinvieTUILiveLog.d(TAG, "getActivity is null");
                return;
            }

            mStatusTipsView.setText("");
            mStatusTipsView.setVisibility(View.GONE);
            if (code == 0) {
                joinPusher();
                return;
            }
            if (code == -1) {
                // 拒绝请求
                Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
            } else if (code == -2) {
                // 主播超时未响应
                Toast.makeText(getActivity(), getString(R.string.sinvie_live_request_time_out),
                        Toast.LENGTH_SHORT).show();
            } else {
                //出现错误
                Toast.makeText(getActivity(), getString(R.string.sinvie_live_error_request_link_mic,
                        msg), Toast.LENGTH_SHORT).show();
            }
            mButtonLink.setEnabled(true);
            //                hideNoticeToast();
            mIsBeingLinkMic = false;
            mButtonLink.setImageResource(R.drawable.ic_sinvie_live_linkmic_on);
        });
    }

    private void joinPusher() {
        SinvieLiveVideoView videoView = mLayoutVideoManager.applyVideoView(mSelfUserId);
        if (videoView == null) {
            Toast.makeText(getActivity(), R.string.sinvie_live_anchor_view_error, Toast.LENGTH_SHORT).show();
            return;
        }
        SinvieBeautyParams beautyParams = new SinvieBeautyParams();
        mLiveRoom.getBeautyManager().setBeautyStyle(beautyParams.mBeautyStyle);
        mLiveRoom.getBeautyManager().setBeautyLevel(beautyParams.mBeautyLevel);
        mLiveRoom.getBeautyManager().setWhitenessLevel(beautyParams.mWhiteLevel);
        mLiveRoom.getBeautyManager().setRuddyLevel(beautyParams.mRuddyLevel);
        mLiveRoom.startCameraPreview(true, videoView.getPlayerVideo(),
                new SinvieTRTCLiveRoomCallback.ActionCallback() {
                    @Override
                    public void onCallback(int code, String msg) {
                        if (code == 0) {
                            mLiveRoom.startPublish("", new SinvieTRTCLiveRoomCallback.ActionCallback() {
                                @Override
                                public void onCallback(int code, String msg) {
                                    if (getActivity() == null) {
                                        SinvieTUILiveLog.d(TAG, "getActivity is null");
                                        return;
                                    }

                                    if (code == 0) {
                                        mButtonLink.setEnabled(true);
                                        mIsBeingLinkMic = true;
                                    } else {
                                        stopLinkMic();
                                        mButtonLink.setEnabled(true);
                                        mButtonLink.setImageResource(R.drawable.ic_sinvie_live_linkmic_on);
                                        Toast.makeText(getActivity(),
                                                getString(R.string.sinvie_live_fail_link_mic, msg),
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    }
                });
    }

    private void stopLinkMic() {
        mIsBeingLinkMic = false;

        mLiveRoom.stopCameraPreview();
        mLiveRoom.stopPublish(null);
        if (mLayoutVideoManager != null) {
            mLayoutVideoManager.recycleVideoView(mSelfUserId);
        }
    }

    //展示礼物面板
    private void showGiftPanel() {
        ISinvieGiftPanelView giftPanelView = new SinvieGiftPanelViewImp(getContext());
        giftPanelView.init(mGiftInfoDataHandler);
        giftPanelView.setGiftPanelDelegate(new SinvieGiftPanelDelegate() {
            @Override
            public void onGiftItemClick(SinvieGiftInfo giftInfo) {
                sendGift(giftInfo);
            }

            @Override
            public void onChargeClick() {

            }
        });
        giftPanelView.show();
    }

    //发送礼物消息出去同时展示礼物动画和弹幕
    private void sendGift(SinvieGiftInfo giftInfo) {
        SinvieGiftInfo giftInfoCopy = giftInfo.copy();
        giftInfoCopy.sendUser = mContext.getString(R.string.sinvie_live_message_me);
        giftInfoCopy.sendUserHeadIcon = SinvieTUIKitLive.getLoginUserInfo().getFaceUrl();
        mLiveRoom.sendRoomCustomMsg(String.valueOf(SinvieConstants.IMCMD_GIFT), giftInfoCopy.giftId,
                new SinvieTRTCLiveRoomCallback.ActionCallback() {
                    @Override
                    public void onCallback(int code, String msg) {
                        if (code != 0) {
                            Toast.makeText(SinvieTUIKitLive.getAppContext(), R.string.sinvie_live_message_send_fail,
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void updateTopToolBar() {
        if (!isEnterRoom) {
            return;
        }
        mGetAudienceRunnable = new Runnable() {
            @Override
            public void run() {
                updateTopAnchorInfo();
                updateTopAudienceInfo();
            }
        };
        //为了防止进房后立即获取列表不全，所以增加了一个延迟
        mHandler.postDelayed(mGetAudienceRunnable, 2000);
    }

    private void updateTopAnchorInfo() {
        mLiveRoom.getAnchorList(new SinvieTRTCLiveRoomCallback.UserListCallback() {
            @Override
            public void onCallback(int code, String msg,
                                   List<SinvieTRTCLiveRoomDef.TRTCLiveUserInfo> list) {
                if (code == 0) {
                    for (SinvieTRTCLiveRoomDef.TRTCLiveUserInfo info : list) {
                        if (info.userId.equals(mAnchorId)) {
                            mAnchorInfo.userId = info.userId;
                            mAnchorInfo.userName = info.userName;
                            mAnchorInfo.avatarUrl = info.avatarUrl;
                            mLayoutTopToolBar.setAnchorInfo(mAnchorInfo);
                            mLayoutVideoManager.updateAnchorOfflineViewBackground(mAnchorInfo.avatarUrl);
                        }
                    }
                } else {
                    SinvieDemoLog.e(TAG, "code: " + code + " msg: " + msg + " list size: " + list.size());
                }
            }
        });
    }

    private void updateTopAudienceInfo() {
        mLiveRoom.getAudienceList(new SinvieTRTCLiveRoomCallback.UserListCallback() {
            @Override
            public void onCallback(int code, String msg,
                                   List<SinvieTRTCLiveRoomDef.TRTCLiveUserInfo> list) {
                if (code == 0) {
                    addAudienceListLayout(list);
                }
            }
        });
    }

    private void addAudienceListLayout(List<SinvieTRTCLiveRoomDef.TRTCLiveUserInfo> list) {
        mLayoutTopToolBar.addAudienceListUser(list);
    }

    private void addAudienceListLayout(SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo) {
        mLayoutTopToolBar.addAudienceListUser(userInfo);
    }

    private void removeAudienceListLayout(SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo) {
        mLayoutTopToolBar.removeAudienceUser(userInfo);
    }

    private void updateIMMessageList(SinvieChatEntity entity) {
        mLayoutChatMessage.addMessageToList(entity);
    }

    /**
     * 处理点赞消息
     */
    public void handlePraiseMsg(SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo) {
        SinvieChatEntity entity = new SinvieChatEntity();

        entity.setSenderName(mContext.getString(R.string.sinvie_live_notification));
        if (TextUtils.isEmpty(userInfo.userName)) {
            entity.setContent(mContext.getString(R.string.sinvie_live_user_click_like, userInfo.userId));
        } else {
            entity.setContent(mContext.getString(R.string.sinvie_live_user_click_like, userInfo.userName));
        }
        entity.setType(SinvieConstants.MEMBER_ENTER);
        updateIMMessageList(entity);
    }

    /**
     * 处理弹幕消息
     */
    public void handleDanmuMsg(SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo, String text) {
        handleTextMsg(userInfo, text);
    }

    /**
     * 处理礼物弹幕消息
     */
    private void handleGiftMsg(SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo, String giftId) {
        if (mGiftInfoDataHandler != null) {
            SinvieGiftInfo giftInfo = mGiftInfoDataHandler.getGiftInfo(giftId);
            if (giftInfo != null) {
                if (userInfo != null) {
                    giftInfo.sendUserHeadIcon = userInfo.avatarUrl;
                    if (!TextUtils.isEmpty(userInfo.userName)) {
                        giftInfo.sendUser = userInfo.userName;
                    } else {
                        giftInfo.sendUser = userInfo.userId;
                    }
                }
            }
        }
    }

    /**
     * 处理文本消息
     */
    public void handleTextMsg(SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo, String text) {
        SinvieChatEntity entity = new SinvieChatEntity();
        if (TextUtils.isEmpty(userInfo.userName)) {
            entity.setSenderName(userInfo.userId);
        } else {
            entity.setSenderName(userInfo.userName);
        }
        entity.setContent(text);
        entity.setType(SinvieConstants.TEXT_TYPE);
        updateIMMessageList(entity);
    }

    @Override
    public void onBackPressed() {
        updateLiveWindowMode(SinvieConstants.WINDOW_MODE_FLOAT);
    }

    protected void updateLiveWindowMode(int mode) {
        if (SinvieFloatWindowLayout.getInstance().mWindowMode == mode) return;

        if (mode == SinvieConstants.WINDOW_MODE_FLOAT) {
            SinvieFloatWindowLayout.IntentParams intentParams =
                    new SinvieFloatWindowLayout.IntentParams(getActivity().getClass(), mRoomId,
                            mAnchorId, isUseCDNPlay, mCdnUrl);
            boolean result =
                    SinvieFloatWindowLayout.getInstance().showFloatWindow(mLayoutVideoManager.mLayoutRoot, intentParams);
            if (!result) {
                exitRoom();
                return;
            }
            SinvieFloatWindowLayout.getInstance().mWindowMode = mode;
            mLayoutVideoManager.updateVideoLayoutByWindowStatus();
            SinvieFloatWindowLayout.getInstance().setFloatWindowLayoutDelegate(new SinvieFloatWindowLayout.FloatWindowLayoutDelegate() {
                @Override
                public void onClose() {
                    exitRoom();
                    if (mLiveRoomAudienceDelegate != null) {
                        mLiveRoomAudienceDelegate.onClose();
                    }

                }
            });
        } else if (mode == SinvieConstants.WINDOW_MODE_FULL) {
            SinvieFloatWindowLayout.getInstance().closeFloatWindow();
            SinvieFloatWindowLayout.getInstance().mWindowMode = mode;
            isFloatRecovery = true;
        }
    }


    /**
     * 显示错误并且退出直播
     *
     * @param errorCode
     * @param errorMsg
     */
    protected void showErrorAndQuit(int errorCode, String errorMsg) {
        if (mDialogError == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext(),
                    R.style.SinvieTUILiveDialogTheme)
                    .setTitle(R.string.sinvie_live_error)
                    .setMessage(errorMsg)
                    .setNegativeButton(R.string.sinvie_live_get_it, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mDialogError.dismiss();
                            exitRoom();
                            if (mLiveRoomAudienceDelegate != null) {
                                mLiveRoomAudienceDelegate.onClose();
                            }
                        }
                    });

            mDialogError = builder.create();
        }
        if (mDialogError.isShowing()) {
            mDialogError.dismiss();
        }
        mDialogError.show();
    }

    private void reportUser() {
        final SinvieReportController reportController = new SinvieReportController();
        String[] reportItems = reportController.getReportItems();
        SinvieActionSheetDialog actionSheetDialog = new SinvieActionSheetDialog(mContext);
        actionSheetDialog.builder();
        actionSheetDialog.setCancelable(false);
        int itemColor = mContext.getResources().getColor(R.color.sinvie_live_action_sheet_blue);
        actionSheetDialog.addSheetItems(reportItems, itemColor,
                new SinvieActionSheetDialog.OnSheetItemClickListener() {
                    @Override
                    public void onClick(int which, String text) {
                        if (mAnchorInfo != null) {
                            String userId = mAnchorInfo.userId;
                            reportController.reportUser(mSelfUserId, userId, text);
                        }
                    }
                });
        actionSheetDialog.show();
    }

    @Override
    public void onClickAnchorAvatar() {

    }

    @Override
    public void onClickFollow(SinvieTRTCLiveRoomDef.LiveAnchorInfo liveAnchorInfo) {
        if (liveAnchorInfo != null) {
            followAnchor(liveAnchorInfo.userId);
        }
    }

    @Override
    public void onClickAudience(SinvieTRTCLiveRoomDef.TRTCLiveUserInfo audienceInfo) {

    }

    @Override
    public void onClickOnlineNum() {

    }

    private void followAnchor(String userId) {
        mLiveRoom.followAnchor(userId, new SinvieTRTCLiveRoomCallback.ActionCallback() {
            @Override
            public void onCallback(int code, String msg) {
                SinvieDemoLog.d(TAG, "followAnchor code:" + code + " msg:" + msg);
                if (code == 0) {
                    mLayoutTopToolBar.setHasFollowed(true);
                    Toast.makeText(SinvieTUIKitLive.getAppContext(),
                            R.string.sinvie_live_follow_anchor_success, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void updateFollowView(String userId) {
        mLiveRoom.checkFollowAnchorState(userId,
                new SinvieTRTCLiveRoomCallback.RoomFollowStateCallback() {
                    @Override
                    public void isFollowed() {
                        mLayoutTopToolBar.setHasFollowed(true);
                    }

                    @Override
                    public void isNotFollowed() {
                        mLayoutTopToolBar.setHasFollowed(false);
                    }

                    @Override
                    public void onFailed(String errorMessage) {
                        mLayoutTopToolBar.setHasFollowed(true);
                    }
                });
    }
}