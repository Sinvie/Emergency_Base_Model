package com.sinvie.emergency_base.ui.scene;

import android.content.Intent;
import android.graphics.Rect;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshLoadMoreListener;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.logic.live_room.SinvieLiveRoomContact;
import com.sinvie.emergency_base.logic.live_room.SinvieLiveRoomPresenter;
import com.sinvie.emergency_base.ui.SinvieIntentConstant;
import com.sinvie.emergency_base.ui.base.SinvieMVPBaseFragment;
import com.sinvie.emergency_base.ui.scene.adapter.SinvieRoomDifficultListAdapter;
import com.sinvie.emergency_base.utils.SinvieActivityUtil;
import com.sinvie.emergency_base.utils.SinvieDemoLog;
import com.sinvie.emergency_base.widget.SinvieCustomTitleBarLayout;
import com.sinvie.emergency_live.tuikit_live.liteav.login.SinvieProfileManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * @param
 * @author JonsonBob
 * @description 直播房间列表 Fragment
 * @return
 * @time 2021/6/28 11:24
 */
public class SinvieLiveRoomFragment extends SinvieMVPBaseFragment<SinvieLiveRoomContact.LiveRoomBaseView, SinvieLiveRoomPresenter> implements SinvieLiveRoomContact.LiveRoomBaseView {
    private static final String TAG = SinvieLiveRoomFragment.class.getSimpleName();

    private SinvieCustomTitleBarLayout mSinvieCustomTitleBarLayout;
    private SmartRefreshLayout mSmartRefreshLayout;
    private RecyclerView mRecyclerView;

    private SinvieRoomDifficultListAdapter mRoomListAdapter;
    private List<SinvieRoomDifficultListAdapter.ScenesRoomInfo> mRoomInfoList = new ArrayList<>();
    /**
     * 上拉加载 下拉刷新
     */
    private final OnRefreshLoadMoreListener mOnRefreshLoadMoreListener =
            new OnRefreshLoadMoreListener() {
                @Override
                public void onLoadMore(@NonNull @NotNull RefreshLayout refreshLayout) {

                }

                @Override
                public void onRefresh(@NonNull @NotNull RefreshLayout refreshLayout) {
                    mRoomInfoList.clear();
                    mPresenter.getScenesRoomList();
                }
            };

    @Override
    public int getLayoutId() {
        return R.layout.fragment_sinvie_live_room;
    }

    @Override
    public void onDataLoad() {
        //初始化界面
        initView();

        //直播间列表
        mPresenter.getScenesRoomList();
    }

    @Override
    protected SinvieLiveRoomContact.LiveRoomBaseView createView() {
        return this;
    }

    @Override
    protected SinvieLiveRoomPresenter createPresenter() {
        return new SinvieLiveRoomPresenter(context);
    }

    private void initView() {
        mSinvieCustomTitleBarLayout = mView.findViewById(R.id.ctl_live_room);
        mSmartRefreshLayout =
                mView.findViewById(R.id.live_scenes_live_room_smart_refresh_layout_list);
        mRecyclerView = mView.findViewById(R.id.live_scenes_live_room_rv_room_list);

        initCustomTitleBarView();

        initRecycleView();
    }

    private void initCustomTitleBarView() {
        //创建直播按钮
        mSinvieCustomTitleBarLayout.setOnRightClickListener(v -> createRoom());
        //填充标题
        mSinvieCustomTitleBarLayout.getMiddleTitle().setText(context.getString(R.string.sinvie_live_scenes_live_room));
    }

    /**
     * 初始化 列表信息
     */
    private void initRecycleView() {
        mSmartRefreshLayout.setOnRefreshLoadMoreListener(mOnRefreshLoadMoreListener);
        //判断是否开启上拉加载
        mSmartRefreshLayout.setEnableLoadMore(mRoomInfoList.size() >= SinvieUIConstants.PAGE_SIZE);
        mRoomListAdapter = new SinvieRoomDifficultListAdapter(context, 0, mRoomInfoList,
                (position, roomInfo) -> {
                    //这里可以写判断是否上锁的接口 判断密码的接口
                    String selfUserId = SinvieProfileManager.getInstance().getUserModel().userId;
                    if (roomInfo.anchorId.equals(selfUserId)) {
                        //如果是主播自己点击自己的房间 重新创建房间!
                        createRoom();
                    } else {
                        enterRoom(roomInfo);
                    }
                });

        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.setAdapter(mRoomListAdapter);
        mRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view,
                                       @NonNull RecyclerView parent,
                                       @NonNull RecyclerView.State state) {
                int space = getResources().getDimensionPixelOffset(R.dimen.sinvie_page_margin);
                outRect.top = space;
                int childLayoutPosition = parent.getChildLayoutPosition(view);
                if (childLayoutPosition % 2 == 0) {
                    outRect.left = space;
                    outRect.right = space / 2;
                } else {
                    outRect.left = space / 2;
                    outRect.right = space;
                }
            }
        });
        mRoomListAdapter.notifyDataSetChanged();
    }

    /**
     * 获取直播房间数据
     */
    @Override
    public void getScenesRoomListSuccess(List<SinvieRoomDifficultListAdapter.ScenesRoomInfo> scenesRoomInfos) {
        mRoomInfoList.addAll(scenesRoomInfos);
        //TODO 添加假的数据
        mRoomInfoList.add(new SinvieRoomDifficultListAdapter.ScenesRoomInfo());

        mSmartRefreshLayout.finishRefresh();
        mSmartRefreshLayout.finishLoadMore();
        SinvieDemoLog.d(TAG, "mRoomInfoList.Size() +" + mRoomInfoList.size());
        refreshView();
    }

    @Override
    public void getScenesRoomListFail(int errCode, String errMsg) {
        SinvieDemoLog.d(TAG, "onFailed: code -> " + errCode + ", msg -> " + errMsg);
        Toast.makeText(context, getString(R.string.sinvie_online_fail) + errMsg,
                Toast.LENGTH_SHORT).show();
        mSmartRefreshLayout.finishRefresh();
        mSmartRefreshLayout.finishLoadMore();
        refreshView();
    }

    /**
     * 刷新列表
     */
    private void refreshView() {
        //判断是否开启上拉加载
        mSmartRefreshLayout.setEnableLoadMore(mRoomInfoList.size() >= SinvieUIConstants.PAGE_SIZE);
        mRoomListAdapter.notifyDataSetChanged();
    }

    /**
     * 跳转直播房间
     */
    private void createRoom() {
        SinvieLiveRoomAnchorActivity.start(context, "");
    }

    /**
     * 跳转 观看房间
     *
     * @param info
     */
    private void enterRoom(SinvieRoomDifficultListAdapter.ScenesRoomInfo info) {
        Intent intent = new Intent(SinvieIntentConstant.LIVE_ROOM_AUDIENCE_ACTIVITY_ACTION);
        intent.putExtra(SinvieUIConstants.ROOM_TITLE, info.roomName);
        intent.putExtra(SinvieUIConstants.GROUP_ID, Integer.valueOf(info.roomId));
        intent.putExtra(SinvieUIConstants.USE_CDN_PLAY, false);
        intent.putExtra(SinvieUIConstants.ANCHOR_ID, info.anchorId);
        intent.putExtra(SinvieUIConstants.PUSHER_NAME, info.anchorName);
        intent.putExtra(SinvieUIConstants.COVER_PIC, info.coverUrl);
        intent.putExtra(SinvieUIConstants.PUSHER_AVATAR, info.coverUrl);
        SinvieActivityUtil.startActivity(context, intent);
    }


}