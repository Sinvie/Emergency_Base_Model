package com.sinvie.emergency_base.ui.report.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieAddResourceBean;
import com.sinvie.emergency_base.http.SinvieNetContact;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/14
 * @Des: 类描述
 */
public class SinvieAddResourceGvAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = SinvieReportItemGvAdapter.class.getSimpleName();

    private static final int VIEW_TYPE_ADD_RES = 0;
    private static final int VIEW_TYPE_RES_SHOW = 1;

    private static final int MAX_RES_SIZE = 4;

    private Context mContext;
    private List<SinvieAddResourceBean> mReportBeanList;

    private onReportGvItemClickListener mItemClickListener;

    private boolean mIsAddResource = true;

    public SinvieAddResourceGvAdapter(Context context,
                                      List<SinvieAddResourceBean> reportBeanList) {
        mContext = context;
        mReportBeanList = reportBeanList;
    }

    public void setItemClickListener(onReportGvItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    public void setAddResource(boolean addResource) {
        mIsAddResource = addResource;
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == VIEW_TYPE_RES_SHOW) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_sinvie_add_report_select_pic, parent, false);
            return new GridViewHolder(view);
        } else if (viewType == VIEW_TYPE_ADD_RES) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_sinvie_add_report_add_res, parent, false);
            return new AddResViewHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        if (position == 0 && mReportBeanList.size() < MAX_RES_SIZE) {
            ((AddResViewHolder) holder).bind();
        } else {
            if (mReportBeanList.size() < MAX_RES_SIZE) {
                ((GridViewHolder) holder).bind(mReportBeanList.get(position - 1));
            } else {
                Log.d(TAG, "onBindViewHolder: " + mReportBeanList.size() + " : " + position);
                ((GridViewHolder) holder).bind(mReportBeanList.get(position));
            }
        }
    }


    @Override
    public int getItemCount() {
        if (mReportBeanList.size() < MAX_RES_SIZE) {
            return mReportBeanList.size() + 1;
        } else {
            return mReportBeanList.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 && mReportBeanList.size() < MAX_RES_SIZE) {
            return VIEW_TYPE_ADD_RES;
        }
        return VIEW_TYPE_RES_SHOW;
    }

    public class GridViewHolder extends RecyclerView.ViewHolder {
        public ImageView mIvGv;
        public ImageView mIvDel;

        public GridViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            mIvGv = itemView.findViewById(R.id.iv_select_pic);
            mIvDel = itemView.findViewById(R.id.iv_delete_pic);
        }

        private void bind(SinvieAddResourceBean bean) {
            if (mIsAddResource && bean.isAdd()) {
                mIvGv.setVisibility(View.GONE);
                mIvDel.setVisibility(View.GONE);
            } else {
                mIvGv.setVisibility(View.VISIBLE);
                mIvDel.setVisibility(View.VISIBLE);

                if (bean.getResourcePath() == null) {
                    return;
                }

                if (bean.isNetPath()) {
                    //加载网络图片url
                    Glide.with(mContext).load(SinvieNetContact.BASE_URL + bean.getResourcePath())
                            .placeholder(R.mipmap.icon_sinvie_img_place)
                            .error(R.mipmap.icon_sinvie_img_error)
                            .into(mIvGv);
                } else {
                    //加载本地图片url
                    Glide.with(mContext).load(bean.getResourcePath())
                            .placeholder(R.mipmap.icon_sinvie_img_place)
                            .error(R.mipmap.icon_sinvie_img_error)
                            .into(mIvGv);
                }
            }
            mIvDel.setOnClickListener(v -> mItemClickListener.onGvDeleteItemListener(bean));
        }
    }

    public class AddResViewHolder extends RecyclerView.ViewHolder {

        private ImageView mIvAddRes;
        private ConstraintLayout mConstraintLayout;

        public AddResViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            mIvAddRes = itemView.findViewById(R.id.iv_select_res);
            mConstraintLayout = itemView.findViewById(R.id.cl_select_res);
        }

        private void bind() {
            mIvAddRes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mItemClickListener.onGvDeleteAddItemListener();
                }
            });
        }

    }

    public interface onReportGvItemClickListener {

        void onGvDeleteItemListener(SinvieAddResourceBean path);

        void onGvDeleteAddItemListener();
    }
}