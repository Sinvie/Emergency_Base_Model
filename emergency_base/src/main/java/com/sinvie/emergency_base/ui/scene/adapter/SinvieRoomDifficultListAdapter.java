package com.sinvie.emergency_base.ui.scene.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.ui.scene.adapter.view_holder.SinvieBaseViewHolder;
import com.sinvie.emergency_base.ui.scene.adapter.view_holder.SinvieGridRoomHolder;
import com.sinvie.emergency_base.ui.scene.adapter.view_holder.SinvieGridTitleHolder;
import com.sinvie.emergency_base.ui.scene.adapter.view_holder.SinvieLinearRoomHolder;
import com.sinvie.emergency_base.ui.scene.adapter.view_holder.SinvieLinearTitleHolder;
import com.sinvie.emergency_base.utils.SinvieDemoLog;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/6/28
 * @Des: 直播列表实现复杂布局
 */
public class SinvieRoomDifficultListAdapter extends RecyclerView.Adapter<SinvieBaseViewHolder> {

    private static final String TAG = SinvieRoomDifficultListAdapter.class.getSimpleName();

    /**
     * 判断列表的状态 0: 线性列表 , 1: 网格列表
     */
    private int mLinearType = 0;

    private Context mContext;
    private List<ScenesRoomInfo> mScenesRoomInfos;
    private OnItemClickListener mOnItemClickListener;

    /**
     * 构造方法
     *
     * @param context             上下文对象
     * @param scenesRoomInfos     数据列表
     * @param onItemClickListener item单击事件监听
     */
    public SinvieRoomDifficultListAdapter(Context context, int type, List<ScenesRoomInfo> scenesRoomInfos,
                                          OnItemClickListener onItemClickListener) {
        mContext = context;
        mLinearType = type;
        mScenesRoomInfos = scenesRoomInfos;
        mOnItemClickListener = onItemClickListener;
    }

    @NonNull
    @NotNull
    @Override
    public SinvieBaseViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent,
                                                   int viewType) {
        View view = null;
        SinvieBaseViewHolder viewHolder = null;
        SinvieDemoLog.d(TAG,"LinearType--> " + mLinearType + ", viewType--> "+viewType);
        if (mLinearType == 0) {
            //当前为线性列表
            if (viewType == 0) {
                //当前为线性列表  标题
                view = LayoutInflater.from(mContext).inflate(R.layout.item_sinvie_live_room_linear_title
                        , parent, false);
                viewHolder = new SinvieLinearTitleHolder(view);
            } else {
                //当前为线性列表 直播间
                view = LayoutInflater.from(mContext).inflate(R.layout.item_sinvie_live_room_linear_room,
                        parent, false);
                viewHolder = new SinvieLinearRoomHolder(view);
            }
        } else {
            //当前为网格布局
            if (viewType == 0) {
                //当前为网格列表 标题
                view = LayoutInflater.from(mContext).inflate(R.layout.item_sinvie_live_room_grid_title,
                        parent, false);
                viewHolder = new SinvieGridTitleHolder(view);
            } else {
                //当前为网格列表 直播间
                view = LayoutInflater.from(mContext).inflate(R.layout.item_sinvie_live_room_grid_room,
                        parent, false);
                viewHolder = new SinvieGridRoomHolder(view);
            }
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull SinvieBaseViewHolder holder, int position) {
        holder.bind(mContext, mScenesRoomInfos.get(position), mOnItemClickListener);
    }

    @Override
    public int getItemCount() {
        return mScenesRoomInfos.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mScenesRoomInfos.get(position).type;
    }

    public interface OnItemClickListener {
        void onItemClick(int position, SinvieRoomDifficultListAdapter.ScenesRoomInfo roomInfo);
    }

    public static class ScenesRoomInfo {
        /**
         * 房间名
         */
        public String roomName;
        /**
         * 房间id
         */
        public String roomId;
        /**
         * 封面图片
         */
        public String coverUrl;
        /**
         * 会员数(观看人数)
         */
        public int memberCount;
        /**
         * 主播名字
         */
        public String anchorName;
        /**
         * 主播id
         */
        public String anchorId;
        /**
         * 用来区分标题和直播间 0: 标题,1:直播间
         */
        public int type = 1;
        /**
         * 用来区分是现场还是任务 0:现场 , 1:任务
         */
        public int roomType = 0;
        /**
         * 直播间是否设置密码
         */
        public boolean isLock = false;
    }
}
