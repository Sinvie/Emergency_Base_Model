package com.sinvie.emergency_base.ui.scene;

import android.app.Application;
import android.content.Intent;
import android.text.TextUtils;

import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.sinvie.emergency_base.http.common.SinvieCommonNetUtils;
import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieDataBean;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.helper.SinvieIBaseLiveListener;
import com.sinvie.emergency_base.helper.SinvieTUIKitLiveListenerManager;
import com.sinvie.emergency_base.http.SinvieRetrofitFactory;
import com.sinvie.emergency_base.http.api.SinvieBaseApiService;
import com.sinvie.emergency_base.http.base.SinvieBaseObserver;
import com.sinvie.emergency_live.tuikit.modules.chat.base.SinvieOfflineMessageBean;
import com.sinvie.emergency_live.tuikit.utils.SinvieToastUtil;
import com.sinvie.emergency_live.tuikit_live.liteav.login.SinvieProfileManager;
import com.sinvie.emergency_live.tuikit_live.liteav.model.SinvieCallModel;
import com.sinvie.emergency_live.tuikit_live.liteav.model.SinvieLiveMessageInfo;
import com.sinvie.emergency_live.tuikit_live.liteav.model.SinvieTRTCAVCallImpl;
import com.sinvie.emergency_live.tuikit_live.live.SinvieTUIKitLive;
import com.sinvie.emergency_live.tuikit_live.live.helper.SinvieTUILiveChatController;
import com.sinvie.emergency_live.tuikit_live.live.modules.liveroom.ui.SinvieDefaultGroupLiveAnchorActivity;
import com.sinvie.emergency_base.ui.main.SinvieMainActivity;
import com.sinvie.emergency_base.utils.SinvieActivityUtil;
import com.sinvie.emergency_base.utils.SinvieConstants;
import com.sinvie.emergency_base.utils.SinvieDemoLog;
import com.sinvie.emergency_base.ui.SinvieIntentConstant;
import com.tencent.imsdk.v2.V2TIMCallback;
import com.tencent.imsdk.v2.V2TIMManager;
import com.tencent.imsdk.v2.V2TIMMessage;
import com.tencent.imsdk.v2.V2TIMSignalingInfo;
import com.tencent.rtmp.TXLiveBase;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class SinvieSceneManager {
    private static final String TAG = SinvieSceneManager.class.getSimpleName();

    public static void init(Application application, String licenseUrl, String licenseKey) {
        if (application != null && licenseUrl != null && licenseKey != null) {
            TXLiveBase.getInstance().setLicence(application, licenseUrl, licenseKey);
        }

        SinvieTUIKitLiveListenerManager.getInstance().registerCallListener(new SceneLiveControllerSinvie());
        SinvieTUILiveChatController.setGroupLiveHandler(new SinvieTUILiveChatController.GroupLiveHandler() {
            @Override
            public boolean startGroupLive(String groupId) {
//                LiveRoomAnchorActivity.start(MyApplication.getInstance(), groupId);
                SinvieDefaultGroupLiveAnchorActivity.start(SinvieLocContext.getContext(), groupId);
                // demo层对消息进行处理，不走默认的逻辑
                return true;
            }
        });

        // 设置自定义的消息渲染时的回调
        SinvieTUILiveChatController.setLiveGroupMessageClickListener((info, groupId) -> {
            String selfUserId = SinvieProfileManager.getInstance().getUserModel().userId;
            if (String.valueOf(info.anchorId).equals(selfUserId)) {
                createRoom(groupId);
            } else {
                checkRoomExist(info);
            }
            return true;
        });
    }

    static class SceneLiveControllerSinvie implements SinvieIBaseLiveListener {

        @Override
        public void handleOfflinePushCall(Intent intent) {
            if (intent == null) {
                return;
            }
            final SinvieCallModel model = (SinvieCallModel) intent.getSerializableExtra(SinvieConstants.CALL_MODEL);
            if (model != null) {
                if (TextUtils.isEmpty(model.groupId)) {
                    SinvieDemoLog.e(TAG, "AVCall groupId is empty");
                } else {
                    ((SinvieTRTCAVCallImpl) (SinvieTRTCAVCallImpl.sharedInstance(SinvieLocContext.getContext()))).
                            processInvite(model.callId, model.sender, model.groupId,
                                    model.invitedList, model.data);
                }
            }
        }

        @Override
        public void handleOfflinePushCall(SinvieOfflineMessageBean bean) {
            if (bean == null || bean.content == null) {
                return;
            }
            final SinvieCallModel model = new Gson().fromJson(bean.content, SinvieCallModel.class);
            SinvieDemoLog.i(TAG, "bean: " + bean + " model: " + model);
            if (model != null) {
                long timeout = V2TIMManager.getInstance().getServerTime() - bean.sendTime;
                if (timeout >= model.timeout) {
                    SinvieToastUtil.toastLongMessage(SinvieLocContext.getContext().getString(R.string.sinvie_call_time_out));
                } else {
                    String callId = model.callId;
                    if (TextUtils.isEmpty(model.groupId)) {
                        callId =
                                ((SinvieTRTCAVCallImpl) SinvieTRTCAVCallImpl.sharedInstance(SinvieLocContext.getContext())).c2cCallId;
                    }
                    ((SinvieTRTCAVCallImpl) (SinvieTRTCAVCallImpl.sharedInstance(SinvieLocContext.getContext()))).
                            processInvite(callId, bean.sender, model.groupId, model.invitedList,
                                    bean.content);
                }
            }
        }

        @Override
        public void redirectCall(SinvieOfflineMessageBean bean) {
            if (bean == null || bean.content == null) {
                return;
            }
            final SinvieCallModel model = new Gson().fromJson(bean.content, SinvieCallModel.class);
            SinvieDemoLog.i(TAG, "bean: " + bean + " model: " + model);
            if (model != null) {
                model.sender = bean.sender;
                model.data = bean.content;
                long timeout = V2TIMManager.getInstance().getServerTime() - bean.sendTime;
                if (timeout >= model.timeout) {
                    SinvieToastUtil.toastLongMessage(SinvieLocContext.getContext().getString(R.string.sinvie_call_time_out));
                } else {
                    if (TextUtils.isEmpty(model.groupId)) {
                        Intent mainIntent = new Intent(SinvieLocContext.getContext(),
                                SinvieMainActivity.class);
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        SinvieLocContext.getContext().startActivity(mainIntent);
                    } else {
                        V2TIMSignalingInfo info = new V2TIMSignalingInfo();
                        info.setInviteID(model.callId);
                        info.setInviteeList(model.invitedList);
                        info.setGroupID(model.groupId);
                        info.setInviter(bean.sender);
                        V2TIMManager.getSignalingManager().addInvitedSignaling(info,
                                new V2TIMCallback() {

                                    @Override
                                    public void onError(int code, String desc) {
                                        SinvieDemoLog.e(TAG,
                                                "addInvitedSignaling code: " + code + " desc: " + desc);
                                    }

                                    @Override
                                    public void onSuccess() {
                                        Intent mainIntent =
                                                new Intent(SinvieIntentConstant.MAIN_ACTIVITY_ACTION);
                                        mainIntent.putExtra(SinvieConstants.CALL_MODEL, model);
                                        mainIntent.putExtra(SinvieConstants.IS_OFFLINE_PUSH_JUMP, true);
                                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        SinvieActivityUtil.startActivity(SinvieLocContext.getContext(),
                                                mainIntent);
                                    }
                                });
                    }
                }
            }
        }

        @Override
        public Fragment getSceneFragment() {
            return new SinvieSceneFragment();
        }

        @Override
        public void refreshUserInfo() {
            SinvieTUIKitLive.refreshLoginUserInfo(null);
        }

        @Override
        public boolean isDialingMessage(V2TIMMessage message) {
            SinvieCallModel callModel = SinvieCallModel.convert2VideoCallData(message);
            boolean isDialing = false;
            if (callModel != null && callModel.action == SinvieCallModel.VIDEO_CALL_ACTION_DIALING) {
                isDialing = true;
            }
            return isDialing;
        }

        @Override
        public Intent putCallExtra(Intent intent, String key, V2TIMMessage message) {
            SinvieCallModel callModel = SinvieCallModel.convert2VideoCallData(message);
            if (callModel != null && intent != null) {
                intent.putExtra(key, callModel);
            }
            return intent;
        }
    }

    private static void checkRoomExist(final SinvieLiveMessageInfo info) {
        SinvieCommonNetUtils.execute(SinvieCommonNetUtils.covert(new SinvieRetrofitFactory().create(SinvieBaseApiService.class).getRoomList()), new SinvieBaseObserver<List<SinvieDataBean>>() {
            @Override
            public void onSuccess(@Nullable List<SinvieDataBean> sinvieDataBeans) {
                //roomid 对比 如果 有 就进去 如果没有 就onFailure
                if (sinvieDataBeans == null) {
                    SinvieToastUtil.toastShortMessage(SinvieTUIKitLive.getAppContext().getString(R.string.sinvie_live_is_over));
                    return;
                }
                for (SinvieDataBean sinvieDataBean : sinvieDataBeans) {
                    if ((info.roomId + "").equals(sinvieDataBean.getRoomId())) {
                        enterRoom(info);
                        return;
                    }
                }
                SinvieToastUtil.toastShortMessage(SinvieTUIKitLive.getAppContext().getString(R.string.sinvie_live_is_over));
            }

            @Override
            public void onFailure(int errCode, @NotNull String errMsg) {
                SinvieToastUtil.toastShortMessage(SinvieTUIKitLive.getAppContext().getString(R.string.sinvie_live_is_over));
            }
                });
    }

    private static void createRoom(String groupId) {
        SinvieLiveRoomAnchorActivity.start(SinvieLocContext.getContext(), groupId);
//        SinvieDefaultGroupLiveAnchorActivity.start(LocContext.getContext(),groupId);
    }

    private static void enterRoom(SinvieLiveMessageInfo info) {
        Intent intent = new Intent(SinvieIntentConstant.LIVE_ROOM_AUDIENCE_ACTIVITY_ACTION);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(SinvieUIConstants.ROOM_TITLE, info.roomName);
        intent.putExtra(SinvieUIConstants.GROUP_ID, info.roomId);
        intent.putExtra(SinvieUIConstants.USE_CDN_PLAY, false);
        intent.putExtra(SinvieUIConstants.ANCHOR_ID, info.anchorId);
        intent.putExtra(SinvieUIConstants.PUSHER_NAME, info.anchorName);
        intent.putExtra(SinvieUIConstants.COVER_PIC, info.roomCover);
        intent.putExtra(SinvieUIConstants.PUSHER_AVATAR, info.roomCover);
        SinvieActivityUtil.startActivity(SinvieLocContext.getContext(), intent);
    }

}
