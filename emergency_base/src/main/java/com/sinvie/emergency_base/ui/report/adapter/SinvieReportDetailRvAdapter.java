package com.sinvie.emergency_base.ui.report.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieReportBean;
import com.sinvie.emergency_base.bean.SinvieReportDetailBean;
import com.sinvie.emergency_base.utils.SinvieChangeBeanUtil;
import com.sinvie.emergency_base.widget.SinvieCustomGridView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/12
 * @Des: 类描述
 */
public class SinvieReportDetailRvAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int FIRST_VIEW_HOLDER = 0;
    private static final int SECOND_VIEW_HOLDER = 1;
    private static final int THIRD_VIEW_HOLDER = 2;

    private Context mContext;
    private List<SinvieReportDetailBean.ReportSupplementsRoomListBean> mDetailBeanList;
    private onReportDetailItemClickListener mItemClickListener;
    private SinvieReportBean.DataRoomListBean mInfoBean;

    public SinvieReportDetailRvAdapter(Context context, List<SinvieReportDetailBean.ReportSupplementsRoomListBean> detailBeanList ) {
        mContext = context;
        mDetailBeanList = detailBeanList;
    }

    public void setFirstInfoBean(SinvieReportBean.DataRoomListBean infoBean) {
        mInfoBean = infoBean;
    }

    public void setOnReportDetailItemClickListener(onReportDetailItemClickListener onReportDetailItemClickListener) {
        mItemClickListener = onReportDetailItemClickListener;
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == FIRST_VIEW_HOLDER) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_sinvie_report_detail_first, parent, false);
            return new ReportDetailFirstViewHolder(view);
        } else if (viewType == SECOND_VIEW_HOLDER) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_sinvie_report_detail_second, parent, false);
            return new ReportDetailSecondViewHolder(view);
        } else {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_sinvie_report_detail_third, parent, false);
            return new ReportDetailThirdViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        if (position == 0) {
            ReportDetailFirstViewHolder firstViewHolder = (ReportDetailFirstViewHolder) holder;
            firstViewHolder.bind(mInfoBean);
        } else if (position == 1) {
            ReportDetailSecondViewHolder secondViewHolder = (ReportDetailSecondViewHolder) holder;
            secondViewHolder.bind(mDetailBeanList.size());
        } else {
            ReportDetailThirdViewHolder thirdViewHolder = ((ReportDetailThirdViewHolder) holder);
            thirdViewHolder.bind(position, mDetailBeanList.get(position - 2));
        }

    }

    @Override
    public int getItemCount() {
        //算上 额外的 viewHolder
        return mDetailBeanList.size() + 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return FIRST_VIEW_HOLDER;
        } else if (position == 1) {
            return SECOND_VIEW_HOLDER;
        } else {
            return THIRD_VIEW_HOLDER;
        }

    }

    public class ReportDetailFirstViewHolder extends RecyclerView.ViewHolder {
        private TextView mTvReportDetailDate;
        private TextView mTvReportDetailEdit;
        private TextView mTvReportDetailTitle;
        private TextView mTvReportDetailType;
        private TextView mTvReportDetailAddress;
        private TextView mTvReportDetailDes;
        private SinvieCustomGridView mGvReportDetail;

        public ReportDetailFirstViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            mTvReportDetailDate = itemView.findViewById(R.id.tv_report_detail_first_date);
            mTvReportDetailEdit = itemView.findViewById(R.id.tv_report_detail_first_edit);
            mTvReportDetailTitle = itemView.findViewById(R.id.tv_report_detail_first_title);
            mTvReportDetailType = itemView.findViewById(R.id.tv_report_detail_first_type);
            mTvReportDetailAddress = itemView.findViewById(R.id.tv_report_detail_first_address);
            mTvReportDetailDes = itemView.findViewById(R.id.tv_report_detail_first_des);
            mGvReportDetail = itemView.findViewById(R.id.gv_report_detail_first);

        }

        public void bind(SinvieReportBean.DataRoomListBean mInfoBean) {
            //-----------填充数据------------------
            mTvReportDetailDate.setText(mInfoBean.getReportTime());
            mTvReportDetailTitle.setText(mInfoBean.getTitle());
            mTvReportDetailType.setText(mInfoBean.getCaseClassName());
            mTvReportDetailAddress.setText(mInfoBean.getAddress());
            mTvReportDetailDes.setText(mInfoBean.getContent());

            SinvieReportItemGvAdapter itemGvAdapter = new SinvieReportItemGvAdapter(mContext, SinvieChangeBeanUtil.getGridImgBeanList(mInfoBean.getAppAttachments()));

            //还要设置 item 点击监听 (查看大图, 播放视频)
            itemGvAdapter.setItemClickListener((gridType, url) -> {
                mItemClickListener.onDetailItemPicListener(gridType,url);
            });

            mGvReportDetail.setAdapter(itemGvAdapter);

            mTvReportDetailEdit.setOnClickListener(v -> {
                //跳转修改界面
                mItemClickListener.onDetailFirstItemEditListener();
            });
        }
    }

    public class ReportDetailSecondViewHolder extends RecyclerView.ViewHolder {
        private TextView mTvReportDetailDataSize;

        public ReportDetailSecondViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            mTvReportDetailDataSize = itemView.findViewById(R.id.tv_report_detail_data_size);
        }

        public void bind(int size) {
            String history =
                    String.format(mContext.getString(R.string.sinvie_report_detail_data_history), size);
            mTvReportDetailDataSize.setText(history);
        }
    }

    public class ReportDetailThirdViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvReportDetailFirstDate;
        private TextView mTvReportDetailFirstDel;
        private TextView mTvReportDetailThirdEdit;
        private TextView mTvReportDetailThirdDes;
        private SinvieCustomGridView mGvReportDetail;
        private TextView mTvReportDetailThirdAddress;

        public ReportDetailThirdViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            mTvReportDetailFirstDate = itemView.findViewById(R.id.tv_report_detail_third_date);
            mTvReportDetailFirstDel = itemView.findViewById(R.id.tv_report_detail_third_del);
            mTvReportDetailThirdEdit = itemView.findViewById(R.id.tv_report_detail_third_edit);
            mTvReportDetailThirdDes = itemView.findViewById(R.id.tv_report_detail_third_des);
            mTvReportDetailThirdAddress =
                    itemView.findViewById(R.id.tv_report_detail_third_address);
            mGvReportDetail = itemView.findViewById(R.id.gv_report_detail);


        }

        public void bind(int position, SinvieReportDetailBean.ReportSupplementsRoomListBean bean) {
            mTvReportDetailFirstDate.setText(bean.getReportTime());
            mTvReportDetailThirdDes.setText(bean.getContent());
            mTvReportDetailThirdAddress.setText(bean.getAddress());

            mTvReportDetailFirstDel.setOnClickListener(v ->
                    mItemClickListener.onDetailThirdItemDelListener(bean.getId()));
            mTvReportDetailThirdEdit.setOnClickListener(v ->
                    mItemClickListener.onDetailThirdItemEditListener(position, bean));

            SinvieReportItemGvAdapter itemGvAdapter = new SinvieReportItemGvAdapter(mContext,
                    SinvieChangeBeanUtil.getGridImgBeanList(bean.getAppAttachments()));
            //还要设置 item 点击监听 (查看大图, 播放视频)
            itemGvAdapter.setItemClickListener((gridType, url) ->
                    mItemClickListener.onDetailItemPicListener(gridType, url));
            mGvReportDetail.setAdapter(itemGvAdapter);
        }

    }


    public interface onReportDetailItemClickListener {

        void onDetailFirstItemEditListener();

        void onDetailThirdItemDelListener(int id);

        void onDetailThirdItemEditListener(int position, SinvieReportDetailBean.ReportSupplementsRoomListBean thirdBean);

        void onDetailItemPicListener(int gridType, String url);
    }
}
