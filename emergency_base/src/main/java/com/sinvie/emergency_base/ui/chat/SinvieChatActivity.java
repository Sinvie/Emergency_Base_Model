package com.sinvie.emergency_base.ui.chat;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.sinvie.emergency_base.R;

/**
 * @description 聊天主界面
 * @param 
 * @return 
 * @author JonsonBob
 * @time 2021/6/25 13:55
 */
public class SinvieChatActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sinvie_chat);
    }
}