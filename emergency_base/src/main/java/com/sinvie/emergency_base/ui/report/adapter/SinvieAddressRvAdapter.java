package com.sinvie.emergency_base.ui.report.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieReportAddressBean;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/16
 * @Des: 类描述
 */
public class SinvieAddressRvAdapter extends RecyclerView.Adapter<SinvieAddressRvAdapter.AddressViewHolder> {

    private Context mContext;
    private List<SinvieReportAddressBean> mAddressBeanList;
    private AddressItemClickListener mItemClickListener;
    private int mSelectPosition = -1;

    public SinvieAddressRvAdapter(Context context, List<SinvieReportAddressBean> addressBeanList) {
        mContext = context;
        mAddressBeanList = addressBeanList;
    }

    public void setItemClickListener(AddressItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    public void resetAdapterClick( ) {
        mSelectPosition = -1;
    }

    @NonNull
    @NotNull
    @Override
    public AddressViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_sinvie_report_select_address,
                parent, false);
        return new AddressViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AddressViewHolder holder, int position) {
        holder.bind(mAddressBeanList.get(position));
        //默认所有选中按钮都不出现
        holder.setItemSelectNormal(mContext,position);
        //防止整体刷新的时候选中的状态改变
        if (mSelectPosition == position && position != 0) {
            holder.setItemSelected(mContext,position);
        }
        //设置item的选中的状态
        holder.itemView.setOnClickListener(v -> {
            if (mSelectPosition == position) {
                return;
            }
            //改变原来选中的item
            changeItemSelected(mSelectPosition, false);
            //设置选中的position
            mSelectPosition = position;
            //改变现在选中的item
            changeItemSelected(mSelectPosition, true);
            //传递给activity
            mItemClickListener.onAddressItemClickListener(mAddressBeanList.get(position));
        });

    }

    /**
     * 设置item选中未选中的状态
     *
     * @param position
     * @param isChange
     */
    private void changeItemSelected(int position, boolean isChange) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("change", isChange);
        //更新adapter
        notifyItemChanged(position, bundle);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AddressViewHolder holder, int position, @NonNull @NotNull List<Object> payloads) {
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position);
        } else {
            Bundle bundle = (Bundle) payloads.get(0);
            if (bundle.getBoolean("change")) {
                //设置选中状态
                holder.setItemSelected(mContext,position);
            } else {
                //设置未选中状态
                holder.setItemSelectNormal(mContext,position);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mAddressBeanList.size();
    }

    public static class AddressViewHolder extends RecyclerView.ViewHolder {

        private final TextView mTvItemLocationName;
        private final TextView mTvItemAddress;
        private final TextView mTvItemCurrentLocation;
        private final ImageView mIvItemAddressSelect;

        public AddressViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            mTvItemLocationName = itemView.findViewById(R.id.tv_item_location_name);
            mTvItemAddress = itemView.findViewById(R.id.tv_item_address);
            mTvItemCurrentLocation = itemView.findViewById(R.id.tv_item_current_location);
            mIvItemAddressSelect = itemView.findViewById(R.id.iv_item_address_selected);
        }

        public void bind(SinvieReportAddressBean bean) {
            mTvItemAddress.setText(bean.getAddress());
            mTvItemLocationName.setText(bean.getLocation());
            mTvItemCurrentLocation.setVisibility(bean.isCurrentLocation() ? View.VISIBLE : View.GONE);
        }

        public void setItemSelected(Context context,int position) {
            //如果是当前定位地址 隐藏提示
            if (position == 0) {
                mTvItemCurrentLocation.setVisibility(View.GONE);
            }
            mIvItemAddressSelect.setVisibility(View.VISIBLE);
            mTvItemAddress.setTextColor(ContextCompat.getColor(context, R.color.sinvie_theme_blue_color));
            mTvItemLocationName.setTextColor(ContextCompat.getColor(context, R.color.sinvie_theme_blue_color));
        }

        private void setItemSelectNormal(Context context,int position) {
            //如果是当前定位地址 显示提示
            if (position == 0) {
                mTvItemCurrentLocation.setVisibility(View.VISIBLE);
            }
            //默认所有的地址都隐藏选中状态
            mIvItemAddressSelect.setVisibility(View.GONE);
            mTvItemLocationName.setTextColor(ContextCompat.getColor(context, R.color.sinvie_theme_text_black_color));
            mTvItemAddress.setTextColor(ContextCompat.getColor(context, R.color.sinvie_theme_text_enable_color));
        }
    }

    public interface AddressItemClickListener {
        void onAddressItemClickListener(SinvieReportAddressBean bean);
    }
}
