package com.sinvie.emergency_base.ui.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.sinvie.emergency_live.tuikit_live.live.base.SinvieBaseFragment;

import org.jetbrains.annotations.NotNull;

public abstract class SinvieAbsFragment extends Fragment {
    protected View mView;
    protected Context context;

    private static final int PERMISSION_REQUEST_CODE = 100;

    private SinvieBaseFragment.OnPermissionGrandCallback mOnPermissionCallback;

    public void onBackPressed() {
    }

    public void forward(Fragment fragment, boolean hide) {
        forward(getId(), fragment, null, hide);
    }

    public void forward(int viewId, Fragment fragment, String name, boolean hide) {
        if (getFragmentManager() == null){
            return;
        }
        FragmentTransaction trans = getFragmentManager().beginTransaction();
        if (hide) {
            trans.hide(this);
            trans.add(viewId, fragment);
        } else {
            trans.replace(viewId, fragment);
        }

        trans.addToBackStack(name);
        trans.commitAllowingStateLoss();
    }

    public void backward() {
        if (getFragmentManager() == null){
            return;
        }
        getFragmentManager().popBackStack();
    }

    public abstract int getLayoutId();

    @Override
    public void onAttach(@NonNull @NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mView != null) {
            return mView;
        }
        return inflater.inflate(getLayoutId(), container, false);
    }

    @Override
    public final void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mView == null) {
            mView = view;
            onViewLoad();
            onDataLoad();
        }
        onViewCreated();
    }

    /**
     * 只在初始化时调用一次
     * before {@link #onDataLoad()}
     */
    public void onViewLoad() {
    }

    /**
     * 只在初始化时调用一次
     * after {@link #onViewLoad()}
     * before {@link #onViewCreated()}
     */
    public void onDataLoad() {
    }

    /**
     * Fragment生命周期Created后调用
     * {@link #onViewDestroyed()}
     */
    public void onViewCreated() {
    }

    @Override
    public final void onDestroyView() {
        super.onDestroyView();

        onViewDestroyed();
        ViewParent vp = mView.getParent();
        if (vp instanceof ViewGroup) {
            ((ViewGroup) vp).removeView(mView);
        }
    }

    /**
     * 视图已经被销毁了
     * {@link #onViewCreated()}
     */
    void onViewDestroyed() {
    }
}
