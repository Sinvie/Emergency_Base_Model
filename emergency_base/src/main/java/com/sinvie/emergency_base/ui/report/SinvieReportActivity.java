package com.sinvie.emergency_base.ui.report;

import android.content.Intent;
import android.view.KeyEvent;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshLoadMoreListener;
import com.sinvie.emergency_conf.CommonService.util.SinvieLogUtil;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieReportBean;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.logic.report.SinvieReportContact;
import com.sinvie.emergency_base.logic.report.SinvieReportPresenter;
import com.sinvie.emergency_live.tuikit.base.ISinvieTitleBarLayout;
import com.sinvie.emergency_live.tuikit.utils.SinvieToastUtil;
import com.sinvie.emergency_base.ui.SinvieIntentConstant;
import com.sinvie.emergency_base.ui.base.SinvieMVPBaseActivity;
import com.sinvie.emergency_base.ui.report.adapter.SinvieReportRvAdapter;
import com.sinvie.emergency_base.utils.SinvieActivityUtil;
import com.sinvie.emergency_base.widget.SinvieCustomTitleBarLayout;
import com.xiasuhuei321.loadingdialog.view.LoadingDialog;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cc.shinichi.library.ImagePreview;

/**
 * @param
 * @author JonsonBob
 * @description 事件上报界面
 * @return
 * @time 2021/7/11 18:34
 */
public class SinvieReportActivity extends SinvieMVPBaseActivity<SinvieReportContact.ReportBaseView,
        SinvieReportPresenter> implements SinvieReportContact.ReportBaseView {

    private static final String TAG = SinvieReportActivity.class.getSimpleName();

    private SinvieCustomTitleBarLayout mReportTitle;
    private EditText mEtSearchReport;
    private SmartRefreshLayout mSmartReport;
    private RecyclerView mRvReport;

    private List<SinvieReportBean.DataRoomListBean> mReportBeanList;
    private SinvieReportRvAdapter mSinvieReportRvAdapter;

    private int mPageIndex = 1; //从第一页开始

    private static int mSelectPosition = -1;

    private LoadingDialog mLoadingDialog;

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SinvieUIConstants.ADD_NEW_REPORT_REQUEST:
                //新增上报返回
                if (resultCode == SinvieUIConstants.ADD_NEW_REPORT_RESULT) {
                    //确定新增上报 刷新上报列表
                    if (mPresenter != null) {
                        mPageIndex = 1;
                        mPresenter.getReportData(mPageIndex, null);
                    }
                }
                break;
            case SinvieUIConstants.REPORT_DETAIL_REQUEST:
                if (resultCode == SinvieUIConstants.REPORT_DETAIL_DEL_RESULT) {
                    //已经删除了某个事报
                    if (mReportBeanList != null && mSinvieReportRvAdapter != null && data != null) {
                        SinvieReportBean.DataRoomListBean bean = data.getParcelableExtra(SinvieUIConstants.REPORT_INFO_BEAN);
                        //使用迭代器删除 然后刷新adapter
                        Iterator<SinvieReportBean.DataRoomListBean> iterator = mReportBeanList.listIterator();
                        while (iterator.hasNext()) {
                            SinvieReportBean.DataRoomListBean listBean = iterator.next();
                            if (listBean.getId() == bean.getId()) {
                                iterator.remove();
                            }
                        }
                        mSinvieReportRvAdapter.notifyDataSetChanged();
                    } else {
                        mPageIndex = 0;
                        mPresenter.getReportData(mPageIndex, null);
                    }
                } else if (resultCode == SinvieUIConstants.REPORT_DETAIL_EDIT_RESULT) {
                    //已经修改了某个事报
                    if (mReportBeanList != null && mSinvieReportRvAdapter != null && data != null) {
                        SinvieReportBean.DataRoomListBean bean = data.getParcelableExtra(SinvieUIConstants.REPORT_INFO_BEAN);
                        //替换掉相等的bean类
                        mReportBeanList.set(mSelectPosition, bean);
                        mSinvieReportRvAdapter.notifyDataSetChanged();
                    }

                }
                break;
        }
    }

    @Override
    public void initializeData() {
    }

    @Override
    public void initializeComposition() {
        setContentView(R.layout.activity_sinvie_report);
        mReportTitle = findViewById(R.id.report_title);
        mEtSearchReport = findViewById(R.id.et_search);
        mSmartReport = findViewById(R.id.smart_report);
        mRvReport = findViewById(R.id.rv_report);
        mEtSearchReport.setHint(getString(R.string.sinvie_report_search_hint));

        mReportBeanList = new ArrayList<>();
        mSinvieReportRvAdapter = new SinvieReportRvAdapter(this, mReportBeanList);
        mSinvieReportRvAdapter.setOnReportRvItemClickListener(mOnReportRvItemClickListener);
        mRvReport.setLayoutManager(new LinearLayoutManager(this));
        mRvReport.setAdapter(mSinvieReportRvAdapter);
        mSmartReport.finishLoadMore();

        mReportTitle.setRightIcon(R.mipmap.icon_sinvie_add_report);
        mReportTitle.setTitle(getString(R.string.sinvie_workbench_report), ISinvieTitleBarLayout.POSITION.MIDDLE);
        mReportTitle.setLeftIcon(R.drawable.svg_sinvie_theme_back);

        //监听输入法回车搜索
        mEtSearchReport.setOnKeyListener((v, keyCode, event) -> {

            //是否是回车键
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN) {
                //隐藏软键盘
                hideSoftKeyBorder();
                //显示等待弹窗
                showLoginDialog();
                //搜索
                mPageIndex = 1;
                mPresenter.getReportData(mPageIndex, mEtSearchReport.getText().toString().trim());
            }
            return false;
        });
        mReportTitle.setOnRightClickListener(v -> {
            //增加新的 事件上报
            Intent intent = new Intent(SinvieIntentConstant.ADD_NEW_REPORT_ACTIVITY_ACTION);
            intent.putExtra(SinvieUIConstants.REPORT_INTENT_TYPE, SinvieUIConstants.REPORT_TYPE_ADD);
            SinvieActivityUtil.startActivityForResult(SinvieReportActivity.this, intent,
                    SinvieUIConstants.ADD_NEW_REPORT_REQUEST);
        });

        mSmartReport.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull @NotNull RefreshLayout refreshLayout) {
                mPageIndex += 1;
                mPresenter.getReportData(mPageIndex, null);
            }

            @Override
            public void onRefresh(@NonNull @NotNull RefreshLayout refreshLayout) {
                mPageIndex = 1;
                mPresenter.getReportData(mPageIndex, null);
            }
        });
        mReportTitle.setOnLeftClickListener(v -> finish());

        mSmartReport.autoRefresh();
    }

    private SinvieReportRvAdapter.OnReportRvItemClickListener mOnReportRvItemClickListener = new SinvieReportRvAdapter.OnReportRvItemClickListener() {
        @Override
        public void onReportRvItemClickListener(int position) {
            //记录当前选择的 position
            mSelectPosition = position;
            SinvieReportBean.DataRoomListBean bean = mReportBeanList.get(position);
            //跳转到详情界面 (携带 信息)
            Intent intent = new Intent(SinvieIntentConstant.REPORT_DETAIL_ACTIVITY_ACTION);
            intent.putExtra(SinvieUIConstants.REPORT_INFO_BEAN, bean);
            SinvieActivityUtil.startActivityForResult(SinvieReportActivity.this, intent, SinvieUIConstants.REPORT_DETAIL_REQUEST);
        }

        @Override
        public void onReportRvItemClickListener(int gridType, String url) {
            SinvieLogUtil.d(TAG, gridType + " : " + url);
            //视屏播放 查看大图界面
            if (gridType == 0) {
                //视频
                Intent intent = new Intent(SinvieIntentConstant.VIDEO_PLAY_ACTIVITY_ACTION);
                intent.putExtra(SinvieUIConstants.PLAY_VIDEO_URL, url);
                SinvieActivityUtil.startActivity(SinvieReportActivity.this, intent);
            } else {
                //图片
                ImagePreview.getInstance().setContext(SinvieReportActivity.this).setIndex(0).setImage(url).start();
            }
        }
    };

    @Override
    protected SinvieReportContact.ReportBaseView createView() {
        return this;
    }

    @Override
    protected SinvieReportPresenter createPresenter() {
        return new SinvieReportPresenter(this);
    }

    @Override
    public void getReportDataSuccess(SinvieReportBean beans) {
        mSmartReport.finishRefresh();
        mSmartReport.finishLoadMore();
        //判断能否上拉加载
        mSmartReport.setEnableLoadMore(beans.getData().size() >= SinvieUIConstants.PAGE_SIZE);

        if (mPageIndex == 1) {
            mReportBeanList.clear();
        }
        mReportBeanList.addAll(beans.getData());
        mSinvieReportRvAdapter.notifyDataSetChanged();
    }

    public void showLoginDialog() {
        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog(this);
        }
        mLoadingDialog.setLoadingText(getString(R.string.sinvie_logining_msg))
                .setInterceptBack(true)

                .setLoadSpeed(LoadingDialog.Speed.SPEED_TWO)
                .setRepeatCount(-1)
                .show();
    }

    @Override
    public void getReportDataFail(int errCode, String errMsg) {
        SinvieToastUtil.toastLongErrorMessage(errCode, errMsg);
        mSmartReport.finishRefresh();
        mSmartReport.finishLoadMore();
    }

    @Override
    public void dismissLoadingDialog() {
        if (mLoadingDialog != null) {
            mLoadingDialog.close();
        }
    }
}