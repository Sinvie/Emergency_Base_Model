package com.sinvie.emergency_base.ui.workbench;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.WorkbenchBean;
import com.sinvie.emergency_base.ui.SinvieIntentConstant;
import com.sinvie.emergency_base.ui.base.SinvieAbsFragment;
import com.sinvie.emergency_base.ui.login.SinvieUserInfo;
import com.sinvie.emergency_base.ui.scene.SinvieLiveRoomAnchorActivity;
import com.sinvie.emergency_base.ui.workbench.adapter.SinvieWorkbenchRvAdapter;
import com.sinvie.emergency_base.utils.SinvieActivityUtil;
import com.sinvie.emergency_base.widget.SinvieWorkbenchItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * @param
 * @author JonsonBob
 * @description 工作台 fragment
 * @return
 * @time 2021/7/6 14:16
 */
public class SinvieWorkbenchFragment extends SinvieAbsFragment {

    private LinearLayoutCompat mLlWorkbenchProclamation;
    private RecyclerView mRvWorkbenchContent;
    private SinvieWorkbenchRvAdapter mSinvieWorkbenchRvAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_sinvie_workbanch;
    }

    @Override
    public void onDataLoad() {
        mLlWorkbenchProclamation = mView.findViewById(R.id.ll_workbench_proclamation);
        mRvWorkbenchContent = mView.findViewById(R.id.rv_workbench_content);

        mSinvieWorkbenchRvAdapter = new SinvieWorkbenchRvAdapter(getGildListData(), context);

        mRvWorkbenchContent.setLayoutManager(new GridLayoutManager(context, 2));
        mRvWorkbenchContent.addItemDecoration(new SinvieWorkbenchItemDecoration(context, R.drawable.bg_sinvie_item_decoration));
        mSinvieWorkbenchRvAdapter.setWorkbenchItemClick((bean) -> {
            SinvieActivityUtil.startActivity(context, bean.getActivityAction());
        });
        mRvWorkbenchContent.setAdapter(mSinvieWorkbenchRvAdapter);

    }

    private List<WorkbenchBean> getGildListData() {
        List<WorkbenchBean> beans = new ArrayList<>();
        //事件上报
        beans.add(new WorkbenchBean(
                1,
                R.string.sinvie_workbench_report,
                R.mipmap.icon_sinvie_report,
                true,
                SinvieIntentConstant.REPORT_ACTIVITY_ACTION,
                getString(R.string.sinvie_view_empty_tips)));
        //视频会议
        beans.add(new WorkbenchBean(
                2,
                R.string.sinvie_workbench_video_conference,
                R.mipmap.icon_sinvie_video_conference,
                SinvieUserInfo.getInstance().isLoginHuaWeiConf(),
                SinvieIntentConstant.CONFERENCE_ACTIVITY_ACTION,
                getString(R.string.sinvie_view_empty_tips)));
        //视频直播
        beans.add(new WorkbenchBean(
                3
                , R.string.sinvie_workbench_live_stream
                , R.mipmap.icon_sinvie_live_stream
                ,true
                , SinvieIntentConstant.LIVE_ROOM_ANCHOR_ACTIVITY_ACTION
                , getString(R.string.sinvie_view_empty_tips)));

        //应急响应
        beans.add(new WorkbenchBean(
                4
                , R.string.sinvie_workbench_emergency_response
                , R.mipmap.icon_sinvie_emergency_response
                ,false
                , SinvieIntentConstant.RESPONSE_ACTIVITY_ACTION
                , getString(R.string.sinvie_view_empty_tips)));

        //协作标会 SinvieIntentConstant.COOPERATE_ACTIVITY_ACTION
        beans.add(new WorkbenchBean(
                5
                , R.string.sinvie_workbench_coordination
                , R.mipmap.icon_sinvie_coordination
                ,false
                , SinvieIntentConstant.COOPERATE_ACTIVITY_ACTION
                , getString(R.string.sinvie_view_empty_tips)));

        //电子地图 IntentConstant.ELECTRONIC_MAP_ACTIVITY_ACTION
        beans.add(new WorkbenchBean(
                6
                , R.string.sinvie_workbench_map
                , R.mipmap.icon_sinvie_map
                ,false
                , SinvieIntentConstant.ELECTRONIC_MAP_ACTIVITY_ACTION
                , getString(R.string.sinvie_view_empty_tips)));

        //通知公告 IntentConstant.ANNOUNCE_ACTIVITY_ACTION
        beans.add(new WorkbenchBean(
                7
                , R.string.sinvie_workbench_notification
                , R.mipmap.icon_sinvie_notifaction
                ,false
                , SinvieIntentConstant.ANNOUNCE_ACTIVITY_ACTION
                , getString(R.string.sinvie_view_empty_tips)));

        //事报管理 IntentConstant.REPORT_MANAGER_ACTIVITY_ACTION
        beans.add(new WorkbenchBean(
                8
                , R.string.sinvie_workbench_report_manager
                , R.mipmap.icon_sinvie_report_manager
                ,false
                , SinvieIntentConstant.REPORT_MANAGER_ACTIVITY_ACTION
                , getString(R.string.sinvie_view_empty_tips)));
        return beans;
    }
}