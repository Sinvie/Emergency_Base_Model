package com.sinvie.emergency_base.ui.call;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;

import androidx.annotation.NonNull;

import com.huawei.ecterminalsdk.base.TsdkMobileAuidoRoute;
import com.sinvie.emergency_conf.CallService.SinvieCallConstant;
import com.sinvie.emergency_conf.CallService.SinvieCallMgr;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.utils.SinvieDialogUtil;

import org.jetbrains.annotations.NotNull;

import java.lang.ref.WeakReference;

public class SinvieCallOutActivitySinvie extends SinvieBaseMediaActivity {
    private AlertDialog mDialog;
    private MyHandler mHandler  ;
    private class MyHandler extends  Handler{
        private WeakReference<SinvieCallOutActivitySinvie> mWeakReference;

        public MyHandler(@NonNull @NotNull Looper looper,SinvieCallOutActivitySinvie activity) {
            super(looper);
            mWeakReference = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if (KeyEvent.KEYCODE_BACK == msg.what) {
                mWeakReference.get().mDialog = SinvieDialogUtil.generateDialog(SinvieCallOutActivitySinvie.this, R.string.sinvie_ntf_end_call,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                SinvieCallMgr.getInstance().endCall(mWeakReference.get().mCallID);
                            }
                        });
                mWeakReference.get().mDialog.show();
            }
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if (R.id.reject_btn == v.getId()) {
            SinvieCallMgr.getInstance().endCall(mCallID);
            finish();
        }
    }

    @Override
    public void initializeComposition() {
        super.initializeComposition();
        mHandler = new MyHandler(Looper.myLooper(),this);
        mRejectBtn.setOnClickListener(this);

        mCallNumberTv.setText(null == mCallNumber ? "" : mCallNumber);
        mCallNameTv.setText(null == mCallNumber ? "" : mCallNumber);

        int currentAudioRoute = SinvieCallMgr.getInstance().getCurrentAudioRoute();
        if (mIsVideoCall) {
            if (currentAudioRoute != SinvieCallConstant.TYPE_LOUD_SPEAKER) {
                SinvieCallMgr.getInstance().setAudioRoute(TsdkMobileAuidoRoute.TSDK_E_MOBILE_AUDIO_ROUTE_LOUDSPEAKER);
            }
            mSpeakerButton.setActivated(true);
            return;
        }

        if (currentAudioRoute == SinvieCallConstant.TYPE_LOUD_SPEAKER) {
            SinvieCallMgr.getInstance().setAudioRoute(TsdkMobileAuidoRoute.TSDK_E_MOBILE_AUDIO_ROUTE_DEFAULT);
        }
        mSpeakerButton.setActivated(false);
    }

    @Override
    public void onBackPressed() {
        mHandler.sendEmptyMessage(KeyEvent.KEYCODE_BACK);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dismissDialog(mDialog);
        mHandler.removeCallbacksAndMessages(null);
    }
}
