package com.sinvie.emergency_base.ui.report;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.LocationSource;
import com.amap.api.maps.MapView;
import com.amap.api.maps.UiSettings;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.model.MyLocationStyle;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.core.PoiItem;
import com.amap.api.services.poisearch.PoiResult;
import com.amap.api.services.poisearch.PoiSearch;
import com.google.gson.Gson;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshLoadMoreListener;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieReportAddressBean;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.ui.report.adapter.SinvieAddressRvAdapter;
import com.sinvie.emergency_base.utils.SinvieDemoLog;
import com.sinvie.emergency_base.widget.SinvieCustomTitleBarLayout;
import com.sinvie.emergency_conf.CommonService.util.SinvieLogUtil;
import com.sinvie.emergency_live.tuikit.base.ISinvieTitleBarLayout;
import com.xiasuhuei321.loadingdialog.view.LoadingDialog;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * @param
 * @author JonsonBob
 * @description 事发地点选择
 * @return
 * @time 2021/8/2 19:52
 */
public class SinvieSelectAddressActivity extends AppCompatActivity implements PoiSearch.OnPoiSearchListener {
    private static final String TAG = SinvieSelectAddressActivity.class.getSimpleName();

    //显示地图需要的变量
    private MapView mapView;//地图控件
    private AMap aMap;//地图对象

    //定位需要的声明
    private AMapLocationClient mLocationClient = null;//定位发起端
    private AMapLocationClientOption mLocationOption = null;//定位参数
    private LocationSource.OnLocationChangedListener mListener = null;//定位监听器

    //标识，用于判断是否只显示一次定位信息和用户重新定位
    private boolean isFirstLoc = true;

    //地图定位点的信息
    private String mCity;
    //需要返回的信息
    private String mAddress = "";
    private double mLongitude = 0;
    private double mLatitude = 0;

    //中心点标记
    private MarkerOptions mMarkerOptions;
    private Marker mCenterMarker;

    private SinvieAddressRvAdapter mAddressAdapter;
    private SinvieCustomTitleBarLayout mTitleBar;
    private SmartRefreshLayout mSmartRefreshLayout;
    private RecyclerView mRecyclerView;
    private EditText mSearchEt;

    /**
     * 地点列表
     */
    private List<SinvieReportAddressBean> mBeanList;
    /**
     * 地图 marker 列表
     */
    private List<Marker> mMarkerList;
    /**
     * 地图列表 页数
     */
    private int mPageIndex = 0;

    /**
     * 当前搜索模式 (拖动搜索不显示 marker) true: 输入搜索 , false 拖动搜索
     */
    private boolean mIsInputSearch = false;

    /**
     * 是否启用拖动搜索 (为了防止地图跳转到item选中的地址 造成拖动搜索)
     */
    private boolean mIsUseCameraChangeSearch = true;

    private LoadingDialog mLoadingDialog;
    /**
     * item点击 跳转到对应的地点
     */
    private SinvieAddressRvAdapter.AddressItemClickListener mItemClickListener = new SinvieAddressRvAdapter.AddressItemClickListener() {
        @Override
        public void onAddressItemClickListener(SinvieReportAddressBean bean) {
            // address = 地址 + 地址名字
            mAddress = bean.getAddress() + bean.getLocation();
            mLatitude = bean.getLatitude();
            mLongitude = bean.getLongitude();
            //此时暂停拖动搜索
            mIsUseCameraChangeSearch = false;
            //选择地址 重新定位
            aMap.moveCamera(CameraUpdateFactory.changeLatLng(new LatLng(bean.getLatitude(), bean.getLongitude())));
        }
    };
    /**
     * 定位监听
     */
    private LocationSource mLocationSource = new LocationSource() {

        /**
         * 激活定位
         *
         * @param onLocationChangedListener 定位监听
         */
        @Override
        public void activate(OnLocationChangedListener onLocationChangedListener) {
            mListener = onLocationChangedListener;
        }

        /**
         * 停止定位
         */
        @Override
        public void deactivate() {
            mListener = null;
        }
    };
    /**
     * 定位回调函数
     */
    private AMapLocationListener mAMapLocationListener = new AMapLocationListener() {
        @Override
        public void onLocationChanged(AMapLocation amapLocation) {
            if (amapLocation != null) {
                if (amapLocation.getErrorCode() == 0) {
                    //定位成功回调信息，设置相关消息
                    amapLocation.getLocationType();//获取当前定位结果来源，如网络定位结果，详见官方定位类型表
                    amapLocation.getLatitude();//获取纬度
                    amapLocation.getLongitude();//获取经度
                    amapLocation.getAccuracy();//获取精度信息
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = new Date(amapLocation.getTime());
                    df.format(date);//定位时间
                    //地址，如果option中设置isNeedAddress为false
                    // ，则没有此结果，网络定位结果中会有地址信息，GPS定位不返回地址信息。
                    amapLocation.getAddress();
                    amapLocation.getCountry();//国家信息
                    amapLocation.getProvince();//省信息
                    amapLocation.getCity();//城市信息
                    amapLocation.getDistrict();//城区信息
                    amapLocation.getStreet();//街道信息
                    amapLocation.getStreetNum();//街道门牌号信息
                    amapLocation.getCityCode();//城市编码
                    amapLocation.getAdCode();//地区编码

                    // 如果不设置标志位，此时再拖动地图时，它会不断将地图移动到当前的位置
                    // 需要设置第一次定位的信息 以及定位点
                    if (isFirstLoc) {
                        //设置缩放级别
                        aMap.moveCamera(CameraUpdateFactory.zoomTo(17));
                        //点击定位按钮 能够将地图的中心移动到定位点
                        mListener.onLocationChanged(amapLocation);
                        //将地图移动到定位点
                        aMap.moveCamera(CameraUpdateFactory.changeLatLng(new LatLng(amapLocation.getLatitude(), amapLocation.getLongitude())));
                        //添加图钉
//                    aMap.addMarker(getMarkerOptions(amapLocation));
                        //获取定位信息 返回时携带信息
                        mCity = amapLocation.getCity();
                        mAddress = mCity
                                + "" + amapLocation.getProvince()
                                + "" + amapLocation.getDistrict()
                                + "" + amapLocation.getStreet()
                                + "" + amapLocation.getStreetNum();
                        mLongitude = amapLocation.getLongitude();
                        mLatitude = amapLocation.getLatitude();

                        mBeanList.add(new SinvieReportAddressBean().setReportAddressBean("当前位置", mAddress,
                                mLongitude, mLatitude, true));
                        mAddressAdapter.notifyDataSetChanged();

                        isFirstLoc = false;
                    }
                } else {
                    //显示错误信息ErrCode是错误码，errInfo是错误信息，详见错误码表。
                    SinvieLogUtil.d("AmapError", "ErrCode:" + amapLocation.getErrorCode() + ", errInfo:" + amapLocation.getErrorInfo());
                    //取消报错提示
//                SinvieToastUtil.toastLongErrorMessage(amapLocation.getErrorCode(), amapLocation.getErrorInfo());
                }
            }
        }

    };
    /**
     * 地图拖动监听
     */
    private AMap.OnCameraChangeListener mOnCameraChangeListener = new AMap.OnCameraChangeListener() {
        @Override
        public void onCameraChange(CameraPosition cameraPosition) {
            SinvieLogUtil.d(TAG, ",mIsUseCameraChangeSearch: " + mIsUseCameraChangeSearch);
        }

        @Override
        public void onCameraChangeFinish(CameraPosition cameraPosition) {
            //拖动结束 记录信息 执行搜索
            SinvieLogUtil.d(TAG, ",mIsUseCameraChangeSearch: " + mIsUseCameraChangeSearch);
            if (mIsUseCameraChangeSearch) {
                //清除地图
                clearMap();

                //开始搜索
                startSearchAddress(cameraPosition.target.latitude, cameraPosition.target.longitude);

            } else {

                //此时开启拖动搜索
                mIsUseCameraChangeSearch = true;
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sinvie_select_address);

        mTitleBar = findViewById(R.id.ctl_select_address);
        mTitleBar.setTitle(getString(R.string.sinvie_report_address), ISinvieTitleBarLayout.POSITION.MIDDLE);
        mTitleBar.setTitle(getString(R.string.sinvie_save), ISinvieTitleBarLayout.POSITION.RIGHT);
        mTitleBar.getRightTitle().setTextColor(ContextCompat.getColor(this, R.color.sinvie_theme_blue_color));
        mTitleBar.getRightIcon().setVisibility(View.GONE);
        mTitleBar.setLeftIcon(R.drawable.svg_sinvie_theme_back);
//        mTitleBar.setRightIcon(R.drawable.icon_select);

        mTitleBar.setOnLeftClickListener(v -> finish());
        mTitleBar.setOnRightClickListener(v -> {
            Intent mIntent = new Intent();
            mIntent.putExtra(SinvieUIConstants.ADDRESS_TYPE_ADDRESS, mAddress);
            mIntent.putExtra(SinvieUIConstants.ADDRESS_TYPE_LONGITUDE, mLongitude + "");
            mIntent.putExtra(SinvieUIConstants.ADDRESS_TYPE_LATITUDE, mLatitude + "");
            setResult(SinvieUIConstants.SELECT_ADDRESS_RESULT, mIntent);
            finish();
        });

        initMapView(savedInstanceState);

        initSearchView();
    }

    private void initSearchView() {
        mSmartRefreshLayout = findViewById(R.id.smart_select_address);
        mRecyclerView = findViewById(R.id.rv_select_address);
        mSearchEt = findViewById(R.id.et_search);
        mSearchEt.setHint(R.string.sinvie_select_address_search_input_hint);

        mBeanList = new ArrayList<>();
        mMarkerList = new ArrayList<>();

        mAddressAdapter = new SinvieAddressRvAdapter(this, mBeanList);
        mAddressAdapter.setItemClickListener(mItemClickListener);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAddressAdapter);


        mSmartRefreshLayout.setEnableLoadMore(false);
        mSmartRefreshLayout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull @NotNull RefreshLayout refreshLayout) {
                mPageIndex += 1;
                //地图搜索
                startSearchAddress();
            }

            @Override
            public void onRefresh(@NonNull @NotNull RefreshLayout refreshLayout) {
                mPageIndex = 1;
                //清除地图
                clearMap();
                //开始搜索
                startSearchAddress();
            }
        });

        //监听输入法回车搜索
        mSearchEt.setOnKeyListener((v, keyCode, event) -> {

            //是否是回车键
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN) {
                //隐藏键盘
                ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                        .hideSoftInputFromWindow(SinvieSelectAddressActivity.this.getCurrentFocus()
                                .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                mPageIndex = 1;
                //清除地图
                clearMap();
                //搜索
                startSearchAddress();
            }
            return false;
        });
    }

    private void initMapView(Bundle savedInstanceState) {
        //显示地图
        mapView = (MapView) findViewById(R.id.mv_select_address);
        //必须要写
        mapView.onCreate(savedInstanceState);
        //获取地图对象
        aMap = mapView.getMap();

        //设置显示定位按钮 并且可以点击
        UiSettings settings = aMap.getUiSettings();
        //设置定位监听
        aMap.setLocationSource(mLocationSource);
        // 是否显示定位按钮
        settings.setMyLocationButtonEnabled(true);
        // 是否可触发定位并显示定位层
        aMap.setMyLocationEnabled(true);

        //定位的小图标 默认是蓝点 这里自定义一团火，其实就是一张图片
        MyLocationStyle myLocationStyle = new MyLocationStyle();
        myLocationStyle.myLocationIcon(BitmapDescriptorFactory.fromResource(R.drawable.icon_sinvie_location));
        myLocationStyle.radiusFillColor(android.R.color.transparent);
        myLocationStyle.strokeColor(android.R.color.transparent);
        //跟随屏幕中心点
        myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_FOLLOW);
        aMap.setMyLocationStyle(myLocationStyle);

        //增加地图移动监听
        aMap.setOnCameraChangeListener(mOnCameraChangeListener);

        //------------------------------------------添加中心标记----------------------------
        mMarkerOptions = new MarkerOptions();
        mMarkerOptions.draggable(false);//可拖放性
        mMarkerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.svg_sinvie_select_location));
        mCenterMarker = aMap.addMarker(mMarkerOptions);
        ViewTreeObserver vto = mapView.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mapView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                mCenterMarker.setPositionByPixels(mapView.getWidth() >> 1, mapView.getHeight() >> 1);
                mCenterMarker.showInfoWindow();
            }
        });
        //开始定位
        initLoc();
    }

    //定位
    private void initLoc() {
        //初始化定位
        mLocationClient = new AMapLocationClient(getApplicationContext());
        //设置定位回调监听
        mLocationClient.setLocationListener(mAMapLocationListener);
        //初始化定位参数
        mLocationOption = new AMapLocationClientOption();
        //设置定位模式为高精度模式，Battery_Saving为低功耗模式，Device_Sensors是仅设备模式
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
        //设置是否返回地址信息（默认返回地址信息）
        mLocationOption.setNeedAddress(true);
        //设置是否只定位一次,默认为false
        mLocationOption.setOnceLocation(false);
        //设置是否强制刷新WIFI，默认为强制刷新
        mLocationOption.setWifiScan(true);
        //设置是否允许模拟位置,默认为false，不允许模拟位置
        mLocationOption.setMockEnable(false);
        //设置定位间隔,单位毫秒,默认为2000ms
        mLocationOption.setInterval(2000);
        //给定位客户端对象设置定位参数
        mLocationClient.setLocationOption(mLocationOption);
        //启动定位
        mLocationClient.startLocation();
    }

    /**
     * 搜索地图 关键字搜索
     */
    private void startSearchAddress() {
        mIsInputSearch = true;
        //第一个参数是搜索关键字
        //第二个参数是搜索类别: 餐饮服务/生活服务/商务住宅 等等
        //第三个参数是搜索范围 不填则表示全国
        //地图搜索
        String keyword = mSearchEt.getText().toString().trim();
        PoiSearch.Query mQuery = new PoiSearch.Query(keyword, "", mCity);
        mQuery.setPageNum(mPageIndex);
        mQuery.setPageSize(SinvieUIConstants.PAGE_SIZE);
        PoiSearch mPoiSearch = new PoiSearch(SinvieSelectAddressActivity.this, mQuery);
        mPoiSearch.setOnPoiSearchListener(SinvieSelectAddressActivity.this);
        //显示弹窗
        showLoadingDialog();
        //发送搜索请求
        mPoiSearch.searchPOIAsyn();
    }

    private void startSearchAddress(double latitude, double longitude) {
        mIsInputSearch = false;
        //开始搜索
        PoiSearch.Query mQuery = new PoiSearch.Query("", "", mCity);
        mQuery.setPageNum(mPageIndex);
        mQuery.setPageSize(SinvieUIConstants.PAGE_SIZE);
        PoiSearch mPoiSearch = new PoiSearch(SinvieSelectAddressActivity.this, mQuery);
        mPoiSearch.setBound(new PoiSearch.SearchBound(new LatLonPoint(latitude, longitude), 100));//设置周边搜索的中心点以及半径
        mPoiSearch.setOnPoiSearchListener(SinvieSelectAddressActivity.this);
        //显示弹窗
        showLoadingDialog();
        //发送搜索请求
        mPoiSearch.searchPOIAsyn();
    }

    /**
     * 自定义一个图钉，并且设置图标，当我们点击图钉时，显示设置的信息
     */
    private MarkerOptions getMarkerOptions(PoiItem poiItem) {
        //设置图钉选项
        MarkerOptions options = new MarkerOptions();
        //图标
        options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.icon_sinvie_report_address));
        //位置
        options.position(new LatLng(poiItem.getLatLonPoint().getLatitude(),
                poiItem.getLatLonPoint().getLongitude()));
        String buffer = poiItem.getProvinceName()
                + "" + poiItem.getCityName()
                + "" + poiItem.getAdName()
                + "" + poiItem.getSnippet();
        //标题
        options.title(buffer);
        //子标题
        options.snippet("当前定位: 经度: " + poiItem.getLatLonPoint().getLongitude()
                + ",纬度: " + poiItem.getLatLonPoint().getLatitude());
        //设置多少帧刷新一次图片资源
        options.period(60);

        return options;

    }


    /**
     * 搜索返回
     */
    @Override
    public void onPoiSearched(PoiResult poiResult, int i) {

        mSmartRefreshLayout.finishLoadMore();
        mSmartRefreshLayout.finishRefresh();

        //关闭等待弹窗
        closeLoadingDialog();

        if (i != 1000) {
            SinvieLogUtil.d(TAG, i + "");
            return;
        }

        SinvieDemoLog.i(TAG, "ERROR_CODE: " + i + ", Gson: " + new Gson().toJson(poiResult.getPois().toString()));
        // 不用清除数据
        for (PoiItem pois : poiResult.getPois()) {
            SinvieReportAddressBean bean = new SinvieReportAddressBean();
            //名称
            bean.setLocation(pois.getTitle());
            //省+市+行政区名
            bean.setAddress(pois.getProvinceName() + pois.getCityName() + pois.getAdName() + pois.getSnippet());
            //纬度
            bean.setLatitude(pois.getLatLonPoint().getLatitude());
            //经度
            bean.setLongitude(pois.getLatLonPoint().getLongitude());
            //是否是当前地址
            bean.setCurrentLocation(false);
            mBeanList.add(bean);
            //只有输入搜索才显示 mark
            Marker marker = aMap.addMarker(getMarkerOptions(pois));
            //添加到marker list 方便清除
            mMarkerList.add(marker);
        }
        //只有等于10条才开启上拉刷新
        mSmartRefreshLayout.setEnableLoadMore(poiResult.getPois().size() == SinvieUIConstants.PAGE_SIZE);
        mAddressAdapter.notifyDataSetChanged();

    }

    @Override
    public void onPoiItemSearched(PoiItem poiItem, int i) {

    }

    /**
     * 清除地图 除了中心点和定位坐标 其他都清除
     */
    private void clearMap() {
        //取消adapter 的选中状态
        mAddressAdapter.resetAdapterClick();
        mAddressAdapter.notifyDataSetChanged();
        //除了当前定位信息 其他数据清除
        Iterator<SinvieReportAddressBean> iterator = mBeanList.listIterator();
        while (iterator.hasNext()) {
            SinvieReportAddressBean bean = iterator.next();
            //当前地址的item留着
            if (!bean.isCurrentLocation()) {
                iterator.remove();
            }
        }
        //清除地图上的 marker
        Iterator<Marker> markerIterator = mMarkerList.listIterator();
        while (markerIterator.hasNext()) {
            Marker marker = markerIterator.next();
            marker.remove();
            markerIterator.remove();
        }

    }

    /**
     * 显示等待弹窗
     */
    private void showLoadingDialog() {
        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog(this);
        }
        mLoadingDialog.setLoadingText(getString(R.string.sinvie_doing_search_address))
                .setInterceptBack(true)
                .setLoadSpeed(LoadingDialog.Speed.SPEED_TWO)
                .setRepeatCount(-1)
                .show();
    }

    private void closeLoadingDialog() {
        if (mLoadingDialog != null) {
            mLoadingDialog.close();
        }
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();

    }
}