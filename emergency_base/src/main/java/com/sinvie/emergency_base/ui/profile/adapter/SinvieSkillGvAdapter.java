package com.sinvie.emergency_base.ui.profile.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinvie.emergency_base.R;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/14
 * @Des: 类描述
 */
public class SinvieSkillGvAdapter extends BaseAdapter {

    private Context mContext;
    private List<String> mSkillList;

    public SinvieSkillGvAdapter(Context context, List<String> skillList) {
        mContext = context;
        mSkillList = skillList;
    }

    @Override
    public int getCount() {
        if (mSkillList == null) {
            mSkillList = new ArrayList<>();
        }
        return mSkillList.size();
    }

    @Override
    public String getItem(int position) {
        return mSkillList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_sinvie_skill,parent,false);
        SkillViewHolder viewHolder = new SkillViewHolder();
        viewHolder.mTvSkill = view.findViewById(R.id.tv_item_skill);
        viewHolder.mTvSkill.setText(mSkillList.get(position));
        return view;
    }

    public class SkillViewHolder{
        TextView mTvSkill;
    }
}
