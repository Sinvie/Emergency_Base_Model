package com.sinvie.emergency_base.ui.coop

import android.annotation.SuppressLint
import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.webkit.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.sinvie.emergency_base.R
import com.sinvie.emergency_base.SinvieOptions
import com.sinvie.emergency_base.http.SinvieNetContact
import com.sinvie.emergency_base.ui.login.SinvieUserInfo
import com.sinvie.emergency_base.utils.file.getFilePathByUri
import com.sinvie.emergency_base.utils.network.downloadBySystem
import com.sinvie.emergency_base.utils.network.getRandomUri
import com.sinvie.emergency_base.utils.showText

class SinvieCoopActivity : AppCompatActivity() {
    private val TAG = SinvieCoopActivity::class.java.simpleName
    private lateinit var webView: WebView
    private lateinit var webViewUrl: String

    private var uploadCallback: ValueCallback<Array<Uri>>? = null

    private val launcher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            it.data?.data?.let { uri ->
                val picPath = uri.getFilePathByUri()
                Log.d(TAG, "upload file:$picPath")
                uploadCallback?.onReceiveValue(arrayOf(uri))
            }
            if (it.data?.data == null) uploadCallback?.onReceiveValue(null)
        }

    val uri = getRandomUri(SinvieOptions.getAppContext(), "jpg")

    private val picLauncher = registerForActivityResult(ActivityResultContracts.TakePicture()) {
        Log.d(TAG, "take picture result:$it")
        if (it) {
            Log.d(TAG, "upload new picture:${uri.getFilePathByUri()}")
            uploadCallback?.onReceiveValue(arrayOf(uri))
        } else {
            uploadCallback?.onReceiveValue(null)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sinvie_coop_main)
        initView()
    }

    @SuppressLint("JavascriptInterface")
    private fun initView() {
        webView = findViewById(R.id.wv_data)
        webViewUrl = SinvieNetContact.COOP_URL

        webView.apply {
            webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    Log.d(TAG, "load page finish")
                }

                override fun shouldOverrideUrlLoading(
                    view: WebView?,
                    request: WebResourceRequest
                ): Boolean {
                    Log.d(TAG, "request url:${request.url}")

                    val requestString = request.url!!.toString()
                    if (requestString.startsWith("tel:")) {
                        val intent = Intent(Intent.ACTION_VIEW, request.url)
                        startActivity(intent)
                        return true
                    }
                    view?.loadUrl(requestString)

                    return true
                }
            }

            webChromeClient = object : WebChromeClient() {
                override fun onProgressChanged(view: WebView, newProgress: Int) {
                    if (newProgress > 98) {
                        Log.d(TAG, "progress:$newProgress");
                    }
                }

                override fun onShowFileChooser(
                    webView: WebView?,
                    filePathCallback: ValueCallback<Array<Uri>>?,
                    fileChooserParams: FileChooserParams?
                ): Boolean {
                    if (fileChooserParams != null) {
                        uploadCallback = filePathCallback
                        if (fileChooserParams.isCaptureEnabled) {
                            Log.d(TAG, "random uri name:${uri.path}")
                            picLauncher.launch(uri)
                            return true
                        }
                        //打开图库
                        val intent = Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                        )
                        intent.type = "video/*;image/*"
                        launcher.launch(intent)
                        return true
                    }

                    return super.onShowFileChooser(webView, filePathCallback, fileChooserParams)
                }

                override fun onGeolocationPermissionsShowPrompt(
                    origin: String?,
                    callback: GeolocationPermissions.Callback
                ) {
                    callback.invoke(origin, true, false)
                }
            }
            WebView.setWebContentsDebuggingEnabled(true)
            settings.javaScriptEnabled = true
            settings.domStorageEnabled = true
            settings.setGeolocationEnabled(true)
            settings.allowUniversalAccessFromFileURLs = true
            settings.allowContentAccess = true
            settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
            setDownloadListener { url, _, contentDisposition, mimetype, _ ->
                showText("文件开始下载！")
                Log.d(TAG, "download url:$url")
                downloadBySystem(url, contentDisposition, mimetype)
                val receiver = DownloadCompleteReceiver()
                val intentFilter = IntentFilter()
                intentFilter.addAction(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
                registerReceiver(receiver, intentFilter)
            }

            setInitialScale(100)
            addJavascriptInterface(this@SinvieCoopActivity, "task")
        }
        webView.loadUrl(webViewUrl)
    }

    override fun onBackPressed() {
        Log.d(TAG, "webview url:${webView.url}")
        if (webView.canGoBack() && webView.url?.contains("#/ReserveList") == false) {
            webView.goBack()
        } else {
            webView.destroy()
            finish()
        }
    }

    @JavascriptInterface
    fun getToken(): String {
        return SinvieUserInfo.getInstance().token
    }

    @JavascriptInterface
    fun getIsApp(): Boolean {
        return true
    }

    inner class DownloadCompleteReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent?) {
            Log.v(TAG, "onReceive. intent:{${intent?.toUri(0)}}")
            intent?.let {
                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE == intent.action) {
                    val downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
                    Log.d(TAG, "downloadId:{$downloadId}")
                    val downloadManager =
                        context.getSystemService(DOWNLOAD_SERVICE) as DownloadManager
                    var type = downloadManager.getMimeTypeForDownloadedFile(downloadId)
                    Log.d(TAG, "getMimeTypeForDownloadedFile:{$type}")
                    if (type.isEmpty()) {
                        type = "*/*"
                    }
                    val uri = downloadManager.getUriForDownloadedFile(downloadId)
                    Log.d(TAG, "UriForDownloadedFile:{$uri}")
                    showText("文件下载成功！请在通知栏查看")
//                    // 打开下载文件
//                    uri?.let {
//                        val handlerIntent = Intent(Intent.ACTION_VIEW)
//                        handlerIntent.setDataAndType(uri, type)
//                        context.startActivity(handlerIntent)
//                    }
                }
            }
        }
    }
}