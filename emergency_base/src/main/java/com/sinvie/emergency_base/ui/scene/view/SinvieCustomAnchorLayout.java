package com.sinvie.emergency_base.ui.scene.view;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieRoomInfo;
import com.sinvie.emergency_live.tuikit_live.live.base.SinvieConfig;
import com.sinvie.emergency_base.ui.scene.SinvieCustomAnchorFragment;

/**
 * @param
 * @author JonsonBob
 * @description 实现了直播间的主播端界面
 * @return
 * @time 2021/7/2 15:10
 */
public class SinvieCustomAnchorLayout extends FrameLayout {
    private CustomLiveRoomAnchorLayoutDelegate mLiveRoomAnchorLayoutDelegate;
    private FragmentManager mFragmentManager;
    private SinvieCustomAnchorFragment mCustomAnchorFragment;

    public SinvieCustomAnchorLayout(@NonNull Context context) {
        super(context);
    }

    public SinvieCustomAnchorLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.view_sinvie_live_room_archor, this);
        mCustomAnchorFragment = new SinvieCustomAnchorFragment();
    }

    /**
     * 通过 roomId 初始化主播端
     *
     * @param fragmentManager 用于管理fragment，activity 请通过 getSupportFragmentManager() 传入
     * @param roomId          会以该 roomId 创建直播房间
     */
    public void initWithRoomId(FragmentManager fragmentManager, int roomId) {
        Bundle bundle = new Bundle();
        bundle.putInt("room_id", roomId);
        mCustomAnchorFragment.setArguments(bundle);

        mFragmentManager = fragmentManager;
        mFragmentManager.beginTransaction()
                .add(R.id.live_anchor_container, mCustomAnchorFragment, "tuikit-live-anchor" +
                        "-fragment")
                .commit();
    }

    /**
     * 设置 UI 回调接口
     *
     * @param liveRoomAnchorLayoutDelegate
     */
    public void setLiveRoomAnchorLayoutDelegate(CustomLiveRoomAnchorLayoutDelegate liveRoomAnchorLayoutDelegate) {
        mLiveRoomAnchorLayoutDelegate = liveRoomAnchorLayoutDelegate;
        mCustomAnchorFragment.setLiveRoomAnchorLayoutDelegate(mLiveRoomAnchorLayoutDelegate);
    }

    /**
     * 请在 Activity 的 onBackPress 函数中调用该函数，这里会实现界面返回提醒
     */
    public void onBackPress() {
        if (mCustomAnchorFragment != null) {
            mCustomAnchorFragment.onBackPressed();
        }
    }

    public void stopLive() {
        if (mCustomAnchorFragment != null) {
            mCustomAnchorFragment.stopLive();
        }
    }

    /**
     * 调用该接口会强制停止
     */
    public void unInit() {
        if (mCustomAnchorFragment != null) {
            mCustomAnchorFragment.onBackPressed();
        }
    }

    /**
     * 是否打开PK按钮
     *
     * @param enable true:打开
     */
    public void enablePK(boolean enable) {
        SinvieConfig.setPKButtonStatus(enable);
    }

    public interface CustomLiveRoomAnchorLayoutDelegate {
        /**
         * 点击界面中的关闭按钮等会回调该通知，可以在Activity中调用finish方法
         */
        void onClose();

        /**
         * UI 组件内部产生错误会通过该接口回调出来
         *
         * @param sinvieRoomInfo  房间信息
         * @param errorCode 错误码
         * @param errorMsg  错误信息
         */
        void onError(SinvieRoomInfo sinvieRoomInfo, int errorCode, String errorMsg);

        /**
         * 房间创建成功后会通过该接口回调出来
         *
         * @param sinvieRoomInfo 房间信息
         */
        void onRoomCreate(SinvieRoomInfo sinvieRoomInfo);

        /**
         * 房间销毁成功后会通过该接口回调出来
         *
         * @param sinvieRoomInfo 房间信息
         */
        void onRoomDestroy(SinvieRoomInfo sinvieRoomInfo);

    }

}
