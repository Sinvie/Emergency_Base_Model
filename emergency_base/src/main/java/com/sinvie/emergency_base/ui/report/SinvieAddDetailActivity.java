package com.sinvie.emergency_base.ui.report;

import android.Manifest;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.utils.widget.ImageFilterView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.hjq.permissions.OnPermissionCallback;
import com.hjq.permissions.XXPermissions;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnResultCallbackListener;
import com.sinvie.emergency_conf.CommonService.util.SinvieLogUtil;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieAddResourceBean;
import com.sinvie.emergency_base.bean.SinvieAppAttachmentsBean;
import com.sinvie.emergency_base.bean.SinvieReportDetailBean;
import com.sinvie.emergency_base.bean.SinvieSaveFileBean;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.logic.report.add_report_detail.SinvieAddReportDetailContact;
import com.sinvie.emergency_base.logic.report.add_report_detail.SinvieAddReportDetailPresenter;
import com.sinvie.emergency_live.tuikit.base.ISinvieTitleBarLayout;
import com.sinvie.emergency_live.tuikit.utils.SinvieToastUtil;
import com.sinvie.emergency_base.ui.base.SinvieMVPBaseActivity;
import com.sinvie.emergency_base.ui.report.adapter.SinvieAddResourceGvAdapter;
import com.sinvie.emergency_base.utils.SinvieClickUtils;
import com.sinvie.emergency_base.utils.SinvieFileUtil;
import com.sinvie.emergency_base.utils.SinviePictureSelectUtils;
import com.sinvie.emergency_base.widget.SinvieCustomTitleBarLayout;
import com.sinvie.emergency_base.widget.SinvieSendVoiceBtn;
import com.xiasuhuei321.loadingdialog.view.LoadingDialog;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SinvieAddDetailActivity extends SinvieMVPBaseActivity<SinvieAddReportDetailContact.AddReportDetailBaseView,
        SinvieAddReportDetailPresenter> implements SinvieAddReportDetailContact.AddReportDetailBaseView,
        View.OnClickListener {

    private static final String TAG = SinvieAddDetailActivity.class.getSimpleName();

    private ConstraintLayout mClAddReportDetail;
    private SinvieCustomTitleBarLayout mCbrAddNewReportDetail;
    private TextView mViewDecorationDetailName;
    private LinearLayout mLlReportDetailDes;
    private TextView mLiveSituation;
    private LinearLayout mLlDetailLiveSituation;
    private RecyclerView mGvReportDetailImg;
    private LinearLayoutCompat mLlAddReportDetailBottom;
    private TextView mTvSaveDetail;
    private TextView mTvCancelDetail;
    private EditText mEdReportDetailDes;
    private SinvieSendVoiceBtn mSinvieSendVoiceBtn;
    private ImageFilterView mIvAddReportDelVoice;

    private int mIntentType = 0;// 默认新增
    private static final int MAX_SELECT_RESOURCE = 4;
    /**
     * 已经上传的文件ID 列表
     */
    private List<String> mAttachmentIds;
    /**
     * 选择资源地址 list
     */
    private List<SinvieAddResourceBean> mResourceBeanList;

    /**
     * 当前选中的文件地址 list 用于上传文件
     */
    private List<String> mFilePaths;

    private String mReportTitle;

    private SinvieAddResourceGvAdapter mAddResourceAdapter;
    private SinvieReportDetailBean.ReportSupplementsRoomListBean mInfoBean;

    private LoadingDialog mLoadingDialog;

    @Override
    public void initializeData() {
        //如果携带信息过来 此处解析 然后 填充信息
        mIntentType = getIntent().getIntExtra(SinvieUIConstants.REPORT_INTENT_TYPE,
                SinvieUIConstants.REPORT_TYPE_ADD);
        mInfoBean = getIntent().getParcelableExtra(SinvieUIConstants.REPORT_DETAIL_INFO_BEAN);
        SinvieLogUtil.d(TAG, new Gson().toJson(mInfoBean));
        mReportTitle = getIntent().getStringExtra(SinvieUIConstants.REPORT_INFO_TITLE);
    }

    @Override
    public void initializeComposition() {
        setContentView(R.layout.activity_sinvie_add_report_detail);
        mClAddReportDetail = findViewById(R.id.cl_add_report_detail);
        mCbrAddNewReportDetail = findViewById(R.id.cbr_add_new_report_detail);
        mViewDecorationDetailName = findViewById(R.id.view_decoration_detail_name);
        mLlReportDetailDes = findViewById(R.id.ll_report_detail_des);
        mLiveSituation = findViewById(R.id.view_decoration_detail_live_situation);
        mLlDetailLiveSituation = findViewById(R.id.ll_detail_live_situation);
        mGvReportDetailImg = findViewById(R.id.gv_report_detail_img);
        mLlAddReportDetailBottom = findViewById(R.id.ll_add_report_detail_bottom);
        mTvSaveDetail = findViewById(R.id.tv_save_detail);
        mTvCancelDetail = findViewById(R.id.tv_cancel_detail);
        mEdReportDetailDes = findViewById(R.id.ed_report_detail_des);
        mSinvieSendVoiceBtn = findViewById(R.id.svb_report_details_voice);
        mIvAddReportDelVoice = findViewById(R.id.iv_add_report_details_del_voice);

        mTvSaveDetail.setOnClickListener(this);
        mTvCancelDetail.setOnClickListener(this);
        mIvAddReportDelVoice.setOnClickListener(this);

        mResourceBeanList = new ArrayList<>();
        mAttachmentIds = new ArrayList<>();
        mFilePaths = new ArrayList<>();

        GridLayoutManager manager = new GridLayoutManager(this, 4);
        mGvReportDetailImg.setLayoutManager(manager);
        //gv 初始化 绑定adapter
        mAddResourceAdapter = new SinvieAddResourceGvAdapter(this, mResourceBeanList);
        mAddResourceAdapter.setItemClickListener(mItemClickListener);
        mGvReportDetailImg.setAdapter(mAddResourceAdapter);

        //标题设置
        mCbrAddNewReportDetail.setLeftIcon(R.drawable.svg_sinvie_theme_back);
        mCbrAddNewReportDetail.setOnLeftClickListener(v -> finish());

        if (mIntentType == SinvieUIConstants.REPORT_TYPE_ADD) {
            mCbrAddNewReportDetail.setTitle(getString(R.string.sinvie_add_report_detail), ISinvieTitleBarLayout.POSITION.MIDDLE);
            mTvSaveDetail.setText(getString(R.string.sinvie_submit_event));
        } else {
            mCbrAddNewReportDetail.setTitle(getString(R.string.sinvie_edit_report_detail), ISinvieTitleBarLayout.POSITION.MIDDLE);
            mTvSaveDetail.setText(getString(R.string.sinvie_save_report));
            onIntentParams();
        }
        mCbrAddNewReportDetail.setOnLeftClickListener(v -> finish());

    }

    private void onIntentParams() {
        //描述
        mEdReportDetailDes.setText(mInfoBean.getContent());
        int imgNum = Math.min(mInfoBean.getAppAttachments().size(), 4);
        //现场情况 只显示四张图
        for (int i = 0; i < imgNum; i++) {
            SinvieAppAttachmentsBean appAttachment = mInfoBean.getAppAttachments().get(i);

            SinvieAddResourceBean bean = new SinvieAddResourceBean();
            bean.setAdd(false);
            bean.setNetPath(true);
            int suffix = SinvieFileUtil.getSuffixFormat(appAttachment.getUrl());
            //格式 图片/视频
            bean.setImg(suffix == SinvieFileUtil.SUFFIX_FORMAT_IMG);
            //图片id
            bean.setResourceId(appAttachment.getId());
            //存入图片资源地址
            bean.setResourcePath(appAttachment.getUrl());
            //资源list 添加
            mResourceBeanList.add(bean);
            //记录图片资源上传后的id
            mAttachmentIds.add(appAttachment.getId() + "");
        }
        Log.d(TAG, "onIntentParams: " + new Gson().toJson(mAttachmentIds));
        mAddResourceAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {

        if (SinvieClickUtils.isFastClick(v.getId())) {
            return;
        }
        if (v.getId() == R.id.tv_cancel_detail) {
            finish();
        } else if (v.getId() == R.id.tv_save_detail) {
            //显示等待弹窗
            showLoadingDialog();
            //判断资源列表是否为空 不为空就开始先上传资源 如果为空就提交信息
            if (mFilePaths == null || mFilePaths.isEmpty()) {
                startSaveDetailInfo();
            } else {
                mPresenter.upLoadFile(mFilePaths);
            }
        } else if (v.getId() == R.id.iv_add_report_details_del_voice) {
            //重置录音按钮
            mSinvieSendVoiceBtn.setResetBtn();
            //删除录音文件
            mSinvieSendVoiceBtn.clearVoiceData();
        }
    }

    private SinvieAddResourceGvAdapter.onReportGvItemClickListener mItemClickListener =
            new SinvieAddResourceGvAdapter.onReportGvItemClickListener() {
                @Override
                public void onGvDeleteItemListener(SinvieAddResourceBean resourceBean) {
                    //删除存储的资源
                    delSelectRes(resourceBean);
                    //刷新列表
                    mAddResourceAdapter.notifyDataSetChanged();
                }

                @Override
                public void onGvDeleteAddItemListener() {
//                    String[] permission = new String[]{Manifest.permission.MANAGE_EXTERNAL_STORAGE};
//                    XXPermissions.with(SinvieAddDetailActivity.this).permission(permission)
//                            .request((permissions, all) -> {
//                                if (all) {
                                    submitOpenCamera();
//                                }
//                            });
                }
            };

    @Override
    protected SinvieAddReportDetailContact.AddReportDetailBaseView createView() {
        return this;
    }

    @Override
    protected SinvieAddReportDetailPresenter createPresenter() {
        return new SinvieAddReportDetailPresenter(this);
    }

    @Override
    public void submitReportDetailSuccess() {
        SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_report_detail_submit_success));
        setResult(SinvieUIConstants.ADD_REPORT_DETAIL_RESULT);
        dismissLoadingDialog();
        finish();
    }

    @Override
    public void submitReportDetailFail(int errCod, String errMsg) {
        SinvieToastUtil.toastLongErrorMessage(errCod, errMsg);
    }

    @Override
    public void upLoadFileSuccess(SinvieSaveFileBean sinvieSaveFileBean, String path, boolean isAll) {
        mAttachmentIds.add(sinvieSaveFileBean.getId() + "");
        mFilePaths.remove(0);
        if (isAll) {
            startSaveDetailInfo();
        } else {
            //已经提交的path从 pathList中删除
//            Iterator<String> iterator = mFilePaths.listIterator();
//            while (iterator.hasNext()) {
//                String filePath = iterator.next();
//                if (filePath.equals(path)) {
//                    iterator.remove();
//                }
//            }
            if (mFilePaths != null && !mFilePaths.isEmpty()) {
                mPresenter.upLoadFile(mFilePaths);
            }
        }
    }

    @Override
    public void upLoadFileFail(int errCode, String errMsg) {
        SinvieToastUtil.toastLongErrorMessage(errCode, errMsg);
    }

    @Override
    public void dismissLoadingDialog() {
        if (mLoadingDialog != null) {
            mLoadingDialog.close();
        }
    }


    private void startSaveDetailInfo() {

        mPresenter.submitReportDetail(
                mIntentType
                , mReportTitle
                , mEdReportDetailDes.getText().toString().trim()
                , mInfoBean
                , mAttachmentIds);
    }

    private void submitOpenCamera() {
        String[] secondPermission = new String[]{
                Manifest.permission.CAMERA};
        XXPermissions.with(SinvieAddDetailActivity.this)
                .permission(secondPermission)
                .request(new OnPermissionCallback() {
                    @Override
                    public void onGranted(List<String> permissions,
                                          boolean all) {
                        if (all) {
                            openPicture();

                        }
                    }
                });
    }

    private void openPicture() {
        SinviePictureSelectUtils.getInstance()
                .init(SinvieAddDetailActivity.this)
                .startSelectPic(MAX_SELECT_RESOURCE - mResourceBeanList.size(),
                        new OnResultCallbackListener<LocalMedia>() {
                            @Override
                            public void onResult(List<LocalMedia> result) {
                                for (LocalMedia media : result) {

//                                    DemoLog.i(TAG, "是否压缩:" + media.isCompressed());
//                                    DemoLog.i(TAG, "压缩:" + media.getCompressPath());
//                                    DemoLog.i(TAG, "原图:" + media.getPath());
//                                    DemoLog.i(TAG, "绝对路径:" + media.getRealPath());
//                                    DemoLog.i(TAG, "是否裁剪:" + media.isCut());
//                                    DemoLog.i(TAG, "裁剪:" + media.getCutPath());
//                                    DemoLog.i(TAG, "是否开启原图:" + media.isOriginal());
//                                    DemoLog.i(TAG, "原图路径:" + media.getOriginalPath());
//                                    DemoLog.i(TAG, "Android Q 特有Path:" + media.getAndroidQToPath());
//                                    DemoLog.i(TAG, "宽高: " + media.getWidth() + "x" + media.getHeight());
//                                    DemoLog.i(TAG, "Size: " + media.getSize());
                                    SinvieAddResourceBean bean = new SinvieAddResourceBean();
                                    bean.setAdd(false);
                                    bean.setImg(true);
                                    bean.setNetPath(false);
                                    bean.setResourcePath(media.getRealPath());
                                    mResourceBeanList.add(bean);
                                    //绝对路径地址
                                    mFilePaths.add(media.getRealPath());
                                }
                                mAddResourceAdapter.notifyDataSetChanged();
                            }

                            @Override
                            public void onCancel() {

                            }
                        });
    }

    /**
     * 等待弹窗
     */
    private void showLoadingDialog() {
        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog(this);
        }
        mLoadingDialog.setLoadingText(getString(R.string.sinvie_report_doing))
                .setSuccessText(getString(R.string.sinvie_do_success))//显示加载成功时的文字
                .setFailedText(getString(R.string.sinvie_report_submit_fail))
                .setInterceptBack(true)
                .setLoadSpeed(LoadingDialog.Speed.SPEED_TWO)
                .setRepeatCount(-1)
                .show();
    }

    /**
     * 删除选择的 pic
     */
    private void delSelectRes(SinvieAddResourceBean resourceBean) {

        //先删除 adapter list 的图片(显示的list)
        Iterator<SinvieAddResourceBean> iterator = mResourceBeanList.listIterator();
        if (iterator.hasNext()) {
            do {
                SinvieAddResourceBean bean = iterator.next();
                if (bean.getResourcePath().equals(resourceBean.getResourcePath())) {
                    iterator.remove();
                }
            } while (iterator.hasNext());
        }

        //无论是修改 还是 增加 都需要删除 file list (如果有)
        Iterator<String> fileIterator = mFilePaths.listIterator();
        if (fileIterator.hasNext()) {
            do {
                String filePath = fileIterator.next();
                if (filePath.equals(resourceBean.getResourcePath())) {
                    //如果删除的 资源地址相等 删除file list的资源
                    fileIterator.remove();
                }
            } while (fileIterator.hasNext());
        }

        //如果是修改report 那需要额外删除传过来的 资源文件 list中的 attachment id(如果删除了)
        if (mIntentType == SinvieUIConstants.REPORT_TYPE_EDIT) {
            Iterator<String> stringIterator = mAttachmentIds.listIterator();
            if (stringIterator.hasNext()) {
                do {
                    String id = stringIterator.next();
                    if (id.equals(resourceBean.getResourceId() + "")) {
                        stringIterator.remove();
                    }
                } while (stringIterator.hasNext());
            }
        }
    }

}