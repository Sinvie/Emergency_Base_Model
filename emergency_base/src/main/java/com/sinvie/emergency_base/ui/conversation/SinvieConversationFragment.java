package com.sinvie.emergency_base.ui.conversation;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_live.tuikit.base.ISinvieUIKitCallBack;
import com.sinvie.emergency_base.ui.base.SinvieAbsFragment;
import com.sinvie.emergency_base.ui.menu.SinvieMenu;
import com.sinvie.emergency_base.utils.SinvieActivityUtil;
import com.sinvie.emergency_base.utils.SinvieConstants;
import com.sinvie.emergency_base.ui.SinvieIntentConstant;

import com.sinvie.emergency_live.tuikit.component.SinvieTitleBarLayout;
import com.sinvie.emergency_live.tuikit.component.action.SinviePopActionClickListener;
import com.sinvie.emergency_live.tuikit.component.action.SinviePopDialogAdapter;
import com.sinvie.emergency_live.tuikit.component.action.SinviePopMenuAction;
import com.sinvie.emergency_live.tuikit.modules.chat.base.SinvieChatInfo;
import com.sinvie.emergency_live.tuikit.modules.conversation.SinvieConversationLayout;
import com.sinvie.emergency_live.tuikit.modules.conversation.SinvieConversationAdapter;
import com.sinvie.emergency_live.tuikit.modules.conversation.SinvieConversationListLayout;
import com.sinvie.emergency_live.tuikit.modules.conversation.base.SinvieConversationInfo;
import com.sinvie.emergency_live.tuikit.modules.search.SinvieSearchMainActivity;
import com.sinvie.emergency_live.tuikit.utils.SinviePopWindowUtil;
import com.sinvie.emergency_live.tuikit.utils.SinvieToastUtil;
import com.tencent.imsdk.v2.V2TIMConversation;
import java.util.ArrayList;
import java.util.List;

/**
 * @param
 * @author JonsonBob
 * @description 对话fragment
 * @return
 * @time 2021/6/25 16:57
 */
public class SinvieConversationFragment extends SinvieAbsFragment {
    private SinvieConversationLayout mConversationLayout;
    private SinvieMenu mSinvieMenu;

    private ListView mConversationPopList;
    private SinviePopDialogAdapter mConversationPopAdapter;
    private PopupWindow mConversationPopWindow;
    private List<SinviePopMenuAction> mConversationPopActions = new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.fragment_sinvie_conversation;
    }

    @Override
    public void onViewCreated() {
        // 从布局文件中获取会话列表面板
        mConversationLayout = mView.findViewById(R.id.conversation_layout);
        mSinvieMenu = new SinvieMenu(getActivity(), (SinvieTitleBarLayout) mConversationLayout.getTitleBar(),
                SinvieMenu.MENU_TYPE_CONVERSATION);

        // 会话列表面板的默认UI和交互初始化
        mConversationLayout.initDefault();
        //TODO 通过API设置ConversataonLayout各种属性的样例，开发者可以打开注释，体验效果
        // ConversationLayoutHelper.customizeConversation(mConversationLayout);
        mConversationLayout.getConversationList().setOnItemClickListener(new SinvieConversationListLayout.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, SinvieConversationInfo conversationInfo) {
                //此处为demo的实现逻辑，更根据会话类型跳转到相关界面，开发者可根据自己的应用场景灵活实现
                if (position == 0) {
                    //search
                    Intent intent = new Intent(getContext(), SinvieSearchMainActivity.class);
                    //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    startChatActivity(conversationInfo);
                }
            }
        });
        mConversationLayout.getConversationList().setOnItemLongClickListener(new SinvieConversationListLayout.OnItemLongClickListener() {
            @Override
            public void OnItemLongClick(View view, int position,
                                        SinvieConversationInfo conversationInfo) {
                if (position == 0) {
                    //search
                    Intent intent = new Intent(getContext(), SinvieSearchMainActivity.class);
                    //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    startPopShow(view, position - SinvieConversationAdapter.HEADER_COUNT,
                            conversationInfo);
                }
            }
        });
        initTitleAction();
        initPopMenuAction();
    }

    @Override
    public void onViewLoad() {
        super.onViewLoad();
    }

    /**
     * 初始化右上角菜单
     */
    private void initTitleAction() {
        mConversationLayout.getTitleBar().setOnRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSinvieMenu.isShowing()) {
                    mSinvieMenu.hide();
                } else {
                    mSinvieMenu.show();
                }
            }
        });
    }

    /**
     * 初始化长按item显示pop
     */
    private void initPopMenuAction() {
        // 设置长按conversation显示PopAction
        List<SinviePopMenuAction> conversationPopActions = new ArrayList<SinviePopMenuAction>();
        SinviePopMenuAction action = new SinviePopMenuAction();
        action.setActionName(getResources().getString(R.string.sinvie_chat_top));
        action.setActionClickListener(new SinviePopActionClickListener() {
            @Override
            public void onActionClick(int position, Object data) {
                mConversationLayout.setConversationTop((SinvieConversationInfo) data,
                        new ISinvieUIKitCallBack() {
                            @Override
                            public void onSuccess(Object data) {

                            }

                            @Override
                            public void onError(String module, int errCode, String errMsg) {
                                SinvieToastUtil.toastLongMessage(module + ", Error code = " + errCode + ", desc" +
                                        " = " + errMsg);
                            }
                        });
            }
        });
        conversationPopActions.add(action);
        action = new SinviePopMenuAction();
        action.setActionClickListener(new SinviePopActionClickListener() {
            @Override
            public void onActionClick(int position, Object data) {
                mConversationLayout.deleteConversation(position, (SinvieConversationInfo) data);
            }
        });
        action.setActionName(getResources().getString(R.string.sinvie_chat_delete));
        conversationPopActions.add(action);

        action = new SinviePopMenuAction();
        action.setActionName(getResources().getString(R.string.sinvie_clear_conversation_message));
        action.setActionClickListener(new SinviePopActionClickListener() {
            @Override
            public void onActionClick(int position, Object data) {
                mConversationLayout.clearConversationMessage(position, (SinvieConversationInfo) data);
            }
        });
        conversationPopActions.add(action);

        mConversationPopActions.clear();
        mConversationPopActions.addAll(conversationPopActions);
    }

    /**
     * 跳转到聊天界面
     *
     * @param conversationInfo
     */
    private void startChatActivity(SinvieConversationInfo conversationInfo) {
        SinvieChatInfo chatInfo = new SinvieChatInfo();
        chatInfo.setType(conversationInfo.isGroup() ? V2TIMConversation.V2TIM_GROUP :
                V2TIMConversation.V2TIM_C2C);
        chatInfo.setGroupType(conversationInfo.isGroup() ? conversationInfo.getGroupType() : "");
        chatInfo.setId(conversationInfo.getId());
        chatInfo.setChatName(conversationInfo.getTitle());
        chatInfo.setDraft(conversationInfo.getDraft());
        Intent intent = new Intent(SinvieIntentConstant.CHAT_ACTIVITY_ACTION);
        intent.putExtra(SinvieConstants.CHAT_INFO, chatInfo);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        SinvieLocContext.getContext().startActivity(intent);
        SinvieActivityUtil.startActivity(SinvieLocContext.getContext(), intent);
    }

    /**
     * 显示长按弹出popup
     *
     * @param view
     * @param position
     * @param info
     */
    private void startPopShow(View view, int position, SinvieConversationInfo info) {
        showItemPopMenu(position, info, view.getX(), view.getY() + view.getHeight() / 2);
    }

    /**
     * 长按会话item弹框
     *
     * @param index            会话序列号
     * @param conversationInfo 会话数据对象
     * @param locationX        长按时X坐标
     * @param locationY        长按时Y坐标
     */
    private void showItemPopMenu(final int index, final SinvieConversationInfo conversationInfo,
                                 float locationX, float locationY) {
        if (mConversationPopActions == null || mConversationPopActions.size() == 0)
            return;
        View itemPop = LayoutInflater.from(getActivity()).inflate(R.layout.view_sinvie_pop_menu, null);
        mConversationPopList = itemPop.findViewById(R.id.pop_menu_list);
        mConversationPopList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SinviePopMenuAction action = mConversationPopActions.get(position);
                if (action.getActionClickListener() != null) {
                    action.getActionClickListener().onActionClick(index, conversationInfo);
                }
                mConversationPopWindow.dismiss();
            }
        });

        for (int i = 0; i < mConversationPopActions.size(); i++) {
            SinviePopMenuAction action = mConversationPopActions.get(i);
            if (conversationInfo.isTop()) {
                if (action.getActionName().equals(getResources().getString(R.string.sinvie_chat_top))) {
                    action.setActionName(getResources().getString(R.string.sinvie_quit_chat_top));
                }
            } else {
                if (action.getActionName().equals(getResources().getString(R.string.sinvie_quit_chat_top))) {
                    action.setActionName(getResources().getString(R.string.sinvie_chat_top));
                }

            }
        }
        mConversationPopAdapter = new SinviePopDialogAdapter();
        mConversationPopList.setAdapter(mConversationPopAdapter);
        mConversationPopAdapter.setDataSource(mConversationPopActions);
        mConversationPopWindow = SinviePopWindowUtil.popupWindow(itemPop, mView, (int) locationX,
                (int) locationY);
        mView.postDelayed(new Runnable() {
            @Override
            public void run() {
                mConversationPopWindow.dismiss();
            }
        }, 10000); // 10s后无操作自动消失
    }
}