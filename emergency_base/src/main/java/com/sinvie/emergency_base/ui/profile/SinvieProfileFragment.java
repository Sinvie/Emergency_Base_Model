package com.sinvie.emergency_base.ui.profile;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.utils.widget.ImageFilterView;

import com.bumptech.glide.Glide;
import com.hjq.permissions.OnPermissionCallback;
import com.hjq.permissions.Permission;
import com.hjq.permissions.XXPermissions;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieContactorBean;
import com.sinvie.emergency_base.logic.profile.SinvieProfileContact;
import com.sinvie.emergency_base.logic.profile.SinvieProfilePresenter;
import com.sinvie.emergency_base.ui.SinvieIntentConstant;
import com.sinvie.emergency_base.ui.base.SinvieMVPBaseFragment;
import com.sinvie.emergency_base.ui.login.SinvieUserInfo;
import com.sinvie.emergency_base.ui.profile.adapter.SinvieSkillGvAdapter;
import com.sinvie.emergency_base.utils.SinvieActivityUtil;
import com.sinvie.emergency_base.utils.SinvieClickUtils;
import com.sinvie.emergency_base.utils.SinvieConstants;
import com.sinvie.emergency_base.widget.SinvieCircleImageView;
import com.sinvie.emergency_base.widget.SinvieCustomGridView;
import com.sinvie.emergency_base.widget.dialog.SinvieTipDialog;
import com.sinvie.emergency_live.tuikit.utils.SinvieToastUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @param
 * @author JonsonBob
 * @description 我 界面
 * @return
 * @time 2021/6/28 9:03
 */
public class SinvieProfileFragment extends SinvieMVPBaseFragment<SinvieProfileContact.ProfileBaseView,
        SinvieProfilePresenter> implements SinvieProfileContact.ProfileBaseView, View.OnClickListener {

    private SinvieCircleImageView mIvProfile;
    private TextView mTvProfileName;
    private TextView mTvProfileSex;
    private ImageView mIvProfileSex;
    private TextView mTvJobTitle;
    private SinvieCustomGridView mGvSkill;
    private CheckBox mCbLocation;
    private LinearLayoutCompat mLlMobile;
    private TextView mTvMobile;
    private LinearLayoutCompat mLlOfficePhone;
    private TextView mTvOfficePhone;
    private LinearLayoutCompat mLlHomePhone;
    private TextView mTvHomePhone;
    private TextView mTvRemark;
    private ImageFilterView mIvMobile;
    private ImageFilterView mIvOfficePhone;
    private ImageFilterView mIvHomePhone;


    private LinearLayoutCompat mLlSignOut;

    private List<SinvieContactorBean> mSinvieContactorBeanList;
    private List<String> mSkillList;
    private SinvieSkillGvAdapter mSinvieSkillGvAdapter;

    private int mPageIndex = 1;

    /**
     * 手机是否开启位置服务，如果没有开启那么所有app将不能使用定位功能
     */
    public static boolean isLocServiceEnable(Context context) {
        LocationManager locationManager =
                (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (gps || network) {
            return true;
        }
        return false;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_sinvie_profile;
    }

    @Override
    public void onDataLoad() {

        mIvProfile = mView.findViewById(R.id.iv_profile);
        mTvProfileName = mView.findViewById(R.id.tv_profile_name);
        mTvProfileSex = mView.findViewById(R.id.tv_profile_sex);
        mIvProfileSex = mView.findViewById(R.id.iv_profile_sex);
        mTvJobTitle = mView.findViewById(R.id.tv_job_title);
        mGvSkill = mView.findViewById(R.id.gv_skill);
        mCbLocation = mView.findViewById(R.id.cb_location);
        mLlMobile = mView.findViewById(R.id.ll_mobile);
        mTvMobile = mView.findViewById(R.id.tv_mobile);
        mLlOfficePhone = mView.findViewById(R.id.ll_office_phone);
        mTvOfficePhone = mView.findViewById(R.id.tv_office_phone);
        mLlHomePhone = mView.findViewById(R.id.ll_home_phone);
        mTvHomePhone = mView.findViewById(R.id.tv_home_phone);
        mTvRemark = mView.findViewById(R.id.tv_remark);
        mLlSignOut = mView.findViewById(R.id.ll_sign_out);

        mIvMobile = mView.findViewById(R.id.iv_mobile);
        mIvOfficePhone = mView.findViewById(R.id.iv_office_phone);
        mIvHomePhone = mView.findViewById(R.id.iv_home_phone);

        mSinvieContactorBeanList = new ArrayList<>();
        mSkillList = new ArrayList<>();
        mSinvieSkillGvAdapter = new SinvieSkillGvAdapter(context, mSkillList);
        mGvSkill.setAdapter(mSinvieSkillGvAdapter);

        //退出登录
        mLlSignOut.setOnClickListener(v -> showTipsDialog());

        mIvMobile.setOnClickListener(this);
        mIvHomePhone.setOnClickListener(this);
        mIvOfficePhone.setOnClickListener(this);

        mCbLocation.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                //判断当前App定位是否被用户开启 必须应用内的定位开关和手机的定位开关都开启 否则提示用户或者跳转到权限界面
                if (isLocServiceEnable(context)) {
                    requestLocPermission();
                } else {
                    SinvieUserInfo.getInstance().setOpenLocation(false);
                }
            }
            mCbLocation.setChecked(isChecked);
        });


        //获取联系人列表 UserInfo.getInstance().getUserId()
        mPresenter.getContactorData(mPageIndex, SinvieUserInfo.getInstance().getUserId());
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (SinvieClickUtils.isFastClick(id)) {
            return;
        }
        if (id == R.id.iv_mobile) {
            callPhone(mTvMobile.getText().toString());
        } else if (id == R.id.iv_home_phone) {
            callPhone(mTvHomePhone.getText().toString());
        } else if (id == R.id.iv_office_phone) {
            callPhone(mTvOfficePhone.getText().toString());
        }
    }

    @Override
    protected SinvieProfileContact.ProfileBaseView createView() {
        return this;
    }

    @Override
    protected SinvieProfilePresenter createPresenter() {
        return new SinvieProfilePresenter(context);
    }

    @Override
    public void getContactorDataSuccess(SinvieContactorBean beans) {
        if (beans.getRecords() != null && beans.getRecords().size() > 0) {
            SinvieContactorBean.RecordsRoomListBean bean = beans.getRecords().get(0);
            //填充数据
            //头像
            Glide.with(context).load(bean.getImages()).error(R.drawable.icon_sinvie_default_head).into(mIvProfile);
            //姓名
            mTvProfileName.setText(bean.getName());
            //性别
            if (bean.getSex() == 0) {
                //男
                mIvProfileSex.setImageResource(R.mipmap.icon_sinvie_sex_man);
                mTvProfileSex.setText(R.string.sinvie_sex_man);
            } else {
                //女
                mIvProfileSex.setImageResource(R.mipmap.icon_sinvie_sex_woman);
                mTvProfileSex.setText(R.string.sinvie_sex_woman);
            }
            //工作场所
            mTvJobTitle.setText(bean.getWorkUnit());
            //手机号码
            setHideCallTvIv(bean.getObile1(), mTvMobile, mIvMobile);
            mIvMobile.setVisibility(View.GONE);
            //办公
            setHideCallTvIv(bean.getOfficeTel(), mTvOfficePhone, mIvOfficePhone);
            //家庭电话
            setHideCallTvIv(bean.getHomeTel(), mTvHomePhone, mIvHomePhone);
            //备注
            mTvRemark.setText(bean.getRemark());

            //判断当前App定位是否被用户开启 必须应用内的定位开关和手机的定位开关都开启 否则提示用户或者跳转到权限界面
            boolean isUseLoc = SinvieUserInfo.getInstance().isOpenLocation()
                    && isLocServiceEnable(context);
            if (isUseLoc) {
                mCbLocation.setChecked(true);
            }
        }

    }

    @Override
    public void getContactorDataFail(int errCode, String errMsg) {
        SinvieToastUtil.toastLongErrorMessage(errCode, errMsg);
    }

    /**
     * 初始化 tips dialog
     */
    private void showTipsDialog() {
        //初始化提示dialog
        SinvieTipDialog mSinvieTipDialog = SinvieTipDialog.newInstance(getString(R.string.sinvie_tips_dialog_title_tips),
                getString(R.string.sinvie_tips_dialog_sign_out),
                getString(R.string.sinvie_tips_dialog_cancel),
                getString(R.string.sinvie_tips_dialog_exit));
        mSinvieTipDialog.setOnRightClickListener(dialog -> {
            //关闭dialog
            dialog.dismiss();
            //退出登录
            Intent intent = new Intent(SinvieIntentConstant.LOGIN_FOR_DEV_ACTIVITY_ACTION);
            intent.setFlags(FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(SinvieConstants.LOGOUT, true);
            SinvieActivityUtil.startActivity(context, intent);
        });
        mSinvieTipDialog.show(getChildFragmentManager(), "permission_dialog");
    }

    private void setHideCallTvIv(String str, TextView tv, ImageView iv) {
        if (str == null || str.isEmpty()) {
            tv.setText("");
            iv.setVisibility(View.GONE);
        } else {
            tv.setText(str);
            iv.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 拨打电话
     *
     * @param phoneNum
     */
    private void callPhone(String phoneNum) {
        if (phoneNum == null || phoneNum.isEmpty()) {
            return;
        }
        XXPermissions.with(context)
                .permission(Permission.CALL_PHONE)
                .request(new OnPermissionCallback() {
                    @Override
                    public void onGranted(List<String> permissions, boolean all) {
                        if (all) {
                            //打电话
                            Intent intent = new Intent(Intent.ACTION_DIAL);
                            Uri data = Uri.parse("tel:" + phoneNum);
                            intent.setData(data);
                            startActivity(intent);
                        } else {
                            SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_tips));
                            XXPermissions.startPermissionActivity(context,
                                    permissions);
                        }
                    }

                    @Override
                    public void onDenied(List<String> permissions, boolean never) {
                        if (never) {
                            SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_never));
                        } else {
                            SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_fail));
                        }
                        XXPermissions.startPermissionActivity(context,
                                permissions);
                    }
                });
    }

    /**
     * 请求权限
     */
    private void requestLocPermission() {
        XXPermissions.with(context)
                .permission(Permission.ACCESS_FINE_LOCATION)
                .permission(Permission.ACCESS_COARSE_LOCATION)
                .permission(Permission.ACCESS_MEDIA_LOCATION)
                .request(new OnPermissionCallback() {
                    @Override
                    public void onGranted(List<String> permissions, boolean all) {
                        if (all) {
                            //开启定位
                            mCbLocation.setChecked(true);
                            //存储定位
                            SinvieUserInfo.getInstance().setOpenLocation(true);
                        } else {
                            SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_tips));
                            XXPermissions.startPermissionActivity(context,
                                    permissions);
                        }
                    }

                    @Override
                    public void onDenied(List<String> permissions, boolean never) {
                        if (never) {
                            SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_never));
                        } else {
                            SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_fail));
                        }
                        XXPermissions.startPermissionActivity(context,
                                permissions);
                    }
                });
    }

}