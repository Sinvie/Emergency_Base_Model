package com.sinvie.emergency_base.ui.menu;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.logic.start_c2c_chat.SinvieStartC2CChatContact;
import com.sinvie.emergency_base.logic.start_c2c_chat.StartC2CChatPresenter;
import com.sinvie.emergency_base.ui.base.SinvieMVPBaseActivity;

public class SinvieStartC2CChatActivity extends SinvieMVPBaseActivity<SinvieStartC2CChatContact.StartC2CChatBaseView, StartC2CChatPresenter> implements SinvieStartC2CChatContact.StartC2CChatBaseView {


    @Override
    public void initializeComposition() {
        setContentView(R.layout.activity_sinvie_start_c2_cchat);
    }

    @Override
    public void initializeData() {

    }

    @Override
    protected SinvieStartC2CChatContact.StartC2CChatBaseView createView() {
        return this;
    }

    @Override
    protected StartC2CChatPresenter createPresenter() {
        return new StartC2CChatPresenter(this);
    }
}