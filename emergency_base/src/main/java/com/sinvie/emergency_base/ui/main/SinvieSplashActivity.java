package com.sinvie.emergency_base.ui.main;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.hjq.permissions.OnPermissionCallback;
import com.hjq.permissions.Permission;
import com.hjq.permissions.XXPermissions;
import com.huawei.ecterminalsdk.base.TsdkLocalAddress;
import com.huawei.ecterminalsdk.models.TsdkManager;
import com.sinvie.emergency_base.BuildConfig;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.logic.call.SinvieCallFunc;
import com.sinvie.emergency_base.logic.conference.SinvieConfFunc;
import com.sinvie.emergency_base.logic.login.SinvieLoginFunc;
import com.sinvie.emergency_base.ui.SinvieIntentConstant;
import com.sinvie.emergency_base.ui.login.SinvieUserInfo;
import com.sinvie.emergency_base.utils.SinvieActivityUtil;
import com.sinvie.emergency_base.utils.SinvieDemoLog;
import com.sinvie.emergency_base.utils.SinvieFileUtil;
import com.sinvie.emergency_base.utils.SinvieZipUtil;
import com.sinvie.emergency_conf.CallService.SinvieCallMgr;
import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_conf.CommonService.util.SinvieDeviceManager;
import com.sinvie.emergency_conf.ConferenceService.SinvieConfConvertUtil;
import com.sinvie.emergency_conf.ConferenceService.SinvieMeetingMgr;
import com.sinvie.emergency_conf.LoginService.SinvieLoginMgr;
import com.sinvie.emergency_conf.ServiceMgr.SinvieServiceMgr;
import com.sinvie.emergency_live.tuikit.SinvieTUIKit;
import com.sinvie.emergency_live.tuikit.base.ISinvieUIKitCallBack;
import com.sinvie.emergency_live.tuikit.modules.chat.base.SinvieOfflineMessageBean;
import com.sinvie.emergency_live.tuikit.utils.SinvieToastUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.List;


public class SinvieSplashActivity extends AppCompatActivity {
    private static final String TAG = SinvieSplashActivity.class.getSimpleName();
    //间隔跳转时间
    private static final int SPLASH_TIME = 1500;
    //预期文件长度
    private static final int EXPECTED_FILE_LENGTH = 7;
    private SinvieUserInfo mSinvieUserInfo;
    private View mFlashView;
    private int mIdoProtocol = 0;
    //sdk 30 以上的读写权限
    private String[] versionPermission = {Permission.MANAGE_EXTERNAL_STORAGE};
    //读写权限
    private String[] writeReadPermission = {Permission.READ_EXTERNAL_STORAGE, Permission.WRITE_EXTERNAL_STORAGE};
    //电话
    private String[] phonePermission = {Permission.CALL_PHONE};
    //相机
    private String[] cameraPermission = {Permission.CAMERA};
    //麦克风
    private String[] MicPermission = {Permission.RECORD_AUDIO};
    //通讯录
    private String[] contactsPermission = {Permission.READ_CONTACTS};
    //通话记录
    private String[] callPermission = {Permission.READ_CALL_LOG, Permission.WRITE_CALL_LOG};
    //日历
    private String[] calendarPermission = {Permission.READ_CALENDAR, Permission.WRITE_CALENDAR,};
    //安装包权限
    private String[] packagePermission = {Permission.REQUEST_INSTALL_PACKAGES};

    private MyHandler mMyHandler;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //判断读写权限
        if (checkAndroidSDKVersion()) {
            //低版本
            if (!XXPermissions.isGranted(this, phonePermission)) {
                SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_tips_dialog_permission_tips));
                return;
            }
        } else {
            //高版本
            if (!XXPermissions.isGranted(this, versionPermission)) {
                SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_tips_dialog_permission_tips));
                return;
            }
        }
        if (XXPermissions.isGranted(this, phonePermission) &&
                XXPermissions.isGranted(this, cameraPermission) &&
                XXPermissions.isGranted(this, MicPermission) &&
                XXPermissions.isGranted(this, contactsPermission) &&
                XXPermissions.isGranted(this, callPermission) &&
                XXPermissions.isGranted(this, calendarPermission) &&
                XXPermissions.isGranted(this, packagePermission)) {
            //跳转登录界面
            startLogin();
        } else {
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sinvie_splash);
        //全屏
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);

        initView();

    }

    private void initView() {
        mFlashView = findViewById(R.id.flash_view);

        mSinvieUserInfo = SinvieUserInfo.getInstance();
        //判断登录状态 如果已经登录 就自动登录 如果未登录 就跳转到登录界面
//        handleData();
        //在此界面动态申请权限然后初始化华为设备信息
//        requestOtherPermission();
        mMyHandler = new MyHandler(this);
        mMyHandler.postAtTime(() -> mMyHandler.sendEmptyMessage(200), 2000);
    }

    private void handleData() {
        if (mSinvieUserInfo != null && mSinvieUserInfo.isAutoLogin()) {
            login();
        } else {
            mFlashView.postDelayed(this::startLogin, SPLASH_TIME);
        }
    }

    private void login() {
//        SinvieDemoLog.d(TAG, "onClick: loginName : " + mSinvieUserInfo.getUserId() + ", userSig : " + mSinvieUserInfo.getUserSig());
        SinvieTUIKit.login(mSinvieUserInfo.getUserId(), mSinvieUserInfo.getUserSig(), new ISinvieUIKitCallBack() {
            @Override
            public void onError(String module, final int code, final String desc) {
                runOnUiThread(() -> {
                    SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_login_tip_failed) + ", " + "errCode = " + code + ", errInfo = " + desc);
                    //TUIKIT登录失败 跳转到手动登录界面
                    startLogin();
                });
//                SinvieDemoLog.i(TAG, "imLogin errorCode = " + code + ", errorInfo = " + desc);
            }

            @Override
            public void onSuccess(Object data) {
                startMain();
            }
        });
    }

    private void startLogin() {
        SinvieActivityUtil.startActivity(this, SinvieIntentConstant.LOGIN_FOR_DEV_ACTIVITY_ACTION);
        finish();
    }

    private void startMain() {
//        SinvieDemoLog.i(TAG, "startMain");
        //先检测离线消息
//        SinvieOfflineMessageBean bean = SinvieOfflineMessageDispatcher.parseOfflineMessage(getIntent());
//        if (bean != null) {
//            SinvieDemoLog.i(TAG, "startMain offlinePush bean is " + bean);
//            SinvieOfflineMessageDispatcher.redirect(bean);
//            finish();
//            return;
//        }
//        SinvieDemoLog.i(TAG, "startMain offlinePush bean is null");
        //如果没有离线消息 就跳转到主界面
        SinvieActivityUtil.startActivity(this, SinvieIntentConstant.MAIN_ACTIVITY_ACTION);
        finish();
    }

    /******************************************* **华为初始化*** *******************************/
    public void initHWConference() {

//        UserInfo.getInstance().initHwLoginData();
        SinvieUserInfo info = SinvieUserInfo.getInstance();
        //初始化服务器地址/端口 为空
//        info.setTupRegisterServer("");
//        info.setTupPort("");
        //存储华为的服务登录信息 (其实是初始化)
        info.setTupSipUri("");
        info.setTupSrtp(0);
        info.setSipTransport(1);
//        info.setApplyConfigPriority(1);
        info.setSecurityTunnel(0);
        info.setUdpPort("");
        info.setTlsPort("");
        info.setTupVpn(true);
        info.setPortConfigPriority(1);
        info.setConfCtrlProtocol(0);

        initConfCtrlProtocol();

        // /data/app/com.sinvie.emergency_live-P7Uzf5OiTpLugLJX_CUTRg==/lib/arm64
        // SDK中.so库的加载路径
        String appPath = getApplicationInfo().nativeLibraryDir;
        SinvieServiceMgr.getServiceMgr().startService(SinvieLocContext.getContext(), appPath, mIdoProtocol);

        SinvieLoginMgr.getInstance().regLoginEventNotification(SinvieLoginFunc.getInstance());
        SinvieCallMgr.getInstance().regCallServiceNotification(SinvieCallFunc.getInstance());
        SinvieMeetingMgr.getInstance().regConfServiceNotification(SinvieConfFunc.getInstance());

        SinvieDemoLog.d("UserInfo -->", new Gson().toJson(SinvieUserInfo.getInstance()));
        //初始化与去初始化业务组件类 设置安全配置
        SinvieServiceMgr.getServiceMgr().securityParam(SinvieUserInfo.getInstance().getTupSrtp(), 1);

        //初始化与去初始化业务组件类 设置网络配置
        SinvieServiceMgr.getServiceMgr().networkParam(
                SinvieUserInfo.getInstance().getTupPort(),
                SinvieUserInfo.getInstance().getTupRegisterServer(),
                SinvieUserInfo.getInstance().getSipTransport());
        new Thread(() -> {
            //设置本端IP
            String localIpAddress =
                    SinvieDeviceManager.getLocalIpAddress(SinvieUserInfo.getInstance().isTupVpn());
            TsdkLocalAddress localAddress = new TsdkLocalAddress(localIpAddress);
            TsdkManager.getInstance().setConfigParam(localAddress);
        }).start();
//        //初始化资源文件
        initResourceFile();
    }

    private void initConfCtrlProtocol() {
        mIdoProtocol = SinvieUserInfo.getInstance().getConfCtrlProtocol();
        SinvieMeetingMgr.getInstance().setConfProtocol(SinvieConfConvertUtil.convertConfctrlProtocol(mIdoProtocol));
    }

    private void initResourceFile() {
        new Thread(this::initDataConfRes).start();
    }

    private void initDataConfRes() {
        String path = SinvieLocContext.getContext().getFilesDir() + "/AnnoRes";
        File file = new File(path);
        if (file.exists()) {
            SinvieDemoLog.i(TAG, file.getAbsolutePath());
            File[] files = file.listFiles();
            if (null != files && EXPECTED_FILE_LENGTH == files.length) {
                return;
            } else {
                SinvieFileUtil.deleteFile(file);
            }
        }

        try {
            InputStream inputStream = getAssets().open("AnnoRes.zip");
            SinvieZipUtil.unZipFile(inputStream, path);
        } catch (IOException e) {
//            LogUtil.i(UIConstants.DEMO_TAG, "close...Exception->e" + e.toString());
        }
    }

    /**
     * 权限检测
     */
    private void requestOtherPermission() {

        XXPermissions request = XXPermissions.with(this);
        if (checkAndroidSDKVersion()) {
            request.permission(writeReadPermission)
                    .permission(phonePermission)
                    .permission(cameraPermission)
                    .permission(MicPermission)
                    .permission(contactsPermission)
                    .permission(callPermission)
                    .permission(calendarPermission)
                    .permission(packagePermission);
        } else {
            request.permission(versionPermission);
        }
        request.request(new OnPermissionCallback() {
            @Override
            public void onGranted(List<String> permissions, boolean all) {
                if (all) {
                    initHWConference();
                    startLogin();
                } else {
                    SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_tips));
                    XXPermissions.startPermissionActivity(SinvieSplashActivity.this,
                            permissions);
                }
            }

            @Override
            public void onDenied(List<String> permissions, boolean never) {
                if (never) {
                    SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_never));
                } else {
                    SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_fail));
                }
                XXPermissions.startPermissionActivity(SinvieSplashActivity.this,
                        permissions);
            }
        });
    }

    private boolean checkAndroidSDKVersion() {
        return Build.VERSION.SDK_INT <= Build.VERSION_CODES.Q;
    }

    /******************************************* **华为初始化 end *******************************/

    private class MyHandler extends Handler {
        private WeakReference<SinvieSplashActivity> mWeakReference;

        public MyHandler(SinvieSplashActivity context) {
            mWeakReference = new WeakReference<SinvieSplashActivity>(context);
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            //收到handler
            mWeakReference.get().requestOtherPermission();
        }
    }
}