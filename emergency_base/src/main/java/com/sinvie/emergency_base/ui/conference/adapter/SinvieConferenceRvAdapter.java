package com.sinvie.emergency_base.ui.conference.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.sinvie.emergency_conf.ConferenceService.SinvieConfBaseInfo;
import com.sinvie.emergency_conf.ConferenceService.SinvieConfConstant;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.utils.SinvieDemoLog;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/8
 * @Des: 类描述
 */
public class SinvieConferenceRvAdapter extends RecyclerView.Adapter<SinvieConferenceRvAdapter.ConferenceViewHolder> {

    private static final String TAG = SinvieConferenceRvAdapter.class.getSimpleName();

    private Context mContext;
    private List<SinvieConfBaseInfo> mConferenceBeanList;

    private ItemConferenceRvClick mItemConferenceRvClick;

    public SinvieConferenceRvAdapter(Context context, List<SinvieConfBaseInfo> conferenceBeanList) {
        mContext = context;
        mConferenceBeanList = conferenceBeanList;
    }

    public void setItemConferenceRvClick(ItemConferenceRvClick itemConferenceRvClick) {
        mItemConferenceRvClick = itemConferenceRvClick;
    }

    @NonNull
    @NotNull
    @Override
    public ConferenceViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent,
                                                   int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_sinvie_conference, parent, false);
        return new ConferenceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ConferenceViewHolder holder, int position) {
        holder.bind(position, mConferenceBeanList.get(position));
    }

    @Override
    public int getItemCount() {
        return mConferenceBeanList.size();
    }

    public interface ItemConferenceRvClick {
        void onItemConferenceRvClick(int position, SinvieConfBaseInfo bean);
    }

    public class ConferenceViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvItemConferenceStatus;
        private TextView mTvItemConferenceTitle;
        private TextView mTvItemConferenceInitiator;
        private TextView mTvItemConferenceDataTime;

        public ConferenceViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            mTvItemConferenceStatus = itemView.findViewById(R.id.tv_item_conference_status);
            mTvItemConferenceTitle = itemView.findViewById(R.id.tv_item_conference_title);
            mTvItemConferenceInitiator = itemView.findViewById(R.id.tv_item_conference_initiator);
            mTvItemConferenceDataTime = itemView.findViewById(R.id.tv_item_conference_data_time);
        }


        public void bind(int position, SinvieConfBaseInfo bean) {
            SinvieDemoLog.d(TAG,"ConferenceRvAdapter Bean ==>" + new Gson().toJson(bean));
            //会议中 只要列表有数据 就是在会议中
            mTvItemConferenceStatus.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_sinvie_conference_status_doing));
            mTvItemConferenceStatus.setTextColor(ContextCompat.getColor(mContext, R.color.sinvie_theme_blue_color));
            mTvItemConferenceStatus.setText(mContext.getString(R.string.sinvie_item_conference_status_doing));

            if (bean.getConfState() == SinvieConfConstant.ConfConveneStatus.GOING){

            }else if (bean.getConfState() == SinvieConfConstant.ConfConveneStatus.CREATING){
                //正在创建

            }else if (bean.getConfState() == SinvieConfConstant.ConfConveneStatus.DESTROYED){
                //结束
                mTvItemConferenceStatus.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_sinvie_conference_status_finish));
                mTvItemConferenceStatus.setTextColor(ContextCompat.getColor(mContext, R.color.sinvie_theme_gray_color));
                mTvItemConferenceStatus.setText(mContext.getString(R.string.sinvie_item_conference_status_finish));
            }else if (bean.getConfState() == SinvieConfConstant.ConfConveneStatus.SCHEDULE){
                //

            }else{
                //unknown

            }

            mTvItemConferenceTitle.setText(String.format(mContext.getString(R.string.sinvie_meeting_title),bean.getSubject(),bean.getSize()));
            String initiator = String.format(mContext.getString(R.string.sinvie_tv_conference_initiator)  , "Admin");
            mTvItemConferenceInitiator.setText(initiator);
            String dataTime = String.format(mContext.getString(R.string.sinvie_tv_conference_data_time), bean.getStartTime(), bean.getRemainTime()+"");
            mTvItemConferenceDataTime.setText(dataTime);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mItemConferenceRvClick.onItemConferenceRvClick(position, bean);
                }
            });
        }
    }
}
