package com.sinvie.emergency_base.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.helper.SinvieIBaseLiveListener;
import com.sinvie.emergency_base.helper.SinvieTUIKitLiveListenerManager;
import com.sinvie.emergency_base.logic.main.SinvieIMainContact;
import com.sinvie.emergency_base.logic.main.SinvieMainPresenter;
import com.sinvie.emergency_base.ui.base.SinvieMVPBaseActivity;
import com.sinvie.emergency_base.ui.constants.SinvieContactFragment;
import com.sinvie.emergency_base.ui.conversation.SinvieConversationFragment;
import com.sinvie.emergency_base.ui.profile.SinvieProfileFragment;
import com.sinvie.emergency_base.ui.task.SinvieTaskFragment;
import com.sinvie.emergency_base.ui.workbench.SinvieWorkbenchFragment;
import com.sinvie.emergency_base.utils.SinvieBrandUtil;
import com.sinvie.emergency_base.utils.SinvieConstants;
import com.sinvie.emergency_base.utils.SinvieDemoLog;
import com.sinvie.emergency_base.utils.SinviePrivateConstants;
import com.sinvie.emergency_live.tuikit.component.SinvieUnreadCountTextView;
import com.sinvie.emergency_live.tuikit.modules.chat.SinvieGroupChatManagerKit;
import com.sinvie.emergency_live.tuikit.modules.conversation.SinvieConversationManagerKit;
import com.sinvie.emergency_live.tuikit.utils.SinvieFileUtil;

public class SinvieMainActivity extends SinvieMVPBaseActivity<SinvieIMainContact.MainBaseView, SinvieMainPresenter> implements
        SinvieConversationManagerKit.MessageUnreadWatcher, SinvieIMainContact.MainBaseView {
    private static final String TAG = SinvieMainActivity.class.getSimpleName();

    private BottomNavigationView mBottomNavigationView;

    private SinvieUnreadCountTextView mMsgTotalUnread = null;

    /**
     * 记录最后一次选中 的 Fragment
     */
    private int mLastTabId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //初始化第三方推送
//        prepareThirdPushToken();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        //初始化第三方推送
//        prepareThirdPushToken();
    }


    @Override
    protected void onResume() {
        super.onResume();

        //在构建界面显示的时候处理离线消息
        handleOfflinePush();
    }

    @Override
    public void initializeData() {


    }

    @Override
    public void initializeComposition() {
        setContentView(R.layout.activity_sinvie_main);
        mMsgTotalUnread = new SinvieUnreadCountTextView(this);
        mBottomNavigationView = findViewById(R.id.bnv_main_menu);
        mBottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            //根据选择的item 显示对应的fragment
            setShowFragment(item.getItemId());
            return true;
        });
        mBottomNavigationView.setItemIconTintList(null);
        initBottomNavigationView();
        // 从application移入到这里，原因在于首次装上app
        // ，需要获取一系列权限，如创建文件夹，图片下载需要指定创建好的文件目录，否则会下载本地失败，聊天页面从而获取不到图片、表情
        SinvieFileUtil.initPath();

        // 未读消息监视器
        SinvieConversationManagerKit.getInstance().addUnreadWatcher(this);

        //群组聊天管理
        SinvieGroupChatManagerKit.getInstance();

        // 初始化时，强制切换tab到上一次的位置
        if (mLastTabId == -1) {
//            mBottomNavigationView.setSelectedItemId(R.id.tab_conversation);
//            setShowFragment(0);
            mBottomNavigationView.setSelectedItemId(R.id.tab_workbench);
            setShowFragment(2);
        } else {
            int i = mLastTabId;
            mLastTabId = -1;
            mBottomNavigationView.setSelectedItemId(i);
            setShowFragment(i);
        }

    }

    /**
     * 初始化 BottomNavigationView
     */
    private void initBottomNavigationView() {

//        addBottomNavigationViewMenu();
        //绑定未读消息 View
        //获取整个的NavigationView
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) mBottomNavigationView.getChildAt(0);
        //这里就是获取所添加的每一个Tab(或者叫menu)，
        View tab = menuView.getChildAt(0);
        BottomNavigationItemView itemView = (BottomNavigationItemView) tab;

        //添加到Tab上
        itemView.addView(mMsgTotalUnread);
        //无消息时可以将它隐藏即可
        mMsgTotalUnread.setVisibility(View.VISIBLE);
    }

    /**
     * 动态添加数据
     */
    private void addBottomNavigationViewMenu() {
        //tab 标题
        int[] title = {R.string.sinvie_tab_info_tab_text, R.string.sinvie_tab_task_tab_text,
                R.string.sinvie_tab_workbench_tab_text, R.string.sinvie_tab_contacts_tab_text,
                R.string.sinvie_tab_myself_tab_text};
        //tab 图片
        int[] tabImg = {R.drawable.selector_sinvie_tab_conversation, R.drawable.selector_sinvie_tab_task,
                R.drawable.selector_sinvie_tab_workbench, R.drawable.selector_sinvie_tab_contact,
                R.drawable.select_sinvie_tab_myself};
        //tab 类型
        int[] tabType = {SinvieUIConstants.TAB_CONVERSATION_TYPE, SinvieUIConstants.TAB_TASK_TYPE,
                SinvieUIConstants.TAB_WORKBENCH, SinvieUIConstants.TAB_CONTACT_TYPE,
                SinvieUIConstants.TAB_MYSELF_TYPE};

        Menu menu = mBottomNavigationView.getMenu();
        for (int i : tabType) {
            //设置菜单标题
            menu.add(0, i, i, title[i]);
            MenuItem item = menu.findItem(i);
            //设置菜单图片
            item.setIcon(tabImg[i]);
        }
    }

    @Override
    public void updateUnread(int count) {
        if (count > 0) {
            mMsgTotalUnread.setVisibility(View.VISIBLE);
        } else {
            mMsgTotalUnread.setVisibility(View.GONE);
        }
        String unreadStr = "" + count;
        if (count > 100) {
            unreadStr = "99+";
        }
        mMsgTotalUnread.setText(unreadStr);
        // 华为离线推送角标
//        SinvieHUAWEIHmsMessageService.updateBadge(this, count);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void setShowFragment(int id) {
        //防止重复跳转
        if (mLastTabId != -1 && mLastTabId == id) {
            return;
        }
        mLastTabId = id;
        //获取选择的fragment
        Fragment current = getTabMenuSelectFragment(id);

        if (current != null && !current.isAdded()) {
            getSupportFragmentManager().beginTransaction().replace(R.id.empty_view,
                    current).commitAllowingStateLoss();
            getSupportFragmentManager().executePendingTransactions();
        } else {
            SinvieDemoLog.w(TAG, "fragment added!");
        }
    }

    /**
     * 获取所要切换的fragment
     *
     * @param id 当前tab id
     * @return
     */
    private Fragment getTabMenuSelectFragment(int id) {
        SinvieDemoLog.d(TAG, "NavigationBottomID: " + id);
        if (id == R.id.tab_conversation) {
            //消息界面
            return new SinvieConversationFragment();
        } else if (id == R.id.tab_task) {
            //任务界面
            return new SinvieTaskFragment();
        } else if (id == R.id.tab_workbench) {
            //工作台界面
            return new SinvieWorkbenchFragment();
        } else if (id == R.id.tab_contact) {
            //通讯录界面
            return new SinvieContactFragment();
        } else if (id == R.id.tab_myself) {
            //我 界面
            return new SinvieProfileFragment();
        } else {
            return null;
        }
    }

    private void handleOfflinePush() {
        boolean isFromOfflinePush = getIntent().getBooleanExtra(SinvieConstants.IS_OFFLINE_PUSH_JUMP,
                false);
        if (isFromOfflinePush) {
            SinvieIBaseLiveListener baseLiveListener =
                    SinvieTUIKitLiveListenerManager.getInstance().getBaseCallListener();
            if (baseLiveListener != null) {
                baseLiveListener.handleOfflinePushCall(getIntent());
            }
        }
    }

    @Override
    protected void onDestroy() {
        SinvieConversationManagerKit.getInstance().destroyConversation();
        mLastTabId = -1;
        super.onDestroy();

    }

    @Override
    protected SinvieIMainContact.MainBaseView createView() {
        return this;
    }

    @Override
    protected SinvieMainPresenter createPresenter() {
        return new SinvieMainPresenter(this);
    }


    /**
     * 准备第三方推送
     */
    private void prepareThirdPushToken() {
//        SinvieThirdPushTokenMgr.getInstance().setPushTokenToTIM();
//
//        if (SinvieBrandUtil.isBrandHuawei()) {
//            // 华为离线推送
//            new Thread() {
//                @Override
//                public void run() {
//                    try {
//                        // read from agconnect-services.json
//                        String appId =
//                                AGConnectServicesConfig.fromContext(SinvieMainActivity.this).getString(
//                                        "client/app_id");
//                        String token =
//                                HmsInstanceId.getInstance(SinvieMainActivity.this).getToken(appId, "HCM");
//                        SinvieDemoLog.i(TAG, "huawei get token:" + token);
//                        if (!TextUtils.isEmpty(token)) {
//                            SinvieThirdPushTokenMgr.getInstance().setThirdPushToken(token);
//                            SinvieThirdPushTokenMgr.getInstance().setPushTokenToTIM();
//                        }
//                    } catch (ApiException e) {
//                        SinvieDemoLog.e(TAG, "huawei get token failed, " + e);
//                    }
//                }
//            }.start();
//        } else if (SinvieBrandUtil.isBrandVivo()) {
//            // vivo离线推送
//            SinvieDemoLog.i(TAG,
//                    "vivo support push: " + PushClient.getInstance(getApplicationContext()).isSupport());
//            PushClient.getInstance(getApplicationContext()).turnOnPush(new IPushActionListener() {
//                @Override
//                public void onStateChanged(int state) {
//                    if (state == 0) {
//                        String regId = PushClient.getInstance(getApplicationContext()).getRegId();
//                        SinvieDemoLog.i(TAG, "vivopush open vivo push success regId = " + regId);
//                        SinvieThirdPushTokenMgr.getInstance().setThirdPushToken(regId);
//                        SinvieThirdPushTokenMgr.getInstance().setPushTokenToTIM();
//                    } else {
//                        // 根据vivo推送文档说明，state = 101 表示该vivo机型或者版本不支持vivo推送，链接：https://dev.vivo
//                        // .com.cn/documentCenter/doc/156
//                        SinvieDemoLog.i(TAG, "vivopush open vivo push fail state = " + state);
//                    }
//                }
//            });
//        } else if (HeytapPushManager.isSupportPush()) {
//            // oppo离线推送
//            SinvieOPPOPushImpl oppo = new SinvieOPPOPushImpl();
//            oppo.createNotificationChannel(this);
//            HeytapPushManager.register(this, SinviePrivateConstants.OPPO_PUSH_APPKEY,
//                    SinviePrivateConstants.OPPO_PUSH_APPSECRET, oppo);
//        }
    }
}