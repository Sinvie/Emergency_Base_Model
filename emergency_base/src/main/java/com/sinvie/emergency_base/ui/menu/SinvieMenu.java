package com.sinvie.emergency_base.ui.menu;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_live.tuikit.component.action.SinviePopActionClickListener;
import com.sinvie.emergency_live.tuikit.component.action.SinviePopMenuAction;
import com.sinvie.emergency_live.tuikit.component.action.SinviePopMenuAdapter;
import com.sinvie.emergency_live.tuikit.utils.SinvieScreenUtil;
import com.sinvie.emergency_live.tuikit.utils.SinvieTUIKitConstants;
import com.sinvie.emergency_base.utils.SinvieActivityUtil;
import com.sinvie.emergency_base.ui.SinvieIntentConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * @description 消息队列右上角菜单界面
 * @param
 * @return
 * @author JonsonBob
 * @time 2021/6/21 15:05
 */
public class SinvieMenu {

    public static final int MENU_TYPE_CONTACT = 1;
    public static final int MENU_TYPE_CONVERSATION = 2;

    // 更多menu
    private ListView mMenuList;
    private SinviePopMenuAdapter mMenuAdapter;
    private PopupWindow mMenuWindow;
    private List<SinviePopMenuAction> mActions = new ArrayList<>();
    private Activity mActivity;
    private View mAttachView;

    public SinvieMenu(Activity activity, View attach, int menuType) {
        mActivity = activity;
        mAttachView = attach;

        initActions(menuType);
    }

    private void initActions(int menuType) {
        SinviePopActionClickListener popActionClickListener = new SinviePopActionClickListener() {
            @Override
            public void onActionClick(int position, Object data) {
                SinviePopMenuAction action = (SinviePopMenuAction) data;
                if (TextUtils.equals(action.getActionName(), mActivity.getResources().getString(R.string.sinvie_add_friend))) {
                    Intent intent = new Intent(SinvieIntentConstant.ADD_MORE_ACTIVITY_ACTION);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(SinvieTUIKitConstants.GroupType.GROUP, false);
                    SinvieActivityUtil.startActivity(SinvieLocContext.getContext(),intent);
                }
                if (TextUtils.equals(action.getActionName(), mActivity.getResources().getString(R.string.sinvie_add_group))) {
                    Intent intent = new Intent(SinvieIntentConstant.ADD_MORE_ACTIVITY_ACTION);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(SinvieTUIKitConstants.GroupType.GROUP, true);
                    SinvieActivityUtil.startActivity(SinvieLocContext.getContext(),intent);
                }
                if (TextUtils.equals(action.getActionName(), mActivity.getResources().getString(R.string.sinvie_start_conversation))) {
                    Intent intent = new Intent(SinvieIntentConstant.START_C2C_CHAT_ACTIVITY_ACTION);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    SinvieActivityUtil.startActivity(SinvieLocContext.getContext(),intent);
                }

                if (TextUtils.equals(action.getActionName(), mActivity.getResources().getString(R.string.sinvie_create_private_group))) {
                    Intent intent = new Intent(SinvieIntentConstant.START_GROUP_CHAT_ACTIVITY_ACTION);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(SinvieTUIKitConstants.GroupType.TYPE, SinvieTUIKitConstants.GroupType.PRIVATE);
                    SinvieActivityUtil.startActivity(SinvieLocContext.getContext(),intent);
                }
                if (TextUtils.equals(action.getActionName(), mActivity.getResources().getString(R.string.sinvie_create_group_chat))) {
                    Intent intent = new Intent(SinvieIntentConstant.START_GROUP_CHAT_ACTIVITY_ACTION);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(SinvieTUIKitConstants.GroupType.TYPE, SinvieTUIKitConstants.GroupType.PUBLIC);
                    SinvieActivityUtil.startActivity(SinvieLocContext.getContext(),intent);
                }
                if (TextUtils.equals(action.getActionName(), mActivity.getResources().getString(R.string.sinvie_create_chat_room))) {
                    Intent intent = new Intent(SinvieIntentConstant.START_GROUP_CHAT_ACTIVITY_ACTION);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(SinvieTUIKitConstants.GroupType.TYPE, SinvieTUIKitConstants.GroupType.CHAT_ROOM);
                    SinvieActivityUtil.startActivity(SinvieLocContext.getContext(),intent);
                }
                mMenuWindow.dismiss();
            }
        };

        // 设置右上角+号显示PopAction
        List<SinviePopMenuAction> menuActions = new ArrayList<SinviePopMenuAction>();

        SinviePopMenuAction action = new SinviePopMenuAction();

        if (menuType == MENU_TYPE_CONVERSATION) {
            action.setActionName(mActivity.getResources().getString(R.string.sinvie_start_conversation));
            action.setActionClickListener(popActionClickListener);
            action.setIconResId(R.mipmap.icon_sinvie_create_c2c);
            menuActions.add(action);
        }

        if (menuType == MENU_TYPE_CONTACT) {
            action = new SinviePopMenuAction();
            action.setActionName(mActivity.getResources().getString(R.string.sinvie_add_friend));
            action.setIconResId(R.drawable.ic_sinvie_group_new_friend);
            action.setActionClickListener(popActionClickListener);
            menuActions.add(action);

            action = new SinviePopMenuAction();
            action.setActionName(mActivity.getResources().getString(R.string.sinvie_add_group));
            action.setIconResId(R.mipmap.icon_sinvie_contact_join_group);
            action.setActionClickListener(popActionClickListener);
            menuActions.add(action);
        }

        if (menuType == MENU_TYPE_CONTACT) {
            mActions.clear();
            mActions.addAll(menuActions);
            return;
        }

        action = new SinviePopMenuAction();
        action.setActionName(mActivity.getResources().getString(R.string.sinvie_create_private_group));
        action.setIconResId(R.drawable.ic_sinvie_group);
        action.setActionClickListener(popActionClickListener);
        menuActions.add(action);

        action = new SinviePopMenuAction();
        action.setActionName(mActivity.getResources().getString(R.string.sinvie_create_group_chat));
        action.setIconResId(R.drawable.ic_sinvie_group);
        action.setActionClickListener(popActionClickListener);
        menuActions.add(action);

        action = new SinviePopMenuAction();
        action.setActionName(mActivity.getResources().getString(R.string.sinvie_create_chat_room));
        action.setIconResId(R.drawable.ic_sinvie_group);
        action.setActionClickListener(popActionClickListener);
        menuActions.add(action);

        mActions.clear();
        mActions.addAll(menuActions);
    }

    public boolean isShowing() {
        if (mMenuWindow == null) {
            return false;
        }
        return mMenuWindow.isShowing();
    }

    public void hide() {
        mMenuWindow.dismiss();
    }

    public void show() {
        if (mActions == null || mActions.size() == 0) {
            return;
        }
        mMenuWindow = new PopupWindow(mActivity);
        mMenuAdapter = new SinviePopMenuAdapter();
        mMenuAdapter.setDataSource(mActions);
        View menuView = LayoutInflater.from(mActivity).inflate(R.layout.view_sinvie_conversation_pop_menu, null);
        // 设置布局文件
        mMenuWindow.setContentView(menuView);

        mMenuList = menuView.findViewById(R.id.conversation_pop_list);
        mMenuList.setAdapter(mMenuAdapter);
        mMenuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SinviePopMenuAction action = (SinviePopMenuAction) mMenuAdapter.getItem(position);
                if (action != null && action.getActionClickListener() != null) {
                    action.getActionClickListener().onActionClick(position, mActions.get(position));
                }
            }
        });
        mMenuWindow.setWidth(SinvieScreenUtil.getPxByDp(160));
        mMenuWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        mMenuWindow.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.bg_sinvie_top_pop));
        // 设置pop获取焦点，如果为false点击返回按钮会退出当前Activity，如果pop中有Editor的话，focusable必须要为true
        mMenuWindow.setFocusable(true);
        // 设置pop可点击，为false点击事件无效，默认为true
        mMenuWindow.setTouchable(true);
        // 设置点击pop外侧消失，默认为false；在focusable为true时点击外侧始终消失
        mMenuWindow.setOutsideTouchable(true);
        backgroundAlpha(0.5f);
        // 相对于 + 号正下面，同时可以设置偏移量
        mMenuWindow.showAtLocation(mAttachView, Gravity.RIGHT | Gravity.TOP, SinvieScreenUtil.getPxByDp(15), SinvieScreenUtil.getPxByDp(80));
        // 设置pop关闭监听，用于改变背景透明度
        mMenuWindow.setOnDismissListener(() -> backgroundAlpha(1.0f));
    }

    /**
     * 此方法用于改变背景的透明度，从而达到“变暗”的效果
     */
    private void backgroundAlpha(float bgAlpha) {
        WindowManager.LayoutParams lp = mActivity.getWindow().getAttributes();
        // 0.0-1.0
        lp.alpha = bgAlpha;
        mActivity.getWindow().setAttributes(lp);
        // everything behind this window will be dimmed.
        // 此方法用来设置浮动层，防止部分手机变暗无效
        mActivity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }
}
