package com.sinvie.emergency_base.ui.scene;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.view.WindowManager;

import com.google.gson.Gson;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieRoomInfo;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.logic.live_room.live_room_anchor.SinvieLiveRoomAnchorContact;
import com.sinvie.emergency_base.logic.live_room.live_room_anchor.SinvieLiveRoomAnchorPresenter;
import com.sinvie.emergency_live.tuikit.base.ISinvieBaseMessageSender;
import com.sinvie.emergency_live.tuikit.base.ISinvieUIKitCallBack;
import com.sinvie.emergency_live.tuikit.base.SinvieTUIListenerManager;
import com.sinvie.emergency_live.tuikit.modules.message.SinvieMessageInfo;
import com.sinvie.emergency_live.tuikit.modules.message.SinvieMessageInfoUtil;
import com.sinvie.emergency_live.tuikit.utils.SinvieToastUtil;
import com.sinvie.emergency_live.tuikit_live.liteav.model.SinvieLiveMessageInfo;
import com.sinvie.emergency_base.ui.base.SinvieMVPBaseActivity;
import com.sinvie.emergency_base.ui.login.SinvieUserInfo;
import com.sinvie.emergency_base.ui.scene.net.SinvieHeartbeatManager;
import com.sinvie.emergency_base.ui.scene.view.SinvieCustomAnchorLayout;
import com.sinvie.emergency_base.utils.SinvieActivityUtil;
import com.sinvie.emergency_base.utils.SinvieDemoLog;
import com.sinvie.emergency_base.ui.SinvieIntentConstant;
import com.sinvie.emergency_base.utils.SinvieNetGPSLocationUtils;
import com.tencent.imsdk.v2.V2TIMManager;

public class SinvieLiveRoomAnchorActivity extends SinvieMVPBaseActivity<SinvieLiveRoomAnchorContact.LiveRoomAnchorBaseView, SinvieLiveRoomAnchorPresenter>
        implements SinvieLiveRoomAnchorContact.LiveRoomAnchorBaseView {
    private static final String TAG = SinvieLiveRoomAnchorActivity.class.getSimpleName();

    private SinvieCustomAnchorLayout mTUILiveRoomAnchorLayout;

    private String mGroupID;

    public static void start(Context context, String groupID) {
        Intent starter = new Intent(SinvieIntentConstant.LIVE_ROOM_ANCHOR_ACTIVITY_ACTION);
        if (context instanceof Application) {
            starter.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        starter.putExtra(SinvieUIConstants.GROUP_ID, groupID);
        SinvieActivityUtil.startActivity(context, starter);
    }

    @Override
    public void initializeData() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setTurnScreenOn(true);
        }
    }

    @Override
    public void initializeComposition() {
        setContentView(R.layout.activity_sinvie_live_room_anchor);


        mGroupID = getIntent().getStringExtra(SinvieUIConstants.GROUP_ID);

        // 判断当前的房间类型
        mTUILiveRoomAnchorLayout = findViewById(R.id.tui_liveroom_anchor_layout);
        mTUILiveRoomAnchorLayout.setLiveRoomAnchorLayoutDelegate(mDelegate);
        //初始化主播房间
        mTUILiveRoomAnchorLayout.initWithRoomId(getSupportFragmentManager(), getRoomId());
        mTUILiveRoomAnchorLayout.enablePK(false);
    }

    /**
     * TODO 这里我们用简单的 userId hashcode，然后room id应该是您后台生成的唯一值
     *
     * @return
     */
    private int getRoomId() {
        String ownerId = V2TIMManager.getInstance().getLoginUser();
        return (mGroupID + ownerId + "liveRoom").hashCode() & 0x7FFFFFFF;
    }

    private final SinvieCustomAnchorLayout.CustomLiveRoomAnchorLayoutDelegate mDelegate = new SinvieCustomAnchorLayout.CustomLiveRoomAnchorLayoutDelegate() {
        @Override
        public void onClose() {
            finish();
        }

        @Override
        public void onError(SinvieRoomInfo sinvieRoomInfo, int errorCode,
                            String errorMsg) {
            //弹窗提示
        }

        @Override
        public void onRoomCreate(SinvieRoomInfo sinvieRoomInfo) {
            // 创建房间
            if (!TextUtils.isEmpty(mGroupID)) {
                sendLiveGroupMessage(sinvieRoomInfo, 1);
            }
            //在这里更新经纬度和地址
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                sinvieRoomInfo.setLiveAddress(SinvieNetGPSLocationUtils.getLocation().getAddress());
            }
            mPresenter.createRoom(sinvieRoomInfo);
        }

        @Override
        public void onRoomDestroy(SinvieRoomInfo sinvieRoomInfo) {
            // 销毁房间
            if (!TextUtils.isEmpty(mGroupID)) {
                sendLiveGroupMessage(sinvieRoomInfo, 0);
            }
            SinvieHeartbeatManager.getInstance().stop();
            //在这里更新经纬度和地址
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                sinvieRoomInfo.setLiveAddress(SinvieNetGPSLocationUtils.getLocation().getAddress());
            }
            mPresenter.destroyRoom(sinvieRoomInfo);
        }

    };

    private void sendLiveGroupMessage(SinvieRoomInfo sinvieRoomInfo, int roomStatus) {
        SinvieLiveMessageInfo liveMessageInfo = new SinvieLiveMessageInfo();
        liveMessageInfo.version = 1;
        liveMessageInfo.roomId = Integer.parseInt(sinvieRoomInfo.getRoomId());
        liveMessageInfo.roomName = sinvieRoomInfo.getLiveRoomTitle();
        liveMessageInfo.roomType = sinvieRoomInfo.getLiveType() + "";
//        liveMessageInfo.roomCover = roomInfo.getco;
        liveMessageInfo.roomStatus = roomStatus;
        liveMessageInfo.anchorId = sinvieRoomInfo.getUserId();
        liveMessageInfo.anchorName = SinvieUserInfo.getInstance().getName();
        sendLiveGroupMessage(mGroupID, liveMessageInfo, null);
    }

    public void sendLiveGroupMessage(String groupID, SinvieLiveMessageInfo info, final ISinvieUIKitCallBack callBack) {
        ISinvieBaseMessageSender baseMessageSender =
                SinvieTUIListenerManager.getInstance().getMessageSender();
        if (baseMessageSender == null) {
            SinvieDemoLog.e(TAG, "sendLiveGroupMessage failed messageSender is null");
            return;
        }
        Gson gson = new Gson();
        String data = gson.toJson(info);
        SinvieMessageInfo messageInfo = SinvieMessageInfoUtil.buildCustomMessage(data);
        baseMessageSender.sendMessage(messageInfo, null, groupID, true, false, callBack);
    }

    @Override
    protected SinvieLiveRoomAnchorContact.LiveRoomAnchorBaseView createView() {
        return this;
    }

    @Override
    protected SinvieLiveRoomAnchorPresenter createPresenter() {
        return new SinvieLiveRoomAnchorPresenter(this);
    }

    @Override
    public void onBackPressed() {
        mTUILiveRoomAnchorLayout.onBackPress();
    }


    @Override
    public void createRoomFail(int errCode, String errMsg) {
        SinvieToastUtil.toastShortMessage(errMsg);
        mTUILiveRoomAnchorLayout.stopLive();
        finish();
    }


    @Override
    public void destroyRoomSuccess(String msg) {

    }

    @Override
    public void destroyRoomFail(int errCode, String errMsg) {
        SinvieToastUtil.toastShortMessage(errMsg);
        finish();
    }

}