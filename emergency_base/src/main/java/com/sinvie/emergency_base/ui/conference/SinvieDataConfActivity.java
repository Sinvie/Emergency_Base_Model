package com.sinvie.emergency_base.ui.conference;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieConfBroadcastConstants;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcast;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcastReceiver;
import com.sinvie.emergency_conf.CommonService.util.SinvieLogUtil;
import com.sinvie.emergency_conf.LoginService.SinvieLoginMgr;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.logic.conference.data_conf.SinvieDataConfPresenter;
import com.sinvie.emergency_base.logic.conference.data_conf.SinvieIDataConfContract;
import com.sinvie.emergency_base.ui.base.SinvieMVPBaseActivity;
import com.sinvie.emergency_base.utils.SinvieBarrageAnimation;
import com.sinvie.emergency_base.utils.SinvieCommonUtil;
import com.sinvie.emergency_base.widget.dialog.SinvieEditDialog;

import java.util.Timer;
import java.util.TimerTask;

/**
 * This class is about data conf Activity.
 */
public class SinvieDataConfActivity extends SinvieMVPBaseActivity<SinvieIDataConfContract.DataConfViewSinvie, SinvieDataConfPresenter>
        implements SinvieIDataConfContract.DataConfViewSinvie, View.OnClickListener {

    private FrameLayout mConfAuxDataShareLayout;
    private FrameLayout mConfShareLayout;
    private RelativeLayout mDataConfLayout;
    private FrameLayout mConfShareEmptyLayout;
    private ImageView mLeaveIV;
    private TextView mTitleTV;
    private ImageView mRightIV;
    private String mSubject;
    private String confID;
    private SinvieDataConfPresenter mPresenter;
    private FrameLayout mHideVideoView;
    private FrameLayout mLocalVideoView;
    private RelativeLayout mTitleBar;
    private LinearLayout mChatBottom;
    private EditText mChatMsg;
    private ImageView mChatSend;
    private ImageView mDataSend;
    private TextView mCallNameTv;
    private RelativeLayout mBarrageLayout;

    private boolean isVideo;
    private MyTimerTask myTimerTask;
    private Timer timer;
    /**
     * 是否第一次执行计时器
     */
    private boolean isFirstStart = true;
    /**
     * 是否触发触摸屏幕事件
     */
    private boolean isPressTouch = false;
    /**
     * 控件是否显示
     */
    private boolean isShowBar = false;

    /**
     * 是否正在共享
     */
    private boolean isStartShare = false;

    /**
     * 是否正在辅流接收
     */
    private boolean isAuxDataShare = false;

    /**
     * 是否已在数据会议中
     */
    private boolean isDateConf = false;

    @Override
    protected SinvieIDataConfContract.DataConfViewSinvie createView() {
        return this;
    }

    @Override
    protected SinvieDataConfPresenter createPresenter() {
        mPresenter = new SinvieDataConfPresenter();
        return mPresenter;
    }

    private String[] mActions = new String[]{SinvieConfBroadcastConstants.ACTION_CALL_END,
            SinvieConfBroadcastConstants.DATA_CONF_USER_LEAVE};

    private SinvieLocBroadcastReceiver receiver = new SinvieLocBroadcastReceiver() {
        @Override
        public void onReceive(String broadcastName, Object obj) {

            switch (broadcastName) {
                case SinvieConfBroadcastConstants.ACTION_CALL_END:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    });
                    break;
                case SinvieConfBroadcastConstants.DATA_CONF_USER_LEAVE:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            onCreate(null);
                        }
                    });
                    break;

                default:
                    break;
            }
        }
    };

    @Override
    public void initializeComposition() {
        setContentView(R.layout.activity_sinvie_data_conf);

        // data layout
        mDataConfLayout = (RelativeLayout) findViewById(R.id.date_conf_rl);

        // data share layout
        mConfShareLayout = (FrameLayout) findViewById(R.id.conf_share_layout);

        // auxData share layout
        mConfAuxDataShareLayout = (FrameLayout) findViewById(R.id.conf_aux_data_share_layout);

        // Data sharing has not started
        mConfShareEmptyLayout = (FrameLayout) findViewById(R.id.conf_share_empty);

        // 需要隐藏的标题栏
        mTitleBar = (RelativeLayout) findViewById(R.id.title_layout_transparent);
        mChatBottom = (LinearLayout) findViewById(R.id.chat_data_meeting_layout);

        // 采集视频
        mHideVideoView = (FrameLayout) findViewById(R.id.hide_video_view);
        mLocalVideoView = (FrameLayout) findViewById(R.id.local_video_view);

        // title
        mRightIV = (ImageView) findViewById(R.id.right_iv);
        mTitleTV = (TextView) findViewById(R.id.conf_title);
        mLeaveIV = (ImageView) findViewById(R.id.leave_iv);
        mCallNameTv = (TextView) findViewById(R.id.call_name_mark);

        // chat
        mChatMsg = (EditText) findViewById(R.id.message_input_et);
        mChatSend = (ImageView) findViewById(R.id.chat_send_iv);
        mDataSend = (ImageView) findViewById(R.id.chat_logo_iv);

        // barrage display view
        mBarrageLayout = (RelativeLayout) findViewById(R.id.barrage_layout);

        mTitleTV.setText(mSubject);
        mRightIV.setVisibility(View.GONE);

        String number = SinvieLoginMgr.getInstance().getTerminal();
        if (number != null) {
            mCallNameTv.setText(getString(R.string.sinvie_self_number) + number);
        }
        mConfShareLayout.setOnClickListener(this);
        mDataConfLayout.setOnClickListener(this);
        mLeaveIV.setOnClickListener(this);
        mChatSend.setOnClickListener(this);
        mDataSend.setOnClickListener(this);

        mPresenter.attachSurfaceView(mConfShareLayout, this);
    }

    @Override
    public void initializeData() {
        Intent intent = getIntent();
        confID = intent.getStringExtra(SinvieUIConstants.CONF_ID);
        isVideo = intent.getBooleanExtra(SinvieUIConstants.IS_VIDEO_CONF, false);
        isStartShare = intent.getBooleanExtra(SinvieUIConstants.IS_START_SHARE_CONF, false);
        isAuxDataShare = intent.getBooleanExtra(SinvieUIConstants.IS_AUX_DATA_SHARE, false);
        isDateConf = intent.getBooleanExtra(SinvieUIConstants.IS_DATE_CONF, false);
        if (confID == null) {
            showToast(R.string.sinvie_empty_conf_id);
            return;
        }

        mPresenter.setConfID(confID);
        mSubject = mPresenter.getSubject();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void finishActivity() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (null != mBarrageLayout) {
                    mBarrageLayout.removeAllViews();
                }
                finish();
            }
        });
    }

    @Override
    public void startAsShare(final boolean isShare) {
        isStartShare = isShare;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setAuxDataViewShow(View.GONE);
                mConfAuxDataShareLayout.setVisibility(View.GONE);
                if (isStartShare) {
                    mConfShareLayout.setVisibility(View.VISIBLE);
                    mConfShareEmptyLayout.setVisibility(View.GONE);
                } else {
                    mConfShareLayout.setVisibility(View.GONE);
                    mConfShareEmptyLayout.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void setAuxDataState(boolean auxDataState) {
        isAuxDataShare = auxDataState;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isAuxDataShare) {
                    setAuxDataViewShow(View.VISIBLE);
                    mConfShareLayout.setVisibility(View.GONE);
                    mConfAuxDataShareLayout.setVisibility(View.VISIBLE);
                    mConfShareEmptyLayout.setVisibility(View.GONE);
                } else {
                    setAuxDataViewShow(View.GONE);
                    if (isStartShare) {
                        mConfShareLayout.setVisibility(View.VISIBLE);
                        mConfAuxDataShareLayout.setVisibility(View.GONE);
                        mConfShareEmptyLayout.setVisibility(View.GONE);
                    } else {
                        mConfShareLayout.setVisibility(View.GONE);
                        mConfAuxDataShareLayout.setVisibility(View.GONE);
                        mConfShareEmptyLayout.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    @Override
    public void displayConfChatMag(final boolean isSelf, final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView tvMsg = new TextView(SinvieLocContext.getContext());
                if (isSelf) {
                    tvMsg.setTextColor(Color.GREEN);
                } else {
                    tvMsg.setTextColor(Color.BLACK);
                }
                tvMsg.setText(msg);
                tvMsg.setTextSize(17);
                tvMsg.setBackgroundResource(R.drawable.bg_sinvie_conf_msg_normal);
                mBarrageLayout.addView(tvMsg);
                tvMsg.measure(0, 0);
                int width = tvMsg.getMeasuredWidth();
                int height = tvMsg.getMeasuredHeight();
                new SinvieBarrageAnimation(tvMsg, mBarrageLayout, width, height);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.registerBroadcast();
        SinvieLocBroadcast.getInstance().registerBroadcast(receiver, mActions);

        if (isVideo || isDateConf) {
            mPresenter.setVideoContainer(this, mLocalVideoView, mHideVideoView, mConfAuxDataShareLayout);
        }

        if (isAuxDataShare) {
            setAuxDataViewShow(View.VISIBLE);
            mConfShareLayout.setVisibility(View.GONE);
            mConfAuxDataShareLayout.setVisibility(View.VISIBLE);
            mConfShareEmptyLayout.setVisibility(View.GONE);
        } else {
            setAuxDataViewShow(View.GONE);
            if (isStartShare) {
                mConfShareLayout.setVisibility(View.VISIBLE);
                mConfAuxDataShareLayout.setVisibility(View.GONE);
                mConfShareEmptyLayout.setVisibility(View.GONE);
            } else {
                mConfShareLayout.setVisibility(View.GONE);
                mConfAuxDataShareLayout.setVisibility(View.GONE);
                mConfShareEmptyLayout.setVisibility(View.VISIBLE);
            }
        }

        // 第一次启动界面让所有按钮显示5s
        if (isFirstStart) {
            startTimer();
        }
    }

    private void setAuxDataViewShow(int isShow) {
        int childCount = mConfAuxDataShareLayout.getChildCount();
        if (childCount > 0) {
            View childAt = mConfAuxDataShareLayout.getChildAt(0);
            childAt.setVisibility(isShow);
        }
    }

    /**
     * 这个方法的作用是把触摸事件的分发方法，其返回值代表触摸事件是否被当前 View 处理完成(true/false)。
     *
     * @param ev
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        mConfShareLayout.onTouchEvent(ev);
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.unregisterBroadcast();
        SinvieLocBroadcast.getInstance().unRegisterBroadcast(receiver, mActions);
        stopTimer();
        if (null != mBarrageLayout) {
            mBarrageLayout.removeAllViews();
        }
    }

    @Override
    public void dismissLoading() {

    }

    @Override
    public void showCustomToast(final int resID) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showToast(resID);
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.conf_share_layout || id == R.id.date_conf_rl) {// 按钮显示，不执行
            if (isFirstStart) {
                return;
            }
            // 点击动作太多，不执行
            if (isPressTouch) {
                return;
            } else {
                isPressTouch = true;
                startTimer();
            }
        } else if (id == R.id.chat_send_iv) {
            if (null == mChatMsg.getText().toString().trim() || "".equals(mChatMsg.getText().toString().trim())) {
                return;
            }
            mPresenter.sendChatMsg(mChatMsg.getText().toString());
            mChatMsg.setText("");
        } else if (id == R.id.chat_logo_iv) {
            showSendDataDialog();
        } else if (id == R.id.leave_iv) {
            finish();
        }
    }

    private void showSendDataDialog() {

        final String senData = mChatMsg.getText().toString().trim();
        SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "showSendDataDialog:" + mChatMsg.getText().toString());
        final SinvieEditDialog dialog = new SinvieEditDialog(this, "input data type you wanna send");

        dialog.setRightButtonListener(new View.OnClickListener() {
            //            String dialogText = dialog.getText();
//            dialogText = (dialogText == null? "0":dialogText);
            @Override
            public void onClick(View v) {
                SinvieCommonUtil.hideSoftInput(SinvieDataConfActivity.this);

                String string = dialog.getText().toString().trim();
                if (string != null && string.matches("\\d+")) {
                    Integer dataType = Integer.parseInt(dialog.getText().toString().trim());
                    if (dataType < 0 || dataType > 85) {
                        showTip();
                    } else {
                        mPresenter.sendConfData(Integer.parseInt(dialog.getText().toString().trim()), senData);
                    }
                } else {
                    showTip();
                }

            }
        });
        dialog.show();
        //mChatMsg.setText("");
    }

    private void showTip() {
        AlertDialog dialog = new AlertDialog.Builder(this).setTitle(SinvieLocContext.getString(R.string.sinvie_info))
                .setPositiveButton(SinvieLocContext.getString(R.string.sinvie_conform), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).setMessage(SinvieLocContext.getString(R.string.sinvie_data_type_tip)).create();
        dialog.show();
    }

    /**
     * Show title bar and bottom controls.
     */
    private void showBar() {
        if (mTitleBar.getVisibility() == View.GONE || mChatBottom.getVisibility() == View.GONE) {
            mTitleBar.setVisibility(View.VISIBLE);
            mChatBottom.setVisibility(View.VISIBLE);
            isShowBar = true;
        }
    }

    /**
     * Hide title bar and bottom controls.
     */
    private void hideBar() {
        if (mTitleBar.getVisibility() == View.VISIBLE || mChatBottom.getVisibility() == View.VISIBLE) {
            mTitleBar.setVisibility(View.GONE);
            mChatBottom.setVisibility(View.GONE);
            isShowBar = false;
        }
    }

    private void initTimer() {
        timer = new Timer();
        myTimerTask = new MyTimerTask();
    }

    /**
     * Start timer
     */
    private void startTimer() {
        initTimer();
        try {
            // 第一次进入界面执行计时器 5s后控件消失；
            // 非第一次执行计时器，0.2s后控件显示再过5s后控件消失
            if (isFirstStart) {
                timer.schedule(myTimerTask, 5000);
            } else {
                timer.schedule(myTimerTask, 200, 5000);
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
            initTimer();
            timer.schedule(myTimerTask, 5000);
        }
    }

    /**
     * Stop timer
     */
    private void stopTimer() {
        if (null != timer) {
            timer.cancel();
            timer = null;
        }
    }

    class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // 第一次启动界面时启动计时
                    if (isFirstStart) {
                        hideBar();
                        isFirstStart = false;
                        stopTimer();
                    } else {
                        if (isShowBar) {
                            // 停止计时器，计时器任务执行完成之后执行stop
                            hideBar();
                            isPressTouch = false;
                            stopTimer();
                        } else {
                            // 启动计时器，触发屏幕时先显示按钮
                            showBar();
                        }
                    }
                }
            });
        }
    }
}
