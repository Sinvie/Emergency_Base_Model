package com.sinvie.emergency_base.ui.workbench.adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.utils.widget.ImageFilterView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.WorkbenchBean;
import com.sinvie.emergency_live.tuikit.utils.SinvieToastUtil;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/6
 * @Des: 类描述
 */
public class SinvieWorkbenchRvAdapter extends RecyclerView.Adapter<SinvieWorkbenchRvAdapter.WorkbenchGridViewHolder> {

    private Context mContext;
    private List<WorkbenchBean> mBeanList;
    private WorkbenchItemClick mWorkbenchItemClick;

    public SinvieWorkbenchRvAdapter(List<WorkbenchBean> beanList, Context context) {
        mBeanList = beanList;
        mContext = context;
    }

    public void setWorkbenchItemClick(WorkbenchItemClick workbenchItemClick) {
        mWorkbenchItemClick = workbenchItemClick;
    }

    @Override
    public WorkbenchGridViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_sinvie_workbench_grid, parent, false);
        return new WorkbenchGridViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull WorkbenchGridViewHolder holder, int position) {
        holder.bind(position);

    }

    @Override
    public int getItemCount() {
        return mBeanList.size();
    }

    public class WorkbenchGridViewHolder extends RecyclerView.ViewHolder {
        private LinearLayoutCompat mLayoutCompat;
        private ImageFilterView mImageFilterView;
        private TextView mTextView;

        public WorkbenchGridViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            mLayoutCompat = itemView.findViewById(R.id.ll_workbench);
            mImageFilterView = itemView.findViewById(R.id.iv_workbench);
            mTextView = itemView.findViewById(R.id.tv_workbench);
        }

        private void bind(int position) {
            WorkbenchBean bean = mBeanList.get(position);
            if (position % 2 == 0) {
                //偶数为左边
                mLayoutCompat.setGravity(Gravity.START);
            } else {
                //奇数为右边
                mLayoutCompat.setGravity(Gravity.END);
            }

            Glide.with(itemView).load(bean.getImg()).into(mImageFilterView);
            mTextView.setText(bean.getTitle());

            if (bean.isUse()) {
                mTextView.setTextColor(ContextCompat.getColor(mContext, R.color.sinvie_theme_text_black_color));
            } else {
                //不能使用此功能时 title的颜色
                mTextView.setTextColor(ContextCompat.getColor(mContext, R.color.sinvie_theme_text_enable_color));
            }
            mLayoutCompat.setOnClickListener(v -> {
                if (bean.isUse()) {
                    mWorkbenchItemClick.onWorkbenchItemClick(bean);
                } else {
                    if (bean.getNotUseMsg() != null) {
                        SinvieToastUtil.toastLongMessage(bean.getNotUseMsg());
                    }
                }
            });
        }
    }

    public interface WorkbenchItemClick {
        void onWorkbenchItemClick(WorkbenchBean bean);
    }

}
