package com.sinvie.emergency_base.ui.call;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huawei.ecterminalsdk.base.TsdkCallStreamInfo;
import com.huawei.ecterminalsdk.base.TsdkCallVideoStreamInfo;
import com.sinvie.emergency_conf.CallService.SinvieCallInfo;
import com.sinvie.emergency_conf.CallService.SinvieCallMgr;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieConfBroadcastConstants;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcast;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcastReceiver;
import com.sinvie.emergency_conf.CommonService.util.SinvieLogUtil;
import com.sinvie.emergency_conf.CommonService.util.SinvieStringUtil;
import com.sinvie.emergency_conf.ConferenceService.SinvieMeetingMgr;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.ui.base.SinvieBaseActivity;

import java.util.ArrayList;

public class SinvieCallQualityActivity extends SinvieBaseActivity implements SinvieLocBroadcastReceiver {

    private int mCallID;
    private TsdkCallStreamInfo mQosInfo = null;
    private String[] mActions = new String[]{SinvieConfBroadcastConstants.ACTION_CALL_CONNECTED,
            SinvieConfBroadcastConstants.ACTION_CALL_END};

    private ImageView mLeaveIV;

    //音频质量
    private TextView textView2_2;
    private TextView textView2_3;
    private TextView textView2_4;
    private TextView textView2_5;
    private TextView textView2_6;

    private TextView textView3_2;
    private TextView textView3_3;
    private TextView textView3_4;
    private TextView textView3_5;
    private TextView textView3_6;

    //发送视频质量
    private TextView textView2_2_2;
    private TextView textView2_2_3;
    private TextView textView2_2_4;
    private TextView textView2_2_5;
    private TextView textView2_2_6;
    private TextView textView2_2_7;
    private TextView textView2_2_8;

    //接收视频质量
    private TextView textView3_2_2;
    private TextView textView3_2_3;
    private TextView textView3_2_4;
    private TextView textView3_2_5;
    private TextView textView3_2_6;
    private TextView textView3_2_7;
    private TextView textView3_2_8;

    //发送辅流质量
    private TextView textView4_2_2;
    private TextView textView4_2_3;
    private TextView textView4_2_4;
    private TextView textView4_2_5;
    private TextView textView4_2_6;
    private TextView textView4_2_7;
    private TextView textView4_2_8;

    //接收辅流质量
    private TextView textView5_2_2;
    private TextView textView5_2_3;
    private TextView textView5_2_4;
    private TextView textView5_2_5;
    private TextView textView5_2_6;
    private TextView textView5_2_7;
    private TextView textView5_2_8;

    private LinearLayout llyt_svc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler.postDelayed(runnable, 1000);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SinvieLocBroadcast.getInstance().registerBroadcast(this, mActions);
        setNetData();
    }

    @Override
    public void initializeView() {

    }

    @Override
    public void initializeData() {
        mCallID = SinvieCallMgr.getInstance().getCallId();

        if (0 == mCallID) {
            mCallID = SinvieMeetingMgr.getInstance().getCurrentConferenceCallID();
            if (0 == mCallID) {
                SinvieLogUtil.e(SinvieUIConstants.DEMO_TAG, "CallId is null");
                return;
            }
            mQosInfo = SinvieCallMgr.getInstance().getCallQuality(mCallID);
        } else {
            mQosInfo = SinvieCallMgr.getInstance().getCallQuality(mCallID);
            if (null == mQosInfo) {
                mCallID = SinvieMeetingMgr.getInstance().getCurrentConferenceCallID();
                mQosInfo = SinvieCallMgr.getInstance().getCallQuality(mCallID);

                if (null == mQosInfo) {
                    SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "mQosInfo is null");
                    return;
                }
            }
        }
    }

    @Override
    public void initializeComposition() {
        setContentView(R.layout.activity_sinvie_call_quality);
        mLeaveIV = (ImageView) findViewById(R.id.leave_iv);
        textView2_2 = (TextView) findViewById(R.id.textView2_2);
        textView2_3 = (TextView) findViewById(R.id.textView2_3);
        textView2_4 = (TextView) findViewById(R.id.textView2_4);
        textView2_5 = (TextView) findViewById(R.id.textView2_5);
        textView2_6 = (TextView) findViewById(R.id.textView2_6);

        textView3_2 = (TextView) findViewById(R.id.textView3_2);
        textView3_3 = (TextView) findViewById(R.id.textView3_3);
        textView3_4 = (TextView) findViewById(R.id.textView3_4);
        textView3_5 = (TextView) findViewById(R.id.textView3_5);
        textView3_6 = (TextView) findViewById(R.id.textView3_6);

        textView2_2_2 = (TextView) findViewById(R.id.textView2_2_2);
        textView2_2_3 = (TextView) findViewById(R.id.textView2_2_3);
        textView2_2_4 = (TextView) findViewById(R.id.textView2_2_4);
        textView2_2_5 = (TextView) findViewById(R.id.textView2_2_5);
        textView2_2_6 = (TextView) findViewById(R.id.textView2_2_6);
        textView2_2_7 = (TextView) findViewById(R.id.textView2_2_7);
        textView2_2_8 = (TextView) findViewById(R.id.textView2_2_8);

        textView3_2_2 = (TextView) findViewById(R.id.textView3_2_2);
        textView3_2_3 = (TextView) findViewById(R.id.textView3_2_3);
        textView3_2_4 = (TextView) findViewById(R.id.textView3_2_4);
        textView3_2_5 = (TextView) findViewById(R.id.textView3_2_5);
        textView3_2_6 = (TextView) findViewById(R.id.textView3_2_6);
        textView3_2_7 = (TextView) findViewById(R.id.textView3_2_7);
        textView3_2_8 = (TextView) findViewById(R.id.textView3_2_8);

        textView4_2_2 = (TextView) findViewById(R.id.textView4_2_2);
        textView4_2_3 = (TextView) findViewById(R.id.textView4_2_3);
        textView4_2_4 = (TextView) findViewById(R.id.textView4_2_4);
        textView4_2_5 = (TextView) findViewById(R.id.textView4_2_5);
        textView4_2_6 = (TextView) findViewById(R.id.textView4_2_6);
        textView4_2_7 = (TextView) findViewById(R.id.textView4_2_7);
        textView4_2_8 = (TextView) findViewById(R.id.textView4_2_8);

        textView5_2_2 = (TextView) findViewById(R.id.textView5_2_2);
        textView5_2_3 = (TextView) findViewById(R.id.textView5_2_3);
        textView5_2_4 = (TextView) findViewById(R.id.textView5_2_4);
        textView5_2_5 = (TextView) findViewById(R.id.textView5_2_5);
        textView5_2_6 = (TextView) findViewById(R.id.textView5_2_6);
        textView5_2_7 = (TextView) findViewById(R.id.textView5_2_7);
        textView5_2_8 = (TextView) findViewById(R.id.textView5_2_8);

        llyt_svc = (LinearLayout) findViewById(R.id.llyt_svc);

        mLeaveIV.setOnClickListener(onback);
    }

    @Override
    public void setActivityDestroy() {

    }

    @Override
    protected void onDestroy() {
        handler.removeCallbacks(runnable);
        super.onDestroy();
        SinvieLocBroadcast.getInstance().unRegisterBroadcast(this, mActions);
    }

    public void setNetData() {
        if (null == mQosInfo) {
            return;
        }

        if (SinvieStringUtil.isNotEmpty(mQosInfo.getAudioStreamInfo().getEncodeProtocol().trim())) {
            textView2_2.setText(mQosInfo.getAudioStreamInfo().getEncodeProtocol());
            textView2_3.setText(mQosInfo.getAudioStreamInfo().getSendBitRate() + "Kbps");
            textView2_4.setText(mQosInfo.getAudioStreamInfo().getSendLossFraction() + "%");
            textView2_5.setText(mQosInfo.getAudioStreamInfo().getSendDelay() + "ms");
            textView2_6.setText(mQosInfo.getAudioStreamInfo().getSendJitter() + "ms");
        }

        if (SinvieStringUtil.isNotEmpty(mQosInfo.getAudioStreamInfo().getDecodeProtocol().trim())) {
            textView3_2.setText(mQosInfo.getAudioStreamInfo().getDecodeProtocol());
            textView3_3.setText(mQosInfo.getAudioStreamInfo().getRecvBitRate() + "Kbps");
            textView3_4.setText(mQosInfo.getAudioStreamInfo().getRecvLossFraction() + "%");
            textView3_5.setText(mQosInfo.getAudioStreamInfo().getRecvDelay() + "ms");
            textView3_6.setText(mQosInfo.getAudioStreamInfo().getRecvJitter() + "ms");
        }

        // 发送视频质量
        if (SinvieStringUtil.isNotEmpty(mQosInfo.getVideoStreamInfo().getEncodeName().trim())) {
            textView2_2_2.setText(mQosInfo.getVideoStreamInfo().getEncodeName());
            textView2_2_3.setText(mQosInfo.getVideoStreamInfo().getSendBitRate() + "Kbps");
            textView2_2_4.setText(mQosInfo.getVideoStreamInfo().getEncoderSize());
            textView2_2_5.setText(mQosInfo.getVideoStreamInfo().getSendFrameRate() + "fps");
            textView2_2_6.setText(mQosInfo.getVideoStreamInfo().getSendLossFraction() + "%");
            textView2_2_7.setText(mQosInfo.getVideoStreamInfo().getSendDelay() + "ms");
            textView2_2_8.setText(mQosInfo.getVideoStreamInfo().getSendJitter() + "ms");
        }

        // 接收视频质量
        if (SinvieStringUtil.isNotEmpty(mQosInfo.getVideoStreamInfo().getDecodeName().trim())) {
            textView3_2_2.setText(mQosInfo.getVideoStreamInfo().getDecodeName());
            textView3_2_3.setText(mQosInfo.getVideoStreamInfo().getRecvBitRate() + "Kbps");
            textView3_2_4.setText(mQosInfo.getVideoStreamInfo().getDecoderSize());
            textView3_2_5.setText(mQosInfo.getVideoStreamInfo().getRecvFrameRate() + "fps");
            textView3_2_6.setText(mQosInfo.getVideoStreamInfo().getRecvLossFraction() + "%");
            textView3_2_7.setText(mQosInfo.getVideoStreamInfo().getRecvDelay() + "ms");
            textView3_2_8.setText(mQosInfo.getVideoStreamInfo().getRecvJitter() + "ms");
        }

        // 发送辅流
        if (SinvieStringUtil.isNotEmpty(mQosInfo.getDataStreamInfo().getEncodeName().trim())) {
            textView4_2_2.setText(mQosInfo.getDataStreamInfo().getEncodeName());
            textView4_2_3.setText(mQosInfo.getDataStreamInfo().getSendBitRate() + "Kbps");
            textView4_2_4.setText(mQosInfo.getDataStreamInfo().getEncoderSize());
            textView4_2_5.setText(mQosInfo.getDataStreamInfo().getSendFrameRate() + "fps");
            textView4_2_6.setText(mQosInfo.getDataStreamInfo().getSendLossFraction() + "%");
            textView4_2_7.setText(mQosInfo.getDataStreamInfo().getSendDelay() + "ms");
            textView4_2_8.setText(mQosInfo.getDataStreamInfo().getSendJitter() + "ms");
        }

        // 接收辅流
        if (SinvieStringUtil.isNotEmpty(mQosInfo.getDataStreamInfo().getDecodeName().trim())) {
            textView5_2_2.setText(mQosInfo.getDataStreamInfo().getDecodeName());
            textView5_2_3.setText(mQosInfo.getDataStreamInfo().getRecvBitRate() + "Kbps");
            textView5_2_4.setText(mQosInfo.getDataStreamInfo().getDecoderSize());
            textView5_2_5.setText(mQosInfo.getDataStreamInfo().getRecvFrameRate() + "fps");
            textView5_2_6.setText(mQosInfo.getDataStreamInfo().getRecvLossFraction() + "%");
            textView5_2_7.setText(mQosInfo.getDataStreamInfo().getRecvDelay() + "ms");
            textView5_2_8.setText(mQosInfo.getDataStreamInfo().getRecvJitter() + "ms");
        }

        // SVC质量统计
        ArrayList<TsdkCallVideoStreamInfo> svcVideoStreamInfoList =
                mQosInfo.getSvcVideoStreamInfo();
        if (svcVideoStreamInfoList != null && svcVideoStreamInfoList.size() > 0) {
            // 初始化发送和接收的数据集合
            ArrayList<TsdkCallVideoStreamInfo> sendSvcVideoList = new ArrayList<>();
            ArrayList<TsdkCallVideoStreamInfo> recvSvcVideoList = new ArrayList<>();
            // 遍历重组发送和接收的数据
            for (TsdkCallVideoStreamInfo svcVideoStreamInfo : svcVideoStreamInfoList) {
                String decoderSize = svcVideoStreamInfo.getDecoderSize();
                String encoderSize = svcVideoStreamInfo.getEncoderSize();
                // 所有的decoderSize非空即是接收的数据
                if (!TextUtils.isEmpty(decoderSize)) {
                    recvSvcVideoList.add(svcVideoStreamInfo);
                }
                // 所有的接收的数据的数据结构中encoderSize非空即是发送的数据
                if (!TextUtils.isEmpty(encoderSize)) {
                    sendSvcVideoList.add(svcVideoStreamInfo);
                }
            }
            // 通过数据size判断对布局进行控制，默认布局为空的。
            if (sendSvcVideoList.size() > 0 || recvSvcVideoList.size() > 0) {
                llyt_svc.setVisibility(View.VISIBLE);
                llyt_svc.removeAllViews();

                // 发送标题显示
                View sendTitleView = getInflate();
                llyt_svc.addView(sendTitleView);
                setViewData(sendTitleView, "状态", "协议", "码率", "分辨率", "帧率", "丢包", "延时", "抖动");
                // 发送数据显示
                if (sendSvcVideoList.size() > 0) {
                    for (TsdkCallVideoStreamInfo sendSvcVideoStreamInfo : sendSvcVideoList) {
                        View sendView = getInflate();
                        llyt_svc.addView(sendView);

                        String encodeName = sendSvcVideoStreamInfo.getEncodeName();
                        String sendBitRate = sendSvcVideoStreamInfo.getSendBitRate() + "Kbps";
                        String encoderSize = sendSvcVideoStreamInfo.getEncoderSize();
                        String sendFrameRate = sendSvcVideoStreamInfo.getSendFrameRate() + "fps";
                        String sendLossFraction = sendSvcVideoStreamInfo.getSendLossFraction() +
                                "%";
                        String sendDelay = sendSvcVideoStreamInfo.getSendDelay() + "ms";
                        String sendJitter = sendSvcVideoStreamInfo.getSendJitter() + "ms";

                        setViewData(sendView, "本地", encodeName, sendBitRate, encoderSize,
                                sendFrameRate, sendLossFraction, sendDelay, sendJitter);
                    }
                }

                // 接收标题显示
                View recvTitleView = getInflate();
                llyt_svc.addView(recvTitleView);
                setViewData(recvTitleView, "会场", "协议", "码率", "分辨率", "帧率", "丢包", "延时", "抖动");

                View emptyTitleView = getInflate();
                llyt_svc.addView(emptyTitleView);
                setViewData(recvTitleView, "", "", "", "", "", "", "", "");
                // 接收数据显示
                if (recvSvcVideoList.size() > 0) {
                    for (TsdkCallVideoStreamInfo recvSvcVideoStreamInfo : recvSvcVideoList) {
                        View recvView = getInflate();
                        llyt_svc.addView(recvView);

                        String decodeName = recvSvcVideoStreamInfo.getDecodeName();
                        String recvBitRate = recvSvcVideoStreamInfo.getRecvBitRate() + "Kbps";
                        String decoderSize = recvSvcVideoStreamInfo.getDecoderSize();
                        String recvFrameRate = recvSvcVideoStreamInfo.getRecvFrameRate() + "fps";
                        String recvLossFraction = recvSvcVideoStreamInfo.getRecvLossFraction() +
                                "%";
                        String recvDelay = recvSvcVideoStreamInfo.getRecvDelay() + "ms";
                        String recvJitter = recvSvcVideoStreamInfo.getRecvJitter() + "ms";

                        setViewData(recvView, "视频流", decodeName, recvBitRate, decoderSize,
                                recvFrameRate, recvLossFraction, recvDelay, recvJitter);
                    }
                }
            }

        }
    }

    /**
     * 获取布局对象
     *
     * @return 布局对象
     */
    private View getInflate() {
        return this.getLayoutInflater().inflate(R.layout.item_sinvie_net_quality, null);
    }

    /**
     * 设置控件上的数据
     *
     * @param view         控件对象
     * @param title        标题
     * @param codeName     协议
     * @param bitRate      码率
     * @param coderSize    分辨率
     * @param frameRate    帧率
     * @param lossFraction 丢包
     * @param delay        延时
     * @param jitter       抖动
     */
    private void setViewData(View view, String title, String codeName, String bitRate,
                             String coderSize,
                             String frameRate, String lossFraction, String delay, String jitter) {
        ((TextView) view.findViewById(R.id.tv_title)).setText(title);
        ((TextView) view.findViewById(R.id.tv_code_name)).setText(codeName);
        ((TextView) view.findViewById(R.id.tv_bit_rate)).setText(bitRate);
        ((TextView) view.findViewById(R.id.tv_coder_size)).setText(coderSize);
        ((TextView) view.findViewById(R.id.tv_frame_rate)).setText(frameRate);
        ((TextView) view.findViewById(R.id.tv_loss_fraction)).setText(lossFraction);
        ((TextView) view.findViewById(R.id.tv_delay)).setText(delay);
        ((TextView) view.findViewById(R.id.tv_jitter)).setText(jitter);
    }

    @Override
    public void onReceive(String broadcastName, Object obj) {
        switch (broadcastName) {
            case SinvieConfBroadcastConstants.ACTION_CALL_CONNECTED:
                if (obj instanceof SinvieCallInfo) {
                    SinvieCallInfo sinvieCallInfo = (SinvieCallInfo) obj;
                    mCallID = sinvieCallInfo.getCallID();
                }
            case SinvieConfBroadcastConstants.ACTION_CALL_END:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                });
                break;

            default:
                break;
        }
    }

    private View.OnClickListener onback = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.leave_iv) {
                SinvieCallQualityActivity.super.onBack();
            }
        }
    };

    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            mQosInfo = SinvieCallMgr.getInstance().getCallQuality(mCallID);
            if (null != mQosInfo) {
                setNetData();
            }
            handler.postDelayed(this, 1000);
        }
    };
}
