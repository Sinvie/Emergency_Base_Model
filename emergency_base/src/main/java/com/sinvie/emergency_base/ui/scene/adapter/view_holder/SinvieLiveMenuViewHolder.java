package com.sinvie.emergency_base.ui.scene.adapter.view_holder;

import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sinvie.emergency_base.R;

import org.jetbrains.annotations.NotNull;

/**
 * @author: JonsonBob
 * @date: 2021/7/3
 * @Des: 类描述
 */
public class SinvieLiveMenuViewHolder extends RecyclerView.ViewHolder {

    public ImageView mIvLiveRoomMenu;

    public SinvieLiveMenuViewHolder(@NonNull @NotNull View itemView) {
        super(itemView);
        mIvLiveRoomMenu = itemView.findViewById(R.id.iv_item_live_room_menu);

    }
}
