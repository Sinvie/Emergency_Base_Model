package com.sinvie.emergency_base.ui.base;


/**
 * @author: JonsonBob
 * @date: 2021/7/6
 * @Des: 类描述
 */
public abstract class SinvieMVPBaseFragment<V, P extends SinvieMVPBasePresenter<V>> extends SinvieAbsFragment {

    protected P mPresenter;
    protected V mBindView;



    @Override
    public void onViewLoad() {
        mPresenter = createPresenter();
        mBindView = createView();
        mPresenter.attachView(mBindView);

    }


    @Override
    void onViewDestroyed() {
        mPresenter.detachView();
    }


    protected abstract V createView();

    protected abstract P createPresenter();
}
