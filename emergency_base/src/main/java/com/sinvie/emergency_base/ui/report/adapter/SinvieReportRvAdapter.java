package com.sinvie.emergency_base.ui.report.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieReportBean;
import com.sinvie.emergency_base.utils.SinvieChangeBeanUtil;
import com.sinvie.emergency_base.widget.SinvieCustomGridView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/11
 * @Des: 类描述
 */
public class SinvieReportRvAdapter extends RecyclerView.Adapter<SinvieReportRvAdapter.ReportViewHolder> {

    private Context mContext;
    private List<SinvieReportBean.DataRoomListBean> mReportBeanList;

    private OnReportRvItemClickListener mOnReportRvItemClickListener;


    public SinvieReportRvAdapter(Context context, List<SinvieReportBean.DataRoomListBean> reportBeanList) {
        mContext = context;
        mReportBeanList = reportBeanList;
    }

    public void setOnReportRvItemClickListener(OnReportRvItemClickListener onReportRvItemClickListener) {
        mOnReportRvItemClickListener = onReportRvItemClickListener;
    }

    @Override
    public ReportViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_sinvie_report, parent, false);
        return new ReportViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ReportViewHolder holder, int position) {
        holder.bind(position, mReportBeanList.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnReportRvItemClickListener.onReportRvItemClickListener(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mReportBeanList.size();
    }

    public class ReportViewHolder extends RecyclerView.ViewHolder {

        private LinearLayoutCompat mItemReportTop;
        private TextView mTvItemReportStatus;
        private TextView mTvItemReportTitle;
        private TextView mTvReportContent;
        private SinvieCustomGridView mGvReport;
        private LinearLayout mLlReportAddress;
        private TextView mTvReportAddress;
        private LinearLayout mLlReportData;
        private TextView mTvReportDate;

        public ReportViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            mTvItemReportStatus = itemView.findViewById(R.id.tv_item_report_status);
            mTvItemReportTitle = itemView.findViewById(R.id.tv_item_report_title);
            mTvReportContent = itemView.findViewById(R.id.tv_report_content);
            mGvReport = itemView.findViewById(R.id.gv_report);
            mTvReportAddress = itemView.findViewById(R.id.tv_report_address);
            mTvReportDate = itemView.findViewById(R.id.tv_report_date);
        }

        private void bind(int position, SinvieReportBean.DataRoomListBean bean) {
            mTvItemReportStatus.setText(bean.getCaseClassName());
            mTvItemReportTitle.setText(bean.getTitle());
            mTvReportContent.setText(bean.getContent());
            mTvReportAddress.setText(bean.getAddress());
            mTvReportDate.setText(bean.getReportTime());

            //判断 图片是否为空 为空就退出
            if (bean.getAppAttachments() == null) {
                return;
            }

            SinvieReportItemGvAdapter itemGvAdapter = new SinvieReportItemGvAdapter(mContext,  SinvieChangeBeanUtil.getGridImgBeanList(bean.getAppAttachments()));
            itemGvAdapter.setItemClickListener((gridType, url) ->  mOnReportRvItemClickListener.onReportRvItemClickListener(gridType, url));
            mGvReport.setAdapter(itemGvAdapter);
        }


    }

    public interface OnReportRvItemClickListener {

        void onReportRvItemClickListener(int position);

        void onReportRvItemClickListener(int gridType,  String url);
    }
}
