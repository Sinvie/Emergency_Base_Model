package com.sinvie.emergency_base.ui.scene;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.WindowManager;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.logic.live_room.live_room_audience.SinvieLiveRoomAudienceContact;
import com.sinvie.emergency_base.logic.live_room.live_room_audience.SinvieLiveRoomAudiencePresenter;
import com.sinvie.emergency_base.ui.SinvieIntentConstant;
import com.sinvie.emergency_base.ui.base.SinvieMVPBaseActivity;
import com.sinvie.emergency_base.ui.scene.view.SinvieCustomAudienceLayout;
import com.sinvie.emergency_base.utils.SinvieActivityUtil;
import com.sinvie.emergency_live.tuikit_live.live.base.SinvieConstants;

public class SinvieLiveRoomAudienceActivity extends SinvieMVPBaseActivity<SinvieLiveRoomAudienceContact.LiveRoomAudienceBaseView, SinvieLiveRoomAudiencePresenter> implements SinvieLiveRoomAudienceContact.LiveRoomAudienceBaseView {
    private static final String TAG = SinvieLiveRoomAudienceActivity.class.getSimpleName();
    private SinvieCustomAudienceLayout mTUILiveRoomAudienceLayout;

    public static void start(Context context) {
        Intent starter = new Intent(SinvieIntentConstant.LIVE_ROOM_AUDIENCE_ACTIVITY_ACTION);
        SinvieActivityUtil.startActivity(context, starter);
    }

    @Override
    public void initializeData() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setTurnScreenOn(true);
        }
        setContentView(R.layout.activity_sinvie_live_room_audience);
    }

    @Override
    public void initializeComposition() {
        initLiveRoomAudienceLayout();

        initLiveRoomAudienceLayoutDelegate();
    }

    private void initLiveRoomAudienceLayout() {
        mTUILiveRoomAudienceLayout = findViewById(R.id.layout_room_audience);

        Intent intent = getIntent();
        int roomId = intent.getIntExtra(SinvieConstants.GROUP_ID, 0);
        String anchorId = intent.getStringExtra(SinvieConstants.ANCHOR_ID);

        mTUILiveRoomAudienceLayout.initWithRoomId(getSupportFragmentManager(), roomId, anchorId, false, "");
    }


    private void initLiveRoomAudienceLayoutDelegate() {
        mTUILiveRoomAudienceLayout.setLiveRoomAudienceDelegate(new SinvieCustomAudienceLayout.CustomLiveRoomAudienceDelegate() {
            @Override
            public void onClose() {
                finish();
            }

            @Override
            public void onError(int errorCode, String message) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        mTUILiveRoomAudienceLayout.onBackPressed();
    }

    @Override
    protected SinvieLiveRoomAudienceContact.LiveRoomAudienceBaseView createView() {
        return this;
    }

    @Override
    protected SinvieLiveRoomAudiencePresenter createPresenter() {
        return new SinvieLiveRoomAudiencePresenter(this);
    }

}