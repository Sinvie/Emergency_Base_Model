package com.sinvie.emergency_base.ui.scene.net;

import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import com.sinvie.emergency_base.bean.SinvieRoomBean;
import com.sinvie.emergency_base.bean.SinvieRoomInfo;
import com.sinvie.emergency_base.http.SinvieNetContact;
import com.sinvie.emergency_base.http.base.SinvieBaseObserver;
import com.sinvie.emergency_base.http.common.SinvieCommonNetUtils;
import com.sinvie.emergency_base.http.repository.SinvieBaseURLRepository;
import com.sinvie.emergency_live.tuikit_live.live.SinvieTUIKitLive;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * @param
 * @author JonsonBob
 * @description 心跳包调整
 * @return
 * @time 2021/6/28 10:53
 */
public class SinvieHeartbeatManager implements Runnable {

    private static final String TAG = "HeartbeatManager";
    private static final String HEARTBEAT_THREAD_NAME = "heartbeat_thread";

    // 心跳上报时间间隔
    private static final long HEARTBEAT_INTERVAL = 10 * 1000;

    private HandlerThread mHandlerThread = null;
    private Handler mHeartbeatHandler = null;

    private SinvieRoomInfo mSinvieRoomInfo;

    private static class SingletonHolder {
        static SinvieHeartbeatManager sInstance = new SinvieHeartbeatManager();
    }

    public static SinvieHeartbeatManager getInstance() {
        return SingletonHolder.sInstance;
    }

    private SinvieHeartbeatManager() {
    }

    public void start(SinvieRoomInfo info) {
        Log.i(TAG,
                "start heartbeat: appId -> " + SinvieTUIKitLive.getSdkAppId() + ", type -> " + info.getLiveType() +
                        ", roomId -> " + info.getRoomId());
        mSinvieRoomInfo = info;
        mHandlerThread = new HandlerThread(HEARTBEAT_THREAD_NAME);
        mHeartbeatHandler = new Handler();
        mHandlerThread.start();
        postHeartbeat();
        sendHeartbeatMessage();
    }

    public void stop() {
        if (mHeartbeatHandler != null) {
            mHeartbeatHandler.removeCallbacks(this);
        }
        mHeartbeatHandler = null;
        if (mHandlerThread != null) {
            mHandlerThread.quit();
        }
        mHandlerThread = null;
    }

    @Override
    public void run() {
        postHeartbeat();
        sendHeartbeatMessage();
    }

    private void sendHeartbeatMessage() {
        mHeartbeatHandler.postDelayed(this, HEARTBEAT_INTERVAL);
    }

    private void postHeartbeat() {
        MultipartBody.Builder builder = new MultipartBody.Builder()
                .addFormDataPart(SinvieNetContact.REQUEST_PARAM_ID, mSinvieRoomInfo.getId())
                .addFormDataPart(SinvieNetContact.REQUEST_PARAM_USER_ID, mSinvieRoomInfo.getUserId())
                .addFormDataPart(SinvieNetContact.REQUEST_PARAM_ROOM_ID, mSinvieRoomInfo.getRoomId())
                .addFormDataPart(SinvieNetContact.REQUEST_PARAM_LIVE_TYPE, mSinvieRoomInfo.getLiveType() + "")
                .addFormDataPart(SinvieNetContact.REQUEST_PARAM_LIVE_ADDRESS, mSinvieRoomInfo.getLiveAddress() + "")
                .addFormDataPart(SinvieNetContact.REQUEST_PARAM_LIVE_STATUS, mSinvieRoomInfo.getLiveStatus() + "");
        if (mSinvieRoomInfo.getEndTime() != null) {
            builder.addFormDataPart(SinvieNetContact.REQUEST_PARAM_LIVE_END_TIME, mSinvieRoomInfo.getEndTime());
        }
        if (mSinvieRoomInfo.getStartTime() != null) {
            builder.addFormDataPart(SinvieNetContact.REQUEST_PARAM_LIVE_START_TIME, mSinvieRoomInfo.getStartTime());
        }
        RequestBody formBody = builder.build();
        SinvieCommonNetUtils.execute(SinvieCommonNetUtils.covert(new SinvieBaseURLRepository().updateRoom(formBody)), new SinvieBaseObserver<SinvieRoomBean>() {
            @Override
            public void onSuccess(@Nullable SinvieRoomBean sinvieRoomBean) {

            }

            @Override
            public void onFailure(int errCode, @NotNull String errMsg) {

            }

        });
    }
}
