package com.sinvie.emergency_base.ui.report;

import android.Manifest;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.utils.widget.ImageFilterView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hjq.permissions.OnPermissionCallback;
import com.hjq.permissions.Permission;
import com.hjq.permissions.XXPermissions;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnResultCallbackListener;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieAddResourceBean;
import com.sinvie.emergency_base.bean.SinvieAppAttachmentsBean;
import com.sinvie.emergency_base.bean.SinvieReportBean;
import com.sinvie.emergency_base.bean.SinvieReportTypeBean;
import com.sinvie.emergency_base.bean.SinvieSaveFileBean;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.logic.report.add_report.SinvieAddReportContact;
import com.sinvie.emergency_base.logic.report.add_report.SinvieAddReportPresenter;
import com.sinvie.emergency_live.tuikit.base.ISinvieTitleBarLayout;
import com.sinvie.emergency_live.tuikit.utils.SinvieToastUtil;
import com.sinvie.emergency_base.ui.SinvieIntentConstant;
import com.sinvie.emergency_base.ui.base.SinvieMVPBaseActivity;
import com.sinvie.emergency_base.ui.report.adapter.SinvieAddResourceGvAdapter;
import com.sinvie.emergency_base.utils.SinvieActivityUtil;
import com.sinvie.emergency_base.utils.SinvieClickUtils;
import com.sinvie.emergency_base.utils.SinvieFileUtil;
import com.sinvie.emergency_base.utils.SinviePictureSelectUtils;
import com.sinvie.emergency_base.widget.SinvieCustomTitleBarLayout;
import com.sinvie.emergency_base.widget.SinvieSendVoiceBtn;
import com.sinvie.emergency_base.widget.dialog.SinvieSelectResDialog;
import com.sinvie.emergency_base.widget.dialog.SinvieSelectorDialog;
import com.sinvie.emergency_widget.selector.ISinvieSelectAble;
import com.xiasuhuei321.loadingdialog.view.LoadingDialog;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SinvieAddReportActivity extends SinvieMVPBaseActivity<SinvieAddReportContact.AddReportBaseView,
        SinvieAddReportPresenter> implements SinvieAddReportContact.AddReportBaseView, View.OnClickListener {

    private static final String TAG = SinvieAddReportActivity.class.getSimpleName();

    private SinvieCustomTitleBarLayout mCbrAddNewReport;
    private LinearLayout mLlReportType;
    private TextView mTvReportType;
    private LinearLayout mLlReportAddress;
    private TextView mTvReportAddress;
    private RecyclerView mGvReportImg;
    private TextView mTvSubmit;
    private TextView mTvCancel;
    private EditText mEdReportName;
    private ImageFilterView mIvGoSelectType;
    private ImageFilterView mIvGoSelectAddress;
    private TextView mViewDecorationDes;
    private EditText mEdReportDes;
    private SinvieSendVoiceBtn mSinvieSendVoiceBtn;
    private ImageFilterView mIvAddReportDelVoice;

    private static final int MAX_SELECT_RESOURCE = 4;

    private int mIntentType = 0;// 默认新增
    private SinvieReportBean.DataRoomListBean mInfoBean;
    /**
     * 只能先填写假数据
     */
    private String mLatitude = "23.23";
    private String mLongitude = "114.14";
    private String mAddress = "广州市海珠区中山科技园B座905";
    /**
     * 事件类型id
     */
    private int mCaseClassId = -1;
    /**
     * 事件选择的所有层级和数据
     */
    private ArrayList<SinvieReportTypeBean> mSelectAblesList;
    /**
     * 已经上传的文件ID 列表 用于保存上传后的 资源网络id
     */
    private List<String> mAttachmentIds;
    /**
     * 当前选中的文件地址 list 用于上传文件
     */
    private List<String> mFilePaths;

    /**
     * 事件类型 list
     */
    private ArrayList<SinvieReportTypeBean> mReportTypeList;
    /**
     * 选择资源地址 list 用于 GV 显示
     */
    private List<SinvieAddResourceBean> mResourceBeanList;

    private SinvieAddResourceGvAdapter mAddResourceAdapter;

    private LoadingDialog mLoadingDialog;

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SinvieUIConstants.SELECT_ADDRESS_REQUEST:
                if (resultCode == SinvieUIConstants.SELECT_ADDRESS_RESULT) {
                    if (data != null) {
                        //获取返回的经纬度
                        mLatitude = data.getStringExtra(SinvieUIConstants.ADDRESS_TYPE_LATITUDE);
                        mLongitude = data.getStringExtra(SinvieUIConstants.ADDRESS_TYPE_LONGITUDE);
                        mAddress = data.getStringExtra(SinvieUIConstants.ADDRESS_TYPE_ADDRESS);

                        if (mTvReportAddress != null) {
                            mTvReportAddress.setText(mAddress);
                        }
                    }
                }
                break;
            case XXPermissions.REQUEST_CODE:
                if (XXPermissions.isGranted(this, Permission.ACCESS_MEDIA_LOCATION) &&
                        XXPermissions.isGranted(this, Permission.ACCESS_FINE_LOCATION) &&
                        XXPermissions.isGranted(this, Permission.ACCESS_COARSE_LOCATION) &&
                        XXPermissions.isGranted(this, Permission.ACCESS_BACKGROUND_LOCATION)) {
                    SinvieActivityUtil.startActivityForResult(SinvieAddReportActivity.this,
                            SinvieIntentConstant.SELECT_ADDRESS_ACTIVITY_ACTION, SinvieUIConstants.SELECT_ADDRESS_REQUEST);
                } else {
                    requestLocationPermission();
                }
                break;
            default:
                break;
        }

    }

    @Override
    public void initializeData() {
        //如果携带信息过来 此处解析 然后 填充信息
        mIntentType = getIntent().getIntExtra(SinvieUIConstants.REPORT_INTENT_TYPE,
                SinvieUIConstants.REPORT_TYPE_ADD);
        if (mIntentType == SinvieUIConstants.REPORT_TYPE_EDIT) {
            mInfoBean = getIntent().getParcelableExtra(SinvieUIConstants.REPORT_INFO_BEAN);
        }
        //申请文件读取权限

        //申请麦克风权限


    }

    @Override
    public void initializeComposition() {
        setContentView(R.layout.activity_sinvie_add_new_report);
        mCbrAddNewReport = findViewById(R.id.cbr_add_new_report);
        mLlReportType = findViewById(R.id.ll_report_type);
        mTvReportType = findViewById(R.id.tv_new_report_type);
        mLlReportAddress = findViewById(R.id.ll_report_address);
        mTvReportAddress = findViewById(R.id.tv_new_report_address);
        mGvReportImg = findViewById(R.id.gv_report_img);
        mTvSubmit = findViewById(R.id.tv_submit);
        mTvCancel = findViewById(R.id.tv_cancel);
        mEdReportName = findViewById(R.id.ed_report_name);
        mIvGoSelectType = findViewById(R.id.iv_go_select_type);
        mIvGoSelectAddress = findViewById(R.id.iv_go_select_address);
        mViewDecorationDes = findViewById(R.id.view_decoration_des);
        mEdReportDes = findViewById(R.id.ed_report_des);
        mSinvieSendVoiceBtn = findViewById(R.id.svb_report_voice);
        mIvAddReportDelVoice = findViewById(R.id.iv_add_report_del_voice);

        //标题设置
        mCbrAddNewReport.setLeftIcon(R.drawable.svg_sinvie_theme_back);
        mCbrAddNewReport.setOnLeftClickListener(v -> finish());

        //点击事件监听
        mTvCancel.setOnClickListener(this);
        mTvSubmit.setOnClickListener(this);
        mLlReportAddress.setOnClickListener(this);
        mLlReportType.setOnClickListener(this);
        mIvAddReportDelVoice.setOnClickListener(this);

        mResourceBeanList = new ArrayList<>();
        mAttachmentIds = new ArrayList<>();
        mFilePaths = new ArrayList<>();

        mGvReportImg.setLayoutManager(new GridLayoutManager(this, 4));
        //gv 初始化 绑定adapter
        mAddResourceAdapter = new SinvieAddResourceGvAdapter(this, mResourceBeanList);
        mAddResourceAdapter.setItemClickListener(mItemClickListener);
        mGvReportImg.setAdapter(mAddResourceAdapter);

        if (mIntentType == SinvieUIConstants.REPORT_TYPE_ADD) {
            mCbrAddNewReport.setTitle(getString(R.string.sinvie_add_report_title), ISinvieTitleBarLayout.POSITION.MIDDLE);
            mTvSubmit.setText(getString(R.string.sinvie_submit_event));
        } else {
            mCbrAddNewReport.setTitle(getString(R.string.sinvie_edit_report_title), ISinvieTitleBarLayout.POSITION.MIDDLE);
            mTvSubmit.setText(getString(R.string.sinvie_save_report));
            onIntentParams();
        }
    }

    /**
     * 填充数据
     */
    private void onIntentParams() {
        //事件名字
        mEdReportName.setText(mInfoBean.getTitle());
        //事件类型
        mTvReportType.setText(mInfoBean.getCaseClassName());
        mCaseClassId = mInfoBean.getCaseClassId();
        //事发地址
        mAddress = mInfoBean.getAddress();
        mTvReportAddress.setText(mAddress);
        //经纬度
        mLatitude = mInfoBean.getLatitude();
        mLongitude = mInfoBean.getLongitude();
        //描述
        mEdReportDes.setText(mInfoBean.getContent());
        int imgNum = Math.min(mInfoBean.getAppAttachments().size(), 4);
        //现场情况 只显示四张图
        for (int i = 0; i < imgNum; i++) {
            SinvieAppAttachmentsBean appAttachment = mInfoBean.getAppAttachments().get(i);

            SinvieAddResourceBean bean = new SinvieAddResourceBean();
            bean.setAdd(false);
            bean.setNetPath(true);
            int suffix = SinvieFileUtil.getSuffixFormat(appAttachment.getUrl());
            //格式 图片/视频
            bean.setImg(suffix == SinvieFileUtil.SUFFIX_FORMAT_IMG);
            //图片id
            bean.setResourceId(appAttachment.getId());
            //存入图片资源地址
            bean.setResourcePath(appAttachment.getUrl());
            mResourceBeanList.add(bean);
            //记录资源id
            mAttachmentIds.add(appAttachment.getId() + "");
        }
        //显示图片
        mAddResourceAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        int vId = v.getId();
        //防止多次点击
        if (SinvieClickUtils.isFastClick(vId)) {
            return;
        }
        if (vId == R.id.tv_submit) {
            //显示等待弹窗
            showLoadingDialog(R.string.sinvie_report_doing, R.string.sinvie_do_success, R.string.sinvie_report_submit_fail);
            //判断资源列表是否为空 不为空就开始先保存资源 如果为空就上传
            if (mFilePaths == null || mFilePaths.isEmpty()) {
                startSaveReportInfo();
            } else {
                mPresenter.upLoadFile(mFilePaths);
            }
        } else if (vId == R.id.tv_cancel) {
            finish();
        } else if (vId == R.id.ll_report_address) {
            //先申请权限
            requestLocationPermission();
        } else if (vId == R.id.ll_report_type) {
            if (mReportTypeList == null) {
                mReportTypeList = new ArrayList<>();
                showLoadingDialog(R.string.sinvie_event_loading_text, R.string.sinvie_event_type_success, R.string.sinvie_event_type_fail);
                //从后台获取type
                mPresenter.getEventType();
            } else {
                //直接展开dialog
//                showSelectDialog(mReportTypeList);
                showSelectorTypeDialog();
            }
        } else if (vId == R.id.iv_add_report_del_voice) {
            //重置录音按钮
            mSinvieSendVoiceBtn.setResetBtn();
            //删除录音文件
            mSinvieSendVoiceBtn.clearVoiceData();
        }
    }

    private final SinvieAddResourceGvAdapter.onReportGvItemClickListener mItemClickListener = new SinvieAddResourceGvAdapter.onReportGvItemClickListener() {
        @Override
        public void onGvDeleteItemListener(SinvieAddResourceBean resourceBean) {
            //删除 选择的 res
            delSelectRes(resourceBean);
            //刷新列表
            mAddResourceAdapter.notifyDataSetChanged();
        }

        @Override
        public void onGvDeleteAddItemListener() {
//            String[] permission = new String[]{Manifest.permission.MANAGE_EXTERNAL_STORAGE};
//            XXPermissions.with(SinvieAddReportActivity.this).permission(permission)
//                    .request((permissions, all) -> {
//                        if (all) {
                            requestPermission();
//                        }
//                    });
        }
    };

    @Override
    protected SinvieAddReportContact.AddReportBaseView createView() {
        return this;
    }

    @Override
    protected SinvieAddReportPresenter createPresenter() {
        return new SinvieAddReportPresenter(this);
    }

    @Override
    public void submitReportSuccess() {
        SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_do_success));
        setResult(SinvieUIConstants.ADD_NEW_REPORT_RESULT);
        //关闭等待弹窗
        dismissLoadingDialog();
        finish();
    }

    @Override
    public void submitReportFail(String errMsg) {
        SinvieToastUtil.toastLongMessage(errMsg);
        //关闭等待弹窗
        dismissLoadingDialog();
    }

    @Override
    public void getEventTypeSuccess(List<SinvieReportTypeBean> beanList) {
        if (beanList == null || beanList.isEmpty()) {
            SinvieToastUtil.toastLongMessage("事件分类列表为空!");
            return;
        }
        //清除旧数据
        mReportTypeList.clear();
        mReportTypeList.addAll(beanList);
//        showSelectDialog(mReportTypeList);
        showSelectorTypeDialog();
        //关闭等待弹窗
        dismissLoadingDialog();
    }

    @Override
    public void getEventTypeFail(String errMsg) {
        SinvieToastUtil.toastLongMessage(errMsg);
        //关闭等待弹窗
        dismissLoadingDialog();
    }

    @Override
    public void upLoadFileSuccess(SinvieSaveFileBean sinvieSaveFileBean, boolean isAll) {
        //添加已经上传的 file id
        mAttachmentIds.add(sinvieSaveFileBean.getId() + "");
        //删除已经上传的 file
        mFilePaths.remove(0);
        //判断是否上传完毕
        if (isAll) {
            startSaveReportInfo();
        } else {
            if (mFilePaths != null && !mFilePaths.isEmpty()) {
                mPresenter.upLoadFile(mFilePaths);
            }
        }
    }

    @Override
    public void upLoadFileFail(int errCode, String errMsg) {
        SinvieToastUtil.toastLongErrorMessage(errCode, errMsg);
        //关闭等待弹窗
        dismissLoadingDialog();
    }

    @Override
    public void showLoadingDialog(int str, int success, int fail) {
        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog(this);
        }
        mLoadingDialog.setLoadingText(getString(str))
                .setSuccessText(getString(success))//显示加载成功时的文字
                .setFailedText(getString(fail)) //加载失败
                .setInterceptBack(true)
                .setLoadSpeed(LoadingDialog.Speed.SPEED_TWO)
                .setRepeatCount(-1)
                .show();
    }

    @Override
    public void dismissLoadingDialog() {
        if (mLoadingDialog != null) {
            mLoadingDialog.close();
        }
    }

    /**
     * 显示无限事件等级选择器
     */
    private void showSelectorTypeDialog() {
        SinvieSelectorDialog sinvieSelectorDialog = SinvieSelectorDialog.newInstance(
                mReportTypeList
                , getString(R.string.sinvie_report_type_hint)
                , mSelectAblesList);
        //设置选择监听
        sinvieSelectorDialog.setListener((selectAbles) -> {
            mSelectAblesList = new ArrayList<>();
            //先记录下选中的所有数据
            for (ISinvieSelectAble selectAble : selectAbles) {
                SinvieReportTypeBean bean = new SinvieReportTypeBean();
                bean.setId(selectAble.getId());
                bean.setName(selectAble.getName());
                mSelectAblesList.add(bean);
            }
            //选中的最后一级的 title 和 id
            String title = selectAbles.get(selectAbles.size() - 1).getName();
            int id = selectAbles.get(selectAbles.size() - 1).getId();
            mTvReportType.setText(title);
            mCaseClassId = id;
        });

        sinvieSelectorDialog.show(getSupportFragmentManager(), "select_type_dialog");

    }

    /**
     * 显示选择资源dialog
     */
    private void showSelectResDialog() {
        //显示dialog
        SinvieSelectResDialog dialog = SinvieSelectResDialog.newInstance("", "");
        dialog.setItemClickListener(position -> {
            if (position == 0) {
                //选择相册
                openPicture();
            } else if (position == 1) {
                //选择拍照
                openCamera(PictureMimeType.ofImage());
            } else {
                //选择视频
                openCamera(PictureMimeType.ofVideo());
            }
        });
        dialog.show(getSupportFragmentManager(), "select_pic_dialog");
    }

    /**
     * 开启相机拍摄
     */
    private void openCamera(int type) {
        SinviePictureSelectUtils.getInstance()
                .init(SinvieAddReportActivity.this)
                .openCamera(type, mResultCallbackListener);
    }

    /**
     * 开启相册
     */
    private void openPicture() {
        SinviePictureSelectUtils.getInstance()
                .init(SinvieAddReportActivity.this)
                .startSelectPic(MAX_SELECT_RESOURCE - mResourceBeanList.size(),
                        mResultCallbackListener);
    }

    /**
     * 选择图片 / 拍照返回
     */
    private OnResultCallbackListener<LocalMedia> mResultCallbackListener = new OnResultCallbackListener<LocalMedia>() {
        @Override
        public void onResult(List<LocalMedia> result) {
            for (LocalMedia media : result) {
//                DemoLog.i(TAG, "是否压缩:" + media.isCompressed());
//                DemoLog.i(TAG, "压缩:" + media.getCompressPath());
//                DemoLog.i(TAG, "原图:" + media.getPath());
//                DemoLog.i(TAG, "绝对路径:" + media.getRealPath());
//                DemoLog.i(TAG, "是否裁剪:" + media.isCut());
//                DemoLog.i(TAG, "裁剪:" + media.getCutPath());
//                DemoLog.i(TAG, "是否开启原图:" + media.isOriginal());
//                DemoLog.i(TAG, "原图路径:" + media.getOriginalPath());
//                DemoLog.i(TAG, "Android Q 特有Path:" + media.getAndroidQToPath());
//                DemoLog.i(TAG, "宽高: " + media.getWidth() + "x" + media.getHeight());
//                DemoLog.i(TAG, "Size: " + media.getSize());
                //添加 Gv item bean
                SinvieAddResourceBean bean = new SinvieAddResourceBean();
                bean.setAdd(false);
                bean.setImg(true);
                bean.setNetPath(false);
                bean.setResourcePath(media.getRealPath());
                mResourceBeanList.add(bean);
                //添加 文件list
                //绝对路径地址
                mFilePaths.add(media.getRealPath());
            }
            mAddResourceAdapter.notifyDataSetChanged();
        }

        @Override
        public void onCancel() {

        }
    };

    /**
     * 开始保存文件
     */
    private void startSaveReportInfo() {
        int id = -1;
        if (mInfoBean != null) {
            id = mInfoBean.getId();
        }
        mPresenter.submitReport(
                id,
                mEdReportName.getText().toString().trim(),
                mAddress,
                mCaseClassId,
                mLatitude,
                mLongitude,
                mEdReportDes.getText().toString().trim(),
                mAttachmentIds
        );
    }

    /**
     * 数据转换
     */
    public static ArrayList<ISinvieSelectAble> getData(List<SinvieReportTypeBean> beanList) {

        ArrayList<ISinvieSelectAble> data = new ArrayList<>();
        for (SinvieReportTypeBean sinvieReportTypeBean : beanList) {
            data.add(new ISinvieSelectAble() {
                @Override
                public String getName() {
                    return sinvieReportTypeBean.getName();
                }

                @Override
                public int getId() {
                    return sinvieReportTypeBean.getId();
                }

                @Override
                public Object getArg() {
                    return sinvieReportTypeBean.getChildren();
                }
            });
        }
        return data;
    }

    /**
     * 显示添加 图片
     *
     * @return
     */
    private int showAddResourceBtn() {
        if (mResourceBeanList.size() == 4) {
            return View.GONE;
        } else {
            return View.VISIBLE;
        }
    }


    /**
     * 申请相机权限
     */
    private void requestPermission() {
        String[] secondPermission = new String[]{
                Manifest.permission.CAMERA};
        XXPermissions.with(SinvieAddReportActivity.this)
                .permission(secondPermission)
                .request(new OnPermissionCallback() {
                    @Override
                    public void onGranted(List<String> permissions, boolean all) {
                        if (all) {
                            showSelectResDialog();
                        } else {
                            SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_tips));
                        }
                    }

                    @Override
                    public void onDenied(List<String> permissions, boolean never) {
                        if (never) {
                            SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_never));
                        } else {
                            SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_fail));
                        }
                        XXPermissions.startPermissionActivity(SinvieAddReportActivity.this,
                                permissions);
                    }
                });
    }

    /**
     * 请求定位权限
     */
    private void requestLocationPermission() {
        XXPermissions.with(this)
                .permission(Permission.ACCESS_COARSE_LOCATION)
                .permission(Permission.ACCESS_FINE_LOCATION)
                .permission(Permission.ACCESS_BACKGROUND_LOCATION)
//                .permission(Permission.RECORD_AUDIO)
                .request(new OnPermissionCallback() {
                    @Override
                    public void onGranted(List<String> permissions, boolean all) {
                        if (all) {
                            SinvieActivityUtil.startActivityForResult(SinvieAddReportActivity.this,
                                    SinvieIntentConstant.SELECT_ADDRESS_ACTIVITY_ACTION, SinvieUIConstants.SELECT_ADDRESS_REQUEST);
                        } else {
                            SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_tips));
                            XXPermissions.startPermissionActivity(SinvieAddReportActivity.this,
                                    permissions);
                        }
                    }

                    @Override
                    public void onDenied(List<String> permissions, boolean never) {
                        if (never) {
                            SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_never));
                        } else {
                            SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_fail));
                        }
                        XXPermissions.startPermissionActivity(SinvieAddReportActivity.this, permissions);
                    }
                });
    }

    /**
     * 删除选择的 pic
     */
    private void delSelectRes(SinvieAddResourceBean resourceBean) {

        //先删除 adapter list 的图片(显示的list)
        Iterator<SinvieAddResourceBean> iterator = mResourceBeanList.listIterator();
        if (iterator.hasNext()) {
            do {
                SinvieAddResourceBean bean = iterator.next();
                if (bean.getResourcePath().equals(resourceBean.getResourcePath())) {
                    iterator.remove();
                }
            } while (iterator.hasNext());
        }

        //无论是修改 还是 增加 都需要删除 file list (如果有)
        Iterator<String> fileIterator = mFilePaths.listIterator();
        if (fileIterator.hasNext()) {
            do {
                String filePath = fileIterator.next();
                if (filePath.equals(resourceBean.getResourcePath())) {
                    //如果删除的 资源地址相等 删除file list的资源
                    fileIterator.remove();
                }
            } while (fileIterator.hasNext());
        }

        //如果是修改report 那需要额外删除传过来的 资源文件 list中的 attachment id(如果删除了)
        if (mIntentType == SinvieUIConstants.REPORT_TYPE_EDIT) {
            Iterator<String> stringIterator = mAttachmentIds.listIterator();
            if (stringIterator.hasNext()) {
                do {
                    String id = stringIterator.next();
                    if (id.equals(resourceBean.getResourceId() + "")) {
                        stringIterator.remove();
                    }
                } while (stringIterator.hasNext());
            }
        }
    }

}