package com.sinvie.emergency_base.ui.scene.adapter.view_holder;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.ui.scene.adapter.SinvieRoomDifficultListAdapter;
import com.sinvie.emergency_base.utils.SinvieClickUtils;

/**
 * @author: JonsonBob
 * @date: 2021/6/28
 * @Des: 线性列表 标题 ViewHolder
 */
public class SinvieLinearTitleHolder extends SinvieBaseViewHolder {

    private LinearLayout mLlTips;
    private TextView mTitle;

    public SinvieLinearTitleHolder(View itemView) {
        super(itemView);
        initView(itemView);
    }

    private void initView(@NonNull final View itemView) {
        mLlTips = itemView.findViewById(R.id.ll_item_live_room_linear_info_tip);
        mTitle = itemView.findViewById(R.id.tv_item_live_room_linear_news_title);
    }

    @Override
    public void bind(Context context, SinvieRoomDifficultListAdapter.ScenesRoomInfo roomInfo,
                     SinvieRoomDifficultListAdapter.OnItemClickListener listener) {
        mTitle.setText(roomInfo.roomName);
        mLlTips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SinvieClickUtils.isFastClick(v.getId())) {
                    listener.onItemClick(getLayoutPosition(), roomInfo);
                }
            }
        });
    }
}
