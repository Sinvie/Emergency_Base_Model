package com.sinvie.emergency_base.ui.base;

import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.utils.SinvieClickUtils;
import com.sinvie.emergency_base.utils.SinvieDemoLog;


public abstract class SinvieBaseActivity extends AppCompatActivity {

    private static final String TAG = SinvieBaseActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Add a log to load the layout
        SinvieActivityStack.getIns().push(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            getWindow().setStatusBarColor(getResources().getColor(R.color.sinvie_status_bar_color));
            getWindow().setNavigationBarColor(getResources().getColor(R.color.sinvie_navigation_bar_color));
            int vis = getWindow().getDecorView().getSystemUiVisibility();
            vis |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            vis |= View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR;
            getWindow().getDecorView().setSystemUiVisibility(vis);
        }

        //关联mvp / 做一些初始化操作
        initializeView();

        initializeData();

        initializeComposition();

        initBackView();
        SinvieDemoLog.i(TAG, "Activity onCreate: " + getClass().getSimpleName());
    }

    @Override
    protected void onResume() {
        super.onResume();
        SinvieClickUtils.clear();
    }

    public abstract void initializeView();

    public abstract void initializeData();

    public abstract void initializeComposition();

    public abstract void setActivityDestroy();

    protected void initBackView() {
        initBackView(R.id.custom_title_left_group);
    }

    protected void initBackView(int resource) {
        View backView = findViewById(resource);
        if (null != backView) {
            backView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBack();
                }
            });
        }
    }

    protected void onBack() {
        SinvieActivityStack.getIns().popup(this);
        // onBackPressed();
    }

    @Override
    protected void onDestroy() {
        setActivityDestroy();
        SinvieActivityStack.getIns().popup(this);
        super.onDestroy();
    }

    public void showToast(int resId) {
        try {
            Toast.makeText(this, getString(resId), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            // 解决在子线程中调用Toast的异常情况处理
            Looper.prepare();
            Toast.makeText(this, getString(resId), Toast.LENGTH_SHORT).show();
            Looper.loop();
        }
    }

    /**
     * 隐藏 软键盘
     */
    public void hideSoftKeyBorder() {
        //隐藏键盘
        ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)) .hideSoftInputFromWindow(this.getCurrentFocus()
                        .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }


}
