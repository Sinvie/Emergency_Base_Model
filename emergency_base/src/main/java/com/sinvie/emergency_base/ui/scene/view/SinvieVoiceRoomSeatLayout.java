package com.sinvie.emergency_base.ui.scene.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_live.tuikit_live.live.utils.SinvieGlideEngine;
import com.sinvie.emergency_live.tuikit_live.live.component.common.SinvieCircleImageView;

import java.util.ArrayList;
import java.util.List;

public class SinvieVoiceRoomSeatLayout extends RecyclerView {

    private Context mContext;

    private List<VoiceRoomSeatEntity> mVoiceRoomSeatEntities;
    private OnItemClickListener mOnItemClickListener;
    private VoiceRoomSeatAdapter mVoiceRoomSeatAdapter;

    public SinvieVoiceRoomSeatLayout(Context context) {
        super(context);
        initialize(context);
    }

    public SinvieVoiceRoomSeatLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    public SinvieVoiceRoomSeatLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    public void setData(List<VoiceRoomSeatEntity> list) {
        mVoiceRoomSeatEntities.clear();
        mVoiceRoomSeatEntities.addAll(list);
        mVoiceRoomSeatAdapter.notifyDataSetChanged();
    }

    public void addData(List<VoiceRoomSeatEntity> list) {
        mVoiceRoomSeatEntities.addAll(list);
        mVoiceRoomSeatAdapter.notifyDataSetChanged();
    }

    public void setEmptyText(String emptyText) {
        mVoiceRoomSeatAdapter.setEmptyText(emptyText);
        mVoiceRoomSeatAdapter.notifyDataSetChanged();
    }

    public void refresh() {
        mVoiceRoomSeatAdapter.notifyDataSetChanged();
    }

    private void initialize(Context context) {
        mContext = context;
        mVoiceRoomSeatEntities = new ArrayList<>();
        mVoiceRoomSeatAdapter = new VoiceRoomSeatAdapter();
        setLayoutManager(new GridLayoutManager(mContext, 4));
        setAdapter(mVoiceRoomSeatAdapter);
    }

    public class VoiceRoomSeatAdapter extends Adapter<VoiceRoomSeatAdapter.ViewHolder> {
        private String mEmptyText;

        public VoiceRoomSeatAdapter() {
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.view_sinvie_live_voice_room_item_seat_layout, parent, false);
            return new ViewHolder(view);
        }

        public void setEmptyText(String emptyText) {
            mEmptyText = emptyText;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            VoiceRoomSeatEntity item = mVoiceRoomSeatEntities.get(position);
            holder.bind(mContext, item);
        }

        @Override
        public int getItemCount() {
            return mVoiceRoomSeatEntities.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public SinvieCircleImageView mCircleImageView;
            public TextView mTextView;
            public FrameLayout mFrameLayoutHeadImg;

            public ViewHolder(View itemView) {
                super(itemView);
                initView();
            }

            public void bind(final Context context, final VoiceRoomSeatEntity entity) {
                itemView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mOnItemClickListener != null) {
                            mOnItemClickListener.onItemClick(entity, getLayoutPosition());
                        }
                    }
                });
                if (entity.isClose) {
                    //close的界面
                    mCircleImageView.setImageResource(R.drawable.ic_sinvie_voice_room_lock);
                    mTextView.setText(SinvieLocContext.getContext().getString(R.string.sinvie_seat_lock));

                    mCircleImageView.setCircleBackgroundColor(context.getResources().getColor(R.color.sinvie_live_voice_room_circle));
                    mFrameLayoutHeadImg.setForeground(null);

                    return;
                }
                if (!entity.isUsed) {
                    // 占位图片
                    mCircleImageView.setImageResource(R.drawable.ic_sinvie_voice_room_head);
                    mTextView.setText(mEmptyText);
                    mTextView.setTextColor(context.getResources().getColor(R.color.sinvie_live_voice_room_text_color_disable));
                } else {
                    if (!TextUtils.isEmpty(entity.userAvatar)) {
                        SinvieGlideEngine.loadImage(mCircleImageView, entity.userAvatar);
                    } else {
                        mCircleImageView.setImageResource(R.drawable.ic_sinvie_voice_room_head);
                    }
                    if (!TextUtils.isEmpty(entity.userName)) {
                        mTextView.setText(entity.userName);
                    } else {
                        mTextView.setText(SinvieLocContext.getContext().getString(R.string.sinvie_find_anchor));
                    }
                }
                if (entity.isMute) {
                    mFrameLayoutHeadImg.setForeground(context.getResources().getDrawable(R.drawable.ic_sinvie_voice_room_head_mute));
                } else {
                    mFrameLayoutHeadImg.setForeground(null);
                }
            }

            private void initView() {
                mCircleImageView = itemView.findViewById(R.id.live_civ_voice_room_item_head);
                mTextView = itemView.findViewById(R.id.live_tv_voice_room_item_name);
                mFrameLayoutHeadImg = itemView.findViewById(R.id.live_fl_voice_room_item_img_head);
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(VoiceRoomSeatEntity entity, int position);
    }

    public static class VoiceRoomSeatEntity {

        public VoiceRoomSeatEntity() {
        }

        public VoiceRoomSeatEntity(String userId, String userName, String userAvatar, boolean isUsed, boolean isClose, boolean isMute) {
            this.userId = userId;
            this.userName = userName;
            this.userAvatar = userAvatar;
            this.isUsed = isUsed;
            this.isClose = isClose;
            this.isMute = isMute;
        }

        public String userId;
        public String userName;
        public String userAvatar;
        public boolean isUsed;
        public boolean isClose;
        public boolean isMute;
    }
}
