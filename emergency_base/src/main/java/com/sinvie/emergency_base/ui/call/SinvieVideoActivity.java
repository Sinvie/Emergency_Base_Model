package com.sinvie.emergency_base.ui.call;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.Gravity;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.huawei.ecterminalsdk.base.TsdkMobileAuidoRoute;
import com.sinvie.emergency_conf.CallService.SinvieCallConstant;
import com.sinvie.emergency_conf.CallService.SinvieCallInfo;
import com.sinvie.emergency_conf.CallService.SinvieCallMgr;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieConfBroadcastConstants;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcast;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcastReceiver;
import com.sinvie.emergency_conf.CommonService.util.SinvieLogUtil;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.logic.call.SinvieIVideoCallContract;
import com.sinvie.emergency_base.logic.call.SinvieVideoCallPresenter;
import com.sinvie.emergency_base.ui.SinvieIntentConstant;
import com.sinvie.emergency_base.ui.base.SinvieMVPBaseActivity;
import com.sinvie.emergency_base.utils.SinvieActivityUtil;
import com.sinvie.emergency_base.utils.SinvieDemoLog;
import com.sinvie.emergency_base.utils.SinvieDialogUtil;
import com.sinvie.emergency_base.utils.SinviePopupWindowUtil;

import org.jetbrains.annotations.NotNull;

import java.lang.ref.WeakReference;

/**
 * This class is about video call activity.
 */
public class SinvieVideoActivity extends SinvieMVPBaseActivity<SinvieIVideoCallContract.VideoCallBaseView,
        SinvieVideoCallPresenter>
        implements View.OnClickListener, SinvieIVideoCallContract.VideoCallBaseView,
        SinvieLocBroadcastReceiver {
    private static final String TAG = SinvieVideoActivity.class.getSimpleName();

    private static final int ADD_LOCAL_VIEW = 101;

    private FrameLayout mHideView;
    private FrameLayout mRemoteView;
    private FrameLayout mLocalView;
    private FrameLayout mVideoHangupArea;
    private TextView mCallNameTv;
    private FrameLayout mHideLocalBtn;
    private FrameLayout mShowLocalBtn;
    private ImageView mCameraStatusImage;
    private TextView mCameraStatusText;
    private ImageView mMediaMoreBtn;
    private LinearLayout mMediaGroupBtn;
    private LinearLayout mSwitchCameraBtn;
    private ImageView mSwitchCameraImage;
    private TextView mSwitchCameraText;
    private FrameLayout mVideoMuteArea;
    private FrameLayout mVideoSpeakerArea;
    private FrameLayout mPlateBtn;
    private PopupWindow mPopupWindow;
    private LinearLayout mPlateArea;
    private ImageView mCloseArea;

    private boolean mIsCameraClose = false;
    private SinvieSecondDialPlateControl mPlateControl;
    private AlertDialog mDialog;

    private final String[] mActions = new String[]{SinvieConfBroadcastConstants.ACTION_CALL_END,
            SinvieConfBroadcastConstants.ADD_LOCAL_VIEW,
            SinvieConfBroadcastConstants.DEL_LOCAL_VIEW,
            SinvieConfBroadcastConstants.CONF_CALL_CONNECTED,
            SinvieConfBroadcastConstants.ACTION_CALL_END_FAILED,
            SinvieConfBroadcastConstants.VIDEO_NO_STREAM_DURATION_MSG,
            SinvieConfBroadcastConstants.CUSTOM_BROADCAST_CONSTANTS_FRONT,
            SinvieConfBroadcastConstants.CUSTOM_BROADCAST_CONSTANTS_BACK,
            SinvieConfBroadcastConstants.ACTION_CALL_ROUTE_CHANGE,
            SinvieConfBroadcastConstants.ACTION_CALL_DEVICES_STATUS_CHANGE
    };

    private static final int NOT_ALPHA = 255;
    private static final int HALF_ALPHA = 127;

    private SinvieCallInfo mSinvieCallInfo;
    private int mCallID;
    private Object thisVideoActivity = this;

    private MyHandler mHandler;

    private class MyHandler extends Handler {

        private WeakReference<SinvieVideoActivity> mWeakReference;

        public MyHandler(@NonNull @NotNull Looper looper, SinvieVideoActivity weakReference) {
            super(looper);
            mWeakReference = new WeakReference<SinvieVideoActivity>(weakReference);
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if (msg.what == ADD_LOCAL_VIEW) {
                mWeakReference.get().addSurfaceView(true);
                mWeakReference.get().mPresenter.setAutoRotation(thisVideoActivity, true);
            }
        }
    }

    private void showDialog() {
        mDialog = SinvieDialogUtil.generateDialog(SinvieVideoActivity.this, R.string.sinvie_ntf_end_call,
                (dialog, which) -> mPresenter.endCall());
        mDialog.show();
    }

    @Override
    public void initializeComposition() {
        // 保持屏幕常亮
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_sinvie_call_video);
        mRemoteView = (FrameLayout) findViewById(R.id.remote_video);
        mLocalView = (FrameLayout) findViewById(R.id.local_video);
        mHideView = (FrameLayout) findViewById(R.id.hide_video_view);
        mVideoHangupArea = (FrameLayout) findViewById(R.id.video_hangup_area);
        mVideoMuteArea = (FrameLayout) findViewById(R.id.video_mute_area);
        mPlateBtn = (FrameLayout) findViewById(R.id.video_plate_area);
        mVideoSpeakerArea = (FrameLayout) findViewById(R.id.video_speaker_area);
        mCallNameTv = (TextView) findViewById(R.id.call_name);
        mHideLocalBtn = (FrameLayout) findViewById(R.id.local_video_hide_btn);
        mShowLocalBtn = (FrameLayout) findViewById(R.id.local_video_show_btn);
        mMediaMoreBtn = (ImageView) findViewById(R.id.media_btn_more);
        mMediaGroupBtn = (LinearLayout) findViewById(R.id.media_btn_group);
        mPlateArea = (LinearLayout) findViewById(R.id.dial_plate_area);
        mCloseArea = (ImageView) findViewById(R.id.hide_dial_btn);
        ImageView audioBtn = (ImageView) findViewById(R.id.call_audio_btn);
        ImageView videoBtn = (ImageView) findViewById(R.id.call_video_btn);
        audioBtn.setVisibility(View.GONE);
        videoBtn.setVisibility(View.GONE);
        mPlateArea.setVisibility(View.GONE);

        mCallNameTv.setText(getString(R.string.sinvie_call_number) + mPresenter.getOppositeNumber());

        mVideoHangupArea.setOnClickListener(this);
        mHideLocalBtn.setOnClickListener(this);
        mShowLocalBtn.setOnClickListener(this);
        mMediaMoreBtn.setOnClickListener(this);
        mVideoMuteArea.setOnClickListener(this);
        mVideoSpeakerArea.setOnClickListener(this);
        mPlateBtn.setOnClickListener(this);
        mCloseArea.setOnClickListener(this);

        mLocalView.setVisibility(View.VISIBLE);
        mPlateControl = new SinvieSecondDialPlateControl(mPlateArea, this.mCallID);

        int currentAudioRoute = SinvieCallMgr.getInstance().getCurrentAudioRoute();
        if (currentAudioRoute != SinvieCallConstant.TYPE_LOUD_SPEAKER) {
            SinvieCallMgr.getInstance().setAudioRoute(TsdkMobileAuidoRoute.TSDK_E_MOBILE_AUDIO_ROUTE_LOUDSPEAKER);
        }
        mVideoSpeakerArea.setActivated(true);
    }

    @Override
    public void onReceive(String broadcastName, final Object obj) {
        SinvieDemoLog.i(TAG, "broadcastName: " + broadcastName + ", Object:" + new Gson().toJson(obj));
        switch (broadcastName) {
            case SinvieConfBroadcastConstants.ACTION_CALL_END:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        callClosed();
                        finish();
                    }
                });
                break;

            case SinvieConfBroadcastConstants.ADD_LOCAL_VIEW:
                mHandler.sendEmptyMessage(ADD_LOCAL_VIEW);
                break;

            case SinvieConfBroadcastConstants.DEL_LOCAL_VIEW:
                break;

            case SinvieConfBroadcastConstants.CONF_CALL_CONNECTED:
                finish();
                break;
            case SinvieConfBroadcastConstants.ACTION_CALL_END_FAILED:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                });
                break;
            case SinvieConfBroadcastConstants.VIDEO_NO_STREAM_DURATION_MSG:
                final int duration = (int) obj;
                SinvieVideoActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(SinvieVideoActivity.this, "无码流时长：" + duration,
                                Toast.LENGTH_SHORT).show();
                    }
                });
                break;
            case SinvieConfBroadcastConstants.CUSTOM_BROADCAST_CONSTANTS_FRONT:
                if (!mIsCameraClose) {
                    mPresenter.switchCameraStatus(false);
                }
                break;
            case SinvieConfBroadcastConstants.CUSTOM_BROADCAST_CONSTANTS_BACK:
                if (!mIsCameraClose) {
                    mPresenter.switchCameraStatus(true);
                }
                break;
            case SinvieConfBroadcastConstants.ACTION_CALL_ROUTE_CHANGE:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int currentAudioRoute = SinvieCallMgr.getInstance().getCurrentAudioRoute();
                        mVideoSpeakerArea.setActivated(currentAudioRoute == SinvieCallConstant.TYPE_LOUD_SPEAKER);
                    }
                });
                break;
            case SinvieConfBroadcastConstants.ACTION_CALL_DEVICES_STATUS_CHANGE:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int route = (int) obj;
                        if (route != 2 && route != 0) {
                            return;
                        }

                        SinvieCallMgr.getInstance().setAudioRoute(TsdkMobileAuidoRoute.TSDK_E_MOBILE_AUDIO_ROUTE_LOUDSPEAKER);
                        mVideoSpeakerArea.setActivated(true);
                    }
                });
                break;
            default:
                break;
        }
    }

    /**
     * On call closed.
     */
    private void callClosed() {
        SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "onCallClosed enter.");
        mPresenter.videoDestroy();
    }

    @Override
    public void initializeData() {
        Intent intent = getIntent();
        mSinvieCallInfo = (SinvieCallInfo) intent.getSerializableExtra(SinvieUIConstants.CALL_INFO);
        SinvieLogUtil.d(SinvieLogUtil.DEMO_LOG, new Gson().toJson(mSinvieCallInfo));
        mPresenter.setCurrentCallInfo(mSinvieCallInfo);
        this.mCallID = mSinvieCallInfo.getCallID();

        mHandler = new MyHandler(Looper.myLooper(), this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        SinvieLocBroadcast.getInstance().registerBroadcast(this, mActions);
        addSurfaceView(false);
        mPresenter.setAutoRotation(this, true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SinvieLocBroadcast.getInstance().unRegisterBroadcast(this, mActions);
        mPresenter.setAutoRotation(this, false);
        dismissDialog(mDialog);
        SinviePopupWindowUtil.getInstance().dismissPopupWindow(mPopupWindow);
    }

    private void dismissDialog(AlertDialog dialog) {
        if (null != dialog) {
            dialog.dismiss();
        }
    }

    @Override
    protected SinvieIVideoCallContract.VideoCallBaseView createView() {
        return this;
    }

    @Override
    protected SinvieVideoCallPresenter createPresenter() {
        return new SinvieVideoCallPresenter(this);
    }

    @Override
    public void onBackPressed() {
        showDialog();
    }

    @Override
    protected void onBack() {
        showDialog();
    }

    @Override
    public void onClick(View view) {

        int id = view.getId();
        if (id == R.id.video_hangup_area) {
            showDialog();
        } else if (id == R.id.local_video_hide_btn) {//只隐藏本地窗口，并不关闭本地视频
            mShowLocalBtn.setVisibility(View.VISIBLE);
            mHideLocalBtn.setVisibility(View.INVISIBLE);

            mLocalView.setVisibility(View.INVISIBLE);
            mHideView.setVisibility(View.VISIBLE);
            mPresenter.getLocalVideoView().setVisibility(View.INVISIBLE);
        } else if (id == R.id.local_video_show_btn) {//重新显示本地窗口，无需再打开本地视频
            mShowLocalBtn.setVisibility(View.INVISIBLE);
            mHideLocalBtn.setVisibility(View.VISIBLE);

            mLocalView.setVisibility(View.VISIBLE);
            mHideView.setVisibility(View.INVISIBLE);
            mPresenter.getLocalVideoView().setVisibility(View.VISIBLE);
        } else if (id == R.id.media_btn_more) {
            View popupView = getLayoutInflater().inflate(R.layout.view_sinvie_popup_video_call, null);

            mSwitchCameraBtn = (LinearLayout) popupView.findViewById(R.id.switch_camera_btn);
            LinearLayout switchCameraStatusBtn =
                    (LinearLayout) popupView.findViewById(R.id.switch_camera_status_btn);
            LinearLayout videoSwitchAudioBtn =
                    (LinearLayout) popupView.findViewById(R.id.video_switch_audio_btn);
            LinearLayout videoHoldBtn = (LinearLayout) popupView.findViewById(R.id.video_hold);
            LinearLayout p2pConfBtn = (LinearLayout) popupView.findViewById(R.id.p2p_conf);
            LinearLayout netQuality =
                    (LinearLayout) popupView.findViewById(R.id.net_quality_ll);

            mCameraStatusImage = (ImageView) popupView.findViewById(R.id.iv_camera_status);
            mCameraStatusText = (TextView) popupView.findViewById(R.id.tv_camera_status);
            mSwitchCameraImage = (ImageView) popupView.findViewById(R.id.iv_camera_switch);
            mSwitchCameraText = (TextView) popupView.findViewById(R.id.tv_camera_switch);

            mCameraStatusText.setText(mIsCameraClose ? getString(R.string.sinvie_open_local_camera) :
                    getString(R.string.sinvie_close_local_camera));

            switchCameraStatusBtn.setOnClickListener(this);
            mSwitchCameraBtn.setOnClickListener(this);
            videoSwitchAudioBtn.setOnClickListener(this);
            videoHoldBtn.setOnClickListener(this);
            p2pConfBtn.setOnClickListener(this);
            netQuality.setOnClickListener(this);
            mPopupWindow = SinviePopupWindowUtil.getInstance().generatePopupWindow(popupView);
            mPopupWindow.showAtLocation(findViewById(R.id.video_call_area),
                    Gravity.RIGHT | Gravity.BOTTOM, 0, mMediaGroupBtn.getHeight());
        } else if (id == R.id.switch_camera_btn) {// 摄像头关闭在UI层不能切换摄像头
            if (mIsCameraClose) {
                showToast(R.string.sinvie_camera_close_can_not_switch);
                return;
            }
            mPresenter.switchCamera();
        } else if (id == R.id.switch_camera_status_btn) {
            mIsCameraClose = !mIsCameraClose;
            mCameraStatusImage.setActivated(mIsCameraClose);
            mCameraStatusText.setText(mIsCameraClose ? getString(R.string.sinvie_open_local_camera) :
                    getString(R.string.sinvie_close_local_camera));

            mPresenter.switchCameraStatus(mIsCameraClose);

            mSwitchCameraImage.getDrawable().setAlpha(!mIsCameraClose ? NOT_ALPHA : HALF_ALPHA);
            mSwitchCameraText.setActivated(mIsCameraClose);
        } else if (id == R.id.video_switch_audio_btn) {
            mPresenter.videoToAudio();
        } else if (id == R.id.video_hold) {
            mPresenter.holdVideo();
        } else if (id == R.id.p2p_conf) {
            mPresenter.transferToConference();
        } else if (id == R.id.video_mute_area) {
            mPresenter.muteCall();
        } else if (id == R.id.video_speaker_area) {
            mPresenter.switchAudioRoute();
        } else if (id == R.id.video_plate_area) {
            mPlateControl.showDialPlate();
        } else if (id == R.id.hide_dial_btn) {
            mPlateControl.hideDialPlate();
        } else if (id == R.id.net_quality_ll) {
            Intent intent = new Intent(SinvieIntentConstant.CALL_QUALITY_ACTIVITY_ACTION);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addCategory(SinvieIntentConstant.DEFAULT_CATEGORY);
            intent.putExtra(SinvieUIConstants.CALL_INFO, mCallID);
            SinvieActivityUtil.startActivity(SinvieVideoActivity.this, intent);
        }
    }

    private void addSurfaceView(ViewGroup container, SurfaceView child) {
        if (child == null) {
            return;
        }
        if (child.getParent() != null) {
            ViewGroup vGroup = (ViewGroup) child.getParent();
            vGroup.removeAllViews();
        }
        container.addView(child);
    }

    private void addSurfaceView(boolean onlyLocal) {
        if (!onlyLocal) {
            addSurfaceView(mRemoteView, mPresenter.getRemoteVideoView());
        }
        addSurfaceView(mLocalView, mPresenter.getLocalVideoView());
        addSurfaceView(mHideView, mPresenter.getHideVideoView());
    }

    @Override
    public void switchMuteBtn(boolean currentMuteStatus) {
        mVideoMuteArea.setActivated(!currentMuteStatus);
    }
}
