package com.sinvie.emergency_base.ui.scene.adapter.view_holder;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sinvie.emergency_base.ui.scene.adapter.SinvieRoomDifficultListAdapter;

import org.jetbrains.annotations.NotNull;

/**
 * @author: JonsonBob
 * @date: 2021/6/29
 * @Des: 类描述
 */
public abstract class SinvieBaseViewHolder extends RecyclerView.ViewHolder {


    public abstract void bind(Context context,
                              final SinvieRoomDifficultListAdapter.ScenesRoomInfo roomInfo,
                              final SinvieRoomDifficultListAdapter.OnItemClickListener listener);

    public SinvieBaseViewHolder(@NonNull @NotNull View itemView) {
        super(itemView);
    }
}
