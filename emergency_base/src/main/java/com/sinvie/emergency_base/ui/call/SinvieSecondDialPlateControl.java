package com.sinvie.emergency_base.ui.call;

import android.view.View;

import com.sinvie.emergency_conf.CallService.SinvieCallMgr;
import com.sinvie.emergency_base.ui.base.SinvieBaseDialPlateControl;

/**
 * This class is about secondary dial plate control.
 */
public class SinvieSecondDialPlateControl extends SinvieBaseDialPlateControl
{
    private int callID;
    public SinvieSecondDialPlateControl(View plate, int callID)
    {
        super(plate);
        this.callID = callID;
    }

    @Override
    protected void handleOnClick(View v)
    {
        Integer obj = (Integer) v.getTag();
        int index = obj.intValue();
        if (index != -1)
        {
            mNumInputEt.append(CODE_ARRAY[index]);
            mNumInputEt.setSelection(mNumInputEt.length());
            SinvieCallMgr.getInstance().reDial(this.callID, index);
        }
    }

    @Override
    protected void handleOnLongClick(View v)
    {
    }
}
