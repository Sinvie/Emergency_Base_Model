package com.sinvie.emergency_base.ui.login

import android.content.Context.LOCATION_SERVICE
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Handler
import android.os.HandlerThread
import android.util.Log
import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext
import com.sinvie.emergency_base.bean.SinvieAddrReportBean
import com.sinvie.emergency_base.http.base.SinvieBaseObserver
import com.sinvie.emergency_base.http.common.covert
import com.sinvie.emergency_base.http.common.execute
import com.sinvie.emergency_base.http.repository.reportLocation
import java.util.*

/**
 * @param
 * @author JonsonBob
 * @description 心跳包调整
 * @return
 * @time 2021/6/28 10:53
 */
class SinvieLocationHeartBeat() : Runnable {
    private var mHandlerThread: HandlerThread? = null
    private var mHeartbeatHandler: Handler? = null
    private var mSinvieAddrInfo: SinvieAddrReportBean? = null

    fun start() {
        val locationManager = SinvieLocContext.getContext().getSystemService(LOCATION_SERVICE) as LocationManager
        var location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        if (location == null) {
            Log.e(TAG,"GPS位置信息为空")
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
            if (location == null) {
                Log.e(TAG,"网络位置信息也为空")
            } else {
                Log.d(TAG,"网络位置信息:$location")
                mSinvieAddrInfo = SinvieAddrReportBean(contactorId = SinvieUserInfo.getInstance().userId.toInt(),
                    longitude = location.longitude,
                    latitude = location.latitude,
                    type = 0,
                    address = location.getAddress()
                )
                mHandlerThread = HandlerThread(HEARTBEAT_THREAD_NAME)
                mHandlerThread!!.start()
                postHeartbeat()
            }
        } else {
            Log.d(TAG,"GPS位置信息:$location")
            mSinvieAddrInfo = SinvieAddrReportBean(contactorId = SinvieUserInfo.getInstance().userId.toInt(),
                longitude = location.longitude,
                latitude = location.latitude,
                type = 0,
                address = location.getAddress()
            )
            mHandlerThread = HandlerThread(HEARTBEAT_THREAD_NAME)
            mHandlerThread!!.start()
            postHeartbeat()
        }
        mHeartbeatHandler = Handler()
        sendHeartbeatMessage()
    }

    fun stop() {
        if (mHeartbeatHandler != null) {
            mHeartbeatHandler!!.removeCallbacks(this)
        }
        mHeartbeatHandler = null
        if (mHandlerThread != null) {
            mHandlerThread!!.quit()
        }
        mHandlerThread = null
    }

    private fun Location.getAddress(): String? {
        val gc = Geocoder(SinvieLocContext.getContext(), Locale.getDefault())
        val resultList = gc.getFromLocation(
            this.latitude,
            this.longitude, 1
        )
        return if (resultList.isNotEmpty()) resultList[0].featureName else null
    }

    override fun run() {
        postHeartbeat()
        sendHeartbeatMessage()
    }

    private fun sendHeartbeatMessage() {
        mHeartbeatHandler!!.postDelayed(this, HEARTBEAT_INTERVAL)
    }

    private fun postHeartbeat() {
        reportLocation(mSinvieAddrInfo!!).covert().execute((object : SinvieBaseObserver<String>() {
            override fun onSuccess(t: String?) {}
            override fun onFailure(errCode: Int, errMsg: String) {}
        }))
    }

    companion object {
        private val TAG = SinvieLocationHeartBeat::class.java.simpleName
        private val HEARTBEAT_THREAD_NAME = "heartbeat_location"
        var instance = SinvieLocationHeartBeat()

        // 心跳上报时间间隔
        private val HEARTBEAT_INTERVAL = (60 * 1000).toLong()
    }
}