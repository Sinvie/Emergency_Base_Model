package com.sinvie.emergency_base.ui.login;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.widget.SinvieCustomTitleBarLayout;
import com.sinvie.emergency_conf.ServiceMgr.SinvieServiceMgr;
import com.sinvie.emergency_live.tuikit.base.ISinvieTitleBarLayout;
import com.sinvie.emergency_live.tuikit.utils.SinvieToastUtil;

public class SinvieLoginSettingActivity extends AppCompatActivity {

    private SinvieCustomTitleBarLayout mTblLoginSetting;
    private LinearLayoutCompat mLlLoginSettingIp;
    private LinearLayoutCompat mLlLoginSettingPort;
    private EditText mEdLoginSettingIpAddress;
    private EditText mEdLoginSettingPort;
    private TextView mTvLoginSettingSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sinvie_login_setting);
        initView();
    }

    private void initView() {
        mTblLoginSetting = findViewById(R.id.tbl_login_setting);
        mLlLoginSettingIp = findViewById(R.id.ll_login_setting_ip);
        mLlLoginSettingPort = findViewById(R.id.ll_login_setting_port);
        mEdLoginSettingIpAddress = findViewById(R.id.ed_login_setting_ip_address);
        mEdLoginSettingPort = findViewById(R.id.ed_login_setting_port);
        mTvLoginSettingSave = findViewById(R.id.tv_login_setting_save);

        initTitleBar();

        mTvLoginSettingSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ipAddress = mEdLoginSettingIpAddress.getText().toString().trim();
                String port = mEdLoginSettingPort.getText().toString().trim();
                if (ipAddress.isEmpty()) {
                    SinvieToastUtil.toastShortMessage(getString(R.string.sinvie_ip_address_empty));
                    return;
                }

                if (port.isEmpty()) {
                    SinvieToastUtil.toastShortMessage(getString(R.string.sinvie_port_empty));
                    return;
                }
                SinvieUserInfo.getInstance().setTupRegisterServer(ipAddress);
                SinvieUserInfo.getInstance().setTupPort(port);

                //初始化与去初始化业务组件类 设置网络配置
                SinvieServiceMgr.getServiceMgr().networkParam(
                        SinvieUserInfo.getInstance().getTupPort(),
                        SinvieUserInfo.getInstance().getTupRegisterServer(),
                        SinvieUserInfo.getInstance().getSipTransport());
                finish();
            }
        });

        mEdLoginSettingIpAddress.setText(SinvieUserInfo.getInstance().getTupRegisterServer());
        mEdLoginSettingPort.setText(SinvieUserInfo.getInstance().getTupPort());
    }

    private void initTitleBar() {
        //隐藏右边的图标
        mTblLoginSetting.getRightIcon().setVisibility(View.GONE);
        //填充左边的后退图标
        mTblLoginSetting.setLeftIcon(R.drawable.svg_sinvie_theme_back);
        mTblLoginSetting.setOnLeftClickListener(v -> {
            //结束界面
            finish();
        });
        //设置中间标题
        mTblLoginSetting.setTitle(getString(R.string.sinvie_login_setting_title),
                ISinvieTitleBarLayout.POSITION.MIDDLE);
    }

}