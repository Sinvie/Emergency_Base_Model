package com.sinvie.emergency_base.ui.play;

import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.MediaController;

import androidx.appcompat.app.AppCompatActivity;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.utils.SinvieDemoLog;
import com.sinvie.emergency_base.widget.SinvieCustomVideoView;
import com.xiasuhuei321.loadingdialog.view.LoadingDialog;

public class SinvieVideoPlayActivity extends AppCompatActivity {

    private static final String TAG = SinvieVideoPlayActivity.class.getSimpleName();
    private SinvieCustomVideoView mVvPlay;
    private ImageView mIvPlayVideoBack;

    private LoadingDialog mLoadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sinvie_video_play);
        initView();
    }

    private void initView() {
        mVvPlay = findViewById(R.id.vv_play);
        mIvPlayVideoBack = findViewById(R.id.iv_play_video_back);

        String url = getIntent().getStringExtra(SinvieUIConstants.PLAY_VIDEO_URL);
        SinvieDemoLog.i(TAG, url);

        mVvPlay.setVideoURI(Uri.parse(url));

        MediaController mediaController = new MediaController(this);
        mediaController.setMediaPlayer(mVvPlay);
        mVvPlay.setMediaController(mediaController);

        mIvPlayVideoBack.setOnClickListener(v -> finish());

        mVvPlay.setPlayPauseListener(new SinvieCustomVideoView.PlayPauseListener() {
            @Override
            public void onPlay() {
                //关闭等待弹窗
                dismissLoadingDialog();
            }

            @Override
            public void onPause() {
                //复现暂停弹窗

            }
        });

        //显示 mediacontroller 控制器
        mediaController.show();
        //开始播放
        mVvPlay.start();
        //显示等待弹窗
        showLoadingDialog();
    }


    private void showLoadingDialog() {
        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog(this);
        }
        mLoadingDialog.setLoadingText(getString(R.string.sinvie_video_play_loading))
                .setInterceptBack(true)
                .setLoadSpeed(LoadingDialog.Speed.SPEED_TWO)
                .setRepeatCount(-1)
                .show();
    }

    private void dismissLoadingDialog() {
        if (mLoadingDialog != null) {
            mLoadingDialog.close();
        }
    }

}