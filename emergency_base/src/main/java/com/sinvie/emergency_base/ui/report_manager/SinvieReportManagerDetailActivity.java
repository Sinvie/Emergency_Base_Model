package com.sinvie.emergency_base.ui.report_manager;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieReportManagerBean;
import com.sinvie.emergency_base.logic.report_manager.report_manager_detail.SinvieReportManagerDetailContact;
import com.sinvie.emergency_base.logic.report_manager.report_manager_detail.SinvieReportManagerDetailPresenter;
import com.sinvie.emergency_base.ui.base.SinvieMVPBaseActivity;
import com.sinvie.emergency_base.ui.report_manager.adapter.SinvieReportManagerRvAdapter;
import com.sinvie.emergency_base.widget.SinvieCustomTitleBarLayout;
import com.sinvie.emergency_live.tuikit.base.ISinvieTitleBarLayout;

import java.util.ArrayList;
import java.util.List;

public class SinvieReportManagerDetailActivity extends SinvieMVPBaseActivity<SinvieReportManagerDetailContact.ReportManagerDetailBaseView,
        SinvieReportManagerDetailPresenter> implements SinvieReportManagerDetailContact.ReportManagerDetailBaseView {

    private SinvieCustomTitleBarLayout mReportManagerDetailsTitle;
    private com.scwang.smart.refresh.layout.SmartRefreshLayout mSmartReportManagerDetail;
    private androidx.recyclerview.widget.RecyclerView mRvReportManagerDetail;

    private SinvieReportManagerRvAdapter mRvAdapter;

    @Override
    public void initializeData() {

    }

    @Override
    public void initializeComposition() {
        setContentView(R.layout.activity_sinvie_report_manager_detail);

        mReportManagerDetailsTitle = findViewById(R.id.report_manager_details_title);
        mSmartReportManagerDetail = findViewById(R.id.smart_report_manager_detail);
        mRvReportManagerDetail = findViewById(R.id.rv_report_manager_detail);

        mReportManagerDetailsTitle.setTitle(getString(R.string.sinvie_report_manager_detail_title), ISinvieTitleBarLayout.POSITION.MIDDLE);
        mReportManagerDetailsTitle.setLeftIcon(R.drawable.svg_sinvie_theme_back);

        mSmartReportManagerDetail.setEnableRefresh(false);
        mSmartReportManagerDetail.setEnableLoadMore(false);

        mRvAdapter = new SinvieReportManagerRvAdapter(this, getReportManagerBeanList());
        mRvReportManagerDetail.setLayoutManager(new LinearLayoutManager(this));
        mRvReportManagerDetail.setAdapter(mRvAdapter);
    }

    private List<SinvieReportManagerBean> getReportManagerBeanList() {
        List<SinvieReportManagerBean> managerBeanList = new ArrayList<>();
        String[] name_1 = {
                "事件标题",
                "事发时间",
                "接报时间",
                "事故单位",
                "辖区范围",
                "事发地点",
                "落图地点",
                "报告单位",
                "事件类型"
        };
        String[] name_2 = {
                "受伤人数(人)",
                "失踪人数(人)",
                "受困人数(人)"
        };
        String[] name_3 = {
                "事发原因",
                "已造成后果",
                "事件发展趋势",
                "已采取措施"
        };
        String[] content_1 = {
                "深证市光明区XX公司危险化学品泄漏",
                "2021年8月23日15:37:49",
                "2021年8月23日15:37:45",
                "深证市光明区XX公司",
                "辖区范围",
                "深证市光明区XX公司",
                "落图地点",
                "光明区XX街道办",
                "公共安全"
        };
        String[] content_2 = {
                "22",
                "0",
                "5"
        };
        String[] content_3 = {
                "事发原因",
                "已造成后果",
                "事件发展趋势",
                "已采取措施"
        };
        for (int i = 0; i < name_1.length; i++) {
            managerBeanList.add(new SinvieReportManagerBean(name_1[i], content_1[i], 0, null));
        }
        managerBeanList.add(new SinvieReportManagerBean(null, null, 1, "信息要素"));
        for (int i = 0; i < name_2.length; i++) {
            managerBeanList.add(new SinvieReportManagerBean(name_2[i], content_2[i], 0, null));
        }
        managerBeanList.add(new SinvieReportManagerBean(null, null, 1, "扩展信息"));
        for (int i = 0; i < name_3.length; i++) {
            managerBeanList.add(new SinvieReportManagerBean(name_3[i], content_3[i], 0, null));
        }
        return managerBeanList;
    }

    @Override
    protected SinvieReportManagerDetailContact.ReportManagerDetailBaseView createView() {
        return this;
    }

    @Override
    protected SinvieReportManagerDetailPresenter createPresenter() {
        return new SinvieReportManagerDetailPresenter();
    }

}