package com.sinvie.emergency_base.ui.call;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.huawei.ecterminalsdk.base.TsdkMobileAuidoRoute;
import com.sinvie.emergency_base.logic.call.SinvieCallFunc;
import com.sinvie.emergency_conf.CallService.SinvieCallConstant;
import com.sinvie.emergency_conf.CallService.SinvieCallInfo;
import com.sinvie.emergency_conf.CallService.SinvieCallMgr;
import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieConfBroadcastConstants;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcast;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcastReceiver;
import com.sinvie.emergency_conf.CommonService.util.SinvieLogUtil;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.ui.SinvieIntentConstant;
import com.sinvie.emergency_base.ui.base.SinvieBaseActivity;
import com.sinvie.emergency_base.utils.SinvieActivityUtil;
import com.sinvie.emergency_base.utils.SinvieDialogUtil;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;

/**
 * This class is about base media activity.
 */
public class SinvieBaseMediaActivity extends SinvieBaseActivity implements View.OnClickListener, SinvieLocBroadcastReceiver {

    private static final String TAG = SinvieBaseMediaActivity.class.getSimpleName();

    private static final int CALL_CONNECTED = 100;
    private static final int CALL_UPGRADE = 101;
    private static final int HOLD_CALL_SUCCESS = 102;
    private static final int VIDEO_HOLD_CALL_SUCCESS = 103;
    private static final int MEDIA_CONNECTED = 104;
    private static final int BLD_TRANSFER_SUCCESS = 105;
    private static final int BLD_TRANSFER_FAILED = 106;
    private static final int AUDIO_TO_VIDEO_FAILED = 107;

    protected ImageView mRejectBtn;
    protected FrameLayout mAudioAcceptCallArea;
    protected FrameLayout mVideoAcceptCallArea;
    protected FrameLayout mDivertCallArea;
    private Timer mDismissDialogTimer;
    private static final int CANCEL_TIME = 25000;

    protected LinearLayout mPlateButton;
    protected LinearLayout mBlindTransferButton;
    protected LinearLayout mHoldCallButton;
    protected LinearLayout mTransferMeeting;
    protected LinearLayout mMuteArea;
    protected LinearLayout mPlateArea;
    protected LinearLayout mNetQuality;
    protected ImageView mCloseArea;

    protected TextView mCallNumberTv;
    protected TextView mCallNameTv;
    protected TextView mHoldCallText;
    protected Toast toast;

    protected String mCallNumber;
    protected String mDisplayName;
    protected boolean mIsVideoCall;
    protected int mCallID;
    protected String mConfID;
    protected boolean mIsConfCall;
    protected int mConfToCallHandle;

    protected SinvieSecondDialPlateControl mPlateControl;

    private String[] mActions = new String[]{SinvieConfBroadcastConstants.ACTION_CALL_CONNECTED,
            SinvieConfBroadcastConstants.CALL_MEDIA_CONNECTED,
            SinvieConfBroadcastConstants.CONF_CALL_CONNECTED,
            SinvieConfBroadcastConstants.ACTION_CALL_END,
            SinvieConfBroadcastConstants.CALL_UPGRADE_ACTION,
            SinvieConfBroadcastConstants.HOLD_CALL_RESULT,
            SinvieConfBroadcastConstants.BLD_TRANSFER_RESULT,
            SinvieConfBroadcastConstants.VIDEO_NO_STREAM_DURATION_MSG,
            SinvieConfBroadcastConstants.CALL_TRANSFER_TO_CONFERENCE,
            SinvieConfBroadcastConstants.CALL_UPDATE_AUDIO_TO_VIDEO,
            SinvieConfBroadcastConstants.ACTION_CALL_ROUTE_CHANGE,
            SinvieConfBroadcastConstants.ACTION_CALL_DEVICES_STATUS_CHANGE
    };
    protected LinearLayout mSpeakerButton;
    private LinearLayout mUpgradeVideoArea;

    private SinvieCallFunc mSinvieCallFunc;

   private MyHandler mHandler;

    private class MyHandler extends Handler{
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MEDIA_CONNECTED:
                    mPlateButton.setVisibility(View.VISIBLE);
                    break;

                case CALL_CONNECTED:
                    showButtons();
                    //boolean isVideoCall = (boolean) msg.obj;
                    mAudioAcceptCallArea.setVisibility(View.GONE);
                    mVideoAcceptCallArea.setVisibility(View.GONE);
                    mDivertCallArea.setVisibility(View.GONE);

                    if (msg.obj instanceof SinvieCallInfo) {
                        SinvieCallInfo sinvieCallInfo = (SinvieCallInfo) msg.obj;
                        if (sinvieCallInfo.isVideoCall()) {

                            Intent intent = new Intent(SinvieIntentConstant.VIDEO_ACTIVITY_ACTION);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addCategory(SinvieIntentConstant.DEFAULT_CATEGORY);

                            intent.putExtra(SinvieUIConstants.CALL_INFO, sinvieCallInfo);
                            SinvieActivityUtil.startActivity(SinvieBaseMediaActivity.this, intent);

                            finish();
                            break;
                        }
                        int currentAudioRoute = SinvieCallMgr.getInstance().getCurrentAudioRoute();
                        if (currentAudioRoute == SinvieCallConstant.TYPE_LOUD_SPEAKER) {
                            SinvieCallMgr.getInstance().setAudioRoute(TsdkMobileAuidoRoute.TSDK_E_MOBILE_AUDIO_ROUTE_DEFAULT);
                        }
                        mSpeakerButton.setActivated(false);
                    }
                    break;
                case CALL_UPGRADE:
                    mDialog = SinvieDialogUtil.generateDialog(SinvieBaseMediaActivity.this, R.string.sinvie_ntf_upgrade_videocall,
                            R.string.sinvie_accept, R.string.sinvie_reject,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    cancelDisDiaTimer();
                                    Executors.newSingleThreadExecutor().execute(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (null == Looper.myLooper()) {
                                                Looper.prepare();
                                            }
                                            SinvieCallMgr.getInstance().acceptAddVideo(mCallID);
                                        }
                                    });
                                }
                            }, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    cancelDisDiaTimer();
                                    SinvieCallMgr.getInstance().rejectAddVideo(mCallID);
                                }
                            });
                    mDialog.show();
                    startDismissDiaLogTimer();
                    break;
                case HOLD_CALL_SUCCESS: {
                    String textDisplayName = null == mDisplayName ? "" : mDisplayName;
                    String textCallNumber = null == mCallNumber ? "" : mCallNumber;
                    if ("Hold".equals(mCallNumberTv.getTag())) {
                        textCallNumber = textCallNumber + "Holding";
                    }
                    mCallNameTv.setText(textDisplayName);
                    mCallNumberTv.setText(textCallNumber);
                }
                break;
                case VIDEO_HOLD_CALL_SUCCESS: {
                    String textDisplayName = null == mDisplayName ? "" : mDisplayName;
                    String textCallNumber = null == mCallNumber ? "" : mCallNumber;
                    textCallNumber = textCallNumber + "Holding";
                    mCallNameTv.setText(textDisplayName);
                    mCallNumberTv.setText(textCallNumber);
                    mHoldCallText.setText(R.string.sinvie_un_hold_call);
                }
                break;
                case BLD_TRANSFER_SUCCESS:
                    Toast.makeText(SinvieBaseMediaActivity.this, "Blind transfer success", Toast.LENGTH_SHORT).show();
                    break;
                case BLD_TRANSFER_FAILED:
                    Toast.makeText(SinvieBaseMediaActivity.this, "Blind transfer failed", Toast.LENGTH_SHORT).show();
                    break;
                case AUDIO_TO_VIDEO_FAILED:
                    setUpdateVideoCallStatus(false);
                    break;
                default:
                    break;
            }
        }
    }


    private void startDismissDiaLogTimer() {
        cancelDisDiaTimer();

        mDismissDialogTimer = new Timer("Dismiss Dialog");
        DismissDialogTimerTask dismissDialogTimerTask = new DismissDialogTimerTask(mDialog, mCallID);
        mDismissDialogTimer.schedule(dismissDialogTimerTask, CANCEL_TIME);
    }

    private void cancelDisDiaTimer() {
        if (mDismissDialogTimer != null) {
            mDismissDialogTimer.cancel();
            mDismissDialogTimer = null;
        }
    }

    private AlertDialog mDialog;

    private static class DismissDialogTimerTask extends TimerTask {
        private final AlertDialog dialog;
        private int callID;

        public DismissDialogTimerTask(AlertDialog dialog, int callID) {
            this.dialog = dialog;
            this.callID = callID;
        }

        @Override
        public void run() {
            if (null != dialog) {
                dialog.dismiss();
            }

            SinvieCallMgr.getInstance().rejectAddVideo(this.callID);
            SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "dialog time out disAgreeUpg");
        }
    }

    private void hideViews() {
        ImageView deleteNumber = (ImageView) findViewById(R.id.delete_panel_btn);
        ImageView audioBtn = (ImageView) findViewById(R.id.call_audio_btn);
        ImageView videoBtn = (ImageView) findViewById(R.id.call_video_btn);

        audioBtn.setVisibility(View.GONE);
        videoBtn.setVisibility(View.GONE);
        deleteNumber.setVisibility(View.GONE);
        SinvieCallConstant.CallStatus callStatus = SinvieCallMgr.getInstance().getCallStatus(mCallID);
        boolean isCall = (SinvieCallConstant.CallStatus.AUDIO_CALLING == callStatus || SinvieCallConstant.CallStatus.VIDEO_CALLING == callStatus);

        mMuteArea.setVisibility(isCall ? View.VISIBLE : View.GONE);
        mPlateButton.setVisibility(isCall ? View.VISIBLE : View.GONE);
        mSpeakerButton.setVisibility(View.VISIBLE);
        mUpgradeVideoArea.setVisibility(isCall ? View.VISIBLE : View.GONE);
        mBlindTransferButton.setVisibility(isCall ? View.VISIBLE : View.GONE);
        mHoldCallButton.setVisibility(isCall ? View.VISIBLE : View.GONE);
        mNetQuality.setVisibility(isCall ? View.VISIBLE : View.GONE);
        mTransferMeeting.setVisibility(isCall ? View.VISIBLE : View.GONE);
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.plate_btn) {
            mPlateControl.showDialPlate();
        } else if (id == R.id.hide_dial_btn) {
            mPlateControl.hideDialPlate();
        } else if (id == R.id.mute_btn) {
            boolean mMuteStatus = mSinvieCallFunc.isMuteStatus();
            if (SinvieCallMgr.getInstance().muteMic(mCallID, !mMuteStatus)) {
                mSinvieCallFunc.setMuteStatus(!mMuteStatus);
                mMuteArea.setActivated(!mMuteStatus);
            }
        } else if (id == R.id.speaker_btn) {
            SinvieCallMgr.getInstance().switchAudioRoute();
        } else if (id == R.id.upgrade_video_btn) {
            boolean b = SinvieCallMgr.getInstance().addVideo(mCallID);
            if (b) {
                mUpgradeVideoArea.setClickable(false);
            }
        } else if (id == R.id.blind_transfer) {
            final EditText editText = new EditText(this);
            editText.setInputType(InputType.TYPE_CLASS_NUMBER);
            new AlertDialog.Builder(this)
                    .setTitle(R.string.sinvie_ipt_warning)
                    .setMessage(R.string.sinvie_blind_transfer_number)
                    .setView(editText)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton(R.string.sinvie_ipt_sure, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SinvieCallMgr.getInstance().blindTransfer(mCallID, editText.getText().toString().trim());
                        }
                    })
                    .show();
        } else if (id == R.id.hold_call) {
            if (SinvieLocContext.getString(R.string.sinvie_hold_call).equals(mHoldCallText.getText())) {
                mHoldCallText.setText(R.string.sinvie_un_hold_call);
                SinvieCallMgr.getInstance().holdCall(mCallID);
            } else if (SinvieLocContext.getString(R.string.sinvie_un_hold_call).equals(mHoldCallText.getText())) {
                mHoldCallText.setText(R.string.sinvie_hold_call);
                SinvieCallMgr.getInstance().unHoldCall(mCallID);
            }
        } else if (id == R.id.transfer_meeting) {//                MeetingMgr.getInstance().callTransferToConference(mCallID);
        } else if (id == R.id.net_quality) {
            Intent intent = new Intent(SinvieIntentConstant.CALL_QUALITY_ACTIVITY_ACTION);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addCategory(SinvieIntentConstant.DEFAULT_CATEGORY);
            intent.putExtra(SinvieUIConstants.CALL_INFO, mCallID);
            SinvieActivityUtil.startActivity(SinvieBaseMediaActivity.this, intent);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SinvieLocBroadcast.getInstance().unRegisterBroadcast(this, mActions);
        dismissDialog(mDialog);
    }

    protected void dismissDialog(AlertDialog dialog) {
        if (null != dialog) {
            dialog.dismiss();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        SinvieLocBroadcast.getInstance().registerBroadcast(this, mActions);
    }

    @Override
    public void initializeView() {

    }

    @Override
    public void initializeData() {
        mHandler = new MyHandler();

        mSinvieCallFunc = com.sinvie.emergency_base.logic.call.SinvieCallFunc.getInstance();

        Intent intent = getIntent();
        SinvieCallInfo sinvieCallInfo = (SinvieCallInfo) intent.getSerializableExtra(SinvieUIConstants.CALL_INFO);

        SinvieLogUtil.d(TAG,new Gson().toJson(sinvieCallInfo));
        mCallNumber = sinvieCallInfo.getPeerNumber();
        mDisplayName = sinvieCallInfo.getPeerDisplayName();
        mIsVideoCall = sinvieCallInfo.isVideoCall();
        mCallID = sinvieCallInfo.getCallID();
        mConfID = sinvieCallInfo.getConfID();
        mIsConfCall = sinvieCallInfo.isFocus();
        if ((null != mConfID) && (!sinvieCallInfo.getConfID().equals(""))) {
            mConfToCallHandle = Integer.parseInt(sinvieCallInfo.getConfID());
        }
    }

    @Override
    public void initializeComposition() {
        setContentView(R.layout.view_sinvie_call_media);

        mPlateButton = (LinearLayout) findViewById(R.id.plate_btn);
        mBlindTransferButton = (LinearLayout) findViewById(R.id.blind_transfer);
        mHoldCallButton = (LinearLayout) findViewById(R.id.hold_call);
        mSpeakerButton = (LinearLayout) findViewById(R.id.speaker_btn);
        mMuteArea = (LinearLayout) findViewById(R.id.mute_btn);
        mPlateArea = (LinearLayout) findViewById(R.id.dial_plate_area);
        mUpgradeVideoArea = (LinearLayout) findViewById(R.id.upgrade_video_btn);
        mRejectBtn = (ImageView) findViewById(R.id.reject_btn);
        mAudioAcceptCallArea = (FrameLayout) findViewById(R.id.audio_accept_call_area);
        mVideoAcceptCallArea = (FrameLayout) findViewById(R.id.video_accept_call_area);
        mNetQuality = (LinearLayout) findViewById(R.id.net_quality);
        mDivertCallArea = (FrameLayout) findViewById(R.id.divert_call_area);
        mCallNumberTv = (TextView) findViewById(R.id.call_number);
        mTransferMeeting = (LinearLayout) findViewById(R.id.transfer_meeting);

        mCloseArea = (ImageView) findViewById(R.id.hide_dial_btn);
        mCallNameTv = (TextView) findViewById(R.id.call_name);
        mHoldCallText = (TextView) findViewById(R.id.hold_call_text);

        mPlateControl = new SinvieSecondDialPlateControl(mPlateArea, mCallID);
        mPlateControl.hideDialPlate();

        mMuteArea.setOnClickListener(this);
        mCloseArea.setOnClickListener(this);
        mPlateButton.setOnClickListener(this);
        mSpeakerButton.setOnClickListener(this);
        mUpgradeVideoArea.setOnClickListener(this);
        mBlindTransferButton.setOnClickListener(this);
        mHoldCallButton.setOnClickListener(this);
        mHoldCallText.setOnClickListener(this);
        mTransferMeeting.setOnClickListener(this);
        mNetQuality.setOnClickListener(this);

        hideViews();
    }

    @Override
    public void setActivityDestroy() {

    }

    @Override
    public void onReceive(final String broadcastName, final Object obj) {
        switch (broadcastName) {
            case SinvieConfBroadcastConstants.ACTION_CALL_CONNECTED:
                mHandler.sendMessage(mHandler.obtainMessage(CALL_CONNECTED, obj));
                break;
            case SinvieConfBroadcastConstants.VIDEO_NO_STREAM_DURATION_MSG:
                final int duration = (int) obj;
                SinvieBaseMediaActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(SinvieBaseMediaActivity.this, "无码流时长：" + duration, Toast.LENGTH_SHORT).show();
                    }
                });
                break;
            case SinvieConfBroadcastConstants.CALL_MEDIA_CONNECTED:
                mHandler.sendMessage(mHandler.obtainMessage(MEDIA_CONNECTED, obj));
                break;
            case SinvieConfBroadcastConstants.CONF_CALL_CONNECTED:
                finish();
                break;
            case SinvieConfBroadcastConstants.ACTION_CALL_END:
                finish();
                break;
            case SinvieConfBroadcastConstants.CALL_UPGRADE_ACTION:
                mHandler.sendEmptyMessage(CALL_UPGRADE);
                break;
            case SinvieConfBroadcastConstants.HOLD_CALL_RESULT:
                if ("HoldSuccess".equals(obj)) {
                    mCallNumberTv.setTag("Hold");
                    mHandler.sendEmptyMessage(HOLD_CALL_SUCCESS);
                } else if ("UnHoldSuccess".equals(obj)) {
                    mCallNumberTv.setTag("UnHold");
                    mHandler.sendEmptyMessage(HOLD_CALL_SUCCESS);
                } else if ("VideoHoldSuccess".equals(obj)) {
                    mHandler.sendEmptyMessage(VIDEO_HOLD_CALL_SUCCESS);
                }
                break;
            case SinvieConfBroadcastConstants.BLD_TRANSFER_RESULT:
                if ("BldTransferSuccess".equals(obj)) {
                    mHandler.sendEmptyMessage(BLD_TRANSFER_SUCCESS);
                } else if ("BldTransferFailed".equals(obj)) {
                    mHandler.sendEmptyMessage(BLD_TRANSFER_FAILED);
                }
                break;
            case SinvieConfBroadcastConstants.CALL_TRANSFER_TO_CONFERENCE:
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (SinvieCallMgr.getInstance().isResumeHold()) {
                            SinvieCallMgr.getInstance().unHoldCall(SinvieCallMgr.getInstance().getOriginal_CallId());
                        }
                    }
                }, 20000);
                break;
            case SinvieConfBroadcastConstants.CALL_UPDATE_AUDIO_TO_VIDEO:
                if (!(boolean) obj) {
                    mHandler.sendEmptyMessage(AUDIO_TO_VIDEO_FAILED);
                }
                break;
            case SinvieConfBroadcastConstants.ACTION_CALL_ROUTE_CHANGE:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int currentAudioRoute = SinvieCallMgr.getInstance().getCurrentAudioRoute();
                        if (currentAudioRoute == SinvieCallConstant.TYPE_LOUD_SPEAKER) {
                            mSpeakerButton.setActivated(true);
                            return;
                        }
                        mSpeakerButton.setActivated(false);
                    }
                });
                break;
            case SinvieConfBroadcastConstants.ACTION_CALL_DEVICES_STATUS_CHANGE:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int route = (int) obj;
                        SinvieLogUtil.e("setAudioRoute", "setAudioRoute:" + route);
                        if (route != 2 && route != 0) {
                            return;
                        }

                        if (mIsVideoCall) {
                            SinvieCallMgr.getInstance().setAudioRoute(TsdkMobileAuidoRoute.TSDK_E_MOBILE_AUDIO_ROUTE_LOUDSPEAKER);
                        } else {
                            SinvieCallMgr.getInstance().setAudioRoute(TsdkMobileAuidoRoute.TSDK_E_MOBILE_AUDIO_ROUTE_DEFAULT);
                        }
                        mSpeakerButton.setActivated(mIsVideoCall);
                    }
                });
                break;
            default:
                break;
        }
    }

    private void showButtons() {
        mMuteArea.setVisibility(View.VISIBLE);
        mPlateButton.setVisibility(View.VISIBLE);
        mSpeakerButton.setVisibility(View.VISIBLE);
        mUpgradeVideoArea.setVisibility(View.VISIBLE);
        mBlindTransferButton.setVisibility(View.VISIBLE);
        mHoldCallButton.setVisibility(View.VISIBLE);
        mTransferMeeting.setVisibility(View.VISIBLE);
        mNetQuality.setVisibility(View.VISIBLE);
    }

    /**
     * 设置转视频状态
     */
    public void setUpdateVideoCallStatus(boolean isSuccess) {
        if (!isSuccess) {
            mUpgradeVideoArea.setClickable(true);
        }
    }
}
