package com.sinvie.emergency_base.ui.login;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_base.utils.SinvieConstants;

/**
 * @author: JonsonBob
 * @date: 2021/6/25
 * @Des: 用户信息缓存类
 */
public class SinvieUserInfo {
    private final static String PER_USER_MODEL = "per_user_model";

    private static SinvieUserInfo sSinvieUserInfo;

    private String account;
    private String pwd;

    private String zone;
    private String phone;
    private String token;
    private String userId;
    private String userSig;
    private String name;
    private String avatar;
    //平台id
    private String platformId;
    //自动登录
    private boolean autoLogin = false;
    //是否开启地址定位
    private boolean isOpenLocation = false;
    //是否登录华为会议
    private boolean isLoginHuaWeiConf = false;
    //是否登录腾讯IM
    private boolean isLoginTencentIm = false;

    /**
     * ***************************************华为登录信息 start**************************************
     */
    //华为的登录账号
    private String hwLoginPassword;
    private String hwLoginUsername;

    //华为会议登录信息
    private String tupRegisterServer;
    private String tupPort = "";
    private String tupSipUri;
    private int tupSrtp;
    private int sipTransport;
    private int applyConfigPriority;
    private int securityTunnel;
    private String udpPort;
    private String tlsPort;
    private int portConfigPriority;
    private int confCtrlProtocol;
    private boolean tupVpn = false;

    /**
     * **************************************华为登录信息 end*****************************************
     */

    public synchronized static SinvieUserInfo getInstance() {
        if (sSinvieUserInfo == null) {
            SharedPreferences shareInfo =
                    SinvieLocContext.getContext().getSharedPreferences(SinvieConstants.USERINFO, 0);
            String json = shareInfo.getString(PER_USER_MODEL, "");
            sSinvieUserInfo = new Gson().fromJson(json, SinvieUserInfo.class);
            if (sSinvieUserInfo == null) {
                sSinvieUserInfo = new SinvieUserInfo();
            }
        }
        return sSinvieUserInfo;
    }

    private SinvieUserInfo() {

    }

    public void setUserInfo(SinvieUserInfo info) {
        SharedPreferences shareInfo =
                SinvieLocContext.getContext().getSharedPreferences(SinvieConstants.USERINFO, 0);
        SharedPreferences.Editor editor = shareInfo.edit();
        editor.putString(PER_USER_MODEL, new Gson().toJson(info));
        editor.apply();
    }

    public void initHwLoginData() {
        //初始化服务器地址/端口 为空
        setTupRegisterServer("");
        setTupPort("");
        //存储华为的服务登录信息 (其实是初始化)
        setTupSipUri("");
        setTupSrtp(0);
        setSipTransport(0);
        setApplyConfigPriority(1);
        setSecurityTunnel(0);
        setUdpPort("");
        setTlsPort("");
        setTupVpn(true);
        setPortConfigPriority(1);
        setConfCtrlProtocol(0);
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
        setUserInfo(this);
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
        setUserInfo(this);
    }

    public String getUserSig() {
        return this.userSig;
    }

    public void setUserSig(String userSig) {
        this.userSig = userSig;
        setUserInfo(this);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
        setUserInfo(this);
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
        setUserInfo(this);
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
        setUserInfo(this);
    }

    public String getZone() {
        return this.zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
        setUserInfo(this);
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String userPhone) {
        this.phone = userPhone;
        setUserInfo(this);
    }

    public Boolean isAutoLogin() {
        return this.autoLogin;
    }

    public void setAutoLogin(boolean autoLogin) {
        this.autoLogin = autoLogin;
        setUserInfo(this);
    }

    public String getAvatar() {
        return this.avatar;
    }

    public void setAvatar(String url) {
        this.avatar = url;
        setUserInfo(this);
    }

    public String getPlatformId() {
        return platformId;
    }

    public void setPlatformId(String platformId) {
        this.platformId = platformId;
        setUserInfo(this);
    }

    public boolean isOpenLocation() {
        return isOpenLocation;
    }

    public void setOpenLocation(boolean openLocation) {
        isOpenLocation = openLocation;
        setUserInfo(this);
    }

    public String getHwLoginPassword() {
        return hwLoginPassword;
    }

    public void setHwLoginPassword(String hwLoginPassword) {
        this.hwLoginPassword = hwLoginPassword;
        setUserInfo(this);
    }

    public String getHwLoginUsername() {
        return hwLoginUsername;
    }

    public void setHwLoginUsername(String hwLoginUsername) {
        this.hwLoginUsername = hwLoginUsername;
        setUserInfo(this);
    }


    public String getTupRegisterServer() {
        return tupRegisterServer;
    }

    public void setTupRegisterServer(String tupRegisterServer) {
        this.tupRegisterServer = tupRegisterServer;
        setUserInfo(this);
    }

    public String getTupPort() {
        return tupPort;
    }

    public void setTupPort(String tupPort) {
        this.tupPort = tupPort;
        setUserInfo(this);
    }

    public String getTupSipUri() {
        return tupSipUri;
    }

    public void setTupSipUri(String tupSipUri) {
        this.tupSipUri = tupSipUri;
        setUserInfo(this);
    }

    public int getTupSrtp() {
        return tupSrtp;
    }

    public void setTupSrtp(int tupSrtp) {
        this.tupSrtp = tupSrtp;
        setUserInfo(this);
    }

    public int getSipTransport() {
        return sipTransport;
    }

    public void setSipTransport(int sipTransport) {
        this.sipTransport = sipTransport;
        setUserInfo(this);
    }

    public int getApplyConfigPriority() {
        return applyConfigPriority;
    }

    public void setApplyConfigPriority(int applyConfigPriority) {
        this.applyConfigPriority = applyConfigPriority;
        setUserInfo(this);
    }

    public int getSecurityTunnel() {
        return securityTunnel;
    }

    public void setSecurityTunnel(int securityTunnel) {
        this.securityTunnel = securityTunnel;
        setUserInfo(this);
    }

    public String getUdpPort() {
        return udpPort;
    }

    public void setUdpPort(String udpPort) {
        this.udpPort = udpPort;
        setUserInfo(this);
    }

    public String getTlsPort() {
        return tlsPort;
    }

    public void setTlsPort(String tlsPort) {
        this.tlsPort = tlsPort;
        setUserInfo(this);
    }

    public int getPortConfigPriority() {
        return portConfigPriority;
    }

    public void setPortConfigPriority(int portConfigPriority) {
        this.portConfigPriority = portConfigPriority;
        setUserInfo(this);
    }

    public int getConfCtrlProtocol() {
        return confCtrlProtocol;
    }

    public void setConfCtrlProtocol(int confCtrlProtocol) {
        this.confCtrlProtocol = confCtrlProtocol;
        setUserInfo(this);
    }

    public boolean isTupVpn() {
        return tupVpn;
    }

    public void setTupVpn(boolean tupVpn) {
        this.tupVpn = tupVpn;
        setUserInfo(this);
    }

    public boolean isLoginHuaWeiConf() {
        return isLoginHuaWeiConf;
    }

    public void setLoginHuaWeiConf(boolean loginHuaWeiConf) {
        isLoginHuaWeiConf = loginHuaWeiConf;
        setUserInfo(this);
    }

    public boolean isLoginTencentIm() {
        return isLoginTencentIm;
    }

    public void setLoginTencentIm(boolean loginTencentIm) {
        isLoginTencentIm = loginTencentIm;
        setUserInfo(this);
    }
}
