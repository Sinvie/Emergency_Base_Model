package com.sinvie.emergency_base.ui.login;


import android.content.Intent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

import com.hjq.permissions.OnPermissionCallback;
import com.hjq.permissions.Permission;
import com.hjq.permissions.XXPermissions;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.logic.login.SinvieILoginContact;
import com.sinvie.emergency_base.logic.login.SinvieLoginBasePresenter;
import com.sinvie.emergency_base.logic.login.SinvieLoginFragToActListener;
import com.sinvie.emergency_base.logic.login.SinvieLoginPresenter;
import com.sinvie.emergency_base.signature.SinvieGenerateUserSig;
import com.sinvie.emergency_base.ui.SinvieIntentConstant;
import com.sinvie.emergency_base.ui.base.SinvieBaseActivity;
import com.sinvie.emergency_base.utils.SinvieActivityUtil;
import com.sinvie.emergency_base.utils.SinvieClickUtils;
import com.sinvie.emergency_base.utils.SinvieDemoLog;
import com.sinvie.emergency_base.widget.dialog.SinvieTipDialog;
import com.sinvie.emergency_live.tuikit.utils.SinvieToastUtil;
import com.xiasuhuei321.loadingdialog.view.LoadingDialog;

import java.util.List;


public class SinvieLoginForDevActivity extends SinvieBaseActivity implements SinvieILoginContact.LoginBaseView,
        SinvieLoginFragToActListener, View.OnClickListener {
    private static final String TAG = SinvieLoginForDevActivity.class.getSimpleName();
    private static final int TSDK_E_LOGIN_ERR_USER_REPEAT_LOGIN = 33554483;
    //如果设置了target > 28，需要增加这个权限，否则不会弹出"始终允许"这个选择框
    private final String BACK_LOCATION_PERMISSION = "android.permission.ACCESS_BACKGROUND_LOCATION";
    private SinvieLoginBasePresenter mPresenter;
    /**
     * 登录方式 0:手机验证码登录 1:账号密码登录
     */
    private int mLoginType = 1;
    private FrameLayout mFlLoginForDev;
    private TextView mTvLoginBtn;
    private TextView mTvSelectLoginType;
    private ImageView mIvLoginToSetting;
    private SinvieLoginWayFragment mLoginFragment;
    private SinvieTipDialog mSinvieTipDialog;
    private boolean mPermissionPass = false;
    //sdk 30 以上的读写权限
//    private String[] versionPermission = {Permission.MANAGE_EXTERNAL_STORAGE};
    //电话
    private String[] phonePermission = {Permission.CALL_PHONE};
    //相机
    private String[] cameraPermission = {Permission.CAMERA};
    //麦克风
    private String[] MicPermission = {Permission.RECORD_AUDIO};
    //通讯录
    private String[] contactsPermission = {Permission.READ_CONTACTS};
    //通话记录
    private String[] callPermission = {Permission.READ_CALL_LOG, Permission.WRITE_CALL_LOG};
    //日历
    private String[] calendarPermission = {Permission.READ_CALENDAR, Permission.WRITE_CALENDAR,};
    //安装包权限
    private String[] packagePermission = {Permission.REQUEST_INSTALL_PACKAGES};
    private LoadingDialog mLoadingDialog;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (
//                XXPermissions.isGranted(this, versionPermission) &&
                        XXPermissions.isGranted(this, phonePermission) &&
                        XXPermissions.isGranted(this, cameraPermission) &&
                        XXPermissions.isGranted(this, MicPermission) &&
                        XXPermissions.isGranted(this, contactsPermission) &&
                        XXPermissions.isGranted(this, callPermission) &&
                        XXPermissions.isGranted(this, calendarPermission) &&
                        XXPermissions.isGranted(this, packagePermission)) {
            mPermissionPass = true;
        } else {
            showTipsDialog();
        }
    }

    @Override
    public void initializeView() {
        mPresenter = new SinvieLoginPresenter(this);
        mPresenter.attachView(this);

    }

    @Override
    public void initializeData() {

    }

    @Override
    public void initializeComposition() {
        setContentView(R.layout.activity_sinvie_login_for_dev);

        mFlLoginForDev = findViewById(R.id.fl_login_for_dev);
        mIvLoginToSetting = findViewById(R.id.iv_login_to_setting);

        mTvSelectLoginType = findViewById(R.id.tv_select_login_type);
        mTvLoginBtn = findViewById(R.id.tv_login_btn);

        //初始登录fragment
        initLoginFragment();

        mTvSelectLoginType.setOnClickListener(this);

        mTvLoginBtn.setOnClickListener(this);

        mIvLoginToSetting.setOnClickListener(this);

//        requestOtherPermission();
        requestLocationPermission();

        mPresenter.registerBroadcast();
    }


    @Override
    public void onClick(View v) {
        if (SinvieClickUtils.isFastClick(v.getId())) {
            return;
        }
        //如果此时权限没有申请
        if (!mPermissionPass) {
            //显示权限未申请
            showTipsDialog();
            return;
        }

        if (v.getId() == R.id.tv_login_btn) {

            showLoginDialog();

//            if (mCheckBox.isChecked()) {
//                UserInfo.getInstance().setHwLoginUsername(mLoginFragment.getAccount());
//                UserInfo.getInstance().setHwLoginPassword(mLoginFragment.getLoginCode());
//                if (UserInfo.getInstance().getTupRegisterServer() == null && UserInfo.getInstance().getTupPort() == null) {
//                    UserInfo.getInstance().setTupRegisterServer("172.16.111.56");
//                    UserInfo.getInstance().setTupPort("5061");
//
//                    //初始化与去初始化业务组件类 设置网络配置
//                    ServiceMgr.getServiceMgr().networkParam(
//                            UserInfo.getInstance().getTupPort(),
//                            UserInfo.getInstance().getTupRegisterServer(),
//                            UserInfo.getInstance().getSipTransport());
//                }
//
//
//                mPresenter.doHWVideoCallLogin();
//                return;
//            }
            String username = null;
            String pws = null;
            try {
                username = mLoginFragment.getAccount();
                pws = mLoginFragment.getLoginCode();
            } catch (Exception e) {
                e.printStackTrace();
            }
            mPresenter.doLogin(username, pws, mLoginType);

        } else if (v.getId() == R.id.tv_select_login_type) {

            initLoginFragment();
        } else if (v.getId() == R.id.iv_login_to_setting) {
            SinvieActivityUtil.startActivity(this,
                    new Intent(SinvieIntentConstant.LOGIN_SETTING_ACTIVITY_ACTION));
        }

    }

    @Override
    public void getMobileCodeSuccess() {
        SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_code_is_send));
//        mPresenter.getUserSig();
    }

    @Override
    public void getMobileCodeFail(int errCode, String errMsg) {
        SinvieToastUtil.toastLongErrorMessage(errCode, errMsg);
    }

    @Override
    public void onLoginSuccess() {
        //获取 userSig 开启IM 登录
        mPresenter.getUserSig();

        // 开始上传实时定位
//        XXPermissions.with(this).permission(
//                Manifest.permission.ACCESS_COARSE_LOCATION,
//                Manifest.permission.ACCESS_FINE_LOCATION,
//                BACK_LOCATION_PERMISSION
//        ).request((permissions, all) -> {
//            if (all) {
        SinvieLocationHeartBeat.Companion.getInstance().start();
//            }
//        });
    }

    @Override
    public void onLoginFail(int errCode, String errDes) {
        // 弹窗提示
        SinvieToastUtil.toastLongErrorMessage(errCode, errDes);
    }

    @Override
    public void getUserSigSuccess() {
        //TODO 此userSig只是测试使用
        SinvieUserInfo.getInstance().setUserSig(SinvieGenerateUserSig.genTestUserSig(SinvieUserInfo.getInstance().getUserId()));
        //userSig获取成功 开启im登录
        mPresenter.doIMVideoCallLogin();
    }

    @Override
    public void getUserSigFail(int errCode, String errDes) {
        // 弹窗提示
        SinvieToastUtil.toastLongErrorMessage(errCode, errDes);
    }

    @Override
    public void onIMLoginSuccess() {
        //IM登录成功 开启华为登录
        boolean isLoginHuawei = SinvieUserInfo.getInstance().isLoginHuaWeiConf();
        if (false) {
            mPresenter.doHWVideoCallLogin();
        } else {
            SinvieActivityUtil.startActivity(this, SinvieIntentConstant.MAIN_ACTIVITY_ACTION);

            dismissLoginDialog(true);
            finish();
        }

        SinvieToastUtil.toastLongMessage("IM 登录成功");
    }

    @Override
    public void onIMLoginFail(int errCode, String errDes) {
        SinvieToastUtil.toastLongErrorMessage(errCode, errDes);
    }


    @Override
    public void onHWLoginSuccess(String msg) {
        runOnUiThread(() -> {
            SinvieToastUtil.toastLongMessage(msg);
            SinvieActivityUtil.startActivity(SinvieLoginForDevActivity.this, SinvieIntentConstant.MAIN_ACTIVITY_ACTION);
            dismissLoginDialog(true);
            finish();
        });
    }

    @Override
    public void onHWLoginFail(int errCode, String errDes) {
        runOnUiThread(() -> {
            if (errCode == TSDK_E_LOGIN_ERR_USER_REPEAT_LOGIN) {
                //重复登录 证明已经登录 直接跳转到会议列表界面
                SinvieActivityUtil.startActivity(SinvieLoginForDevActivity.this, SinvieIntentConstant.MAIN_ACTIVITY_ACTION);
                finish();
            } else {
                SinvieToastUtil.toastLongErrorMessage(errCode, errDes);
                SinvieDemoLog.d(TAG, errCode + ":" + errDes);
            }
        });
    }

    @Override
    public void dismissLoginDialog(boolean isSuccess) {
        if (mLoadingDialog != null) {
            mLoadingDialog.close();
        }
    }

    @Override
    public void onSentCode() {
        mPresenter.getMobileCode(mLoginFragment.getAccount());
    }

    @Override
    public void onLockLoginBtn(boolean isLockLogin) {
        mTvLoginBtn.setEnabled(isLockLogin);
    }

    /**
     * 初始化登录fragment
     */
    private void initLoginFragment() {
        setSelectLoginTypeBtn();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        mLoginFragment = SinvieLoginWayFragment.newInstance(mLoginType);
        mLoginFragment.setListener(this);
        transaction.replace(R.id.fl_login_for_dev, mLoginFragment);
        transaction.commit();
    }

    /**
     * 初始化 tips dialog
     */
    private void showTipsDialog() {
        //初始化提示dialog
        mSinvieTipDialog = SinvieTipDialog.newInstance(getString(R.string.sinvie_tips_dialog_title_warning),
                getString(R.string.sinvie_tips_dialog_permission_tips),
                getString(R.string.sinvie_tips_dialog_next),
                getString(R.string.sinvie_tips_dialog_exit));
        mSinvieTipDialog.setOnLeftClickListener(dialog -> {
            //检测权限
//            requestOtherPermission();
            requestLocationPermission();
        });
        mSinvieTipDialog.setOnRightClickListener(dialog -> {
            //关闭dialog
            dialog.dismiss();
            //退出程序
            finish();
        });
        mSinvieTipDialog.show(getSupportFragmentManager(), "permission_dialog");
    }


    public void showLoginDialog() {
        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog(this);
        }
        mLoadingDialog.setLoadingText(getString(R.string.sinvie_logining_msg))
                .setSuccessText(getString(R.string.sinvie_login_tip_success))//显示加载成功时的文字
                .setFailedText(getString(R.string.sinvie_login_tip_failed))
                .setInterceptBack(true)
                .setLoadSpeed(LoadingDialog.Speed.SPEED_TWO)
                .setRepeatCount(-1)
                .show();

    }

    public void setSelectLoginTypeBtn() {
        if (mLoginType == 0) {
            mLoginType = 1;
            mTvSelectLoginType.setText(getString(R.string.sinvie_login_type_of_code));
        } else {
            mLoginType = 0;
            mTvSelectLoginType.setText(getString(R.string.sinvie_login_type_of_pwd));
        }
    }

    /**
     * 权限检测
     */
    private void requestOtherPermission() {

        XXPermissions.with(this)
//                .permission(versionPermission)
                .permission(phonePermission)
                .permission(cameraPermission)
                .permission(MicPermission)
                .permission(contactsPermission)
                .permission(callPermission)
                .permission(calendarPermission)
                .permission(packagePermission)
                .request(new OnPermissionCallback() {
                    @Override
                    public void onGranted(List<String> permissions, boolean all) {
                        if (all) {
                            mPermissionPass = true;
                        } else {
                            SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_tips));
                        }
                    }

                    @Override
                    public void onDenied(List<String> permissions, boolean never) {
                        if (never) {
                            SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_never));
                        } else {
                            SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_fail));
                        }
                        XXPermissions.startPermissionActivity(SinvieLoginForDevActivity.this,
                                permissions);
                    }
                });
    }

    private void requestLocationPermission() {
        // 开始上传实时定位
        XXPermissions.with(this)
                .permission(
                        Permission.ACCESS_COARSE_LOCATION,
                        Permission.ACCESS_FINE_LOCATION,
                        Permission.ACCESS_BACKGROUND_LOCATION
                ).request(new OnPermissionCallback() {
            @Override
            public void onGranted(List<String> permissions, boolean all) {
                if (all) {
                    requestOtherPermission();
                } else {
                    SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_tips));
                }
            }

            @Override
            public void onDenied(List<String> permissions, boolean never) {
                if (never) {
                    SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_never));
                } else {
                    SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_fail));
                }
                XXPermissions.startPermissionActivity(SinvieLoginForDevActivity.this, permissions);
            }
        });
    }

    @Override
    public void setActivityDestroy() {

        if (mLoadingDialog != null) {
            mLoadingDialog.close();
            mLoadingDialog = null;
        }

        mPresenter.unRegisterBroadcast();
        mPresenter.destroyPresenter();
        mPresenter.detachView();
    }


}