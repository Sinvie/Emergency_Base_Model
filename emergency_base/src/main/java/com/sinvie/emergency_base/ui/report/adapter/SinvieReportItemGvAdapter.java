package com.sinvie.emergency_base.ui.report.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieGridImgBean;
import com.sinvie.emergency_base.utils.SinvieDemoLog;
import com.sinvie.emergency_base.utils.SinvieFileUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/11
 * @Des: 类描述
 */
public class SinvieReportItemGvAdapter extends BaseAdapter {

    private static final String TAG = SinvieReportItemGvAdapter.class.getSimpleName();

    private Context mContext;
    private List<SinvieGridImgBean> mReportBeanList;

    private onReportGvItemClickListener mItemClickListener;


    public SinvieReportItemGvAdapter(Context context,
                                     List<SinvieGridImgBean> reportBeanList) {
        mContext = context;
        mReportBeanList = reportBeanList;
    }

    public void setItemClickListener(onReportGvItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    @Override
    public int getCount() {
        if (mReportBeanList == null) {
            mReportBeanList = new ArrayList<>();
        }
        return Math.min(mReportBeanList.size(), 4);
    }

    @Override
    public SinvieGridImgBean getItem(int position) {
        return mReportBeanList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(mContext).inflate(R.layout.item_sinvie_report_grid, parent,
                false);
        GridViewHolder viewHolder = new GridViewHolder();
        viewHolder.mIvGv = convertView.findViewById(R.id.iv_item_report_gv);
        initView(viewHolder, mReportBeanList.get(position));
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemClickListener.onReportGvItemClickListener(mReportBeanList.get(position).getGridType(), mReportBeanList.get(position).getUrl());
            }
        });
        return convertView;
    }

    private void initView(GridViewHolder viewHolder, SinvieGridImgBean bean) {
        if (bean.getUrl() == null) {
            return;
        }
        SinvieDemoLog.d(TAG, bean.getUrl());
        int suffixFormat = SinvieFileUtil.getSuffixFormat(bean.getUrl());
        if (suffixFormat == SinvieFileUtil.SUFFIX_FORMAT_VIDEO) {
            //如果是视频 取消加载 显示视频播放
            bean.setGridType(0);
            Glide.with(mContext).load(R.mipmap.icon_sinvie_play_video_1)
                    .placeholder(R.mipmap.icon_sinvie_img_place)
                    .error(R.mipmap.icon_sinvie_img_error)
                    .into(viewHolder.mIvGv);
        } else if (suffixFormat == SinvieFileUtil.SUFFIX_FORMAT_IMG) {
            //加载图片
            bean.setGridType(1);
            //加载图片url
            Glide.with(mContext).load(bean.getUrl())
                    .placeholder(R.mipmap.icon_sinvie_img_place)
                    .error(R.mipmap.icon_sinvie_img_error)
                    .into(viewHolder.mIvGv);
        } else {
            //格式不对 无法播放
            Glide.with(mContext).load(R.mipmap.icon_sinvie_img_error)
                    .placeholder(R.mipmap.icon_sinvie_img_place)
                    .error(R.mipmap.icon_sinvie_img_error)
                    .into(viewHolder.mIvGv);
        }

    }

    private static class GridViewHolder {
        public ImageView mIvGv;

    }

    public interface onReportGvItemClickListener {
        /**
         * GridView adapter item Click Listener
         *
         * @param gridType 当前是视频还是img
         * @param url      the img url or video url
         */
        void onReportGvItemClickListener(int gridType, String url);
    }
}
