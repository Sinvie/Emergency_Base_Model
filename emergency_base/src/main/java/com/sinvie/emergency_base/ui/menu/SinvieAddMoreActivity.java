package com.sinvie.emergency_base.ui.menu;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.logic.add_more.SinvieAddMoreContact;
import com.sinvie.emergency_base.logic.add_more.SinvieAddMorePresenter;
import com.sinvie.emergency_base.ui.base.SinvieMVPBaseActivity;

public class SinvieAddMoreActivity extends SinvieMVPBaseActivity<SinvieAddMoreContact.AddMoreBaseView, SinvieAddMorePresenter> implements SinvieAddMoreContact.AddMoreBaseView {

    @Override
    public void initializeComposition() {
        setContentView(R.layout.activity_sinvie_add_more);
    }

    @Override
    public void initializeData() {

    }

    @Override
    protected SinvieAddMoreContact.AddMoreBaseView createView() {
        return this;
    }

    @Override
    protected SinvieAddMorePresenter createPresenter() {
        return new SinvieAddMorePresenter(this);
    }
}