package com.sinvie.emergency_base.ui.report_manager.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieReportManagerBean;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/8/23
 * @Des: 类描述
 */
public class SinvieReportManagerRvAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_TYPE_CONTENT = 0;
    private static final int ITEM_TYPE_TIPS = 1;

    private Context mContext;
    private List<SinvieReportManagerBean> mBeanList;

    public SinvieReportManagerRvAdapter(Context context, List<SinvieReportManagerBean> beanList) {
        mContext = context;
        mBeanList = beanList;
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == ITEM_TYPE_CONTENT) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_sinvie_report_manager_detail, parent, false);
            return new ReportManagerViewHolder(view);
        } else {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_sinvie_report_manager_detail_type, parent, false);
            return new ReportManagerTypeViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        if (mBeanList.get(position).getItemType() == ITEM_TYPE_CONTENT) {
            ((ReportManagerViewHolder) holder).bind(mBeanList.get(position));
        } else if (mBeanList.get(position).getItemType() == ITEM_TYPE_TIPS) {
            ((ReportManagerTypeViewHolder) holder).bind(mBeanList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        if (mBeanList == null) {
            mBeanList = new ArrayList<>();
        }
        return mBeanList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mBeanList.get(position).getItemType();
    }

    public static class ReportManagerViewHolder extends RecyclerView.ViewHolder {

        private final TextView mTvName;
        private final TextView mTvContent;

        public ReportManagerViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);

            mTvName = itemView.findViewById(R.id.tv_item_report_manager_detail_name);
            mTvContent = itemView.findViewById(R.id.tv_item_report_manager_detail_content);
        }

        private void bind(SinvieReportManagerBean bean) {
            mTvName.setText(bean.getReportName());
            mTvContent.setText(bean.getReportContent());
        }
    }

    public static class ReportManagerTypeViewHolder extends RecyclerView.ViewHolder {

        private final TextView mTvType;

        public ReportManagerTypeViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            mTvType = itemView.findViewById(R.id.tv_report_manager_detail_type);
        }

        private void bind(SinvieReportManagerBean bean) {
            mTvType.setText(bean.getTypeName());
        }
    }


}
