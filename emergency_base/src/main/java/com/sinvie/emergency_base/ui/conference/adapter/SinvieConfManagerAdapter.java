package com.sinvie.emergency_base.ui.conference.adapter;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.huawei.ecterminalsdk.base.TsdkConfRole;
import com.sinvie.emergency_conf.ConferenceService.SinvieMeetingMgr;
import com.sinvie.emergency_conf.ConferenceService.SinvieMember;
import com.sinvie.emergency_base.R;

import java.util.List;

/**
 * This adapter is about conf manager
 * 会议管理适配层
 */
public class SinvieConfManagerAdapter extends BaseAdapter
{

    private Context context;
    private List<SinvieMember> mSinvieConfMemberEntity;

    public SinvieConfManagerAdapter(Context context)
    {
        this.context = context;
    }

    public void setData(List<SinvieMember> sinvieConfMemberEntity)
    {
        this.mSinvieConfMemberEntity = sinvieConfMemberEntity;
    }

    @Override
    public int getCount()
    {
        return mSinvieConfMemberEntity == null ? 0 : mSinvieConfMemberEntity.size();
    }

    @Override
    public Object getItem(int position)
    {
        return mSinvieConfMemberEntity.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder viewHolder;
        if (convertView == null)
        {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_sinvie_conf_manager, null);
            viewHolder = new ViewHolder();
            viewHolder.chairmanPicIV = (ImageView) convertView.findViewById(R.id.chairman_pic);
            viewHolder.memberNameTV = (TextView) convertView.findViewById(R.id.member_name_tv);
            viewHolder.memberNumberTV = (TextView) convertView.findViewById(R.id.member_number_tv);
            viewHolder.callingStateIV = (ImageView) convertView.findViewById(R.id.calling_state_iv);
            viewHolder.handUpIV = (ImageView) convertView.findViewById(R.id.hand_up_iv);
            viewHolder.presenterIV = (ImageView) convertView.findViewById(R.id.presenter_pic);
            viewHolder.dataConfIV = (ImageView) convertView.findViewById(R.id.data_conf_iv);
            viewHolder.reqTalk = (ImageView) convertView.findViewById(R.id.req_talk_tv);
            viewHolder.broadcastIV = (ImageView) convertView.findViewById(R.id.broadcast_iv);
            convertView.setTag(viewHolder);
        }
        viewHolder = (ViewHolder) convertView.getTag();

        SinvieMember sinvieConfMemberEntity = mSinvieConfMemberEntity.get(position);
        viewHolder.memberNameTV.setText(sinvieConfMemberEntity.getDisplayName());
        viewHolder.memberNumberTV.setText(sinvieConfMemberEntity.getNumber());
        boolean isChairMan = (sinvieConfMemberEntity.getRole() == TsdkConfRole.TSDK_E_CONF_ROLE_CHAIRMAN);
        viewHolder.chairmanPicIV.setVisibility(isChairMan ? View.VISIBLE : View.INVISIBLE);
        SinvieMember sinvieMember = SinvieMeetingMgr.getInstance().getCurrentConferenceSelf();
        if (sinvieMember != null)
        {
            boolean isChair = (sinvieMember.getRole() == TsdkConfRole.TSDK_E_CONF_ROLE_CHAIRMAN);
            viewHolder.handUpIV.setVisibility(sinvieConfMemberEntity.isHandUp() && !isChairMan ? View.VISIBLE : View.GONE);
            viewHolder.broadcastIV.setVisibility(sinvieConfMemberEntity.isBroadcastSelf() ? View.VISIBLE : View.GONE);
        }
        else
        {
            viewHolder.handUpIV.setVisibility(sinvieConfMemberEntity.isHandUp() ? View.VISIBLE : View.GONE);
        }
        updateMemberStatus(viewHolder, sinvieConfMemberEntity);

        return convertView;

    }

    /**
     * Update attendee Status
     * 更新与会者状态
     *
     * @param viewHolder        视图持有者
     * @param sinvieConfMemberEntity 与会者信息
     */
    private void updateMemberStatus(ViewHolder viewHolder, SinvieMember sinvieConfMemberEntity)
    {
        ImageView stateIV = viewHolder.callingStateIV;
        viewHolder.callingStateIV.setBackgroundResource(R.drawable.selector_sinvie_conf_calling_animation);
        AnimationDrawable animationDrawable;

        switch (sinvieConfMemberEntity.getStatus())
        {
            case CALLING:
                animationDrawable = (AnimationDrawable) stateIV.getBackground();
                stateIV.post(new MyRunnable(animationDrawable));
                break;
            case IN_CONF:
                if (sinvieConfMemberEntity.isMute())
                {
                    stateIV.setBackgroundResource(R.drawable.icon_sinvie_conf_calling_mute);
                    stateIV.setContentDescription("conf_calling_mute");
                }
                else
                {
                    animationDrawable = (AnimationDrawable) stateIV.getBackground();
                    stateIV.setBackgroundResource(R.drawable.icon_sinvie_conf_calling_success);
                    stateIV.setContentDescription("conf_calling_success");
                    animationDrawable.stop();
                }

                break;
            default:
                stateIV.setBackgroundResource(R.drawable.icon_sinvie_conf_calling_leave);
                stateIV.setContentDescription("conf_calling_leave");
                break;

        }

        if (sinvieConfMemberEntity.isPresent())
        {
            viewHolder.presenterIV.setVisibility(View.VISIBLE);
        }
        else
        {
            viewHolder.presenterIV.setVisibility(View.GONE);
        }

        if (sinvieConfMemberEntity.isInDataConference())
        {
            viewHolder.dataConfIV.setVisibility(View.VISIBLE);
        }
        else
        {
            viewHolder.dataConfIV.setVisibility(View.GONE);
        }

        if (sinvieConfMemberEntity.isReqTalk())
        {
            viewHolder.reqTalk.setVisibility(View.VISIBLE);
        }
        else
        {
            viewHolder.reqTalk.setVisibility(View.GONE);
        }
    }

    private static class MyRunnable implements Runnable
    {
        private AnimationDrawable animationDrawable;

        public MyRunnable(AnimationDrawable drawable)
        {
            this.animationDrawable = drawable;
        }

        @Override
        public void run()
        {
            animationDrawable.start();
        }
    }

    /**
	 * View-holding classes improve processing performance
     * to reduce the overhead of view memory consumption
     * 视图持有类，为减少视图占用内存的开销，提高处理性能
     */
    private static class ViewHolder
    {
        public ImageView chairmanPicIV;
        public TextView memberNameTV;
        public TextView memberNumberTV;
        public ImageView callingStateIV;
        public ImageView handUpIV;
        public ImageView presenterIV;
        public ImageView dataConfIV;
        public ImageView reqTalk;
        public ImageView broadcastIV;
    }
}
