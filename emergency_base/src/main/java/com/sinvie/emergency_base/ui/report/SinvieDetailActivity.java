package com.sinvie.emergency_base.ui.report;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;

import com.google.gson.Gson;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.sinvie.emergency_conf.CommonService.util.SinvieLogUtil;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieReportBean;
import com.sinvie.emergency_base.bean.SinvieReportDetailBean;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.logic.report.report_detail.SinvieReportDetailContact;
import com.sinvie.emergency_base.logic.report.report_detail.SinvieReportDetailPresenter;
import com.sinvie.emergency_live.tuikit.base.ISinvieTitleBarLayout;
import com.sinvie.emergency_live.tuikit.utils.SinvieToastUtil;
import com.sinvie.emergency_base.ui.SinvieIntentConstant;
import com.sinvie.emergency_base.ui.base.SinvieMVPBaseActivity;
import com.sinvie.emergency_base.ui.report.adapter.SinvieReportDetailRvAdapter;
import com.sinvie.emergency_base.utils.SinvieActivityUtil;
import com.sinvie.emergency_base.widget.SinvieCustomTitleBarLayout;
import com.sinvie.emergency_base.widget.dialog.SinvieTipDialog;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cc.shinichi.library.ImagePreview;

/**
 * @param
 * @author JonsonBob
 * @description 事报详情界面 事报信息 + 续报列表信息
 * @return
 * @time 2021/7/11 22:48
 */
public class SinvieDetailActivity extends SinvieMVPBaseActivity<SinvieReportDetailContact.ReportDetailBaseView, SinvieReportDetailPresenter> implements SinvieReportDetailContact.ReportDetailBaseView {

    private static final String TAG = SinvieDetailActivity.class.getSimpleName();

    private SinvieCustomTitleBarLayout mCtrReportDetail;
    private SmartRefreshLayout mSmartReportDetail;
    private RecyclerView mRvReportDetail;

    private List<SinvieReportDetailBean.ReportSupplementsRoomListBean> mDetailBeanList;
    private SinvieReportDetailRvAdapter mAdapter;

    //    private int mPageIndex = 1;
    private SinvieReportBean.DataRoomListBean mInfoBean;

    private SinvieTipDialog mSinvieTipDialog;

    /**
     * 是否修改 事报信息
     */
    private static boolean mIsUpdateReport = false;

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        SinvieLogUtil.d(TAG, "onActivityResult: " + requestCode + " : " + resultCode);
        switch (requestCode) {
            case SinvieUIConstants.ADD_REPORT_DETAIL_REQUEST:
                if (resultCode == SinvieUIConstants.ADD_REPORT_DETAIL_RESULT) {
                    //完成了 续报的修改/新增 刷新列表
                    if (mPresenter != null && mInfoBean != null && mSmartReportDetail != null) {
//                        mPresenter.getReportDetailInfo(mInfoBean.getId());
                        mSmartReportDetail.setEnableRefresh(true);
                        //修改的是续报
                        mIsUpdateReport = false;
                        mSmartReportDetail.autoRefresh();
                    }
                }
            case SinvieUIConstants.ADD_NEW_REPORT_REQUEST:
                if (resultCode == SinvieUIConstants.ADD_NEW_REPORT_RESULT) {
                    //完成了 事报的修改
                    if (mPresenter != null && mInfoBean != null && mSmartReportDetail != null) {
//                    mPresenter.getReportDetailInfo(mInfoBean.getId());
                        mSmartReportDetail.setEnableRefresh(true);
                        //修改的是事报
                        mIsUpdateReport = true;
                        mSmartReportDetail.autoRefresh();
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void initializeData() {
        mInfoBean = getIntent().getParcelableExtra(SinvieUIConstants.REPORT_INFO_BEAN);
    }

    @Override
    public void initializeComposition() {
        setContentView(R.layout.activity_sinvie_report_detail);

        mCtrReportDetail = findViewById(R.id.ctr_report_detail);
        mSmartReportDetail = findViewById(R.id.smart_report_detail);
        mRvReportDetail = findViewById(R.id.rv_report_detail);

        mDetailBeanList = new ArrayList<>();
        mAdapter = new SinvieReportDetailRvAdapter(this, mDetailBeanList);
        mAdapter.setFirstInfoBean(mInfoBean);
        mAdapter.setOnReportDetailItemClickListener(mOnReportDetailItemClickListener);
        mRvReportDetail.setLayoutManager(new LinearLayoutManager(this));
        mRvReportDetail.setAdapter(mAdapter);


        mCtrReportDetail.setTitle(getString(R.string.sinvie_add_report_title),
                ISinvieTitleBarLayout.POSITION.MIDDLE);
        mCtrReportDetail.setLeftIcon(R.drawable.svg_sinvie_theme_back);
        mCtrReportDetail.setRightIcon(R.mipmap.icon_sinvie_delete_report);
        mCtrReportDetail.setRightSecondIcon(R.mipmap.icon_sinvie_add_report_detail);
        mCtrReportDetail.setOnLeftClickListener(v -> finish());
        mCtrReportDetail.getRightIcon().setOnClickListener(v -> {
            //删除事报
            showDelDetailDialog(getString(R.string.sinvie_tip_del_report), dialog -> {
                mPresenter.delReportDetailInfo(mInfoBean.getId());
                dialog.dismiss();
            });
        });
        mCtrReportDetail.getRightSecondIcon().setOnClickListener(v -> {
            //增加续报
            Intent intent = new Intent(SinvieIntentConstant.ADD_REPORT_DETAIL_ACTIVITY_ACTION);
            intent.putExtra(SinvieUIConstants.REPORT_INTENT_TYPE, SinvieUIConstants.REPORT_TYPE_ADD);
            SinvieReportDetailBean.ReportSupplementsRoomListBean bean =
                    new SinvieReportDetailBean.ReportSupplementsRoomListBean();
            bean.setAddress(mInfoBean.getAddress());
            bean.setEventInfoId(mInfoBean.getId());
            bean.setLatitude(mInfoBean.getLatitude());
            bean.setLongitude(mInfoBean.getLongitude());
            intent.putExtra(SinvieUIConstants.REPORT_DETAIL_INFO_BEAN, bean);
            intent.putExtra(SinvieUIConstants.REPORT_INFO_TITLE, mInfoBean.getTitle());
            SinvieActivityUtil.startActivityForResult(SinvieDetailActivity.this, intent,
                    SinvieUIConstants.ADD_REPORT_DETAIL_REQUEST);
        });

        //上拉下拉
        mSmartReportDetail.setOnRefreshListener(refreshLayout -> {

            mSmartReportDetail.setEnableRefresh(false);
            mPresenter.getReportDetailInfo(mInfoBean.getId());
        });

        //上拉加载设置为false 后期可能做分页
        mSmartReportDetail.setEnableLoadMore(false);
        mSmartReportDetail.autoRefresh();
    }

    private SinvieReportDetailRvAdapter.onReportDetailItemClickListener mOnReportDetailItemClickListener =
            new SinvieReportDetailRvAdapter.onReportDetailItemClickListener() {

                @Override
                public void onDetailFirstItemEditListener() {
                    //点击修改按钮跳转 修改界面
                    Intent intent = new Intent(SinvieIntentConstant.ADD_NEW_REPORT_ACTIVITY_ACTION);
                    intent.putExtra(SinvieUIConstants.REPORT_INFO_BEAN, mInfoBean);
                    intent.putExtra(SinvieUIConstants.REPORT_INTENT_TYPE, SinvieUIConstants.REPORT_TYPE_EDIT);
                    intent.putExtra(SinvieUIConstants.REPORT_INFO_TITLE, mInfoBean.getTitle());
                    SinvieActivityUtil.startActivityForResult(SinvieDetailActivity.this, intent,
                            SinvieUIConstants.ADD_NEW_REPORT_REQUEST);
                }

                @Override
                public void onDetailThirdItemDelListener(int id) {
                    showDelDetailDialog(getString(R.string.sinvie_tip_del_report_detail), dialog -> {
                        //删除续报
                        mPresenter.deleteReportFollowUp(id);
                        dialog.dismiss();
                    });
                }

                @Override
                public void onDetailThirdItemEditListener(int position,
                                                          SinvieReportDetailBean.ReportSupplementsRoomListBean thirdBean) {
                    //修改续报
                    Intent intent = new Intent(SinvieIntentConstant.ADD_REPORT_DETAIL_ACTIVITY_ACTION);
                    intent.putExtra(SinvieUIConstants.REPORT_INTENT_TYPE, SinvieUIConstants.REPORT_TYPE_EDIT);
                    intent.putExtra(SinvieUIConstants.REPORT_DETAIL_INFO_BEAN, thirdBean);
                    intent.putExtra(SinvieUIConstants.REPORT_INFO_TITLE, mInfoBean.getTitle());
                    SinvieActivityUtil.startActivityForResult(SinvieDetailActivity.this, intent,
                            SinvieUIConstants.ADD_REPORT_DETAIL_REQUEST);
                }

                @Override
                public void onDetailItemPicListener(int gridType, String url) {
                    //视屏播放 查看大图界面
                    if (gridType == 0) {
                        //视频
                        Intent intent = new Intent(SinvieIntentConstant.VIDEO_PLAY_ACTIVITY_ACTION);
                        intent.putExtra(SinvieUIConstants.PLAY_VIDEO_URL, url);
                        SinvieActivityUtil.startActivity(SinvieDetailActivity.this, intent);
                    } else {
                        //图片
                        ImagePreview
                                .getInstance()
                                .setContext(SinvieDetailActivity.this)
                                .setIndex(0)
                                .setImage(url)
                                .start();
                    }
                }
            };

    @Override
    protected SinvieReportDetailContact.ReportDetailBaseView createView() {
        return this;
    }

    @Override
    protected SinvieReportDetailPresenter createPresenter() {
        return new SinvieReportDetailPresenter(this);
    }

    @Override
    public void getReportDetailInfoSuccess(SinvieReportDetailBean bean) {
        mSmartReportDetail.finishRefresh();
        mSmartReportDetail.finishLoadMore();
        SinvieLogUtil.d(TAG, "getReportDetailInfoSuccess: " + new Gson().toJson(bean));

        if (mIsUpdateReport) {
            //使用gson 更新数据
            mInfoBean = new Gson().fromJson(new Gson().toJson(bean), SinvieReportBean.DataRoomListBean.class);
            Intent intent = new Intent();
            intent.putExtra(SinvieUIConstants.REPORT_INFO_BEAN,mInfoBean);
            setResult(SinvieUIConstants.REPORT_DETAIL_EDIT_RESULT,intent);
        }

        //更新UI
        int size = bean.getReportSupplements() == null ? 0 :
                bean.getReportSupplements().size();
        mDetailBeanList.clear();
        mDetailBeanList.addAll(bean.getReportSupplements());

        mAdapter.setFirstInfoBean(mInfoBean);
        //更新adapter
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void getReportDetailInfoFail(int errCode, String errMsg) {
        mSmartReportDetail.finishRefresh();
        mSmartReportDetail.finishLoadMore();

        SinvieToastUtil.toastLongMessage(errMsg);
    }

    @Override
    public void delReportDetailInfoSuccess() {
        Intent intent = new Intent();
        intent.putExtra(SinvieUIConstants.REPORT_INFO_BEAN, mInfoBean);
        setResult(SinvieUIConstants.REPORT_DETAIL_DEL_RESULT, intent);
        finish();
    }

    @Override
    public void delReportDetailInfoFail(int errCode, String errMsg) {
        SinvieToastUtil.toastLongMessage(errMsg);
    }

    @Override
    public void deleteReportFollowUpSuccess(int id) {
        Iterator<SinvieReportDetailBean.ReportSupplementsRoomListBean> iterator =
                mDetailBeanList.listIterator();
        while (iterator.hasNext()) {
            SinvieReportDetailBean.ReportSupplementsRoomListBean bean = iterator.next();
            //判断id 然后删除 此处为了方便直接删除第一个
            if (bean.getId() == id) {
                iterator.remove();
            }
        }
        //刷新数据库
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void deleteReportFollowUpFail(int errCode, String errMsg) {

        SinvieToastUtil.toastLongMessage(errMsg);
    }

    /**
     * 初始化 tips dialog
     */
    private void showDelDetailDialog(String title, SinvieTipDialog.TipDialogRightClickListener listener) {
        //初始化提示dialog
        mSinvieTipDialog = SinvieTipDialog.newInstance(
                getString(R.string.sinvie_tips_dialog_title_hint),
                title,
                getString(R.string.sinvie_tips_dialog_cancel),
                getString(R.string.sinvie_tips_dialog_del));
        mSinvieTipDialog.setOnRightClickListener(listener);
        mSinvieTipDialog.show(getSupportFragmentManager(), "permission_dialog");
    }
}