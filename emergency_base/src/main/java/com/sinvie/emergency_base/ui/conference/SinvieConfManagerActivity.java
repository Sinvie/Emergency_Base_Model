package com.sinvie.emergency_base.ui.conference;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hjq.permissions.OnPermissionCallback;
import com.hjq.permissions.Permission;
import com.hjq.permissions.XXPermissions;
import com.huawei.ecterminalsdk.base.TsdkCallInfo;
import com.huawei.ecterminalsdk.base.TsdkMobileAuidoRoute;
import com.huawei.ecterminalsdk.base.TsdkWatchSvcAttendees;
import com.huawei.ecterminalsdk.base.TsdkonEvtAuditDir;
import com.huawei.ecterminalsdk.models.TsdkCommonResult;
import com.huawei.ecterminalsdk.models.TsdkManager;
import com.huawei.ecterminalsdk.models.call.TsdkCall;
import com.huawei.ecterminalsdk.models.call.TsdkCallManager;
import com.huawei.videoengine.HarmonyCaptureScreen;
import com.sinvie.emergency_conf.CallService.SinvieCallConstant;
import com.sinvie.emergency_conf.CallService.SinvieCallMgr;
import com.sinvie.emergency_conf.CallService.SinvieSession;
import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieConfBroadcastConstants;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcast;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcastReceiver;
import com.sinvie.emergency_conf.CommonService.util.SinvieLogUtil;
import com.sinvie.emergency_conf.ConferenceService.SinvieConfBaseInfo;
import com.sinvie.emergency_conf.ConferenceService.SinvieConfConstant;
import com.sinvie.emergency_conf.ConferenceService.SinvieMeetingMgr;
import com.sinvie.emergency_conf.ConferenceService.SinvieMember;
import com.sinvie.emergency_conf.LoginService.SinvieLoginMgr;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.logic.conference.confManager.SinvieConfManagerBasePresenter;
import com.sinvie.emergency_base.logic.conference.confManager.SinvieConfManagerPresenter;
import com.sinvie.emergency_base.logic.conference.confManager.SinvieIConfManagerContract;
import com.sinvie.emergency_base.service.SinvieFloatingButtonService;
import com.sinvie.emergency_base.ui.SinvieIntentConstant;
import com.sinvie.emergency_base.ui.base.SinvieActivityStack;
import com.sinvie.emergency_base.ui.base.SinvieMVPBaseActivity;
import com.sinvie.emergency_base.ui.call.SinvieSecondDialPlateControl;
import com.sinvie.emergency_base.ui.conference.adapter.SinviePopupConfListAdapter;
import com.sinvie.emergency_base.utils.SinvieActivityUtil;
import com.sinvie.emergency_base.utils.SinvieCommonUtil;
import com.sinvie.emergency_base.utils.SinviePopupWindowUtil;
import com.sinvie.emergency_base.utils.SinvieSystemUtil;
import com.sinvie.emergency_base.widget.dialog.SinvieConfirmDialog;
import com.sinvie.emergency_base.widget.dialog.SinvieEditDialog;
import com.sinvie.emergency_base.widget.dialog.SimpleListDialog;
import com.sinvie.emergency_live.tuikit.utils.SinvieToastUtil;
import com.sinvie.emergency_base.widget.dialog.SinvieThreeInputDialog;
import com.sinvie.emergency_base.widget.dialog.SinvieTripleDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.huawei.ecterminalsdk.base.TsdkConfEnvType.TSDK_E_CONF_ENV_HOSTED_CONVERGENT_CONFERENCE;
import static com.huawei.ecterminalsdk.base.TsdkConfEnvType.TSDK_E_CONF_ENV_ON_PREMISES_CONFERENCING_ONLY;

/**
 * @param
 * @author JonsonBob
 * @description 会议管理类
 * @return
 * @time 2021/7/10 9:50
 */
public class SinvieConfManagerActivity extends SinvieMVPBaseActivity<SinvieIConfManagerContract.ConfManagerViewSinvie,
        SinvieConfManagerBasePresenter>
        implements SinvieIConfManagerContract.ConfManagerViewSinvie, View.OnClickListener {

    private RelativeLayout mVideoConfLayout;
    private RelativeLayout mTitleLayout;
    private LinearLayout mConfMediaLayout;
    private FrameLayout mConfRemoteVideoLayout;
    private FrameLayout mConfSmallLayout;
    private FrameLayout mHideVideoLayout;
    private ImageView mRecordPoint;
    private ImageView ivIsChecked;

    private FrameLayout mHideLocalVideoBtn;
    private FrameLayout mShowLocalVideoBtn;

    private LinearLayout mConfButton;
    private ImageView mLeaveIV;
    private TextView mTitleTV;
    private ImageView mRightIV;
    private ImageView mShareIV;
    private FrameLayout mConfHangup;
    private FrameLayout mConfMute;
    private FrameLayout mConfSpeaker;
    private FrameLayout mConfAddAttendee;
    private FrameLayout mConfAttendee;
    private PopupWindow mPopupWindow;
    private ListView mConfMemberListView;
    private SinviePopupConfListAdapter mAdapter;
    private FrameLayout mConfMore;
    private ImageView cameraStatusIV;
    private TextView cameraStatusTV;
    private RelativeLayout mAudioConfLayout;
    private TextView mAudioConfAttendeeTV;
    private TextView mCallNameTv;

    private FrameLayout mConfPlate;
    private LinearLayout mPlateArea;
    private ImageView mCloseArea;

    private String confID;
    private boolean isCameraClose = false;
    private boolean isVideo = false;
    private boolean isDateConf = false;
    private List<Object> items = new ArrayList<>();
    private int mOrientation = 1;

    private MyTimerTask myTimerTask;
    private Timer timer;
    private boolean isFirstStart = true;
    private boolean isPressTouch = false;
    private boolean isShowBar = false;

    private boolean isStartShare = false;
    private boolean isAuxDataShare = false;
    private boolean isUnidirectLiveBroadcastConf = SinvieMeetingMgr.getInstance().isFlag();
    private int directionType = 0;
    // 观众会场是否申请发言
    private boolean flag = false;

    private SinvieMember selfEntity;
    private FrameLayout mConfScreenShare;
    private ImageView mIvScreenShare;

    private static final int OVERLAY_PERMISSION_REQ_CODE = 1002;
    private static final int CAPTURE_PERMISSION_REQ_CODE = 1003;
    private MediaProjectionManager mediaProjectionManager;
    private MediaProjection mediaProjection;

    private SinvieSecondDialPlateControl mPlateControl;

    private LinearLayout mConfRemoteVideoLayoutSvc;
    private FrameLayout mConfRemoteVideoLayoutA;
    private FrameLayout mConfRemoteVideoLayoutB;
    private FrameLayout mConfRemoteVideoLayoutC;
    private FrameLayout mConfRemoteVideoLayoutD;
    private TsdkCallInfo callInfo;
    private boolean isChecked = false;
    private Toast mToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        ComponentName componentName = manager.getRunningTasks(1).get(0).topActivity;
        String className = componentName.getClassName();
        if (!TextUtils.isEmpty(className) && className.equals(SinvieConfManagerActivity.class.getName())) {
            SinvieActivityStack.getIns().popup(SinvieConfManagerActivity.class);
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    protected SinvieIConfManagerContract.ConfManagerViewSinvie createView() {
        return this;
    }

    @Override
    protected SinvieConfManagerBasePresenter createPresenter() {
        return new SinvieConfManagerPresenter();
    }

    private String[] mActions = new String[]{
            SinvieConfBroadcastConstants.ACTION_CALL_END,
            SinvieConfBroadcastConstants.RELEASE_CHAIRMAN_RESULT,
            SinvieConfBroadcastConstants.CONF_AUX_DATA_SEND,
            SinvieConfBroadcastConstants.CONF_AUX_DATA_FAILED,
            SinvieConfBroadcastConstants.VIDEO_NO_STREAM_DURATION_MSG,
            SinvieConfBroadcastConstants.REFRESH_SVC_VIEW,
            SinvieConfBroadcastConstants.CONF_AUDIT_DIR,
            SinvieConfBroadcastConstants.CUSTOM_BROADCAST_CONSTANTS_FRONT,
            SinvieConfBroadcastConstants.CUSTOM_BROADCAST_CONSTANTS_BACK,
            SinvieConfBroadcastConstants.DATE_CONFERENCE_AUX_DATA_STATE,
            SinvieConfBroadcastConstants.CHECKIN_CONF_MSG,
            SinvieConfBroadcastConstants.ACTION_CALL_ROUTE_CHANGE,
            SinvieConfBroadcastConstants.ACTION_CALL_DEVICES_STATUS_CHANGE
    };

    private SinvieLocBroadcastReceiver receiver = new SinvieLocBroadcastReceiver() {
        @Override
        public void onReceive(String broadcastName, final Object obj) {
            int result;
            switch (broadcastName) {
                case SinvieConfBroadcastConstants.ACTION_CALL_END:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    });
                    break;
                case SinvieConfBroadcastConstants.RELEASE_CHAIRMAN_RESULT:
                    result = (int) obj;
                    if (result == 0) {
                        SinvieMember sinvieMember = SinvieMeetingMgr.getInstance().getCurrentConferenceSelf();
                        if (sinvieMember != null) {
                            if (sinvieMember.isHandUp()) {
                                SinvieMeetingMgr.getInstance().handup(false, sinvieMember);
                            }
                        }
                    }
                    break;
                case SinvieConfBroadcastConstants.CONF_AUX_DATA_SEND:
                    //app切换到后台开启服务
                    toBackStartService();
                    break;
                case SinvieConfBroadcastConstants.DATE_CONFERENCE_AUX_DATA_STATE:
                    boolean auxDataState = (boolean) obj;
                    if (!auxDataState) {
                        SinvieSystemUtil.setTopApp(SinvieConfManagerActivity.this);
                        //停止悬浮窗后台的服务
                        Intent intent = new Intent(SinvieConfManagerActivity.this, SinvieFloatingButtonService.class);
                        SinvieConfManagerActivity.this.stopService(intent);
                    }
                    break;
                case SinvieConfBroadcastConstants.CONF_AUX_DATA_FAILED:
                    SinvieConfManagerActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(SinvieConfManagerActivity.this, "发送辅流失败...", Toast.LENGTH_SHORT).show();
                        }
                    });
                    break;
                case SinvieConfBroadcastConstants.CONF_AUDIT_DIR:
                    if (null != obj) {
                        TsdkonEvtAuditDir notify = (TsdkonEvtAuditDir) obj;
                        directionType = notify.param.directionType;
                    }
                    break;
                case SinvieConfBroadcastConstants.VIDEO_NO_STREAM_DURATION_MSG:
                    final int duration = (int) obj;
                    SinvieConfManagerActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(SinvieConfManagerActivity.this, "无码流时长：" + duration, Toast.LENGTH_SHORT).show();
                        }
                    });
                    break;
                case SinvieConfBroadcastConstants.REFRESH_SVC_VIEW:
                    final ArrayList<Integer> lableList = (ArrayList) obj;
                    SinvieConfManagerActivity.this.runOnUiThread(() -> {
                        for (int i = 0; i < lableList.size(); i++) {
                            int lable = lableList.get(i);
                            switch (i) {
                                case 0:
                                    if (lable == 0) {
                                        mConfRemoteVideoLayoutA.setVisibility(View.GONE);
                                    } else {
                                        mConfRemoteVideoLayoutA.setVisibility(View.VISIBLE);
                                    }
                                    break;
                                case 1:
                                    if (lable == 0) {
                                        mConfRemoteVideoLayoutB.setVisibility(View.GONE);
                                    } else {
                                        mConfRemoteVideoLayoutB.setVisibility(View.VISIBLE);
                                    }
                                    break;
                                case 2:
                                    if (lable == 0) {
                                        mConfRemoteVideoLayoutC.setVisibility(View.GONE);
                                    } else {
                                        mConfRemoteVideoLayoutC.setVisibility(View.VISIBLE);
                                    }
                                    break;
                                case 3:
                                    if (lable == 0) {
                                        mConfRemoteVideoLayoutD.setVisibility(View.GONE);
                                    } else {
                                        mConfRemoteVideoLayoutD.setVisibility(View.VISIBLE);
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    });
                    break;
                case SinvieConfBroadcastConstants.CUSTOM_BROADCAST_CONSTANTS_FRONT:
                    if (isVideo && !isCameraClose) {
                        mPresenter.closeOrOpenLocalVideo(false);
                    }
                    break;
                case SinvieConfBroadcastConstants.CUSTOM_BROADCAST_CONSTANTS_BACK:
                    if (isVideo && !isCameraClose) {
                        mPresenter.closeOrOpenLocalVideo(true);
                    }
                    break;
                case SinvieConfBroadcastConstants.CHECKIN_CONF_MSG:
                    final TsdkCommonResult isCheckedResult = (TsdkCommonResult) obj;
                    SinvieConfManagerActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (isCheckedResult.getResult() == 0) {
                                isChecked = true;
                                ivIsChecked.setVisibility(View.VISIBLE);
                            } else {
                                ivIsChecked.setVisibility(View.GONE);
                            }
                        }
                    });
                    break;
                case SinvieConfBroadcastConstants.ACTION_CALL_ROUTE_CHANGE:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            int currentAudioRoute = SinvieCallMgr.getInstance().getCurrentAudioRoute();
                            if (currentAudioRoute == SinvieCallConstant.TYPE_LOUD_SPEAKER) {
                                mConfSpeaker.setActivated(true);
                                return;
                            }
                            mConfSpeaker.setActivated(false);
                        }
                    });
                    break;
                case SinvieConfBroadcastConstants.ACTION_CALL_DEVICES_STATUS_CHANGE:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            int route = (int) obj;
                            SinvieLogUtil.e("setAudioRoute", "setAudioRoute:" + route);
                            if (route != 2 && route != 0) {
                                return;
                            }

                            if (isVideo) {
                                SinvieCallMgr.getInstance().setAudioRoute(TsdkMobileAuidoRoute.TSDK_E_MOBILE_AUDIO_ROUTE_LOUDSPEAKER);
                            } else {
                                SinvieCallMgr.getInstance().setAudioRoute(TsdkMobileAuidoRoute.TSDK_E_MOBILE_AUDIO_ROUTE_DEFAULT);
                            }
                            mConfSpeaker.setActivated(isVideo);
                        }
                    });
                    break;
                default:
                    break;
            }
        }
    };

    private void toBackStartService() {
        //app切换到后台
        appReturnToBack();
        // share2 创建悬浮窗按钮（权限处理）
        Intent mIntent = new Intent(SinvieConfManagerActivity.this, SinvieFloatingButtonService.class);
        startService(mIntent);
    }

    private void requestPermission() {

        XXPermissions.with(this)
                // 申请悬浮窗权限
                .permission(Permission.SYSTEM_ALERT_WINDOW)
                .request(new OnPermissionCallback() {
                    @Override
                    public void onGranted(List<String> permissions, boolean all) {
                        if (all) {
                            requestCapturePermission();
                        } else {
                            SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_tips));
                        }
                    }

                    @Override
                    public void onDenied(List<String> permissions, boolean never) {
                        if (never) {
                            SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_never));
                        } else {
                            SinvieToastUtil.toastLongMessage(getString(R.string.sinvie_permission_fail));
                        }
                        XXPermissions.startPermissionActivity(SinvieConfManagerActivity.this,
                                permissions);
                    }
                });

    }

    public void requestCapturePermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            //5.0 之后才允许使用屏幕截图
            Toast.makeText(SinvieConfManagerActivity.this, "Android 5.0", Toast.LENGTH_SHORT).show();
        } else {
            mediaProjectionManager =  (MediaProjectionManager) getApplication().getSystemService(Context.MEDIA_PROJECTION_SERVICE);
            SinvieActivityUtil.startActivityForResult(this, mediaProjectionManager.createScreenCaptureIntent(), CAPTURE_PERMISSION_REQ_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == OVERLAY_PERMISSION_REQ_CODE) {
            if (Build.VERSION.SDK_INT >= 23) {
                if (!Settings.canDrawOverlays(this)) {
                    Toast.makeText(this, "权限授予失败，无法开启悬浮窗", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "权限授予成功！", Toast.LENGTH_SHORT).show();
                    //有悬浮窗权限开启服务绑定 绑定权限
                    //请求截屏的权限
                    requestCapturePermission();
                }
            }
        } else if (requestCode == CAPTURE_PERMISSION_REQ_CODE) {
            if (resultCode != Activity.RESULT_OK) {
                Toast.makeText(this, "取消了共享", Toast.LENGTH_SHORT).show();
                return;
            }
            HarmonyCaptureScreen.setProjectionResult(resultCode, data);
            //调发送辅流的接口
            sendAuxData();
        } else {

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void appReturnToBack() {
        // share1 应用切换到后台
        moveTaskToBack(true);
    }

    @Override
    public void initializeComposition() {
        setContentView(R.layout.activity_sinvie_conf_manager);
        mConfButton = (LinearLayout) findViewById(R.id.media_btn_group);
        mVideoConfLayout = (RelativeLayout) findViewById(R.id.conference_video_layout);
        mTitleLayout = (RelativeLayout) findViewById(R.id.title_layout_transparent);
        mConfMediaLayout = (LinearLayout) findViewById(R.id.media_btn_group);
        mRecordPoint = (ImageView) findViewById(R.id.record_view);
        ivIsChecked = (ImageView) findViewById(R.id.iv_is_checked);

        //title
        mLeaveIV = (ImageView) findViewById(R.id.leave_iv);
        mTitleTV = (TextView) findViewById(R.id.conf_title);
        mRightIV = (ImageView) findViewById(R.id.right_iv);
        mShareIV = (ImageView) findViewById(R.id.share_iv);
        mCallNameTv = (TextView) findViewById(R.id.call_name_mark);

        //main tab
        mConfHangup = (FrameLayout) findViewById(R.id.conf_hangup);
        mConfMute = (FrameLayout) findViewById(R.id.conf_mute);
        mConfSpeaker = (FrameLayout) findViewById(R.id.conf_loud_speaker);
        mConfAddAttendee = (FrameLayout) findViewById(R.id.conf_add_attendee);
        mConfAttendee = (FrameLayout) findViewById(R.id.conf_attendee);
        mConfMore = (FrameLayout) findViewById(R.id.btn_conf_more);

        mConfPlate = (FrameLayout) findViewById(R.id.conf_dial_plate);
        mPlateArea = (LinearLayout) findViewById(R.id.dial_plate_area);
        mCloseArea = (ImageView) findViewById(R.id.hide_dial_btn);

        ImageView callAudioBtn = (ImageView) findViewById(R.id.call_audio_btn);
        ImageView callVideoBtn = (ImageView) findViewById(R.id.call_video_btn);
        callAudioBtn.setVisibility(View.GONE);
        callVideoBtn.setVisibility(View.GONE);

        mConfScreenShare = (FrameLayout) findViewById(R.id.conf_screen_share);
        mIvScreenShare = (ImageView) findViewById(R.id.iv_screen_share);

        mConfRemoteVideoLayout = (FrameLayout) findViewById(R.id.conf_remote_video_layout);
        mHideLocalVideoBtn = (FrameLayout) findViewById(R.id.local_video_hide);
        mHideVideoLayout = (FrameLayout) findViewById(R.id.hide_video_view);
        mShowLocalVideoBtn = (FrameLayout) findViewById(R.id.local_video_hide_cancel);
        mConfSmallLayout = (FrameLayout) findViewById(R.id.conf_video_small_logo);

        mConfRemoteVideoLayoutSvc = (LinearLayout) findViewById(R.id.conf_remote_video_layout_svc);
        mConfRemoteVideoLayoutA = (FrameLayout) findViewById(R.id.conf_remote_video_a);
        mConfRemoteVideoLayoutB = (FrameLayout) findViewById(R.id.conf_remote_video_b);
        mConfRemoteVideoLayoutC = (FrameLayout) findViewById(R.id.conf_remote_video_c);
        mConfRemoteVideoLayoutD = (FrameLayout) findViewById(R.id.conf_remote_video_d);

        mAudioConfAttendeeTV = (TextView) findViewById(R.id.tv_audio_conf_attendee);
        mAudioConfLayout = (RelativeLayout) findViewById(R.id.audio_conf_layout_logo);

        // 在与会者列表上报之前会控按钮全部屏蔽
        mConfMute.setVisibility(View.GONE);
        mConfAddAttendee.setVisibility(View.GONE);
        mConfAttendee.setVisibility(View.GONE);
        mConfMore.setVisibility(View.GONE);

        if (!isUnidirectLiveBroadcastConf) {
            if (isVideo) {
                //video layout
                mVideoConfLayout.setVisibility(View.VISIBLE);

                //title
                mRightIV.setVisibility(View.VISIBLE);
                mConfScreenShare.setVisibility(View.VISIBLE);
//                mConfScreenShare.setVisibility(View.GONE);

                mHideLocalVideoBtn.setOnClickListener(this);
                mShowLocalVideoBtn.setOnClickListener(this);
                mRightIV.setOnClickListener(this);

                //重新显示本地窗口，无需再打开本地视频
                mPresenter.changeLocalVideoVisible(true);
            } else {
                mAudioConfAttendeeTV.setSelected(true);

                mAudioConfLayout.setVisibility(View.VISIBLE);
                mVideoConfLayout.setVisibility(View.INVISIBLE);

                //title
                mRightIV.setVisibility(View.GONE);
                mConfScreenShare.setVisibility(View.GONE);
            }
        } else {
            mConfScreenShare.setVisibility(View.GONE);
            mConfPlate.setVisibility(View.GONE);
            mConfAttendee.setVisibility(View.GONE);
            mConfSpeaker.setVisibility(View.GONE);
            mConfMute.setVisibility(View.GONE);
            mConfSmallLayout.setVisibility(View.GONE);
            mHideVideoLayout.setVisibility(View.GONE);
            mHideLocalVideoBtn.setVisibility(View.GONE);
        }
        String number = SinvieLoginMgr.getInstance().getTerminal();
        if (number != null) {
            mCallNameTv.setText(getString(R.string.sinvie_self_number) + number);
        }
        mVideoConfLayout.setOnClickListener(this);
        mConfHangup.setOnClickListener(this);
        mConfMute.setOnClickListener(this);
        mConfSpeaker.setOnClickListener(this);
        mConfAddAttendee.setOnClickListener(this);
        mConfAttendee.setOnClickListener(this);
        mConfMore.setOnClickListener(this);
        mLeaveIV.setOnClickListener(this);
        mShareIV.setOnClickListener(this);

        mConfPlate.setOnClickListener(this);
        mCloseArea.setOnClickListener(this);

        mIvScreenShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //请求悬浮窗权限
                requestPermission();
            }
        });

        int callId = SinvieMeetingMgr.getInstance().getCurrentConferenceCallID();
        mPlateControl = new SinvieSecondDialPlateControl(mPlateArea, callId);

        if (isVideo) {
            SinvieCallMgr.getInstance().setAudioRoute(TsdkMobileAuidoRoute.TSDK_E_MOBILE_AUDIO_ROUTE_LOUDSPEAKER);
        } else {
            SinvieCallMgr.getInstance().setAudioRoute(TsdkMobileAuidoRoute.TSDK_E_MOBILE_AUDIO_ROUTE_DEFAULT);
        }
        mConfSpeaker.setActivated(isVideo);

        // 默认是AVC
        if (callInfo.getIsSvcCall() == 0) {
            mConfRemoteVideoLayout.setVisibility(View.VISIBLE);
            mConfRemoteVideoLayoutSvc.setVisibility(View.GONE);
            mPresenter.setVideoContainer(this, mConfSmallLayout, mConfRemoteVideoLayout,
                    mHideVideoLayout);
        } else {
            mConfRemoteVideoLayout.setVisibility(View.GONE);
            mConfRemoteVideoLayoutSvc.setVisibility(View.VISIBLE);
            mPresenter.setVideoContainer(
                    this, mConfSmallLayout,
                    mConfRemoteVideoLayoutA, mConfRemoteVideoLayoutB,
                    mConfRemoteVideoLayoutC, mConfRemoteVideoLayoutD,
                    mHideVideoLayout);
        }
    }

    /**
     * 发送辅流接口
     */
    private void sendAuxData() {
        int callId = SinvieMeetingMgr.getInstance().getCurrentConferenceCallID();
        SinvieSession sinvieCallSession = SinvieCallMgr.getInstance().getCallSessionByCallID(callId);
        TsdkCall tsdkCall = sinvieCallSession.getTsdkCall();
        int result = tsdkCall.startAuxData();
        if (result != 0) {
            SinvieLogUtil.e(SinvieUIConstants.DEMO_TAG, "send aux data failed. result = " + result);
            return;
        }
        SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "sendAuxData");
    }

    @Override
    protected void onResume() {
        int callId = SinvieMeetingMgr.getInstance().getCurrentConferenceCallID();
        SinvieSession sinvieCallSession = SinvieCallMgr.getInstance().getCallSessionByCallID(callId);
        if (sinvieCallSession != null) {
            TsdkCall tsdkCall = sinvieCallSession.getTsdkCall();
            // 停止发送辅流接口
            int result = tsdkCall.stopAuxData();
            if (result != 0) {
                SinvieLogUtil.e(SinvieUIConstants.DEMO_TAG, "stop aux data failed. result = " + result);
            }
        }
        //停止悬浮窗后台的服务
        Intent intent = new Intent(SinvieConfManagerActivity.this, SinvieFloatingButtonService.class);
        SinvieConfManagerActivity.this.stopService(intent);
        super.onResume();
        mPresenter.registerBroadcast();

        SinvieLocBroadcast.getInstance().registerBroadcast(receiver, mActions);
        // 刷新当前扬声器状态
        updateLoudSpeakerButton(SinvieCallMgr.getInstance().getCurrentAudioRoute());
        if (callInfo.getIsSvcCall() == 0) {
            mConfRemoteVideoLayout.setVisibility(View.VISIBLE);
            mConfRemoteVideoLayoutSvc.setVisibility(View.GONE);
            mPresenter.setVideoContainer(this, mConfSmallLayout, mConfRemoteVideoLayout,
                    mHideVideoLayout);
        } else {
            mConfRemoteVideoLayout.setVisibility(View.GONE);
            mConfRemoteVideoLayoutSvc.setVisibility(View.VISIBLE);
            mPresenter.setVideoContainer(
                    this, mConfSmallLayout,
                    mConfRemoteVideoLayoutA, mConfRemoteVideoLayoutB,
                    mConfRemoteVideoLayoutC, mConfRemoteVideoLayoutD,
                    mHideVideoLayout);
        }
        if (!isUnidirectLiveBroadcastConf) {
            if (!isVideo) {
                mConfScreenShare.setVisibility(View.GONE);
                return;
            } else {
                mVideoConfLayout.setVisibility(View.VISIBLE);
                mRightIV.setVisibility(View.VISIBLE);
                mShareIV.setVisibility(View.VISIBLE);
                mConfScreenShare.setVisibility(View.VISIBLE);
//                mConfScreenShare.setVisibility(View.GONE);
            }
        } else {
            if (flag) {
                mConfScreenShare.setVisibility(View.VISIBLE);
//                mConfScreenShare.setVisibility(View.GONE);
                mConfSmallLayout.setVisibility(View.VISIBLE);
            } else {
                mConfScreenShare.setVisibility(View.GONE);
                mConfSmallLayout.setVisibility(View.GONE);
            }
        }

        if (isFirstStart) {
            startTimer();
        }

        if (mOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            if (mConfSmallLayout.getLayoutParams().width != dp2ps(this, 160) ||
                    mConfSmallLayout.getLayoutParams().height != dp2ps(this, 90)) {
                mConfSmallLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        mConfSmallLayout.getLayoutParams().width = dp2ps(SinvieConfManagerActivity.this
                                , 160);
                        mConfSmallLayout.getLayoutParams().height =
                                dp2ps(SinvieConfManagerActivity.this, 90);
                    }
                });
            }
        } else {
            if (mConfSmallLayout.getLayoutParams().width != dp2ps(this, 90) ||
                    mConfSmallLayout.getLayoutParams().height != dp2ps(this, 160)) {
                mConfSmallLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        mConfSmallLayout.getLayoutParams().width = dp2ps(SinvieConfManagerActivity.this
                                , 90);
                        mConfSmallLayout.getLayoutParams().height =
                                dp2ps(SinvieConfManagerActivity.this, 160);
                    }
                });
            }
        }

        mPresenter.setAutoRotation(this, true, mOrientation);

        // 以下处理用于PBX下的视频会议
//        Member self = MeetingMgr.getInstance().getCurrentConferenceSelf();
//        if (self == null || self.getCameraEntityList().isEmpty())
//        {
//            LogUtil.i(UIConstants.DEMO_TAG,  "no camera--------- ");
//        }
//        else
//        {
//            //打开前置摄像头
//            mPresenter.shareSelfVideo(self.getCameraEntityList().get(1).getDeviceID());
//        }

    }

    /**
     * 获取呼叫信息
     *
     * @return 呼叫信息
     */
    private TsdkCallInfo getTsdkCallInfo() {
        int callId = SinvieMeetingMgr.getInstance().getCurrentConferenceCallID();
        TsdkCallManager callManager = TsdkManager.getInstance().getCallManager();
        TsdkCall tsdkCall = callManager.getCallByCallId(callId);
        return tsdkCall.getCallInfo();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopTimer();
        isFirstStart = true;
        isPressTouch = false;
        isShowBar = false;
    }

    @Override
    public void initializeData() {
        Intent intent = getIntent();
        confID = intent.getStringExtra(SinvieUIConstants.CONF_ID);
        isVideo = intent.getBooleanExtra(SinvieUIConstants.IS_VIDEO_CONF, false);

        SinvieMeetingMgr.getInstance().setIsInConf(1);

        if (confID == null) {
            showToast(R.string.sinvie_empty_conf_id);
            return;
        }
        callInfo = getTsdkCallInfo();
        mPresenter.setConfID(confID);
        mAdapter = new SinviePopupConfListAdapter(this);

        if (!isVideo) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
            return;
        }
        if (SinvieConfConstant.ConfProtocol.IDO_PROTOCOL == SinvieMeetingMgr.getInstance().getConfProtocol()) {
            items.add(getString(R.string.sinvie_voice_control_mode));
        } else {
            items.add(getString(R.string.sinvie_broadcast_mode));
            items.add(getString(R.string.sinvie_voice_control_mode));
            items.add(getString(R.string.sinvie_free_mode));
        }

        // 获取屏幕方向
        Configuration configuration = this.getResources().getConfiguration();
        mOrientation = configuration.orientation;

        selfEntity = SinvieMeetingMgr.getInstance().getCurrentConferenceSelf();

    }

    @Override
    public void finishActivity() {
        stopTimer();
        finish();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void dismissLoading() {

    }

    @Override
    public void showCustomToast(final int resID) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showToast(resID);
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.conference_video_layout) {
            if (!isVideo) {
                return;
            }
            if (isFirstStart) {
                return;
            }
            if (isPressTouch) {
                return;
            } else {
                isPressTouch = true;
                startTimer();
            }
        } else if (id == R.id.conf_hangup) {
            SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "conference hangup!");
            if (!mPresenter.isChairMan()) {
                showLeaveConfDialog();
            } else {
                showEndConfDialog();
            }
        } else if (id == R.id.conf_mute) {
            SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "conference mute!");
            mPresenter.muteSelf();
        } else if (id == R.id.conf_loud_speaker) {
            SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "conference speaker!");
            updateLoudSpeakerButton(mPresenter.switchLoudSpeaker());
        } else if (id == R.id.conf_dial_plate) {
            mPlateControl.showDialPlate();
        } else if (id == R.id.hide_dial_btn) {
            mPlateControl.hideDialPlate();
        } else if (id == R.id.local_video_hide) {
            mPresenter.changeLocalVideoVisible(false);

            mHideLocalVideoBtn.setVisibility(View.GONE);
            mShowLocalVideoBtn.setVisibility(View.VISIBLE);

            mConfSmallLayout.setVisibility(View.GONE);
            mHideVideoLayout.setVisibility(View.VISIBLE);
        } else if (id == R.id.local_video_hide_cancel) {
            mPresenter.changeLocalVideoVisible(true);

            mHideLocalVideoBtn.setVisibility(View.VISIBLE);
            mShowLocalVideoBtn.setVisibility(View.GONE);

            mHideVideoLayout.setVisibility(View.GONE);
            mConfSmallLayout.setVisibility(View.VISIBLE);
        } else if (id == R.id.right_iv) {//调用选看接口
            int isSvcCall = callInfo.getIsSvcCall();
            if (isSvcCall == 0) {
                final List<SinvieMember> sinvieMemberList = mPresenter.getMemberList();
                if (null == sinvieMemberList || sinvieMemberList.size() <= 0) {
                    return;
                }
                final View popupView = getLayoutInflater().inflate(R.layout.view_sinvie_popup_conf_list,
                        null);
                mConfMemberListView =
                        (ListView) popupView.findViewById(R.id.popup_conf_member_list);

                View headView =
                        getLayoutInflater().inflate(R.layout.item_sinvie_popup_video_conf_list, null);
                final TextView tvDisplayName = (TextView) headView.findViewById(R.id.name_tv);
                ImageView isMainHall = (ImageView) headView.findViewById(R.id.host_logo);
                tvDisplayName.setText(SinvieLocContext.getString(R.string.sinvie_main_conference));
                isMainHall.setImageResource(R.drawable.icon_sinvie_group_detail_group);
                mConfMemberListView.addHeaderView(headView);

                mAdapter.setData(sinvieMemberList);
                mConfMemberListView.setAdapter(mAdapter);
                mPopupWindow = SinviePopupWindowUtil.getInstance().generatePopupWindow(popupView);
                mPopupWindow.showAsDropDown(findViewById(R.id.right_iv));
                mConfMemberListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position,
                                            long id) {
                        SinvieMember sinvieConfMemberEntity = new SinvieMember();
                        if (0 != position) {
                            sinvieConfMemberEntity = sinvieMemberList.get(position - 1);
                        }
                        mPresenter.watchAttendee(sinvieConfMemberEntity);
                        mPopupWindow.dismiss();
                    }
                });
            } else {
                watchSvcAttendees();
            }
        } else if (id == R.id.conf_add_attendee) {
            showAddMemberDialog();
        } else if (id == R.id.conf_attendee) {
            Intent intent = new Intent(SinvieIntentConstant.CONF_MEMBER_LIST_ACTIVITY_ACTION);
            intent.putExtra(SinvieUIConstants.CONF_ID, confID);
            intent.putExtra(SinvieUIConstants.IS_VIDEO_CONF, isVideo);
            intent.putExtra(SinvieUIConstants.IS_DATE_CONF, isDateConf);
            SinvieActivityUtil.startActivity(this, intent);
        } else if (id == R.id.btn_conf_more) {
            showMoreConfCtrl();
        } else if (id == R.id.leave_iv) {//   if (selfEntity.getRole() != TSDK_E_CONF_ROLE_CHAIRMAN)
            if (!mPresenter.isChairMan()) {
                showLeaveConfDialog();
            } else {
                showEndConfDialog();
            }
        } else if (id == R.id.share_iv) {
            Intent shareIntent = new Intent(SinvieIntentConstant.CONF_DATA_ACTIVITY_ACTION);
            shareIntent.putExtra(SinvieUIConstants.CONF_ID, confID);
            shareIntent.putExtra(SinvieUIConstants.IS_VIDEO_CONF, isVideo);
            shareIntent.putExtra(SinvieUIConstants.IS_START_SHARE_CONF, isStartShare);
            shareIntent.putExtra(SinvieUIConstants.IS_AUX_DATA_SHARE, isAuxDataShare);
            shareIntent.putExtra(SinvieUIConstants.IS_DATE_CONF, isDateConf);
            SinvieActivityUtil.startActivity(this, shareIntent);
        }
    }

    // 观看与会者接口调用的数据处理
    private void watchSvcAttendees() {
        int ssrcTableStart = callInfo.getSsrcTableStart();
        List<TsdkWatchSvcAttendees> watchAttendeeList = new ArrayList<>();
        for (int i = ssrcTableStart; i < ssrcTableStart + 4; i++) {
            TsdkWatchSvcAttendees watchSvcAttendees = new TsdkWatchSvcAttendees();
            watchSvcAttendees.setWidth(720);
            watchSvcAttendees.setHeight(1080);
            watchSvcAttendees.setLableId(i);
            watchAttendeeList.add(watchSvcAttendees);
        }

        int result = SinvieMeetingMgr.getInstance().watchSvcAttendee(watchAttendeeList);
        if (0 != result) {
            SinvieLogUtil.d(SinvieUIConstants.DEMO_TAG, "watchSvcAttendee fail result : " + result);
        }
    }

    private boolean isContainPresenter() {
        final List<SinvieMember> sinvieMembers = mPresenter.getMemberList();
        if (null == sinvieMembers || sinvieMembers.size() <= 0) {
            return false;
        }
        for (SinvieMember sinvieMember : sinvieMembers) {
            if (sinvieMember.isPresent()) {
                return true;
            }
        }
        return false;
    }

    private View.OnClickListener moreButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mPopupWindow != null && mPopupWindow.isShowing()) {
                mPopupWindow.dismiss();
            }
            int id = v.getId();
            if (id == R.id.switch_camera_ll) {
                SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "conference switch camera!");
                // 摄像头关闭在UI层不能切换摄像头
                if (isCameraClose) {
                    showToast(R.string.sinvie_camera_close_can_not_switch);
                    return;
                }
                mPresenter.switchCamera();
            } else if (id == R.id.close_camera_ll) {
                isCameraClose = !isCameraClose;
                boolean result = mPresenter.closeOrOpenLocalVideo(isCameraClose);
                if (!result) {
                    showToast(cameraStatusIV.isActivated() ? R.string.sinvie_close_video_failed :
                            R.string.sinvie_open_video_failed);
                }
            } else if (id == R.id.hand_up_ll) {
                mPresenter.handUpSelf();
            } else if (id == R.id.mute_all_ll) {
                mPresenter.muteConf(true);
            } else if (id == R.id.cancel_mute_all_ll) {
                mPresenter.muteConf(false);
            } else if (id == R.id.lock_conf_ll) {
                mPresenter.lockConf(true);
            } else if (id == R.id.un_lock_conf_ll) {
                mPresenter.lockConf(false);
            } else if (id == R.id.upgrade_conf_ll) {
                mPresenter.updateConf();
            } else if (id == R.id.request_chairman_ll) {
                showRequestChairmanDialog();
            } else if (id == R.id.release_chairman_ll) {
                mPresenter.releaseChairman();
            } else if (id == R.id.set_conf_mode_ll) {
                showConfMode();
            } else if (id == R.id.start_record_ll) {
                mPresenter.recordConf(true);
            } else if (id == R.id.stop_record_ll) {
                mPresenter.recordConf(false);
            } else if (id == R.id.apply_speak_ll) {
                if (0 == directionType) {
                    SinvieMeetingMgr.getInstance().switchAuditSitesDir(1);
                    directionType = 1;
                    flag = true;
                } else {
                    SinvieMeetingMgr.getInstance().switchAuditSitesDir(0);
                    directionType = 0;
                    flag = false;
                }
            } else if (id == R.id.net_quality_ll) {
                Intent intent = new Intent(SinvieIntentConstant.CALL_QUALITY_ACTIVITY_ACTION);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addCategory(SinvieIntentConstant.DEFAULT_CATEGORY);
                intent.putExtra(SinvieUIConstants.CALL_INFO, confID);
                SinvieActivityUtil.startActivity(SinvieConfManagerActivity.this, intent);
            }
        }
    };

    private void showRequestChairmanDialog() {
        final SinvieEditDialog dialog = new SinvieEditDialog(this, R.string.sinvie_input_chairman_password);
        dialog.setRightButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SinvieCommonUtil.hideSoftInput(SinvieConfManagerActivity.this);
                mPresenter.requestChairman(dialog.getText());
            }
        });
        dialog.show();
    }

    private void showConfMode() {
        final SimpleListDialog dialog = new SimpleListDialog(this, items);
        dialog.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialog.dismiss();
                mPresenter.onItemDetailClick((String) items.get(position), null);
            }
        });
        dialog.show();
    }

    @Override
    public void refreshMemberList(final List<SinvieMember> list) {
        if (null == list || list.size() <= 0) {
            return;
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showMoreButton();

                // 自动调用SVC选看接口
                int isSvcCall = callInfo.getIsSvcCall();
                if (isSvcCall != 0) {
                    SinvieLogUtil.d(SinvieUIConstants.DEMO_TAG, "auto invoke watchSvcAttendee()");
                    watchSvcAttendees();
                }

                mAdapter.setData(list);
                mAdapter.notifyDataSetChanged();

                if (!isVideo) {
                    mAudioConfAttendeeTV.setText(getAttendeeName(list));
                }
            }
        });
    }

    private String getAttendeeName(List<SinvieMember> list) {
        if (1 == list.size()) {
            return list.get(0).getDisplayName();
        }

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            if (i == list.size() - 1) {
                builder.append(list.get(i).getDisplayName());
            } else {
                builder.append(list.get(i).getDisplayName() + ", ");
            }
        }

        return builder.toString();
    }

    private void showMoreButton() {
        if (isUnidirectLiveBroadcastConf) {
            if (1 == directionType) {
                mConfScreenShare.setVisibility(View.VISIBLE);
                mConfAttendee.setVisibility(View.VISIBLE);
                mConfMute.setVisibility(View.VISIBLE);
                mConfMore.setVisibility(View.VISIBLE);

                mPresenter.changeLocalVideoVisible(true);
                mHideVideoLayout.setVisibility(View.GONE);
                mConfSmallLayout.setVisibility(View.VISIBLE);

            } else {
                mConfScreenShare.setVisibility(View.GONE);
                mConfAttendee.setVisibility(View.GONE);
                mConfMute.setVisibility(View.GONE);
                mConfMore.setVisibility(View.VISIBLE);

                mPresenter.changeLocalVideoVisible(false);
                mConfSmallLayout.setVisibility(View.GONE);
                mHideVideoLayout.setVisibility(View.VISIBLE);
            }

        } else {
            mConfAttendee.setVisibility(View.VISIBLE);
            mConfMute.setVisibility(View.VISIBLE);
            if (!SinvieMeetingMgr.getInstance().isAnonymous()) {
                mConfMore.setVisibility(View.VISIBLE);
            }
            if (mPresenter.isChairMan()) {
                mConfAddAttendee.setVisibility(View.VISIBLE);
            } else {
                mConfAddAttendee.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void showItemClickDialog(final List<Object> items, final SinvieMember sinvieMember) {
        final SimpleListDialog dialog = new SimpleListDialog(this, items);
        dialog.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialog.dismiss();
                mPresenter.onItemDetailClick((String) items.get(position), sinvieMember);
            }
        });
        dialog.show();
    }

    @Override
    public void updateUpgradeConfBtn(final boolean isInDataConf) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                isDateConf = isInDataConf;
                mShareIV.setVisibility(isInDataConf || isVideo ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void updateConfTypeIcon(final SinvieConfBaseInfo sinvieConfBaseInfo) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mPresenter.isSupportRecord() && mPresenter.isRecord()) {
                    mRecordPoint.setVisibility(View.VISIBLE);
                } else {
                    mRecordPoint.setVisibility(View.GONE);
                }
                mTitleTV.setText(mPresenter.getConfBaseInfoSubject());
            }
        });
    }

    @Override
    public void showMessage(final String message) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                toastMakeText(ConfManagerActivity.this, message, Toast.LENGTH_SHORT);
//            }
//        });
    }

    public void toastMakeText(Context context, String text, int duration) {
        if (mToast == null) {
            mToast = Toast.makeText(context, text, duration);
        } else {
            mToast.setText(text);
            mToast.setDuration(duration);
        }
        mToast.show();
    }

    @Override
    public void startAsShare(boolean isShare) {
        isStartShare = isShare;
    }

    @Override
    public void setAuxDataState(boolean auxDataState) {
        isAuxDataShare = auxDataState;
    }

    private void showLeaveConfDialog() {
        SinvieConfirmDialog dialog = new SinvieConfirmDialog(this, R.string.sinvie_leave_conf);
        dialog.setRightButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.closeConf();
                SinvieActivityStack.getIns().popup(SinvieConfMemberListActivity.class);
                stopTimer();
                finish();
            }
        });
        dialog.show();
    }

    private void showEndConfDialog() {
        SinvieTripleDialog dialog = new SinvieTripleDialog(this);
        dialog.setRightButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.closeConf();
                SinvieActivityStack.getIns().popup(SinvieConfMemberListActivity.class);
                finish();
            }
        });
        dialog.setLeftButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.finishConf();
                SinvieActivityStack.getIns().popup(SinvieConfMemberListActivity.class);
                stopTimer();
                finish();
            }
        });
        dialog.show();
    }

    @Override
    protected void onBack() {
        super.onBack();
        mPresenter.closeConf();
        mPresenter.unregisterBroadcast();
    }

    @Override
    public void onBackPressed() {
        if (!mPresenter.isChairMan()) {
            showLeaveConfDialog();
        } else {
            showEndConfDialog();
        }
        mPresenter.unregisterBroadcast();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SinvieLocBroadcast.getInstance().unRegisterBroadcast(receiver, mActions);
        mPresenter.leaveVideo();
        mPresenter.unregisterBroadcast();
        mPresenter.setAutoRotation(this, false, mOrientation);
        SinviePopupWindowUtil.getInstance().dismissPopupWindow(mPopupWindow);

        SinvieMeetingMgr.getInstance().setIsInConf(0);
    }

    @Override
    public void updateMuteButton(final boolean isMute) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!isUnidirectLiveBroadcastConf) {
                    mConfMute.setVisibility(View.VISIBLE);
                }
                mConfMute.setActivated(isMute);
            }
        });
    }

    @Override
    public void updateAttendeeButton(SinvieMember sinvieMember) {

    }

    @Override
    public void updateIsCheckedButton(final boolean isCheck) {
        if (isChecked) {
            return;
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isCheck) {
                    ivIsChecked.setVisibility(View.VISIBLE);
                } else {
                    ivIsChecked.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void updateLocalVideo() {
        if (callInfo != null) {
            // 默认是AVC
            if (callInfo.getIsSvcCall() == 0) {
                mPresenter.setVideoContainer(this, mConfSmallLayout, mConfRemoteVideoLayout,
                        mHideVideoLayout);
            } else {
                mPresenter.setVideoContainer(
                        this, mConfSmallLayout,
                        mConfRemoteVideoLayoutA, mConfRemoteVideoLayoutB,
                        mConfRemoteVideoLayoutC, mConfRemoteVideoLayoutD,
                        mHideVideoLayout);
            }
        }
    }

    private void updateLoudSpeakerButton(int type) {
        if (type == SinvieCallConstant.TYPE_LOUD_SPEAKER) {
            mConfSpeaker.setActivated(true);
        } else {
            mConfSpeaker.setActivated(false);
        }
    }

    private synchronized PopupWindow generatePopupWindow(View view, int width, int height) {
        final PopupWindow popupWindow = new PopupWindow(view, width, height);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popupWindow.setFocusable(true);
        view.setFocusableInTouchMode(true);
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_MENU && popupWindow.isShowing()) {
                    popupWindow.dismiss();
                    return true;
                }
                return false;
            }
        });

        return popupWindow;
    }

    private void showAddMemberDialog() {
        final SinvieThreeInputDialog editDialog = new SinvieThreeInputDialog(this);
        editDialog.setRightButtonListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.addMember(editDialog.getInput2(), editDialog.getInput1(),
                        editDialog.getInput3());
            }
        });

        editDialog.setHint1(R.string.sinvie_input_number);
        editDialog.setHint2(R.string.sinvie_input_name);
        editDialog.setHint3(R.string.sinvie_input_account);
        editDialog.show();
    }

    private void showMoreConfCtrl() {
        int wrap = LinearLayout.LayoutParams.WRAP_CONTENT;
        View popupView = getLayoutInflater().inflate(R.layout.view_sinvie_popup_conf_btn_list, null);

        LinearLayout switchCameraBtn = (LinearLayout) popupView.findViewById(R.id.switch_camera_ll);
        LinearLayout closeCameraBtn = (LinearLayout) popupView.findViewById(R.id.close_camera_ll);
        LinearLayout handUpLayout = (LinearLayout) popupView.findViewById(R.id.hand_up_ll);
        LinearLayout muteAllLayout = (LinearLayout) popupView.findViewById(R.id.mute_all_ll);
        LinearLayout cancelMuteAllLayout =
                (LinearLayout) popupView.findViewById(R.id.cancel_mute_all_ll);
        LinearLayout lockLayout = (LinearLayout) popupView.findViewById(R.id.lock_conf_ll);
        LinearLayout unlockLayout = (LinearLayout) popupView.findViewById(R.id.un_lock_conf_ll);
        LinearLayout startRecordLayout =
                (LinearLayout) popupView.findViewById(R.id.start_record_ll);
        LinearLayout endRecordLayout = (LinearLayout) popupView.findViewById(R.id.stop_record_ll);
        LinearLayout upgradeLayout = (LinearLayout) popupView.findViewById(R.id.upgrade_conf_ll);
        LinearLayout requestChairManLayout =
                (LinearLayout) popupView.findViewById(R.id.request_chairman_ll);
        LinearLayout releaseChairManLayout =
                (LinearLayout) popupView.findViewById(R.id.release_chairman_ll);
        LinearLayout seConfModeLayout =
                (LinearLayout) popupView.findViewById(R.id.set_conf_mode_ll);
        cameraStatusIV = (ImageView) popupView.findViewById(R.id.iv_camera_status);
        cameraStatusTV = (TextView) popupView.findViewById(R.id.tv_camera_status);
        ImageView handUpIV = (ImageView) popupView.findViewById(R.id.hand_up_iv);
        TextView handUpTV = (TextView) popupView.findViewById(R.id.hand_up_tv);
        LinearLayout postPone = (LinearLayout) popupView.findViewById(R.id.post_pone_ll);
        LinearLayout applySpeak = (LinearLayout) popupView.findViewById(R.id.apply_speak_ll);
        TextView applySpeakTV = (TextView) popupView.findViewById(R.id.apply_speak_tv);
        LinearLayout netQuality = (LinearLayout) popupView.findViewById(R.id.net_quality_ll);

        cameraStatusIV.setActivated(isCameraClose);
        cameraStatusTV.setText(isCameraClose ? getString(R.string.sinvie_open_local_camera) :
                getString(R.string.sinvie_close_local_camera));

        postPone.setVisibility(View.GONE);

        if (!isUnidirectLiveBroadcastConf) {
            // 主席：会场静音、锁定、释放主席权限； 普通与会者：举手、申请主席
            if (mPresenter.isChairMan()) {
                applySpeak.setVisibility(View.GONE);

                if (mPresenter.isConfMute()) {
                    cancelMuteAllLayout.setVisibility(View.VISIBLE);
                    muteAllLayout.setVisibility(View.GONE);
                } else {
                    cancelMuteAllLayout.setVisibility(View.GONE);
                    muteAllLayout.setVisibility(View.VISIBLE);
                }

                if (isDateConf) {
                    upgradeLayout.setVisibility(View.GONE);
                } else {
                    upgradeLayout.setVisibility(View.VISIBLE);
                }

                // 融合会议显示锁定
                if (TSDK_E_CONF_ENV_HOSTED_CONVERGENT_CONFERENCE == SinvieMeetingMgr.getInstance().getConfEnvType() ||
                        TSDK_E_CONF_ENV_ON_PREMISES_CONFERENCING_ONLY == SinvieMeetingMgr.getInstance().getConfEnvType()) {
                    if (mPresenter.isConfLock()) {
                        unlockLayout.setVisibility(View.VISIBLE);
                        lockLayout.setVisibility(View.GONE);
                    } else {
                        unlockLayout.setVisibility(View.GONE);
                        lockLayout.setVisibility(View.VISIBLE);
                    }

                } else {
                    unlockLayout.setVisibility(View.GONE);
                    lockLayout.setVisibility(View.GONE);
                }

                if (mPresenter.isSupportRecord()) {
                    if (mPresenter.isRecord()) {
                        startRecordLayout.setVisibility(View.GONE);
                        endRecordLayout.setVisibility(View.VISIBLE);
                    } else {
                        startRecordLayout.setVisibility(View.VISIBLE);
                        endRecordLayout.setVisibility(View.GONE);
                    }
                } else {
                    startRecordLayout.setVisibility(View.GONE);
                    endRecordLayout.setVisibility(View.GONE);
                }

                requestChairManLayout.setVisibility(View.GONE);
                releaseChairManLayout.setVisibility(View.VISIBLE);
                handUpLayout.setVisibility(View.GONE);
            } else {
                applySpeak.setVisibility(View.GONE);
                cancelMuteAllLayout.setVisibility(View.GONE);
                muteAllLayout.setVisibility(View.GONE);
                unlockLayout.setVisibility(View.GONE);
                lockLayout.setVisibility(View.GONE);
                upgradeLayout.setVisibility(View.GONE);

                requestChairManLayout.setVisibility(View.VISIBLE);
                releaseChairManLayout.setVisibility(View.GONE);
                seConfModeLayout.setVisibility(View.GONE);

                startRecordLayout.setVisibility(View.GONE);
                endRecordLayout.setVisibility(View.GONE);

                if (TSDK_E_CONF_ENV_HOSTED_CONVERGENT_CONFERENCE == SinvieMeetingMgr.getInstance().getConfEnvType() ||
                        TSDK_E_CONF_ENV_ON_PREMISES_CONFERENCING_ONLY == SinvieMeetingMgr.getInstance().getConfEnvType()) {
                    if (mPresenter.isHandUp()) {
                        handUpIV.setActivated(false);
                        handUpTV.setText(R.string.sinvie_conf_cancel_hand_up);
                    } else {
                        handUpIV.setActivated(true);
                        handUpTV.setText(R.string.sinvie_conf_hand_up);
                    }
                } else {
                    handUpLayout.setVisibility(View.GONE);
                }


            }

            // 音频不显示关闭、切换摄像头、选看和广播以及设置会议模式
            if (!isVideo) {
                switchCameraBtn.setVisibility(View.GONE);
                closeCameraBtn.setVisibility(View.GONE);
                seConfModeLayout.setVisibility(View.GONE);
            } else {
                switchCameraBtn.setOnClickListener(moreButtonListener);
                closeCameraBtn.setOnClickListener(moreButtonListener);
                seConfModeLayout.setOnClickListener(moreButtonListener);
            }
        } else {
            switchCameraBtn.setVisibility(View.GONE);
            if (1 == directionType) {
                closeCameraBtn.setVisibility(View.VISIBLE);
                closeCameraBtn.setOnClickListener(moreButtonListener);
            } else {
                closeCameraBtn.setVisibility(View.GONE);
            }

            handUpLayout.setVisibility(View.GONE);
            requestChairManLayout.setVisibility(View.GONE);
            muteAllLayout.setVisibility(View.GONE);
            cancelMuteAllLayout.setVisibility(View.GONE);
            lockLayout.setVisibility(View.GONE);
            unlockLayout.setVisibility(View.GONE);
            startRecordLayout.setVisibility(View.GONE);
            endRecordLayout.setVisibility(View.GONE);
            upgradeLayout.setVisibility(View.GONE);
            releaseChairManLayout.setVisibility(View.GONE);
            seConfModeLayout.setVisibility(View.GONE);
            applySpeak.setVisibility(View.GONE);
            applySpeak.setVisibility(View.VISIBLE);
        }

        handUpLayout.setOnClickListener(moreButtonListener);
        muteAllLayout.setOnClickListener(moreButtonListener);
        cancelMuteAllLayout.setOnClickListener(moreButtonListener);
        lockLayout.setOnClickListener(moreButtonListener);
        unlockLayout.setOnClickListener(moreButtonListener);
        startRecordLayout.setOnClickListener(moreButtonListener);
        endRecordLayout.setOnClickListener(moreButtonListener);
        upgradeLayout.setOnClickListener(moreButtonListener);
        requestChairManLayout.setOnClickListener(moreButtonListener);
        releaseChairManLayout.setOnClickListener(moreButtonListener);
        netQuality.setOnClickListener(moreButtonListener);
        applySpeak.setOnClickListener(moreButtonListener);

        mPopupWindow = generatePopupWindow(popupView, wrap, wrap);
        mPopupWindow.showAtLocation(findViewById(R.id.media_btn_group),
                Gravity.RIGHT | Gravity.BOTTOM, 0, mConfButton.getHeight());
    }

    /**
     * 屏幕旋转时调用此方法
     *
     * @param newConfig
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == 2) {
            mConfSmallLayout.getLayoutParams().width = dp2ps(this, 160);
            mConfSmallLayout.getLayoutParams().height = dp2ps(this, 90);
        } else {
            mConfSmallLayout.getLayoutParams().width = dp2ps(this, 90);
            mConfSmallLayout.getLayoutParams().height = dp2ps(this, 160);
        }

        if (this.mOrientation == newConfig.orientation) {
            return;
        } else {
            this.mOrientation = newConfig.orientation;
            mPresenter.setAutoRotation(this, true, this.mOrientation);
        }
    }

    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     *
     * @param context
     * @param dpValue
     * @return
     */
    private int dp2ps(Context context, float dpValue) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (scale * dpValue + 0.5f);
    }

    private void showButton() {
        if (mTitleLayout.getVisibility() == View.GONE || mConfMediaLayout.getVisibility() == View.GONE) {
            mTitleLayout.setVisibility(View.VISIBLE);
            mConfMediaLayout.setVisibility(View.VISIBLE);
            isShowBar = true;
        }
    }

    private void hideButton() {
        if (mTitleLayout.getVisibility() == View.VISIBLE || mConfMediaLayout.getVisibility() == View.VISIBLE) {
            mTitleLayout.setVisibility(View.GONE);
            mConfMediaLayout.setVisibility(View.GONE);
            isShowBar = false;
        }
    }

    private void startTimer() {
        initTimer();
        try {
            if (isFirstStart) {
                timer.schedule(myTimerTask, 5000);
            } else {
                timer.schedule(myTimerTask, 200, 5000);
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
            initTimer();
            timer.schedule(myTimerTask, 5000);
        }
    }

    private void stopTimer() {
        if (null != timer) {
            timer.cancel();
            timer = null;
        }
    }

    private void initTimer() {
        timer = new Timer();
        myTimerTask = new MyTimerTask();
    }

    class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (isFirstStart) {
                        hideButton();
                        isFirstStart = false;
                        stopTimer();
                    } else {
                        if (isShowBar) {
                            hideButton();
                            isPressTouch = false;
                            stopTimer();
                        } else {
                            showButton();
                        }
                    }
                }
            });
        }
    }

}
