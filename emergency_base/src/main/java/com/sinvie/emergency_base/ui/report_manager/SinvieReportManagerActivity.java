package com.sinvie.emergency_base.ui.report_manager;

import android.content.Intent;
import android.view.KeyEvent;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshLoadMoreListener;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieReportBean;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.logic.report_manager.SinvieReportManagerContact;
import com.sinvie.emergency_base.logic.report_manager.SinvieReportManagerPresenter;
import com.sinvie.emergency_base.ui.SinvieIntentConstant;
import com.sinvie.emergency_base.ui.base.SinvieMVPBaseActivity;
import com.sinvie.emergency_base.ui.report.adapter.SinvieReportRvAdapter;
import com.sinvie.emergency_base.utils.SinvieActivityUtil;
import com.sinvie.emergency_base.widget.SinvieCustomTitleBarLayout;
import com.sinvie.emergency_conf.CommonService.util.SinvieLogUtil;
import com.sinvie.emergency_live.tuikit.base.ISinvieTitleBarLayout;
import com.sinvie.emergency_live.tuikit.utils.SinvieToastUtil;
import com.xiasuhuei321.loadingdialog.view.LoadingDialog;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import cc.shinichi.library.ImagePreview;

public class SinvieReportManagerActivity extends SinvieMVPBaseActivity<SinvieReportManagerContact.ReportManagerBaseView, SinvieReportManagerPresenter> implements SinvieReportManagerContact.ReportManagerBaseView {

    private static final String TAG = SinvieReportManagerActivity.class.getSimpleName();
    private static int mSelectPosition = -1;
    private ConstraintLayout mClSearch;
    private LinearLayoutCompat mLlSearch;
    private LinearLayoutCompat mLlSearchInput;
    private EditText mEtSearch;
    private SmartRefreshLayout mSmartReportManager;
    private RecyclerView mRvReportManager;
    private List<SinvieReportBean.DataRoomListBean> mReportBeanList;
    private SinvieReportRvAdapter mSinvieReportRvAdapter;
    private int mPageIndex = 1; //从第一页开始
    private LoadingDialog mLoadingDialog;
    private SinvieCustomTitleBarLayout mReportManagerTitle;
    private SinvieReportRvAdapter.OnReportRvItemClickListener mOnReportRvItemClickListener = new SinvieReportRvAdapter.OnReportRvItemClickListener() {
        @Override
        public void onReportRvItemClickListener(int position) {
            //记录当前选择的 position
            mSelectPosition = position;
            SinvieReportBean.DataRoomListBean bean = mReportBeanList.get(position);
            //跳转到详情界面 (携带 信息)
            Intent intent = new Intent(SinvieIntentConstant.REPORT_MANAGER_DETAIL_ACTIVITY_ACTION);
            intent.putExtra(SinvieUIConstants.REPORT_INFO_BEAN, bean);
            SinvieActivityUtil.startActivityForResult(SinvieReportManagerActivity.this, intent, SinvieUIConstants.REPORT_DETAIL_REQUEST);
        }

        @Override
        public void onReportRvItemClickListener(int gridType, String url) {
            SinvieLogUtil.d(TAG, gridType + " : " + url);
            //视屏播放 查看大图界面
            if (gridType == 0) {
                //视频
                Intent intent = new Intent(SinvieIntentConstant.VIDEO_PLAY_ACTIVITY_ACTION);
                intent.putExtra(SinvieUIConstants.PLAY_VIDEO_URL, url);
                SinvieActivityUtil.startActivity(SinvieReportManagerActivity.this, intent);
            } else {
                //图片
                ImagePreview.getInstance().setContext(SinvieReportManagerActivity.this).setIndex(0).setImage(url).start();
            }
        }
    };

    @Override
    public void initializeData() {

    }

    @Override
    public void initializeComposition() {
        setContentView(R.layout.activity_sinvie_report_manager);
        mReportManagerTitle = findViewById(R.id.report_manager_title);
        mClSearch = findViewById(R.id.cl_search);
        mLlSearch = findViewById(R.id.ll_search);
        mLlSearchInput = findViewById(R.id.ll_search_input);
        mEtSearch = findViewById(R.id.et_search);
        mSmartReportManager = findViewById(R.id.smart_report_manager);
        mRvReportManager = findViewById(R.id.rv_report_manager);

        mEtSearch.setHint(getString(R.string.sinvie_report_search_hint));

        mReportBeanList = new ArrayList<>();
        mSinvieReportRvAdapter = new SinvieReportRvAdapter(this, mReportBeanList);
        mSinvieReportRvAdapter.setOnReportRvItemClickListener(mOnReportRvItemClickListener);
        mRvReportManager.setLayoutManager(new LinearLayoutManager(this));
        mRvReportManager.setAdapter(mSinvieReportRvAdapter);
        mSmartReportManager.finishLoadMore();

        mReportManagerTitle.setTitle(getString(R.string.sinvie_workbench_report_manager), ISinvieTitleBarLayout.POSITION.MIDDLE);
        mReportManagerTitle.setLeftIcon(R.drawable.svg_sinvie_theme_back);

        //监听输入法回车搜索
        mEtSearch.setOnKeyListener((v, keyCode, event) -> {

            //是否是回车键
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN) {
                //隐藏软键盘
                hideSoftKeyBorder();
                //显示等待弹窗
                showLoginDialog();
                //搜索
                mPageIndex = 1;
                //获取数据
                mPresenter.getReportManagerList(mPageIndex, mEtSearch.getText().toString().trim());
            }
            return false;
        });

        mSmartReportManager.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull @NotNull RefreshLayout refreshLayout) {
                mPageIndex += 1;
                //获取数据
                mPresenter.getReportManagerList(mPageIndex, "");
            }

            @Override
            public void onRefresh(@NonNull @NotNull RefreshLayout refreshLayout) {
                mPageIndex = 1;
                //获取数据
                mPresenter.getReportManagerList(mPageIndex, "");
            }
        });

        mSmartReportManager.autoRefresh();

    }

    @Override
    protected SinvieReportManagerContact.ReportManagerBaseView createView() {
        return this;
    }

    @Override
    protected SinvieReportManagerPresenter createPresenter() {
        return new SinvieReportManagerPresenter(this);
    }

    @Override
    public void getReportManagerSuccess(SinvieReportBean beans) {
        mSmartReportManager.finishRefresh();
        mSmartReportManager.finishLoadMore();
        if (beans == null || beans.getData() == null) {
            return;
        }

        //判断能否上拉加载
        mSmartReportManager.setEnableLoadMore(beans.getData().size() >= SinvieUIConstants.PAGE_SIZE);

        if (mPageIndex == 1) {
            mReportBeanList.clear();
        }
        mReportBeanList.addAll(beans.getData());
        mSinvieReportRvAdapter.notifyDataSetChanged();
    }

    @Override
    public void getReportManagerFail(int errCode, String errMsg) {
        SinvieToastUtil.toastLongErrorMessage(errCode, errMsg);
        mSmartReportManager.finishRefresh();
        mSmartReportManager.finishLoadMore();
    }

    @Override
    public void dismissLoadingDialog() {
        if (mLoadingDialog != null) {
            mLoadingDialog.close();
            mLoadingDialog = null;
        }
    }

    /**
     * 显示等待弹窗
     */
    public void showLoginDialog() {
        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog(this);
        }
        mLoadingDialog.setLoadingText(getString(R.string.sinvie_logining_msg))
                .setInterceptBack(true)
                .setLoadSpeed(LoadingDialog.Speed.SPEED_TWO)
                .setRepeatCount(-1)
                .show();
    }

}