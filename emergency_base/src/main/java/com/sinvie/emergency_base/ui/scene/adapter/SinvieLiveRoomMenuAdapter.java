package com.sinvie.emergency_base.ui.scene.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieLiveMenuBean;
import com.sinvie.emergency_base.ui.scene.adapter.view_holder.SinvieLiveMenuViewHolder;

import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/3
 * @Des: 类描述
 */
public class SinvieLiveRoomMenuAdapter extends RecyclerView.Adapter<SinvieLiveMenuViewHolder> {

    private Context mContext;
    private List<SinvieLiveMenuBean> mImgList = null;
    private LiveRoomItemClickListener mItemClickListener;

    public SinvieLiveRoomMenuAdapter(Context context, List<SinvieLiveMenuBean> imgList) {
        mContext = context;
        mImgList = imgList;
    }

    public void setItemClickListener(LiveRoomItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    @Override
    public SinvieLiveMenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_sinvie_live_room_menu, parent,false);
        return new SinvieLiveMenuViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SinvieLiveMenuViewHolder holder, int position) {
        Glide.with(holder.itemView).load(mImgList.get(position).getImage()).into(holder.mIvLiveRoomMenu);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemClickListener.onLiveRoomItemClickListener(mImgList.get(position).getId(),
                        position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mImgList.size();
    }

    public interface LiveRoomItemClickListener {

        void onLiveRoomItemClickListener(int id, int position);
    }
}
