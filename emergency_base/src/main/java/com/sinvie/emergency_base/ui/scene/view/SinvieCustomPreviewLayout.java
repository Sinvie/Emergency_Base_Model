package com.sinvie.emergency_base.ui.scene.view;

import static android.content.Context.INPUT_METHOD_SERVICE;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieLiveMenuBean;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.ui.scene.adapter.SinvieLiveRoomMenuAdapter;
import com.sinvie.emergency_base.widget.dialog.SinviePwdDialog;
import com.sinvie.emergency_live.tuikit_live.live.SinvieTUIKitLive;
import com.sinvie.emergency_live.tuikit_live.live.modules.liveroom.model.SinvieTRTCLiveRoomDef;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/2
 * @Des: 自定义 主播开播设置界面
 */
public class SinvieCustomPreviewLayout extends ConstraintLayout {

    private Context mContext;
    /**
     * 直播封面
     */
    private ImageView mIvLiveCover;
    private TextView mTvLiveCover;
    /**
     * 直播标题输入
     */
    private EditText mEdLiveTitle;
    /**
     * 直播菜单
     */
    private RecyclerView mRvLiveMenu;
    /**
     * 开启直播
     */
    private TextView mTvStartNewLive;
    /**
     * 选择直播模式
     */
    private LinearLayoutCompat mLlSelectLiveType;
    private TextView mTvSelectLiveType;
    private RadioGroup mRgLiveType;
    private RadioButton mRbLiveTypeSite;
    private RadioButton mRbLiveTypeTask;
    /**
     * 房间号
     */
    private TextView mTvLiveRoomId;
    /**
     * 实时网速
     */
    private LinearLayoutCompat mLlLiveNet;
    private TextView mTvLiveNet;
    /**
     * 开播时间
     */
    private TextView mTvLiveTime;
    /**
     * 交互接口
     */
    private CustomPreviewCallback mPreviewCallback;

    private List<SinvieLiveMenuBean> mSinvieLiveMenuBeans;

    private FragmentManager mFragmentManager;
    private SinviePwdDialog mSinvieLiveRoomPwdDialog;
    private SinvieLiveRoomMenuAdapter mRoomMenuAdapter;

    private String mLiveCoverPath;

    public SinvieCustomPreviewLayout(Context context) {
        super(context);
        mContext = context;
        initView();
    }

    public SinvieCustomPreviewLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initView();
    }

    public SinvieCustomPreviewLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initView();
    }

    private void initView() {
        inflate(mContext, R.layout.view_sinvie_custom_live_room_preview_layout, this);
        mIvLiveCover = findViewById(R.id.iv_live_cover);
        mTvLiveCover = findViewById(R.id.tv_live_cover);
        mEdLiveTitle = findViewById(R.id.ed_live_title);
        mRvLiveMenu = findViewById(R.id.rv_live_menu);
        mTvStartNewLive = findViewById(R.id.tv_start_new_live);
        mLlSelectLiveType = findViewById(R.id.ll_live_type);
        mTvSelectLiveType = findViewById(R.id.tv_select_live_type);
        mRgLiveType = findViewById(R.id.rg_live_type);
        mRbLiveTypeSite = findViewById(R.id.rb_live_type_site);
        mRbLiveTypeTask = findViewById(R.id.rb_live_type_task);
        mTvLiveRoomId = findViewById(R.id.tv_live_room_id);
        mLlLiveNet = findViewById(R.id.ll_live_net);
        mTvLiveNet = findViewById(R.id.tv_live_net);
        mTvLiveTime = findViewById(R.id.tv_live_time);

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        mRvLiveMenu.setLayoutManager(manager);
        mSinvieLiveMenuBeans = new ArrayList<>();
        mRoomMenuAdapter = new SinvieLiveRoomMenuAdapter(mContext, getLiveRoomRvData());
        mRoomMenuAdapter.setItemClickListener(this::setLiveRoomMenu);
        mRvLiveMenu.setAdapter(mRoomMenuAdapter);

        //选择封面
        mIvLiveCover.setOnClickListener(v -> {
            if (mPreviewCallback != null) {
                mPreviewCallback.onSelectCover();
            }
        });
        //开启直播
        mTvStartNewLive.setOnClickListener(v -> {

            String roomName = mEdLiveTitle.getText().toString().trim();
            if (TextUtils.isEmpty(roomName)) {
                Toast.makeText(SinvieTUIKitLive.getAppContext(), R.string.sinvie_live_room_name_empty, Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(mLiveCoverPath)) {
                Toast.makeText(SinvieTUIKitLive.getAppContext(), R.string.sinvie_live_room_cover_empty, Toast.LENGTH_SHORT).show();
                return;
            }

            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mEdLiveTitle.getWindowToken(), 0);
            int audioQuality = SinvieUIConstants.LIVE_ROOM_TASK;
            if (mRbLiveTypeSite.isChecked()) {
                audioQuality = SinvieUIConstants.LIVE_ROOM_SITE;
            }
            if (mPreviewCallback != null) {
                mPreviewCallback.onStartLive(roomName, audioQuality);
            }
        });
        mRoomMenuAdapter.notifyDataSetChanged();
    }

    private List<SinvieLiveMenuBean> getLiveRoomRvData() {
        int[] img = {R.mipmap.icon_sinvie_live_back
                , R.mipmap.icon_sinvie_switch_camera
//                , R.mipmap.icon_change_horizontal
//                , R.mipmap.icon_not_lock_screen
        };
        int[] id = {SinvieUIConstants.LIVE_ROOM_MENU_CLOSE
                , SinvieUIConstants.LIVE_ROOM_MENU_SWITCH_CAMERA
//                , UIConstants.LIVE_ROOM_MENU_SCENES_HORIZONTAL
//                , UIConstants.LIVE_ROOM_MENU_NOT_LOCK
        };
        for (int i = 0; i < img.length; i++) {
            mSinvieLiveMenuBeans.add(new SinvieLiveMenuBean(id[i], img[i]));

        }

        return mSinvieLiveMenuBeans;
    }

    /**
     * 初始化密码弹窗 dialog FragmentManager
     *
     * @param fragmentManager
     */
    private void initPwdDialog(FragmentManager fragmentManager) {
        mFragmentManager = fragmentManager;
    }

    private void setLiveRoomMenu(int id, int position) {
        if (mPreviewCallback == null) {
            return;
        }
        //判断 右侧按钮item
        if (id == SinvieUIConstants.LIVE_ROOM_MENU_CLOSE) {
            mPreviewCallback.onClose();
        } else if (id == SinvieUIConstants.LIVE_ROOM_MENU_SWITCH_CAMERA) {
            mPreviewCallback.onSwitchCamera();
        } else if (id == SinvieUIConstants.LIVE_ROOM_MENU_SCENES_HORIZONTAL) {
            mPreviewCallback.onSceneOrientation();
        } else if (id == SinvieUIConstants.LIVE_ROOM_MENU_NOT_LOCK) {
            if (mFragmentManager != null) {
                //弹出密码框
                mSinvieLiveRoomPwdDialog = new SinviePwdDialog();
                mSinvieLiveRoomPwdDialog.setOnLiveRoomPwdListener(pwd -> {
                    //返回密码
                    //更改list 数据 刷新列表 未锁->锁
                    mSinvieLiveMenuBeans.get(position).setId(SinvieUIConstants.LIVE_ROOM_MENU_LOCK);
                    mSinvieLiveMenuBeans.get(position).setImage(R.mipmap.icon_sinvie_lock_screen);
                    mPreviewCallback.onLockLiveRoom(true, pwd);
                    //刷新列表
                    mRoomMenuAdapter.notifyDataSetChanged();
                });
                mSinvieLiveRoomPwdDialog.show(mFragmentManager, "live_room_pwd_dialog");
            }
        } else if (id == SinvieUIConstants.LIVE_ROOM_MENU_LOCK) {
            //更改list 数据 刷新列表 锁->未锁
            mSinvieLiveMenuBeans.get(position).setId(SinvieUIConstants.LIVE_ROOM_MENU_NOT_LOCK);
            mSinvieLiveMenuBeans.get(position).setImage(R.mipmap.icon_sinvie_not_lock_screen);
            mRoomMenuAdapter.notifyDataSetChanged();
            mPreviewCallback.onLockLiveRoom(false, null);

        }
    }

    /**
     * 设置直播右侧菜单
     *
     * @param sinvieLiveMenuBeans
     */
    public void setLiveMenuBeans(List<SinvieLiveMenuBean> sinvieLiveMenuBeans) {
        mSinvieLiveMenuBeans = sinvieLiveMenuBeans;
        mRoomMenuAdapter.notifyDataSetChanged();
    }

    /**
     * 设置回调
     *
     * @param previewCallback
     */
    public void setPreviewCallback(CustomPreviewCallback previewCallback) {
        mPreviewCallback = previewCallback;
    }

    /**
     * 设置封面
     */
    public void setLiveRoomCover(String path) {
        //设置封面字体tv gone
        mTvLiveCover.setVisibility(GONE);
        mLiveCoverPath = path;
        Glide.with(mContext).load(mLiveCoverPath).into(mIvLiveCover);
    }

    /**
     * 设置room num
     */
    public void setRoomId(String roomId) {
        mTvLiveRoomId.setText(String.format(mContext.getString(R.string.sinvie_room_id), roomId));
    }

    /**
     * 显示实时网速
     *
     * @param netSpeed 网速
     */
    public void setLiveNetSpeed(String netSpeed) {
        mTvLiveNet.setText(netSpeed);
    }

    /**
     * 设置直播时长
     *
     * @param time
     */
    public void setLiveTime(String time) {
        mTvLiveTime.setText(time);
    }

    /**
     * 开始直播 隐藏 bottom view , 隐藏 type view 禁止更改标题 , 禁止更改头像 ,显示房间号,显示实时网速,显示直播计时
     */
    public void startLive() {
        //隐藏 开启直播 btn
        mTvStartNewLive.setVisibility(GONE);
        //隐藏 type btn
        mLlSelectLiveType.setVisibility(GONE);
        //禁止更改标题
        mEdLiveTitle.setEnabled(false);
        //禁止更改头像
        mIvLiveCover.setOnClickListener(null);
        //显示房间号 view
        mTvLiveRoomId.setVisibility(VISIBLE);
        //显示实时网速 view
        mLlLiveNet.setVisibility(VISIBLE);
        //显示直播计时 view
        mTvLiveTime.setVisibility(VISIBLE);

    }

    public void addAudienceListUser(List<SinvieTRTCLiveRoomDef.TRTCLiveUserInfo> list) {

    }

    public void addAudienceListUser(SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo) {

    }

    public void removeAudienceUser(SinvieTRTCLiveRoomDef.TRTCLiveUserInfo userInfo) {

    }

    /**
     * 设置按钮显示隐藏
     *
     * @param visibility
     */
    private void setBottomViewVisibility(int visibility) {
        mTvStartNewLive.setVisibility(visibility);
    }


    /**
     * 直播间 右侧菜单 adapter
     */
    public interface CustomPreviewCallback {
        /**
         * 调转摄像头
         */
        void onSwitchCamera();

        /**
         * 选择/切换封面
         */
        void onSelectCover();

        /**
         * 更改屏幕方向
         */
        void onSceneOrientation();

        /**
         * 直播间是否上锁
         *
         * @param isLock true: 上锁, false: 没有锁
         * @param pwd    上锁之后的密码
         */
        void onLockLiveRoom(boolean isLock, String pwd);

        /**
         * 开启直播
         *
         * @param roomName         直播间名称(标题)
         * @param audioQualityType 直播间状态
         */
        void onStartLive(String roomName, int audioQualityType);

        /**
         * 关闭直播间
         */
        void onClose();
    }
}
