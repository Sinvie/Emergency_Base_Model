package com.sinvie.emergency_base.ui;

/**
 * @author: JonsonBob
 * @date: 2021/6/25
 * @Des: 隐式跳转
 */
public class SinvieIntentConstant {
    public static final String DEFAULT_CATEGORY = "android.intent.category.sdkdemo";

    public static final String MAIN_ACTIVITY_ACTION = "android.intent.action.sinvie_main_activity";
    public static final String LOGIN_FOR_DEV_ACTIVITY_ACTION = "android.intent.action.login_for_dev_activity";
    public static final String LOGIN_SETTING_ACTIVITY_ACTION = "android.intent.action.setting_login_activity";
    public static final String REPORT_ACTIVITY_ACTION = "android.intent.action.report_activity";
    public static final String REPORT_DETAIL_ACTIVITY_ACTION = "android.intent.action.report_detail_activity";
    public static final String ADD_NEW_REPORT_ACTIVITY_ACTION = "android.intent.action.add_new_report_activity";
    public static final String ADD_REPORT_DETAIL_ACTIVITY_ACTION = "android.intent.action.add_report_detail_activity";
    public static final String SELECT_ADDRESS_ACTIVITY_ACTION ="android.intent.action.select_address_activity";
    public static final String VIDEO_PLAY_ACTIVITY_ACTION = "android.intent.action.video_play_activity";
    public static final String REPORT_MANAGER_ACTIVITY_ACTION = "android.intent.action.report_manager_activity";
    public static final String REPORT_MANAGER_DETAIL_ACTIVITY_ACTION = "android.intent.action.report_manager_detail_activity";

    //=================================腾讯IM 实时音视频================================================/
    public static final String CHAT_ACTIVITY_ACTION = "android.intent.action.chat_activity";
    public static final String ADD_MORE_ACTIVITY_ACTION = "android.intent.action.add_more_activity";
    public static final String START_C2C_CHAT_ACTIVITY_ACTION = "android.intent.action.start_c2c_chat_activity";
    public static final String START_GROUP_CHAT_ACTIVITY_ACTION =   "android.intent.action.start_group_chat_activity";

    /*=================================直播界面========================================*/
    public static final String LIVE_ROOM_ANCHOR_ACTIVITY_ACTION = "android.intent.action.live_room_anchor_activity";
    public static final String LIVE_ROOM_AUDIENCE_ACTIVITY_ACTION = "android.intent.action.live_room_audience_activity";

    /*==================================会议界面=======================================*/
    public static final String CONFERENCE_ACTIVITY_ACTION = "android.intent.action.huawei_conference_activity";
    public static final String CONF_MANAGER_ACTIVITY_ACTION = "android.intent.action.huawei_conf_manager_activity";
    public static final String CALL_OUT_ACTIVITY_ACTION = "android.intent.action.huawei_call_out_activity";
    public static final String CALL_IN_ACTIVITY_ACTION = "android.intent.action.huawei_call_in_activity";
    public static final String VIDEO_ACTIVITY_ACTION = "android.intent.action.huawei_video_activity";
    public static final String CONF_MEMBER_LIST_ACTIVITY_ACTION = "android.intent.action.huawei_attendee_list_activity";
    public static final String CONF_DATA_ACTIVITY_ACTION = "android.intent.action.huawei_conf_data_activity";

    public static final String CALL_QUALITY_ACTIVITY_ACTION = "android.intent.action.huawei_call_quality_activity";

    // 电子地图
    public static final String ELECTRONIC_MAP_ACTIVITY_ACTION = "android.intent.action.electronic_map";
    // 协作标绘
    public static final String COOPERATE_ACTIVITY_ACTION = "android.intent.action.cooperate_main";
    // 响应通告
    public static final String RESPONSE_ACTIVITY_ACTION = "android.intent.action.response";
    // 通知公告
    public static final String ANNOUNCE_ACTIVITY_ACTION = "android.intent.action.announce";
}
