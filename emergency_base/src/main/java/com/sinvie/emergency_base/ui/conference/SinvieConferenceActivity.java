package com.sinvie.emergency_base.ui.conference;

import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.utils.widget.ImageFilterView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshLoadMoreListener;
import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_conf.ConferenceService.SinvieConfBaseInfo;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.logic.conference.SinvieConfBasePresenter;
import com.sinvie.emergency_base.logic.conference.SinvieConferenceContact;
import com.sinvie.emergency_base.logic.conference.SinvieConfPresenter;
import com.sinvie.emergency_live.tuikit.base.ISinvieTitleBarLayout;
import com.sinvie.emergency_live.tuikit.utils.SinvieToastUtil;
import com.sinvie.emergency_base.ui.base.SinvieBaseActivity;
import com.sinvie.emergency_base.ui.conference.adapter.SinvieConferenceRvAdapter;
import com.sinvie.emergency_base.widget.SinvieCustomTitleBarLayout;
import com.sinvie.emergency_base.widget.SinvieWorkbenchItemDecoration;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class SinvieConferenceActivity extends SinvieBaseActivity implements SinvieConferenceContact.ConferenceSinvieBaseView {

    private SinvieCustomTitleBarLayout mTbConference;
    private EditText mEtConferenceSearch;
    private LinearLayoutCompat mLlConferenceStatus;
    private ImageFilterView mIvConferenceStatus;
    private TextView mTvConferenceStatus;
    private RecyclerView mRvConference;

    private SinvieConferenceRvAdapter mRvAdapter;
    private List<SinvieConfBaseInfo> mSinvieConfBaseInfoList;
    private SmartRefreshLayout mSmartConference;
    private int mPageIndex = 1;

    private SinvieConfBasePresenter mPresenter;

    private static String mConfId;

//    private String[] broadcastNames = new String[]{
//            CustomBroadcastConstants.GET_CONF_LIST_FAILED_RESULT,
//            CustomBroadcastConstants.GET_CONF_LIST_RESULT,
//            CustomBroadcastConstants.CANCEL_CONF_MSG};
//
//    private LocBroadcastReceiver receiver = new LocBroadcastReceiver() {
//        @Override
//        public void onReceive(String broadcastName, Object obj) {
//            mPresenter.receiveBroadcast(broadcastName, obj);
//        }
//    };

    @Override
    public void initializeView() {

    }

    @Override
    public void initializeData() {
        setContentView(R.layout.activity_sinvie_conference);
        mPresenter = new SinvieConfPresenter(this);
        mPresenter.attachView(this);
    }

    @Override
    public void initializeComposition() {
        mTbConference = findViewById(R.id.tb_conference);
        mEtConferenceSearch = findViewById(R.id.et_search);
        mLlConferenceStatus = findViewById(R.id.ll_conference_status);
        mIvConferenceStatus = findViewById(R.id.iv_conference_status);
        mTvConferenceStatus = findViewById(R.id.tv_conference_status);
        mRvConference = findViewById(R.id.rv_conference);
        mSmartConference = findViewById(R.id.smart_conference);

        mTbConference.getRightIcon().setVisibility(View.GONE);
        mTbConference.setLeftIcon(R.drawable.picture_icon_back_arrow);
        mTbConference.setTitle(getString(R.string.sinvie_conference_title), ISinvieTitleBarLayout.POSITION.MIDDLE);

        mEtConferenceSearch.setHint(getString(R.string.sinvie_conference_search_hint));

        //监听输入法回车搜索
        mEtConferenceSearch.setOnKeyListener((v, keyCode, event) -> {

            //是否是回车键
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN) {
                //隐藏键盘
                ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                        .hideSoftInputFromWindow(SinvieConferenceActivity.this.getCurrentFocus()
                                .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

            }
            mPageIndex = 1;
            //清除旧数据
            mSinvieConfBaseInfoList.clear();
            //获取数据
            mPresenter.getConferenceList(mPageIndex);
            return false;
        });

        mSinvieConfBaseInfoList = new ArrayList<>();

        mRvAdapter = new SinvieConferenceRvAdapter(this, mSinvieConfBaseInfoList);
        mRvAdapter.setItemConferenceRvClick((position, bean) -> {
            //item 单击
        });
        mRvConference.setLayoutManager(new LinearLayoutManager(this));
        mRvConference.addItemDecoration(new SinvieWorkbenchItemDecoration(this,
                R.drawable.icon_sinvie_item_decoration_conference));
        mRvConference.setAdapter(mRvAdapter);

        mSmartConference.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull @NotNull RefreshLayout refreshLayout) {

                mPageIndex += 1;
                //获取数据
                mPresenter.getConferenceList(mPageIndex);
            }

            @Override
            public void onRefresh(@NonNull @NotNull RefreshLayout refreshLayout) {
                mPageIndex = 0;
                //清除旧数据
                mSinvieConfBaseInfoList.clear();
                //获取数据
                mPresenter.getConferenceList(mPageIndex);
            }
        });

//        LocBroadcast.getInstance().registerBroadcast(receiver, broadcastNames);
        mPresenter.registerBroadcast();
        mSmartConference.autoRefresh();
    }

    @Override
    public void setActivityDestroy() {
        mPresenter.detachView();
    }

    @Override
    public void refreshConfList(List<SinvieConfBaseInfo> tupConfInfoList) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Toast.makeText(SinvieLocContext.getContext(), "query conf list success.",  Toast.LENGTH_SHORT).show();
                mSinvieConfBaseInfoList.addAll(tupConfInfoList);
                mRvAdapter.notifyDataSetChanged();
                //判断能否上拉加载
                mSmartConference.setEnableLoadMore(tupConfInfoList.size() >= SinvieUIConstants.PAGE_SIZE);
                mSmartConference.finishLoadMore();
                mSmartConference.finishRefresh();
            }
        });
    }

    @Override
    public void refreshConfListEmpty() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //判断能否上拉加载
                mSmartConference.setEnableLoadMore(false);
                mSmartConference.finishLoadMore();
                mSmartConference.finishRefresh();
            }
        });
    }

    @Override
    public void joinConfSuccess(String confID) {
       runOnUiThread(new Runnable() {
           @Override
           public void run() {
               mConfId = confID;
               //改变会议状态
               mLlConferenceStatus.setVisibility(View.VISIBLE);
               Glide.with(SinvieConferenceActivity.this).load(R.drawable.icon_sinvie_conference_status_doing).into(mIvConferenceStatus);
               mTvConferenceStatus.setText(String.format(getString(R.string.sinvie_meeting_doing),confID));
               mTvConferenceStatus.setTextColor(getResources().getColor(R.color.sinvie_theme_green_color));
           }
       });

    }

    @Override
    public void leaveConf() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //改变会议状态
                mLlConferenceStatus.setVisibility(View.VISIBLE);
                Glide.with(SinvieConferenceActivity.this).load(R.drawable.icon_sinvie_conference_status_fail).into(mIvConferenceStatus);
                mTvConferenceStatus.setText(R.string.sinvie_meeting_leave);
                mTvConferenceStatus.setTextColor(getResources().getColor(R.color.sinvie_theme_reject_color));
            }
        });
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void dismissLoading() {

    }

    @Override
    public void showCustomToast(int resID) {
        SinvieToastUtil.toastLongMessage(getString(resID));
    }

    @Override
    protected void onDestroy() {
        mPresenter.unregisterBroadcast();
        super.onDestroy();
    }
}