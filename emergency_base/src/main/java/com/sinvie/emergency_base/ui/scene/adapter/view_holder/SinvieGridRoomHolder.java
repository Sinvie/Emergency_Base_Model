package com.sinvie.emergency_base.ui.scene.adapter.view_holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.ui.scene.adapter.SinvieRoomDifficultListAdapter;
import com.sinvie.emergency_base.utils.SinvieClickUtils;

/**
 * @author: JonsonBob
 * @date: 2021/6/28
 * @Des: 类描述
 */
public class SinvieGridRoomHolder extends SinvieBaseViewHolder {
    //直播封面
    private ImageView mIvRoomCover;
    //直播房间锁
    private ImageView mIvRoomLock;
    //直播类型
    private TextView mTvRoomType;
    //直播房间号
    private TextView mTvRoomNum;
    //直播标题
    private TextView mTvRoomTitle;

    public SinvieGridRoomHolder(View itemView) {
        super(itemView);
        initView(itemView);
    }

    private void initView(@NonNull final View itemView) {
        //直播封面
        mIvRoomCover = itemView.findViewById(R.id.iv_item_live_room_grid_room);
        //直播房间锁
        mIvRoomLock = itemView.findViewById(R.id.iv_item_live_room_grid_lock);
        //直播类型
        mTvRoomType = itemView.findViewById(R.id.tv_item_live_room_grid_room_type);
        //直播房间号
        mTvRoomNum = itemView.findViewById(R.id.tv_item_live_room_grid_num);
        //直播标题
        mTvRoomTitle = itemView.findViewById(R.id.tv_item_live_room_grid_title);
    }
    @Override
    public void bind(Context context, SinvieRoomDifficultListAdapter.ScenesRoomInfo roomInfo,
                     SinvieRoomDifficultListAdapter.OnItemClickListener listener) {

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SinvieClickUtils.isFastClick(v.getId())) {
                    listener.onItemClick(getLayoutPosition(), roomInfo);
                }
            }
        });
    }
}
