package com.sinvie.emergency_base.ui.login;

import static com.sinvie.emergency_base.common.SinvieUIConstants.LOGIN_TYPE;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.logic.login.SinvieLoginFragToActListener;
import com.sinvie.emergency_base.ui.base.SinvieAbsFragment;
import com.sinvie.emergency_base.utils.SinvieClickUtils;
import com.sinvie.emergency_base.widget.SinvieLoginTextWatcher;
import com.sinvie.emergency_live.tuikit.utils.SinvieToastUtil;

/**
 * @param
 * @author JonsonBob
 * @description 登录方式Fragment
 * @return
 * @time 2021/6/30 9:11
 */
public class SinvieLoginWayFragment extends SinvieAbsFragment {

    /**
     * 倒计时总长
     */
    private static final int COUNT_DOWN_TIMER = 60 * 1000;
    private static final int COUNT_TIME_INTERVAL = 1000;
    /**
     * 登录方式不同 0:手机验证码登录, 1:账号密码登录
     */
    private int mLoginType;
    /**
     * 判断loginType = 0  的条件
     */
    private boolean mInputPhone = false;
    private boolean mInputCode = false;
    /**
     * 判断loginType = 1  的条件
     */
    private boolean mInputAccount = false;
    private boolean mInputPwd = false;

    /**
     * code 登录条件
     */
    private EditText mEtLoginPhone;
    private EditText mEtLoginCode;
    private TextView mTvGetLoginCodeBtn;
    /**
     * 密码 登录条件
     */
    private EditText mEtLoginAccount;
    private EditText mEtLoginPwd;


    private CountDownTimer mCountDownTimer;

    private SinvieLoginFragToActListener mListener;

    public SinvieLoginWayFragment() {
        // Required empty public constructor
    }

    public static SinvieLoginWayFragment newInstance(int type) {
        SinvieLoginWayFragment fragment = new SinvieLoginWayFragment();
        Bundle args = new Bundle();
        args.putInt(LOGIN_TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }

    public void setListener(SinvieLoginFragToActListener listener) {
        mListener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mLoginType = getArguments().getInt(LOGIN_TYPE, 0);
        }
    }

    @Override
    public int getLayoutId() {
        if (mLoginType == 0) {
            return R.layout.fragment_sinvie_login_way_code;
        } else {
            return R.layout.fragment_sinvie_login_way_pwd;
        }
    }

    @Override
    public void onViewLoad() {
        if (mLoginType == 0) {
            initCodeView();
        } else {
            initPwdView();
        }

        //登录按钮解封逻辑
        registerTextChanged();

        //自动填充上次登录密码
        if (SinvieUserInfo.getInstance().getAccount() != null && mEtLoginAccount != null) {
            mEtLoginAccount.setText(SinvieUserInfo.getInstance().getAccount());
        }
        if (SinvieUserInfo.getInstance().getPwd() != null && mEtLoginPwd != null) {
            mEtLoginPwd.setText(SinvieUserInfo.getInstance().getPwd());
        }
    }

    /**
     * 初始化 验证码 界面view
     */
    private void initCodeView() {
        mEtLoginPhone = mView.findViewById(R.id.et_login_phone);
        mTvGetLoginCodeBtn = mView.findViewById(R.id.tv_get_login_code_btn);
        mEtLoginCode = mView.findViewById(R.id.et_login_code);

        //自动填充上次登录密码
        if (SinvieUserInfo.getInstance().getAccount() != null && mEtLoginAccount != null) {
            mEtLoginPhone.setText(SinvieUserInfo.getInstance().getAccount());
        }

        mTvGetLoginCodeBtn.setOnClickListener(v -> {
            if (SinvieClickUtils.isFastClick(v.getId())) {
                return;
            }
            //判断条件
            if (TextUtils.isEmpty(mEtLoginPhone.getText().toString().trim())) {
                SinvieToastUtil.toastLongMessage(context.getString(R.string.sinvie_account_information_not_empty));
                return;
            }

            initCountDown();

            //改变发送按钮状态
            mTvGetLoginCodeBtn.setEnabled(false);

            mListener.onSentCode();
        });
    }

    /**
     * 初始化 密码 界面view
     */
    private void initPwdView() {
        mEtLoginAccount = mView.findViewById(R.id.et_login_account);
        mEtLoginPwd = mView.findViewById(R.id.et_login_pwd);

    }

    /**
     * 判断输入信息是否合格登录按钮是否能够解封
     */
    private void registerTextChanged() {
        if (mLoginType == 0) {
            //验证码登录
            mEtLoginPhone.addTextChangedListener(new SinvieLoginTextWatcher(mLoginType, 0) {
                @Override
                public void checkInputCondition(boolean open) {
                    mInputPhone = open;
                    openLoginBtnLock();
                }
            });
            mEtLoginCode.addTextChangedListener(new SinvieLoginTextWatcher(mLoginType, 1) {
                @Override
                public void checkInputCondition(boolean open) {
                    mInputCode = open;
                    openLoginBtnLock();
                }
            });
        } else {
            //密码登录
            mEtLoginAccount.addTextChangedListener(new SinvieLoginTextWatcher(mLoginType, 2) {
                @Override
                public void checkInputCondition(boolean open) {
                    mInputAccount = open;
                    openLoginBtnLock();
                }
            });
            mEtLoginPwd.addTextChangedListener(new SinvieLoginTextWatcher(mLoginType, 3) {
                @Override
                public void checkInputCondition(boolean open) {
                    mInputPwd = open;
                    openLoginBtnLock();
                }
            });
        }
    }

    private void openLoginBtnLock() {
        if (mLoginType == 0) {
            if (mListener != null) {
                mListener.onLockLoginBtn(mInputCode && mInputPhone);
            }
        } else {
            if (mListener != null) {
                mListener.onLockLoginBtn(mInputAccount && mInputPwd);
            }
        }
    }

    /**
     * activity 中获取 fragment 中的 登录名/手机号
     *
     * @return
     */
    public String getAccount() {
        if (mLoginType == 0) {
            return mEtLoginPhone.getText().toString().trim();
        } else {
            return mEtLoginAccount.getText().toString().trim();
        }
    }

    /**
     * activity 中获取fragment中的 密码/验证码
     *
     * @return
     */
    public String getLoginCode() {
        if (mLoginType == 0) {
            return mEtLoginCode.getText().toString().trim();

        } else {
            return mEtLoginPwd.getText().toString().trim();
        }
    }

    private void initCountDown() {
        mCountDownTimer = new CountDownTimer(COUNT_DOWN_TIMER, COUNT_TIME_INTERVAL) {
            @Override
            public void onTick(long millisUntilFinished) {
                //倒计时每秒显示
                mTvGetLoginCodeBtn.setText(String.format(context.getResources().getString(R.string.sinvie_login_sent_code), millisUntilFinished / COUNT_TIME_INTERVAL));
            }

            @Override
            public void onFinish() {
                //恢复发送按钮状态
                mTvGetLoginCodeBtn.setEnabled(true);
                mTvGetLoginCodeBtn.setText(context.getString(R.string.sinvie_login_re_sent_code));
                mTvGetLoginCodeBtn.setTextColor(ContextCompat.getColor(context, R.color.sinvie_theme_reject_color));
            }
        }.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mCountDownTimer != null) {
            mCountDownTimer.onFinish();
            mCountDownTimer.cancel();
            mCountDownTimer = null;
        }
    }
}