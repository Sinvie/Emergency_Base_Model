package com.sinvie.emergency_base.ui.scene;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.ui.base.SinvieAbsFragment;
import com.sinvie.emergency_base.ui.scene.view.SinvieScenesLayout;

/**
 * @description 直播屏幕显示Fragment
 * @param
 * @return
 * @author JonsonBob
 * @time 2021/6/28 11:18
 */
public class SinvieSceneFragment extends SinvieAbsFragment {

    @Override
    public int getLayoutId() {
        return R.layout.fragment_sinvie_scene;
    }


    @Override
    public void onViewLoad() {
        SinvieScenesLayout sinvieScenesLayout = mView.findViewById(R.id.scenes_layout);
        sinvieScenesLayout.setFragmentManager(getChildFragmentManager());
    }
}