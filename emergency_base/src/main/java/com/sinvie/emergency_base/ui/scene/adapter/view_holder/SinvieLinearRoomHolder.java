package com.sinvie.emergency_base.ui.scene.adapter.view_holder;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.ui.scene.adapter.SinvieRoomDifficultListAdapter;
import com.sinvie.emergency_base.utils.SinvieClickUtils;
import com.sinvie.emergency_base.utils.SinvieImageUtils;

/**
 * @author: JonsonBob
 * @date: 2021/6/28
 * @Des: 线性列表 直播间 ViewHolder
 */
public class SinvieLinearRoomHolder extends SinvieBaseViewHolder {

    //直播点击
    private ConstraintLayout mConstraintLayout;
    //直播封面
    private ImageView mIvRoomCover;
    //直播房间锁
    private ImageView mIvRoomLock;
    //直播类型
    private TextView mTvRoomType;
    //直播房间号
    private TextView mTvRoomNum;
    //直播标题
    private TextView mTvRoomTitle;

    public SinvieLinearRoomHolder(View itemView) {
        super(itemView);
        initView(itemView);
    }

    private void initView(@NonNull final View itemView) {
        //直播点击
        mConstraintLayout = itemView.findViewById(R.id.cl_item_live_room_linear_room);
        //直播封面
        mIvRoomCover = itemView.findViewById(R.id.iv_item_live_room_linear_cover);
        //直播房间锁
        mIvRoomLock = itemView.findViewById(R.id.iv_item_live_room_linear_lock);
        //直播类型
        mTvRoomType = itemView.findViewById(R.id.tv_item_live_room_linear_room_type);
        //直播房间号
        mTvRoomNum = itemView.findViewById(R.id.tv_item_live_room_linear_num);
        //直播标题
        mTvRoomTitle = itemView.findViewById(R.id.tv_item_live_room_linear_title);
    }

    @Override
    public void bind(Context context, SinvieRoomDifficultListAdapter.ScenesRoomInfo roomInfo,
                     SinvieRoomDifficultListAdapter.OnItemClickListener listener) {
        //直播房间锁
        if (roomInfo.isLock) {
            //显示直播被锁
            mIvRoomLock.setVisibility(View.VISIBLE);
            //TODO 直播封面 最好有一张占位符或者未加载图片
            Glide.with(context).load(roomInfo.coverUrl).into(mIvRoomCover);
            //高斯模糊
            Bitmap bitmap = SinvieImageUtils.fastblur(context,
                    ((BitmapDrawable) mIvRoomCover.getDrawable()).getBitmap(), 5);
            //重新加载一下图片
            Glide.with(context).load(bitmap).error(R.mipmap.icon_sinvie_error).into(mIvRoomCover);
        } else {
            mIvRoomLock.setVisibility(View.GONE);
            Glide.with(context).load(roomInfo.coverUrl).error(R.mipmap.icon_sinvie_error).into(mIvRoomCover);
        }
        //直播类型 0:现场 , 1:任务
        if (roomInfo.roomType == 0) {
            mTvRoomType.setText(R.string.sinvie_room_type_site);
        } else {
            mTvRoomType.setText(R.string.sinvie_room_type_task);
        }
        //直播房间号
//        mTvRoomNum.setText(roomInfo.roomId);
        mTvRoomNum.setText(String.format(context.getString(R.string.sinvie_room_id),"777777"));
        //直播标题
//        mTvRoomTitle.setText(roomInfo.roomName);
        mTvRoomTitle.setText("红荔社区-受灾害群众疏散人群离开现场");
        //item 点击
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SinvieClickUtils.isFastClick(v.getId())) {
                    listener.onItemClick(getLayoutPosition(), roomInfo);
                }
            }
        });
    }
}
