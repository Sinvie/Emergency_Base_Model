package com.sinvie.emergency_base.ui.menu;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.logic.strat_group_chat.SinvieStartGroupChatContact;
import com.sinvie.emergency_base.logic.strat_group_chat.SinvieStartGroupChatPresenter;
import com.sinvie.emergency_base.ui.base.SinvieMVPBaseActivity;

public class SinvieStartGroupChatActivity extends SinvieMVPBaseActivity<SinvieStartGroupChatContact.StartGroupChatBaseView, SinvieStartGroupChatPresenter>
        implements SinvieStartGroupChatContact.StartGroupChatBaseView {

    @Override
    public void initializeComposition() {
        setContentView(R.layout.activity_sinvie_start_group_chat);
    }

    @Override
    public void initializeData() {

    }

    @Override
    protected SinvieStartGroupChatContact.StartGroupChatBaseView createView() {
        return this;
    }

    @Override
    protected SinvieStartGroupChatPresenter createPresenter() {
        return new SinvieStartGroupChatPresenter(this);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}