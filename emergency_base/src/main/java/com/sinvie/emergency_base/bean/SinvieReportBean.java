package com.sinvie.emergency_base.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/11
 * @Des: 类描述
 */
public class SinvieReportBean {


    private List<DataRoomListBean> data;
    private int totalElements;
    private int totalPages;

    public List<DataRoomListBean> getData() {
        return data;
    }

    public void setData(List<DataRoomListBean> data) {
        this.data = data;
    }

    public int getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(int totalElements) {
        this.totalElements = totalElements;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public static class DataRoomListBean implements Parcelable {
        private String address;
        private List<SinvieAppAttachmentsBean> appAttachments;
        private int caseClassId;
        private String caseClassName;
        private int contactorId;
        private String content;
        private int createUserId;
        private int eventId;
        private int id;
        private int isDeleted;
        private String latitude;
        private String longitude;
        private int platformId;
        private String reportTime;
        private String title;
        private int updateUserId;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public List<SinvieAppAttachmentsBean> getAppAttachments() {
            return appAttachments;
        }

        public void setAppAttachments(List<SinvieAppAttachmentsBean> appAttachments) {
            this.appAttachments = appAttachments;
        }

        public int getCaseClassId() {
            return caseClassId;
        }

        public void setCaseClassId(int caseClassId) {
            this.caseClassId = caseClassId;
        }

        public String getCaseClassName() {
            return caseClassName;
        }

        public void setCaseClassName(String caseClassName) {
            this.caseClassName = caseClassName;
        }

        public int getContactorId() {
            return contactorId;
        }

        public void setContactorId(int contactorId) {
            this.contactorId = contactorId;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public int getCreateUserId() {
            return createUserId;
        }

        public void setCreateUserId(int createUserId) {
            this.createUserId = createUserId;
        }

        public int getEventId() {
            return eventId;
        }

        public void setEventId(int eventId) {
            this.eventId = eventId;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(int isDeleted) {
            this.isDeleted = isDeleted;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public int getPlatformId() {
            return platformId;
        }

        public void setPlatformId(int platformId) {
            this.platformId = platformId;
        }


        public String getReportTime() {
            return reportTime;
        }

        public void setReportTime(String reportTime) {
            this.reportTime = reportTime;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getUpdateUserId() {
            return updateUserId;
        }

        public void setUpdateUserId(int updateUserId) {
            this.updateUserId = updateUserId;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.address);
            dest.writeTypedList(this.appAttachments);
            dest.writeInt(this.caseClassId);
            dest.writeString(this.caseClassName);
            dest.writeInt(this.contactorId);
            dest.writeString(this.content);
            dest.writeInt(this.createUserId);
            dest.writeInt(this.eventId);
            dest.writeInt(this.id);
            dest.writeInt(this.isDeleted);
            dest.writeString(this.latitude);
            dest.writeString(this.longitude);
            dest.writeInt(this.platformId);
            dest.writeString(this.reportTime);
            dest.writeString(this.title);
            dest.writeInt(this.updateUserId);
        }

        public void readFromParcel(Parcel source) {
            this.address = source.readString();
            this.appAttachments = source.createTypedArrayList(SinvieAppAttachmentsBean.CREATOR);
            this.caseClassId = source.readInt();
            this.caseClassName = source.readString();
            this.contactorId = source.readInt();
            this.content = source.readString();
            this.createUserId = source.readInt();
            this.eventId = source.readInt();
            this.id = source.readInt();
            this.isDeleted = source.readInt();
            this.latitude = source.readString();
            this.longitude = source.readString();
            this.platformId = source.readInt();
            this.reportTime = source.readString();
            this.title = source.readString();
            this.updateUserId = source.readInt();
        }

        public DataRoomListBean() {
        }

        protected DataRoomListBean(Parcel in) {
            this.address = in.readString();
            this.appAttachments = in.createTypedArrayList(SinvieAppAttachmentsBean.CREATOR);
            this.caseClassId = in.readInt();
            this.caseClassName = in.readString();
            this.contactorId = in.readInt();
            this.content = in.readString();
            this.createUserId = in.readInt();
            this.eventId = in.readInt();
            this.id = in.readInt();
            this.isDeleted = in.readInt();
            this.latitude = in.readString();
            this.longitude = in.readString();
            this.platformId = in.readInt();
            this.reportTime = in.readString();
            this.title = in.readString();
            this.updateUserId = in.readInt();
        }

        public static final Parcelable.Creator<DataRoomListBean> CREATOR = new Parcelable.Creator<DataRoomListBean>() {
            @Override
            public DataRoomListBean createFromParcel(Parcel source) {
                return new DataRoomListBean(source);
            }

            @Override
            public DataRoomListBean[] newArray(int size) {
                return new DataRoomListBean[size];
            }
        };
    }
}
