package com.sinvie.emergency_base.bean;

/**
 * @author: JonsonBob
 * @date: 2021/7/5
 * @Des: 类描述
 */
public class SinvieRoomBean {

    private int eventId;
    private int id;
    private String liveAddress;
    private Object liveEndTime;
    private String liveRoomTitle;
    private String liveStartTime;
    private String liveStatus;
    private String liveType;
    private String roomId;
    private UserVoRoomListBean userVo;

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLiveAddress() {
        return liveAddress;
    }

    public void setLiveAddress(String liveAddress) {
        this.liveAddress = liveAddress;
    }

    public Object getLiveEndTime() {
        return liveEndTime;
    }

    public void setLiveEndTime(Object liveEndTime) {
        this.liveEndTime = liveEndTime;
    }

    public String getLiveRoomTitle() {
        return liveRoomTitle;
    }

    public void setLiveRoomTitle(String liveRoomTitle) {
        this.liveRoomTitle = liveRoomTitle;
    }

    public String getLiveStartTime() {
        return liveStartTime;
    }

    public void setLiveStartTime(String liveStartTime) {
        this.liveStartTime = liveStartTime;
    }

    public String getLiveStatus() {
        return liveStatus;
    }

    public void setLiveStatus(String liveStatus) {
        this.liveStatus = liveStatus;
    }

    public String getLiveType() {
        return liveType;
    }

    public void setLiveType(String liveType) {
        this.liveType = liveType;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public UserVoRoomListBean getUserVo() {
        return userVo;
    }

    public void setUserVo(UserVoRoomListBean userVo) {
        this.userVo = userVo;
    }

    public static class UserVoRoomListBean {
        private String department;
        private String name;
        private String roleName;
        private int userId;
        private String username;

        public String getDepartment() {
            return department;
        }

        public void setDepartment(String department) {
            this.department = department;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getRoleName() {
            return roleName;
        }

        public void setRoleName(String roleName) {
            this.roleName = roleName;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }
}
