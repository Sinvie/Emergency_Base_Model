package com.sinvie.emergency_base.bean;

/**
 * @author: JonsonBob
 * @date: 2021/7/12
 * @Des: 类描述
 */
public class SinvieSaveFileBean {

    private String fileContentType;
    private int id;
    private int size;
    private long sysDtCreate;
    private long sysDtLastUpdate;
    private int sysIStatus;
    private String filename;
    private String path;
    private String title;
    private String type;
    private String url;

    public String getFileContentType() {
        return fileContentType;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public long getSysDtCreate() {
        return sysDtCreate;
    }

    public void setSysDtCreate(long sysDtCreate) {
        this.sysDtCreate = sysDtCreate;
    }

    public long getSysDtLastUpdate() {
        return sysDtLastUpdate;
    }

    public void setSysDtLastUpdate(long sysDtLastUpdate) {
        this.sysDtLastUpdate = sysDtLastUpdate;
    }

    public int getSysIStatus() {
        return sysIStatus;
    }

    public void setSysIStatus(int sysIStatus) {
        this.sysIStatus = sysIStatus;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
