package com.sinvie.emergency_base.bean.electronic

data class ProtectRep<T>(
    val msg: String? = null,
    val code: Int,
    val ResoureProtectTargetEntity: T
)

data class ProtectBean(
    val id: Int,
    val name: String,
    val protectType: String,
    val protectTypeParentName: String,
    val location: String,
    val longitude: Float,
    val latitude: Float,
    val level: String,
    val protectLevel: String,
    val riskReason: String,
    val trafficConditions: String,
    val generalSituation: String,
    val platformId: Int,
    val createUserId: Int,
    val gmtCreate: String,
    val gmtModified: String,
    val deleted: Int
)