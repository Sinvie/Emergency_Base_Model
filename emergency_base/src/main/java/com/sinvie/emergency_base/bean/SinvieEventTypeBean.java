package com.sinvie.emergency_base.bean;

import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/12
 * @Des: 类描述
 */
public class SinvieEventTypeBean {

    private String code;
    private int level;
    private List<ChildrenRoomListBean> children;
    private String name;
    private int id;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public List<ChildrenRoomListBean> getChildren() {
        return children;
    }

    public void setChildren(List<ChildrenRoomListBean> children) {
        this.children = children;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public static class ChildrenRoomListBean {
        private String code;
        private int level;
        private List<ChildrenRoomListBean> children;
        private String name;
        private int id;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }

        public List<ChildrenRoomListBean> getChildren() {
            return children;
        }

        public void setChildren(List<ChildrenRoomListBean> children) {
            this.children = children;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
