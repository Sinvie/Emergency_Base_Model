package com.sinvie.emergency_base.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @author: JonsonBob
 * @date: 2021/7/13
 * @Des: 类描述
 */
public class SinvieAppAttachmentsBean implements Parcelable {
    private int id;
    private int relationId;
    private int size;
    private int platformId;
    private String extension;
    private String filename;
    private String path;
    private String remark;
    private String title;
    private String type;
    private String url;
    private DictionaryTypeRoomListBean dictionaryType;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRelationId() {
        return relationId;
    }

    public void setRelationId(int relationId) {
        this.relationId = relationId;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getPlatformId() {
        return platformId;
    }

    public void setPlatformId(int platformId) {
        this.platformId = platformId;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public DictionaryTypeRoomListBean getDictionaryType() {
        return dictionaryType;
    }

    public void setDictionaryType(DictionaryTypeRoomListBean dictionaryType) {
        this.dictionaryType = dictionaryType;
    }


    public static class DictionaryTypeRoomListBean implements Parcelable {
        private String code;
        private int createUserId;
        private String gmtCreate;
        private String gmtModified;
        @SerializedName("id")
        private int idX;
        private int isDeleted;
        private int modifiedUserId;
        private String name;
        private int orderNum;
        private int parentId;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public int getCreateUserId() {
            return createUserId;
        }

        public void setCreateUserId(int createUserId) {
            this.createUserId = createUserId;
        }

        public String getGmtCreate() {
            return gmtCreate;
        }

        public void setGmtCreate(String gmtCreate) {
            this.gmtCreate = gmtCreate;
        }

        public String getGmtModified() {
            return gmtModified;
        }

        public void setGmtModified(String gmtModified) {
            this.gmtModified = gmtModified;
        }

        public int getIdX() {
            return idX;
        }

        public void setIdX(int idX) {
            this.idX = idX;
        }

        public int getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(int isDeleted) {
            this.isDeleted = isDeleted;
        }

        public int getOdifiedUserId() {
            return modifiedUserId;
        }

        public void setOdifiedUserId(int odifiedUserId) {
            modifiedUserId = odifiedUserId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getOrderNum() {
            return orderNum;
        }

        public void setOrderNum(int orderNum) {
            this.orderNum = orderNum;
        }

        public int getParentId() {
            return parentId;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.code);
            dest.writeInt(this.createUserId);
            dest.writeString(this.gmtCreate);
            dest.writeString(this.gmtModified);
            dest.writeInt(this.idX);
            dest.writeInt(this.isDeleted);
            dest.writeInt(this.modifiedUserId);
            dest.writeString(this.name);
            dest.writeInt(this.orderNum);
            dest.writeInt(this.parentId);
        }

        public void readFromParcel(Parcel source) {
            this.code = source.readString();
            this.createUserId = source.readInt();
            this.gmtCreate = source.readString();
            this.gmtModified = source.readString();
            this.idX = source.readInt();
            this.isDeleted = source.readInt();
            this.modifiedUserId = source.readInt();
            this.name = source.readString();
            this.orderNum = source.readInt();
            this.parentId = source.readInt();
        }

        public DictionaryTypeRoomListBean() {
        }

        protected DictionaryTypeRoomListBean(Parcel in) {
            this.code = in.readString();
            this.createUserId = in.readInt();
            this.gmtCreate = in.readString();
            this.gmtModified = in.readString();
            this.idX = in.readInt();
            this.isDeleted = in.readInt();
            this.modifiedUserId = in.readInt();
            this.name = in.readString();
            this.orderNum = in.readInt();
            this.parentId = in.readInt();
        }

        public static final Creator<DictionaryTypeRoomListBean> CREATOR = new Creator<DictionaryTypeRoomListBean>() {
            @Override
            public DictionaryTypeRoomListBean createFromParcel(Parcel source) {
                return new DictionaryTypeRoomListBean(source);
            }

            @Override
            public DictionaryTypeRoomListBean[] newArray(int size) {
                return new DictionaryTypeRoomListBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.relationId);
        dest.writeInt(this.size);
        dest.writeInt(this.platformId);
        dest.writeString(this.extension);
        dest.writeString(this.filename);
        dest.writeString(this.path);
        dest.writeString(this.remark);
        dest.writeString(this.title);
        dest.writeString(this.type);
        dest.writeString(this.url);
        dest.writeParcelable(this.dictionaryType, flags);
    }

    public void readFromParcel(Parcel source) {
        this.id = source.readInt();
        this.relationId = source.readInt();
        this.size = source.readInt();
        this.platformId = source.readInt();
        this.extension = source.readString();
        this.filename = source.readString();
        this.path = source.readString();
        this.remark = source.readString();
        this.title = source.readString();
        this.type = source.readString();
        this.url = source.readString();
        this.dictionaryType = source.readParcelable(DictionaryTypeRoomListBean.class.getClassLoader());
    }

    public SinvieAppAttachmentsBean() {
    }

    protected SinvieAppAttachmentsBean(Parcel in) {
        this.id = in.readInt();
        this.relationId = in.readInt();
        this.size = in.readInt();
        this.platformId = in.readInt();
        this.extension = in.readString();
        this.filename = in.readString();
        this.path = in.readString();
        this.remark = in.readString();
        this.title = in.readString();
        this.type = in.readString();
        this.url = in.readString();
        this.dictionaryType = in.readParcelable(DictionaryTypeRoomListBean.class.getClassLoader());
    }

    public static final Parcelable.Creator<SinvieAppAttachmentsBean> CREATOR = new Parcelable.Creator<SinvieAppAttachmentsBean>() {
        @Override
        public SinvieAppAttachmentsBean createFromParcel(Parcel source) {
            return new SinvieAppAttachmentsBean(source);
        }

        @Override
        public SinvieAppAttachmentsBean[] newArray(int size) {
            return new SinvieAppAttachmentsBean[size];
        }
    };
}
