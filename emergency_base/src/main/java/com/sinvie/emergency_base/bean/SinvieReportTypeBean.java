package com.sinvie.emergency_base.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/13
 * @Des: 类描述
 */
public class SinvieReportTypeBean implements Parcelable {

    private String code;
    private int level;
    private List<SinvieReportTypeBean> children;
    private String name;
    private int id;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public List<SinvieReportTypeBean> getChildren() {
        return children;
    }

    public void setChildren(List<SinvieReportTypeBean> children) {
        this.children = children;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.code);
        dest.writeInt(this.level);
        dest.writeList(this.children);
        dest.writeString(this.name);
        dest.writeInt(this.id);
    }

    public void readFromParcel(Parcel source) {
        this.code = source.readString();
        this.level = source.readInt();
        this.children = new ArrayList<SinvieReportTypeBean>();
        source.readList(this.children, SinvieReportTypeBean.class.getClassLoader());
        this.name = source.readString();
        this.id = source.readInt();
    }

    public SinvieReportTypeBean() {
    }

    protected SinvieReportTypeBean(Parcel in) {
        this.code = in.readString();
        this.level = in.readInt();
        this.children = new ArrayList<SinvieReportTypeBean>();
        in.readList(this.children, SinvieReportTypeBean.class.getClassLoader());
        this.name = in.readString();
        this.id = in.readInt();
    }

    public static final Parcelable.Creator<SinvieReportTypeBean> CREATOR = new Parcelable.Creator<SinvieReportTypeBean>() {
        @Override
        public SinvieReportTypeBean createFromParcel(Parcel source) {
            return new SinvieReportTypeBean(source);
        }

        @Override
        public SinvieReportTypeBean[] newArray(int size) {
            return new SinvieReportTypeBean[size];
        }
    };
}
