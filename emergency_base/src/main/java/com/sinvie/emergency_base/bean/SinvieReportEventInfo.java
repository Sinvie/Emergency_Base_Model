package com.sinvie.emergency_base.bean;

import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/12
 * @Des: 类描述
 */
public class SinvieReportEventInfo {

    private Integer id;
    private String title;
    private int caseClassId;
    private String address;
    private String latitude;
    private String longitude;
    private String content;
    private List<String> attachmentIds;
    private String reportTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCaseClassId() {
        return caseClassId;
    }

    public void setCaseClassId(int caseClassId) {
        this.caseClassId = caseClassId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<String> getAttachmentIds() {
        return attachmentIds;
    }

    public void setAttachmentIds(List<String> attachmentIds) {
        this.attachmentIds = attachmentIds;
    }

    public String getReportTime() {
        return reportTime;
    }

    public void setReportTime(String reportTime) {
        this.reportTime = reportTime;
    }
}
