package com.sinvie.emergency_base.bean;

import static com.sinvie.emergency_base.bean.SinvieCodeLoginBean.*;

/**
 * @author: JonsonBob
 * @date: 2021/7/5
 * @Des: 登录 bean类
 */
public class SinvieLoginBean {
    
    private String msg;
    private int code;
    private int userId;
    private int platformId;
    private int isAdmin;
    private String deptName;
    private String name;
    private String username;
    private PlatformRoomListBean platform;
    private String token;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String sg) {
        msg = sg;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPlatformId() {
        return platformId;
    }

    public void setPlatformId(int platformId) {
        this.platformId = platformId;
    }

    public int getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(int isAdmin) {
        this.isAdmin = isAdmin;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public PlatformRoomListBean getPlatform() {
        return platform;
    }

    public void setPlatform(PlatformRoomListBean platform) {
        this.platform = platform;
    }
}
