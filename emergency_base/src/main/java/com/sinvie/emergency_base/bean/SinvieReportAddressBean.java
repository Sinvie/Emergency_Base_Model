package com.sinvie.emergency_base.bean;

/**
 * @author: JonsonBob
 * @date: 2021/7/16
 * @Des: 类描述
 */
public class SinvieReportAddressBean {
    private String location;
    private String address;
    private double longitude;
    private double latitude;
    private boolean isCurrentLocation;

    public SinvieReportAddressBean setReportAddressBean(String location, String address,
                                                        double longitude, double latitude,
                                                        boolean isCurrentLocation) {
        this.location = location;
        this.address = address;
        this.longitude = longitude;
        this.latitude = latitude;
        this.isCurrentLocation = isCurrentLocation;
        return this;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAddress() {
        return address;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isCurrentLocation() {
        return isCurrentLocation;
    }

    public void setCurrentLocation(boolean currentLocation) {
        isCurrentLocation = currentLocation;
    }
}
