package com.sinvie.emergency_base.bean;

import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/15
 * @Des: 类描述
 */
public class SinvieContactorBean {


    private List<RecordsRoomListBean> records;
    private int total;
    private int size;
    private int current;
    private boolean searchCount;
    private int pages;

    public List<RecordsRoomListBean> getRecords() {
        return records;
    }

    public void setRecords(List<RecordsRoomListBean> records) {
        this.records = records;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public boolean isSearchCount() {
        return searchCount;
    }

    public void setSearchCount(boolean searchCount) {
        this.searchCount = searchCount;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public static class RecordsRoomListBean {
        private int id;
        private String name;
        private String age;
        private int sex;
        private String workUnit;
        private String position;
        private String importance;
        private String officeTel;
        private String mobile1;
        private String mobile2;
        private String homeTel;
        private String fax;
        private String otherTel;
        private String commonlyUsed;
        private String firstName;
        private String email;
        private String platformId;
        private String remark;
        private String longitude;
        private String latitude;
        private String openId;
        private String address;
        private int contactorFlag;
        private String smsPromptExpiredTime;
        private String group;
        private String groups;
        private String groupId;
        private String errorMsg;
        private String verificationCode;
        private String groupVerificationCode;
        private String groupName;
        private String mailGroups;
        private String createUserId;
        private String gmtCreate;
        private String modifiedUserId;
        private String gmtModified;
        private String deleted;
        private String urgentContactorId;
        private String urgentContactorName;
        private String urgentContactorMobile1;
        private String phone;
        private String personnelMark;
        private String images;
        private String orderNum;
        private String defaultPhoneType;
        private String contactType;
        private String wxUser;
        private int messageUpCount;
        private int receiveCount;
        private String randomPassword;
        private String token;
        private String roleEntity;
        private String roleId;
        private String identifyId;
        private String responsibilities;
        private String emergencyType;
        private String emergencyPosition;
        private String appUser;
        private String appStatus;
        private String deviceType;
        private String isOnline;
        private String deviceList;
        private String defaultDevice;
        private String isAttention;
        private String params;
        private String smsGroupMemberNumber;
        private String type;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public int getSex() {
            return sex;
        }

        public void setSex(int sex) {
            this.sex = sex;
        }

        public String getWorkUnit() {
            return workUnit;
        }

        public void setWorkUnit(String workUnit) {
            this.workUnit = workUnit;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public String getImportance() {
            return importance;
        }

        public void setImportance(String importance) {
            this.importance = importance;
        }

        public String getOfficeTel() {
            return officeTel;
        }

        public void setOfficeTel(String officeTel) {
            this.officeTel = officeTel;
        }

        public String getObile1() {
            return mobile1;
        }

        public void setObile1(String obile1) {
            mobile1 = obile1;
        }

        public String getObile2() {
            return mobile2;
        }

        public void setObile2(String obile2) {
            mobile2 = obile2;
        }

        public String getHomeTel() {
            return homeTel;
        }

        public void setHomeTel(String homeTel) {
            this.homeTel = homeTel;
        }

        public String getFax() {
            return fax;
        }

        public void setFax(String fax) {
            this.fax = fax;
        }

        public String getOtherTel() {
            return otherTel;
        }

        public void setOtherTel(String otherTel) {
            this.otherTel = otherTel;
        }

        public String getCommonlyUsed() {
            return commonlyUsed;
        }

        public void setCommonlyUsed(String commonlyUsed) {
            this.commonlyUsed = commonlyUsed;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPlatformId() {
            return platformId;
        }

        public void setPlatformId(String platformId) {
            this.platformId = platformId;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getOpenId() {
            return openId;
        }

        public void setOpenId(String openId) {
            this.openId = openId;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public int getContactorFlag() {
            return contactorFlag;
        }

        public void setContactorFlag(int contactorFlag) {
            this.contactorFlag = contactorFlag;
        }

        public String getSmsPromptExpiredTime() {
            return smsPromptExpiredTime;
        }

        public void setSmsPromptExpiredTime(String smsPromptExpiredTime) {
            this.smsPromptExpiredTime = smsPromptExpiredTime;
        }

        public String getGroup() {
            return group;
        }

        public void setGroup(String group) {
            this.group = group;
        }

        public String getGroups() {
            return groups;
        }

        public void setGroups(String groups) {
            this.groups = groups;
        }

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public String getErrorMsg() {
            return errorMsg;
        }

        public void setErrorMsg(String errorMsg) {
            this.errorMsg = errorMsg;
        }

        public String getVerificationCode() {
            return verificationCode;
        }

        public void setVerificationCode(String verificationCode) {
            this.verificationCode = verificationCode;
        }

        public String getGroupVerificationCode() {
            return groupVerificationCode;
        }

        public void setGroupVerificationCode(String groupVerificationCode) {
            this.groupVerificationCode = groupVerificationCode;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public String getAilGroups() {
            return mailGroups;
        }

        public void setAilGroups(String ailGroups) {
            mailGroups = ailGroups;
        }

        public String getCreateUserId() {
            return createUserId;
        }

        public void setCreateUserId(String createUserId) {
            this.createUserId = createUserId;
        }

        public String getGmtCreate() {
            return gmtCreate;
        }

        public void setGmtCreate(String gmtCreate) {
            this.gmtCreate = gmtCreate;
        }

        public String getOdifiedUserId() {
            return modifiedUserId;
        }

        public void setOdifiedUserId(String odifiedUserId) {
            modifiedUserId = odifiedUserId;
        }

        public String getGmtModified() {
            return gmtModified;
        }

        public void setGmtModified(String gmtModified) {
            this.gmtModified = gmtModified;
        }

        public String getDeleted() {
            return deleted;
        }

        public void setDeleted(String deleted) {
            this.deleted = deleted;
        }

        public String getUrgentContactorId() {
            return urgentContactorId;
        }

        public void setUrgentContactorId(String urgentContactorId) {
            this.urgentContactorId = urgentContactorId;
        }

        public String getUrgentContactorName() {
            return urgentContactorName;
        }

        public void setUrgentContactorName(String urgentContactorName) {
            this.urgentContactorName = urgentContactorName;
        }

        public String getUrgentContactorMobile1() {
            return urgentContactorMobile1;
        }

        public void setUrgentContactorMobile1(String urgentContactorMobile1) {
            this.urgentContactorMobile1 = urgentContactorMobile1;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPersonnelMark() {
            return personnelMark;
        }

        public void setPersonnelMark(String personnelMark) {
            this.personnelMark = personnelMark;
        }

        public String getImages() {
            return images;
        }

        public void setImages(String images) {
            this.images = images;
        }

        public String getOrderNum() {
            return orderNum;
        }

        public void setOrderNum(String orderNum) {
            this.orderNum = orderNum;
        }

        public String getDefaultPhoneType() {
            return defaultPhoneType;
        }

        public void setDefaultPhoneType(String defaultPhoneType) {
            this.defaultPhoneType = defaultPhoneType;
        }

        public String getContactType() {
            return contactType;
        }

        public void setContactType(String contactType) {
            this.contactType = contactType;
        }

        public String getWxUser() {
            return wxUser;
        }

        public void setWxUser(String wxUser) {
            this.wxUser = wxUser;
        }

        public int getEssageUpCount() {
            return messageUpCount;
        }

        public void setEssageUpCount(int essageUpCount) {
            messageUpCount = essageUpCount;
        }

        public int getReceiveCount() {
            return receiveCount;
        }

        public void setReceiveCount(int receiveCount) {
            this.receiveCount = receiveCount;
        }

        public String getRandomPassword() {
            return randomPassword;
        }

        public void setRandomPassword(String randomPassword) {
            this.randomPassword = randomPassword;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getRoleEntity() {
            return roleEntity;
        }

        public void setRoleEntity(String roleEntity) {
            this.roleEntity = roleEntity;
        }

        public String getRoleId() {
            return roleId;
        }

        public void setRoleId(String roleId) {
            this.roleId = roleId;
        }

        public String getIdentifyId() {
            return identifyId;
        }

        public void setIdentifyId(String identifyId) {
            this.identifyId = identifyId;
        }

        public String getResponsibilities() {
            return responsibilities;
        }

        public void setResponsibilities(String responsibilities) {
            this.responsibilities = responsibilities;
        }

        public String getEmergencyType() {
            return emergencyType;
        }

        public void setEmergencyType(String emergencyType) {
            this.emergencyType = emergencyType;
        }

        public String getEmergencyPosition() {
            return emergencyPosition;
        }

        public void setEmergencyPosition(String emergencyPosition) {
            this.emergencyPosition = emergencyPosition;
        }

        public String getAppUser() {
            return appUser;
        }

        public void setAppUser(String appUser) {
            this.appUser = appUser;
        }

        public String getAppStatus() {
            return appStatus;
        }

        public void setAppStatus(String appStatus) {
            this.appStatus = appStatus;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getIsOnline() {
            return isOnline;
        }

        public void setIsOnline(String isOnline) {
            this.isOnline = isOnline;
        }

        public String getDeviceList() {
            return deviceList;
        }

        public void setDeviceList(String deviceList) {
            this.deviceList = deviceList;
        }

        public String getDefaultDevice() {
            return defaultDevice;
        }

        public void setDefaultDevice(String defaultDevice) {
            this.defaultDevice = defaultDevice;
        }

        public String getIsAttention() {
            return isAttention;
        }

        public void setIsAttention(String isAttention) {
            this.isAttention = isAttention;
        }

        public String getParams() {
            return params;
        }

        public void setParams(String params) {
            this.params = params;
        }

        public String getSmsGroupMemberNumber() {
            return smsGroupMemberNumber;
        }

        public void setSmsGroupMemberNumber(String smsGroupMemberNumber) {
            this.smsGroupMemberNumber = smsGroupMemberNumber;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
