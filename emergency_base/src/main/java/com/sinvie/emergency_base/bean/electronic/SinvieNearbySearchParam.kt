package com.sinvie.emergency_base.bean.electronic

/**
 * 周边检索请求参数
 */
data class NearbySearchParam(
    var solrType: Int? = null,  //搜索类型
    var keywords: String? = null,  //搜索关键字
    var currentPage: Int? = null,
    var pageSize: Int? = null,
    var eventId: Int? = null,  //事件id，不区分事件为-1
    var searchAllType: Int? = null,  //是否搜索所有资源类型，所有为1
    var resoureTypeList: List<Int>? = null,//搜索资源类型列表，searchAllType为0时使用
    var longitude: Double? = null,  //周边搜索，中心点经度
    var latitude: Double? = null,  //周边搜索，中心点纬度
    var radius: Int? = null,  //周边搜索，半径，单位km
    var otherParam: OtherParam? = null,
    var polygon: String? = null // 设定的搜索区域，移动端周边检索暂不使用
)

data class OtherParam(
    var onlyInStock: Int? = null,  //物资搜索，只搜索有库存的物资； 0：否，1，是 默认0；
    var typeName: String? = null  //任务类型筛选
)
