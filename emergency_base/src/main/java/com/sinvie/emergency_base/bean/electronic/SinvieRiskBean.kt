package com.sinvie.emergency_base.bean.electronic

data class RiskRep<T>(
    val msg: String? = null,
    val code: Int,
    val riskDanger: T
)

data class RiskInfo(
    val id: Int,
    val typeName: String,
    val contacts: String,                            //联系人
    val contactTelephone: String,                    //联系电话
    val address: String,
    val deadlineForRectification: String,            //整改期限
    val possibleImpact: String,                      //造成影响
    val longitude: Float,
    val latitude: Float,
    val rectificationMeasuresSituation: String,      //整改措施
    val controllability: Int,                        //可控程度
    val level: Int,                                  //隐患等级
    val disposingCapacityOrResourceReserve: String,  //处理能力
    val platformId: Int
)