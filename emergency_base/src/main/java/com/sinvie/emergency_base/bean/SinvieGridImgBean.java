package com.sinvie.emergency_base.bean;

/**
 * @author: JonsonBob
 * @date: 2021/7/13
 * @Des: GridView 显示的 item bean
 */
public class SinvieGridImgBean {
    /**
     * 区分 视频 还是 图片
     */
    int gridType;
    /**
     * 网络地址
     */
    String url;

    public int getGridType() {
        return gridType;
    }

    public void setGridType(int gridType) {
        this.gridType = gridType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
