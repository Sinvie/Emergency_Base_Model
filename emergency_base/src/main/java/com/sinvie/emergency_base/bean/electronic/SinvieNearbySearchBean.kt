package com.sinvie.emergency_base.bean.electronic

data class NearbySearchBean(val list: List<ResourceTypeBean>)

data class ResourceTypeBean(
    var typeName: String,
    var type: Int,
    var list: List<ResourceSearchBean>
)

data class ResourceSearchBean(
    var typeName: String? = null,
    var searchType: Int? = null,
    var id: String? = null,
    var deviceId: String? = null,  //部分资源为deviceId
    var type: Int? = null,
    var address: String,
    var name: String? = null,
    var deviceName: String? = null,  //部分资源为deviceName
    var longitude: String,
    var latitude: String
)