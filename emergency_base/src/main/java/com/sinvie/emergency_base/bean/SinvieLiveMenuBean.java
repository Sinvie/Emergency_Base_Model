package com.sinvie.emergency_base.bean;

/**
 * @author: JonsonBob
 * @date: 2021/7/3
 * @Des: 类描述
 */
public class SinvieLiveMenuBean {
    int id;
    int image;

    public SinvieLiveMenuBean(int id, int image) {
        this.id = id;
        this.image = image;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
