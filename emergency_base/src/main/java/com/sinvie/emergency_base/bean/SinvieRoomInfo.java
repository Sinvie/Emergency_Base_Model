package com.sinvie.emergency_base.bean;

/**
 * @author: JonsonBob
 * @date: 2021/7/6
 * @Des: 类描述
 */
public class SinvieRoomInfo {

    //房间主键id
    private String id;
    ///主播id
    private String userId;
    /// 【字段含义】事件ID
    private int eventId;
    /// 【字段含义】直播定位地址
    private String liveAddress = "广州省海珠区中山科技园B座905";
    /// 【字段含义】直播结束时间
    private String endTime;
    /// 【字段含义】直播标题
    private String liveRoomTitle;
    /// 【字段含义】直播开始时间
    private String startTime;
    /// 【字段含义】直播间状态 0: 播放 ,1: 关闭
    private int liveStatus;
    /// 【字段含义】直播类型 0,现场 , 1:任务
    private int liveType;
    /// 【字段含义】房间唯一标识
    private String roomId;
    /// 【字段含义】cdn模式下的播放流地址
    public String streamUrl;
    /// 【字段含义】房间的状态: 单人/连麦/PK
    public int roomStatus;
    /// 【字段含义】房间人数
    public int memberCount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public String getLiveAddress() {
        return liveAddress;
    }

    public void setLiveAddress(String liveAddress) {
        this.liveAddress = liveAddress;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String liveEndTime) {
        this.endTime = liveEndTime;
    }

    public String getLiveRoomTitle() {
        return liveRoomTitle;
    }

    public void setLiveRoomTitle(String liveRoomTitle) {
        this.liveRoomTitle = liveRoomTitle;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String liveStartTime) {
        this.startTime = liveStartTime;
    }

    public int getLiveStatus() {
        return liveStatus;
    }

    public void setLiveStatus(int liveStatus) {
        this.liveStatus = liveStatus;
    }

    public int getLiveType() {
        return liveType;
    }

    public void setLiveType(int liveType) {
        this.liveType = liveType;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getStreamUrl() {
        return streamUrl;
    }

    public void setStreamUrl(String streamUrl) {
        this.streamUrl = streamUrl;
    }

    public int getRoomStatus() {
        return roomStatus;
    }

    public void setRoomStatus(int roomStatus) {
        this.roomStatus = roomStatus;
    }

    public int getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(int memberCount) {
        this.memberCount = memberCount;
    }
}
