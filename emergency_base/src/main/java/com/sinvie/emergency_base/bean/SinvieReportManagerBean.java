package com.sinvie.emergency_base.bean;

/**
 * @author: JonsonBob
 * @date: 2021/8/23
 * @Des: 类描述
 */
public class SinvieReportManagerBean {
    private String reportName;
    private String reportContent;
    private int itemType;
    private String typeName;

    public SinvieReportManagerBean(String reportName, String reportContent, int itemType, String typeName) {
        this.reportName = reportName;
        this.reportContent = reportContent;
        this.itemType = itemType;
        this.typeName = typeName;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getReportContent() {
        return reportContent;
    }

    public void setReportContent(String reportContent) {
        this.reportContent = reportContent;
    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
