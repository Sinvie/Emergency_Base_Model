package com.sinvie.emergency_base.bean;

/**
 * @author: JonsonBob
 * @date: 2021/12/11
 * @Des: 类描述
 */
public class WorkbenchBean {
    /**
     * 当前item的id
     */
    private Integer itemId;
    /**
     * 当前item的 icon
     */
    private Integer img;
    /**
     * 当前item的 标题
     */
    private Integer title;
    /**
     * 此item 是否可点击
     */
    private boolean isUse = false;
    /**
     * 此item所对应的跳转界面
     */
    private String activityAction;

    /**
     * 此item不能单击的提示 当 isUser = true 时无效
     */
    private String notUseMsg;

    public WorkbenchBean() {
    }

    public WorkbenchBean(Integer itemId, Integer title, Integer img, boolean isUse, String activityAction, String notUseMsg) {
        this.itemId = itemId;
        this.img = img;
        this.title = title;
        this.isUse = isUse;
        this.activityAction = activityAction;
        this.notUseMsg = notUseMsg;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public Integer getTitle() {
        return title;
    }

    public void setTitle(Integer title) {
        this.title = title;
    }

    public Integer getImg() {
        return img;
    }

    public void setImg(Integer img) {
        this.img = img;
    }

    public boolean isUse() {
        return isUse;
    }

    public void setUse(boolean use) {
        isUse = use;
    }

    public String getActivityAction() {
        return activityAction;
    }

    public void setActivityAction(String activityAction) {
        this.activityAction = activityAction;
    }

    public String getNotUseMsg() {
        return notUseMsg;
    }

    public void setNotUseMsg(String notUseMsg) {
        this.notUseMsg = notUseMsg;
    }
}
