package com.sinvie.emergency_base.bean

data class SinvieAddrReportBean(
    var eventId: Int? = null,        //H5上报时需传
    var contactorId: Int,            //人员id，app位置上报时需传
    var deviceCode: String? = null,  //设备编码，设备上报时需传
    var resultId: Int? = null,       //H5上报时需传
    var type: Int,                   //类型，-1:离线通知，0：app位置上报，1：设备位置上报，2：H5上报
    var longitude: Double,
    var latitude: Double,
    var address: String?,
    var reportTime: String? = null,
    var deviceType: Int? = null      //设备类型，设备上报时需传
)