package com.sinvie.emergency_base.bean;

/**
 * @author: JonsonBob
 * @date: 2021/7/14
 * @Des: 类描述
 */
public class SinvieAddResourceBean {

    /**
     * 是否是添加 item
     */
    private boolean isAdd = false;
    /**
     * 是否是网络图片
     */
    private boolean isNetPath = false;
    /**
     * 资源地址 本地/网络
     */
    private String resourcePath;
    /**
     * id (不是系统资源id)
     */
    private int resourceId;
    /**
     * 是不是图片
     */
    private boolean isImg;

    public boolean isAdd() {
        return isAdd;
    }

    public void setAdd(boolean add) {
        isAdd = add;
    }

    public String getResourcePath() {
        return resourcePath;
    }

    public void setResourcePath(String resourcePath) {
        this.resourcePath = resourcePath;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    public boolean isImg() {
        return isImg;
    }

    public void setImg(boolean img) {
        isImg = img;
    }

    public boolean isNetPath() {
        return isNetPath;
    }

    public void setNetPath(boolean netPath) {
        isNetPath = netPath;
    }
}
