package com.sinvie.emergency_base.bean.electronic

data class SinvieSearchResourceResultBean<T>(
    var code: Int = 0,
    var message: String? = null,
    var success: Boolean = false,
    val data: T? = null
)
