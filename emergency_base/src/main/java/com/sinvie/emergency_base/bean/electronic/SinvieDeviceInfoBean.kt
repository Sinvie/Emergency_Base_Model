package com.sinvie.emergency_base.bean.electronic

data class DeviceInfoBean(val appDevice: DeviceData)
data class DeviceData(
    val id: Int,
    val name: String,
    val code: String,
    val number: String,
    val longitude: Double,
    val latitude: Double,
    val address: String,
    val type: Int,
    val streamMediaUrl: String,
    val status: Int,
    val lastLoginTime: String? = null,
    val order: Int? = null,
    val haveGroup: Int,
    val mark: Int
)

data class DeviceDetail(
    val title: String,
    val description: String
)
