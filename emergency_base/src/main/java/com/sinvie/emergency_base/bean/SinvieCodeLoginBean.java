package com.sinvie.emergency_base.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author: JonsonBob
 * @date: 2021/7/15
 * @Des: 类描述
 */
public class SinvieCodeLoginBean {


    private String msg;
    private String deptName;
    private int code;
    private int platformId;
    private int userId;
    private PlatformRoomListBean platform;
    private String username;
    private String token;
    private AppDeviceRoomListBean appDevice;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String sg) {
        msg = sg;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getPlatformId() {
        return platformId;
    }

    public void setPlatformId(int platformId) {
        this.platformId = platformId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public PlatformRoomListBean getPlatform() {
        return platform;
    }

    public void setPlatform(PlatformRoomListBean platform) {
        this.platform = platform;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public AppDeviceRoomListBean getAppDevice() {
        return appDevice;
    }

    public void setAppDevice(AppDeviceRoomListBean appDevice) {
        this.appDevice = appDevice;
    }

    public static class PlatformRoomListBean {
        private String ancestors;
        private int animationFlag;
        private String backgroundImage;
        private int deleted;
        private int distance;
        private String enName;
        private long gmtCreate;
        private String icon;
        private int id;
        private String isLogo;
        private double latitude;
        private double longitude;
        private int orderNum;
        private int parentId;
        private String platformName;
        private String platformTitle;
        private int platformTypeId;
        private String qrCode;
        private String remark;
        private String status;
        private String titleColor;
        private int townId;
        private String townName;

        public String getAncestors() {
            return ancestors;
        }

        public void setAncestors(String ancestors) {
            this.ancestors = ancestors;
        }

        public int getAnimationFlag() {
            return animationFlag;
        }

        public void setAnimationFlag(int animationFlag) {
            this.animationFlag = animationFlag;
        }

        public String getBackgroundImage() {
            return backgroundImage;
        }

        public void setBackgroundImage(String backgroundImage) {
            this.backgroundImage = backgroundImage;
        }

        public int getDeleted() {
            return deleted;
        }

        public void setDeleted(int deleted) {
            this.deleted = deleted;
        }

        public int getDistance() {
            return distance;
        }

        public void setDistance(int distance) {
            this.distance = distance;
        }

        public String getEnName() {
            return enName;
        }

        public void setEnName(String enName) {
            this.enName = enName;
        }

        public long getGmtCreate() {
            return gmtCreate;
        }

        public void setGmtCreate(long gmtCreate) {
            this.gmtCreate = gmtCreate;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getIsLogo() {
            return isLogo;
        }

        public void setIsLogo(String isLogo) {
            this.isLogo = isLogo;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public int getOrderNum() {
            return orderNum;
        }

        public void setOrderNum(int orderNum) {
            this.orderNum = orderNum;
        }

        public int getParentId() {
            return parentId;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }

        public String getPlatformName() {
            return platformName;
        }

        public void setPlatformName(String platformName) {
            this.platformName = platformName;
        }

        public String getPlatformTitle() {
            return platformTitle;
        }

        public void setPlatformTitle(String platformTitle) {
            this.platformTitle = platformTitle;
        }

        public int getPlatformTypeId() {
            return platformTypeId;
        }

        public void setPlatformTypeId(int platformTypeId) {
            this.platformTypeId = platformTypeId;
        }

        public String getQrCode() {
            return qrCode;
        }

        public void setQrCode(String qrCode) {
            this.qrCode = qrCode;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTitleColor() {
            return titleColor;
        }

        public void setTitleColor(String titleColor) {
            this.titleColor = titleColor;
        }

        public int getTownId() {
            return townId;
        }

        public void setTownId(int townId) {
            this.townId = townId;
        }

        public String getTownName() {
            return townName;
        }

        public void setTownName(String townName) {
            this.townName = townName;
        }
    }


    public static class AppDeviceRoomListBean {
        private String number;
        private String address;
        @SerializedName("code")
        private String codeX;
        private String loginUsername;
        private String loginUrl;
        private String loginPassword;
        private String name;
        private int id;
        private String streamMediaUrl;
        private int type;
        private String loginPort;
        private int status;

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCodeX() {
            return codeX;
        }

        public void setCodeX(String codeX) {
            this.codeX = codeX;
        }

        public String getLoginUsername() {
            return loginUsername;
        }

        public void setLoginUsername(String loginUsername) {
            this.loginUsername = loginUsername;
        }

        public String getLoginUrl() {
            return loginUrl;
        }

        public void setLoginUrl(String loginUrl) {
            this.loginUrl = loginUrl;
        }

        public String getLoginPassword() {
            return loginPassword;
        }

        public void setLoginPassword(String loginPassword) {
            this.loginPassword = loginPassword;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getStreamMediaUrl() {
            return streamMediaUrl;
        }

        public void setStreamMediaUrl(String streamMediaUrl) {
            this.streamMediaUrl = streamMediaUrl;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getLoginPort() {
            return loginPort;
        }

        public void setLoginPort(String loginPort) {
            this.loginPort = loginPort;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }
    }
}
