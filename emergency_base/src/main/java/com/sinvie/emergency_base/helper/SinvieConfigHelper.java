package com.sinvie.emergency_base.helper;

import android.os.Environment;
import android.util.Log;

import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_live.tuikit.SinvieTUIKit;
import com.sinvie.emergency_live.tuikit.config.SinvieCustomFaceConfig;
import com.sinvie.emergency_live.tuikit.config.SinvieGeneralConfig;
import com.sinvie.emergency_live.tuikit.config.SinvieTUIKitConfigs;

import java.io.File;

public class SinvieConfigHelper {

    public SinvieConfigHelper() {

    }

    public SinvieTUIKitConfigs getConfigs() {
        SinvieGeneralConfig config = new SinvieGeneralConfig();
        // 显示对方是否已读的view将会展示
        config.setShowRead(true);
        config.setAppCacheDir(SinvieLocContext.getContext().getFilesDir().getPath());
        if (new File(Environment.getExternalStorageDirectory() + "/111222333").exists()) {
            config.setTestEnv(true);
        }
        Log.d("TAG", "setGeneralConfig: " + "pass 1 ");
        SinvieTUIKit.getConfigs().setGeneralConfig(config);
        SinvieTUIKit.getConfigs().setCustomFaceConfig(initCustomFaceConfig());
        return SinvieTUIKit.getConfigs();
    }

    private SinvieCustomFaceConfig initCustomFaceConfig() {
        //创建一个表情组对象
//        SinvieCustomFaceGroup faceConfigs = new SinvieCustomFaceGroup();
//        //设置表情组每页可显示的表情列数
//        faceConfigs.setPageColumnCount(5);
//        //设置表情组每页可显示的表情行数
//        faceConfigs.setPageRowCount(2);
//        //设置表情组号
//        faceConfigs.setFaceGroupId(1);
//        //设置表情组的主ICON
//        faceConfigs.setFaceIconPath("4349/xx07@2x.png");
//        //设置表情组的名称
//        faceConfigs.setFaceIconName("4350");
//        for (int i = 1; i <= 15; i++) {
//            //创建一个表情对象
//            SinvieCustomFaceConfig faceConfig = new SinvieCustomFaceConfig();
//            String index = "" + i;
//            if (i < 10)
//                index = "0" + i;
//            //设置表情所在Asset目录下的路径
//            faceConfig.setAssetPath("4349/xx" + index + "@2x.png");
//            //设置表情所名称
//            faceConfig.setFaceName("xx" + index + "@2x");
//            //设置表情宽度
//            faceConfig.setFaceWidth(240);
//            //设置表情高度
//            faceConfig.setFaceHeight(240);
//            faceConfigs.addCustomFace(faceConfig);
//        }
//        groupFaces.add(faceConfigs);

        return null;
    }

}
