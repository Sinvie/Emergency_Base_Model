package com.sinvie.emergency_base.helper;

import com.sinvie.emergency_live.tuikit.utils.SinvieTUIKitConstants;
import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;

/**
 * 自定义消息的bean实体，用来与json的相互转化
 */
public class SinvieHelloMessage {
    String businessID = SinvieTUIKitConstants.BUSINESS_ID_CUSTOM_HELLO;
    String text = SinvieLocContext.getContext().getString(com.sinvie.emergency_base.R.string.sinvie_welcome_tip);
    String link = "https://cloud.tencent.com/document/product/269/3794";

    int version = SinvieTUIKitConstants.JSON_VERSION_UNKNOWN;
}
