package com.sinvie.emergency_base.helper;

public class SinvieTUIKitLiveListenerManager {
    private static final SinvieTUIKitLiveListenerManager INSTANCE = new SinvieTUIKitLiveListenerManager();

    private SinvieIBaseLiveListener mSinvieIBaseLiveListener;

    public static SinvieTUIKitLiveListenerManager getInstance() {
        return INSTANCE;
    }

    public void registerCallListener(SinvieIBaseLiveListener callListener) {
        this.mSinvieIBaseLiveListener = callListener;
    }

    public SinvieIBaseLiveListener getBaseCallListener() {
        return this.mSinvieIBaseLiveListener;
    }
}
