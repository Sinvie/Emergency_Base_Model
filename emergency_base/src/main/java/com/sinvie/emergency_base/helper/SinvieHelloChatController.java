package com.sinvie.emergency_base.helper;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_live.tuikit.SinvieTUIKit;
import com.sinvie.emergency_live.tuikit.base.ISinvieBaseAction;
import com.sinvie.emergency_live.tuikit.base.ISinvieBaseInfo;
import com.sinvie.emergency_live.tuikit.base.ISinvieBaseViewHolder;
import com.sinvie.emergency_live.tuikit.base.SinvieTUIChatControllerListener;
import com.sinvie.emergency_live.tuikit.base.SinvieTUIConversationControllerListener;
import com.sinvie.emergency_live.tuikit.modules.chat.layout.message.holder.ISinvieCustomMessageViewGroup;
import com.sinvie.emergency_live.tuikit.modules.chat.layout.message.holder.ISinvieOnCustomMessageDrawListener;
import com.sinvie.emergency_live.tuikit.modules.chat.layout.message.holder.SinvieMessageBaseHolder;
import com.sinvie.emergency_live.tuikit.modules.chat.layout.message.holder.SinvieMessageCustomHolder;
import com.sinvie.emergency_live.tuikit.modules.message.SinvieMessageInfo;
import com.sinvie.emergency_live.tuikit.modules.message.SinvieMessageInfoUtil;
import com.sinvie.emergency_live.tuikit.utils.SinvieTUIKitConstants;
import com.sinvie.emergency_base.utils.SinvieDemoLog;
import com.tencent.imsdk.v2.V2TIMCustomElem;
import com.tencent.imsdk.v2.V2TIMMessage;

import java.util.List;

/**
 * @description 测试聊天控制器
 * @param
 * @return
 * @author JonsonBob
 * @time 2021/6/25 14:07
 */
public class SinvieHelloChatController implements SinvieTUIChatControllerListener {
    private static final String TAG = SinvieHelloChatController.class.getSimpleName();

    @Override
    public List<ISinvieBaseAction> onRegisterMoreActions() {
        return null;
    }

    @Override
    public ISinvieBaseInfo createCommonInfoFromTimMessage(V2TIMMessage timMessage) {
        if (timMessage.getElemType() == V2TIMMessage.V2TIM_ELEM_TYPE_CUSTOM) {
            V2TIMCustomElem customElem = timMessage.getCustomElem();
            if (customElem == null || customElem.getData() == null) {
                return null;
            }
            SinvieHelloMessage helloMessage = null;
            try {
                helloMessage = new Gson().fromJson(new String(customElem.getData()), SinvieHelloMessage.class);
            } catch (Exception e) {
                SinvieDemoLog.w(TAG, "invalid json: " + new String(customElem.getData()) + " " + e.getMessage());
            }
            if (helloMessage != null && TextUtils.equals(helloMessage.businessID, SinvieTUIKitConstants.BUSINESS_ID_CUSTOM_HELLO)) {
                SinvieMessageInfo messageInfo = new HelloMessageInfo();
                messageInfo.setMsgType(HelloMessageInfo.MSG_TYPE_HELLO);
                SinvieMessageInfoUtil.setMessageInfoCommonAttributes(messageInfo, timMessage);
                Context context = SinvieTUIKit.getAppContext();
                if (context != null) {
                    messageInfo.setExtra(context.getString(R.string.sinvie_custom_msg));
                }
                return messageInfo;
            }
        }
        return null;
    }

    @Override
    public ISinvieBaseViewHolder createCommonViewHolder(ViewGroup parent, int viewType) {
        if (viewType != HelloMessageInfo.MSG_TYPE_HELLO) {
            return null;
        }
        if (parent == null) {
            return null;
        }
        LayoutInflater inflater = LayoutInflater.from(SinvieTUIKit.getAppContext());
        View contentView = inflater.inflate(R.layout.item_sinvie_item_content, parent, false);
        return new HelloViewHolder(contentView);
    }

    @Override
    public boolean bindCommonViewHolder(ISinvieBaseViewHolder baseViewHolder, ISinvieBaseInfo baseInfo, int position) {
        if (baseViewHolder instanceof ISinvieCustomMessageViewGroup && baseInfo instanceof HelloMessageInfo) {
            ISinvieCustomMessageViewGroup customHolder = (ISinvieCustomMessageViewGroup) baseViewHolder;
            SinvieMessageInfo msg = (SinvieMessageInfo) baseInfo;
            new CustomMessageDraw().onDraw(customHolder, msg, position);
            return true;
        }
        return false;
    }

    static class HelloMessageInfo extends SinvieMessageInfo {
        public static final int MSG_TYPE_HELLO = 100002;
    }

    static class HelloViewHolder extends SinvieMessageCustomHolder {

        public HelloViewHolder(View itemView) {
            super(itemView);
        }
    }

    public static class HelloConversationController implements SinvieTUIConversationControllerListener {

        @Override
        public CharSequence getConversationDisplayString(ISinvieBaseInfo baseInfo) {
            if (baseInfo instanceof HelloMessageInfo) {
                return SinvieLocContext.getContext().getString(R.string.sinvie_welcome_tip);
            }
            return null;
        }
    }

    public static class CustomMessageDraw implements ISinvieOnCustomMessageDrawListener {

        /**
         * 自定义消息渲染时，会调用该方法，本方法实现了自定义消息的创建，以及交互逻辑
         *
         * @param parent 自定义消息显示的父View，需要把创建的自定义消息view添加到parent里
         * @param info   消息的具体信息
         */
        @Override
        public void onDraw(ISinvieCustomMessageViewGroup parent, SinvieMessageInfo info, int position) {
            // 获取到自定义消息的json数据
            if (info.getTimMessage().getElemType() != V2TIMMessage.V2TIM_ELEM_TYPE_CUSTOM) {
                return;
            }
            V2TIMCustomElem elem = info.getTimMessage().getCustomElem();
            // 自定义的json数据，需要解析成bean实例
            SinvieHelloMessage data = null;
            try {
                data = new Gson().fromJson(new String(elem.getData()), SinvieHelloMessage.class);
            } catch (Exception e) {
                SinvieDemoLog.w(TAG, "invalid json: " + new String(elem.getData()) + " " + e.getMessage());
            }
            if (data == null) {
                SinvieDemoLog.e(TAG, "No Custom Data: " + new String(elem.getData()));
            } else if (data.version == SinvieTUIKitConstants.JSON_VERSION_1
                    || (data.version == SinvieTUIKitConstants.JSON_VERSION_4 && data.businessID.equals(SinvieTUIKitConstants.BUSINESS_ID_CUSTOM_HELLO))) {
                if (parent instanceof SinvieMessageBaseHolder) {
                    SinvieHelloTIMUIController.onDraw(parent, data, position, ((SinvieMessageBaseHolder) parent).getOnItemClickListener(), info);
                }
            } else {
                SinvieDemoLog.w(TAG, "unsupported version: " + data);
            }
        }
    }

}

