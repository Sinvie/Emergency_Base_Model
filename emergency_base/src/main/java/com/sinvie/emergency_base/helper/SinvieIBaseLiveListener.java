package com.sinvie.emergency_base.helper;

import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.sinvie.emergency_live.tuikit.modules.chat.base.SinvieOfflineMessageBean;
import com.tencent.imsdk.v2.V2TIMMessage;

public interface SinvieIBaseLiveListener {
    void handleOfflinePushCall(Intent intent);

    void handleOfflinePushCall(SinvieOfflineMessageBean bean);

    void redirectCall(SinvieOfflineMessageBean bean);

    Fragment getSceneFragment();

    void refreshUserInfo();

    boolean isDialingMessage(V2TIMMessage message);

    Intent putCallExtra(Intent intent, String key, V2TIMMessage message);

}
