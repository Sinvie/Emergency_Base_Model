package com.sinvie.emergency_base.helper;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.sinvie.emergency_base.R;
import com.sinvie.emergency_live.tuikit.modules.chat.layout.message.SinvieMessageLayout;
import com.sinvie.emergency_live.tuikit.modules.chat.layout.message.holder.ISinvieCustomMessageViewGroup;
import com.sinvie.emergency_live.tuikit.modules.message.SinvieMessageInfo;
import com.sinvie.emergency_live.tuikit.utils.SinvieToastUtil;
import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_base.utils.SinvieDemoLog;

/**
 * @param
 * @author JonsonBob
 * @description 自定义消息控制器
 * @return
 * @time 2021/6/25 14:07
 */
public class SinvieHelloTIMUIController {

    private static final String TAG = SinvieHelloTIMUIController.class.getSimpleName();

    public static void onDraw(ISinvieCustomMessageViewGroup parent, final SinvieHelloMessage data, final int position, final SinvieMessageLayout.OnItemLongClickListener onItemLongClickListener, final SinvieMessageInfo info) {

        // 把自定义消息view添加到TUIKit内部的父容器里
        View view = LayoutInflater.from(SinvieLocContext.getContext()).inflate(R.layout.view_sinvie_custom_message_layout, null, false);
        parent.addMessageContentView(view);

        // 自定义消息view的实现，这里仅仅展示文本信息，并且实现超链接跳转
        TextView textView = view.findViewById(R.id.test_custom_message_tv);
        final String text = SinvieLocContext.getContext().getString(R.string.sinvie_no_support_msg);
        if (data == null) {
            textView.setText(text);
        } else {
            textView.setText(data.text);
        }
        view.setClickable(true);
        view.setOnClickListener(v -> {
            if (data == null) {
                SinvieDemoLog.e(TAG, "Do what?");
                SinvieToastUtil.toastShortMessage(text);
                return;
            }
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            Uri content_url = Uri.parse(data.link);
            intent.setData(content_url);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            SinvieLocContext.getContext().startActivity(intent);
        });
        view.setOnLongClickListener(v -> {
            if (onItemLongClickListener != null) {
                onItemLongClickListener.onMessageLongClick(v, position, info);
            }
            return false;
        });
    }
}
