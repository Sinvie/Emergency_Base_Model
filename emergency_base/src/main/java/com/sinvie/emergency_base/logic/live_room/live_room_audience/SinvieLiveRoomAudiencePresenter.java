package com.sinvie.emergency_base.logic.live_room.live_room_audience;

import android.content.Context;

import com.sinvie.emergency_base.ui.base.SinvieMVPBasePresenter;

/**
 * @author: JonsonBob
 * @date: 2021/6/30
 * @Des: 类描述
 */
public class SinvieLiveRoomAudiencePresenter extends SinvieMVPBasePresenter<SinvieLiveRoomAudienceContact.LiveRoomAudienceBaseView> implements SinvieLiveRoomAudienceContact.LiveRoomAudienceBasePresenter {
    private Context mContext;
    private SinvieLiveRoomAudienceModel mSinvieLiveRoomAudienceModel;

    public SinvieLiveRoomAudiencePresenter(Context context) {
        mContext = context;
        mSinvieLiveRoomAudienceModel = new SinvieLiveRoomAudienceModel();
    }
}
