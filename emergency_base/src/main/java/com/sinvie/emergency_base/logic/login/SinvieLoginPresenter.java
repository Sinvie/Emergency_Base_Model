package com.sinvie.emergency_base.logic.login;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieConfBroadcastConstants;
import com.sinvie.emergency_conf.CommonService.util.SinvieDeviceManager;
import com.sinvie.emergency_conf.CommonService.util.SinvieLogUtil;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieCodeLoginBean;
import com.sinvie.emergency_base.bean.SinvieLoginBean;
import com.sinvie.emergency_base.http.SinvieNetContact;
import com.sinvie.emergency_base.http.base.SinvieBaseObserver;
import com.sinvie.emergency_base.http.common.SinvieCommonNetUtils;
import com.sinvie.emergency_base.http.repository.SinvieBaseURLRepository;
import com.sinvie.emergency_base.ui.login.SinvieUserInfo;
import com.sinvie.emergency_base.utils.SinvieDemoLog;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


/**
 * @author: JonsonBob
 * @date: 2021/6/29
 * @Des: 登录 presenter
 */
public class SinvieLoginPresenter extends SinvieLoginBasePresenter {

    private static final String TAG = SinvieLoginPresenter.class.getSimpleName();

    private SinvieLoginModel mSinvieLoginModel;

    public SinvieLoginPresenter(Context context) {
        mContext = context;
        mSinvieLoginModel = new SinvieLoginModel();
        broadcastNames = new String[]{
                SinvieConfBroadcastConstants.LOGIN_SUCCESS,
                SinvieConfBroadcastConstants.ACTION_IM_LOGIN_SUCCESS,
                SinvieConfBroadcastConstants.LOGIN_FAILED,
                SinvieConfBroadcastConstants.ACTION_ENTERPRISE_GET_SELF_RESULT};
    }

    @Override
    public void doLogin(String account, String pwd, int loginType) {
        //判断网络
        if (!SinvieDeviceManager.isNetworkAvailable(mContext)) {
            getView().dismissLoginDialog(false);
            getView().showToast(R.string.sinvie_network_be_disconnected);
            return;
        }
        //判断条件
        if (TextUtils.isEmpty(account) || TextUtils.isEmpty(pwd)) {
            getView().dismissLoginDialog(false);
            getView().showToast(R.string.sinvie_account_information_not_empty);
            return;
        }
        //登录方式 0:手机验证码登录 1:账号密码登录
        if (loginType == 1) {
            //账号面登录接口单独写 因为返回格式乱七八糟
            doUrlLogin(account, pwd);
//            doTestLogin(account, pwd);
        } else {
            //封装验证码登录
            doBaseUrlLogin(account, pwd);
        }

    }

    /**
     * 获取 IM userSig
     */
    @Override
    public void getUserSig() {
        //判断网络
        if (!SinvieDeviceManager.isNetworkAvailable(mContext)) {
            getView().dismissLoginDialog(false);
            getView().showToast(R.string.sinvie_network_be_disconnected);
            return;
        }
        //从userinfo 取出来 userId
        SinvieCommonNetUtils.execute(SinvieCommonNetUtils.covert(new SinvieBaseURLRepository().getUserSig()),
                new SinvieBaseObserver<String>() {
                    @Override
                    public void onSuccess(@Nullable String str) {
                        if (isViewAtteached()) {
                            //如果返回成功 存下userSig
                            SinvieUserInfo.getInstance().setUserSig(str);
                            getView().getUserSigSuccess();
                        }
                    }

                    @Override
                    public void onFailure(int errCode, @NotNull String errMsg) {
                        if (isViewAtteached()) {
                            getView().getUserSigFail(errCode, errMsg);
                            getView().dismissLoginDialog(false);
                        }
                    }

                });
    }

    @Override
    public void getMobileCode(String mobile) {
        //判断网络
        if (!SinvieDeviceManager.isNetworkAvailable(mContext)) {
            SinvieDemoLog.e(TAG, "network has been disconnected");
            getView().dismissLoginDialog(false);
            getView().showToast(R.string.sinvie_network_be_disconnected);
            return;
        }
        //判断条件
        if (TextUtils.isEmpty(mobile)) {
            getView().dismissLoginDialog(false);
            getView().showToast(R.string.sinvie_account_information_not_empty);
            return;
        }
        //向后台申请获取验证码
        SinvieCommonNetUtils.execute(SinvieCommonNetUtils.covert(new SinvieBaseURLRepository().getMobileCode(mobile)), new SinvieBaseObserver<String>() {
            @Override
            public void onSuccess(@Nullable String s) {
                if (isViewAtteached()) {
                    getView().getMobileCodeSuccess();
                }
            }

            @Override
            public void onFailure(int errCode, @NotNull String errMsg) {
                if (isViewAtteached()) {
                    getView().getMobileCodeFail(errCode, errMsg);
                    getView().dismissLoginDialog(false);
                }
            }
        });

    }

    private void doTestLogin(String account, String pwd) {
        SinvieCommonNetUtils.execute(SinvieCommonNetUtils.covertPwdLogin(new SinvieBaseURLRepository().doLogin(account, pwd)), new SinvieBaseObserver<SinvieLoginBean>() {
            @Override
            public void onFailure(int errCode, @NotNull String errMsg) {
                if (isViewAtteached()) {
                    Log.e(TAG, "onFailure: " + errMsg);
                    getView().onLoginFail(SinvieNetContact.FAIL_CODE, errMsg);
                    getView().dismissLoginDialog(false);
                }
            }

            @Override
            public void onSuccess(@Nullable SinvieLoginBean sinvieLoginBean) {

                if (sinvieLoginBean != null) {
                    //账号密码
                    SinvieUserInfo.getInstance().setAccount(account);
                    SinvieUserInfo.getInstance().setPwd(pwd);

                    mSinvieLoginModel.saveLoginBean(sinvieLoginBean);

                    if (isViewAtteached()) {
                        getView().onLoginSuccess();
                    }
                } else {
                    if (isViewAtteached()) {
                        getView().onLoginFail(SinvieNetContact.FAIL_EMPTY,
                                mContext.getString(R.string.sinvie_login_error_empty));
                        getView().dismissLoginDialog(false);
                    }
                }
            }
        });

    }

    /**
     * 登录 服务器后台
     *
     * @param account 账号
     * @param pwd     密码
     */
    private void doUrlLogin(String account, String pwd) {
        SinvieCommonNetUtils.execute(SinvieCommonNetUtils.covertCodeLogin(new SinvieBaseURLRepository().doLogin(account, pwd, "3")), new SinvieBaseObserver<SinvieCodeLoginBean>() {
            @Override
            public void onFailure(int errCode, @NotNull String errMsg) {
                if (isViewAtteached()) {
                    getView().onLoginFail(errCode, errMsg);
                    getView().dismissLoginDialog(false);
                }
            }

            @Override
            public void onSuccess(@Nullable SinvieCodeLoginBean loginBean) {
                SinvieLogUtil.d(TAG, "CodeLoginBean : " + new Gson().toJson(loginBean));
                if (loginBean != null) {
                    //账号密码
                    SinvieUserInfo.getInstance().setAccount(account);
                    SinvieUserInfo.getInstance().setPwd(pwd);

                    mSinvieLoginModel.saveLoginBean(loginBean);

                    if (isViewAtteached()) {
                        getView().onLoginSuccess();
                    }
                } else {
                    if (isViewAtteached()) {
                        getView().onLoginFail(SinvieNetContact.FAIL_EMPTY, mContext.getString(R.string.sinvie_login_error_empty));
                    }
                }
            }
        });

    }

    /**
     * 登录 默认服务器后台
     *
     * @param account 账号
     * @param pwd     密码
     */
    private void doBaseUrlLogin(String account, String pwd) {
        SinvieCommonNetUtils.execute(SinvieCommonNetUtils.covertCodeLogin(new SinvieBaseURLRepository().doLogin(account, pwd, "2")), new SinvieBaseObserver<SinvieCodeLoginBean>() {
            @Override
            public void onFailure(int errCode, @NotNull String errMsg) {
                if (isViewAtteached()) {
                    getView().onLoginFail(errCode, errMsg);
                    getView().dismissLoginDialog(false);
                }
            }

            @Override
            public void onSuccess(@Nullable SinvieCodeLoginBean loginBean) {

                if (loginBean != null) {
                    //账号密码
                    SinvieUserInfo.getInstance().setAccount(account);
                    SinvieUserInfo.getInstance().setPwd(pwd);

                    mSinvieLoginModel.saveLoginBean(loginBean);

                    if (isViewAtteached()) {
                        getView().onLoginSuccess();
                    }
                } else {
                    if (isViewAtteached()) {
                        getView().onLoginFail(SinvieNetContact.FAIL_EMPTY,
                                mContext.getString(R.string.sinvie_login_error_empty));
//                        getView().dismissLoginDialog(false);
                    }
                }
            }
        });
    }
}
