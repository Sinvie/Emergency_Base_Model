package com.sinvie.emergency_base.logic.conference.confManager;


import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;

import com.huawei.ecterminalsdk.base.TsdkConfMediaType;
import com.huawei.ecterminalsdk.base.TsdkConfRole;
import com.sinvie.emergency_conf.CallService.SinvieCallMgr;
import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieConfBroadcastConstants;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcast;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcastReceiver;
import com.sinvie.emergency_conf.CommonService.util.SinvieLogUtil;
import com.sinvie.emergency_conf.ConferenceService.SinvieConfBaseInfo;
import com.sinvie.emergency_conf.ConferenceService.SinvieConfConstant;
import com.sinvie.emergency_conf.ConferenceService.SinvieMeetingMgr;
import com.sinvie.emergency_conf.ConferenceService.SinvieMember;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.ui.base.SinvieMVPBasePresenter;

import java.util.List;

public abstract class SinvieConfManagerBasePresenter extends SinvieMVPBasePresenter<SinvieIConfManagerContract.ConfManagerViewSinvie>
        implements SinvieIConfManagerContract.ConfManagerPresenter {
    private String confID;

    private String number = "";

    protected String[] broadcastNames;

    protected SinvieLocBroadcastReceiver receiver = new SinvieLocBroadcastReceiver() {
        @Override
        public void onReceive(String broadcastName, Object obj) {
            int result;
            switch (broadcastName) {
                case SinvieConfBroadcastConstants.CONF_STATE_UPDATE:
                    SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "conf state update ");

                    String conferenceID = (String) obj;
                    if (!conferenceID.equals(confID)) {
                        return;
                    }

                    //判断会议状态，如果会议结束，则关闭会议界面
                    SinvieConfBaseInfo sinvieConfBaseInfo =
                            SinvieMeetingMgr.getInstance().getCurrentConferenceBaseInfo();
                    if (null == sinvieConfBaseInfo) {
                        return;
                    }
                    getView().updateConfTypeIcon(sinvieConfBaseInfo);

                    List<SinvieMember> sinvieMemberList =
                            SinvieMeetingMgr.getInstance().getCurrentConferenceMemberList();
                    if (sinvieMemberList == null) {
                        return;
                    }
                    getView().refreshMemberList(sinvieMemberList);
                    for (SinvieMember sinvieMember : sinvieMemberList) {
                        if (sinvieMember.isPresent()) {
                            number = sinvieMember.getNumber();
                        }
                        if (!"".equals(number)) {
                            if (number.equals(sinvieMember.getNumber())) {
                                if (!sinvieMember.isPresent() || sinvieMember.getStatus() == SinvieConfConstant.ParticipantStatus.LEAVED) {
                                    SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.DATA_CONF_USER_LEAVE, null);
                                }
                            }
                        }
                        if (sinvieMember.isSelf()) {
                            SinvieLogUtil.i(SinvieLogUtil.DEMO_LOG, "isSelf:" + sinvieMember.getNumber() + "account" +
                                    ":" + sinvieMember.getAccountId() + "isinDataConference:" + sinvieMember.isInDataConference());
                            getView().updateMuteButton(sinvieMember.isMute());
                            getView().updateUpgradeConfBtn(sinvieMember.isInDataConference());
                            getView().updateAttendeeButton(sinvieMember);
                            getView().updateIsCheckedButton(sinvieConfBaseInfo.getIsChecked());
                        }
                    }
                    break;

                case SinvieConfBroadcastConstants.GET_DATA_CONF_PARAM_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_get_data_conf_params_fail);
                        return;
                    }
//                    MeetingMgr.getInstance().joinDataConf();
                    break;

                case SinvieConfBroadcastConstants.DATA_CONFERENCE_JOIN_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_join_data_conf_fail);
                    }
                    break;

                case SinvieConfBroadcastConstants.GET_CONF_END:
                    getView().finishActivity();
                    break;

                case SinvieConfBroadcastConstants.DATE_CONFERENCE_START_SHARE_STATUS:
                    getView().startAsShare(true);
                    getView().showCustomToast(R.string.sinvie_share_begin);
                    break;

                case SinvieConfBroadcastConstants.DATE_CONFERENCE_END_SHARE_STATUS:
                    getView().startAsShare(false);
                    getView().showCustomToast(R.string.sinvie_share_end);
                    break;

                case SinvieConfBroadcastConstants.DATE_CONFERENCE_AUX_DATA_STATE:
                    boolean auxDataState = (boolean) obj;
                    getView().setAuxDataState(auxDataState);
                    break;

                // 升级会议结果
                case SinvieConfBroadcastConstants.UPGRADE_CONF_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_upgrade_conf_fail);
                        return;
                    } else {
                        getView().showCustomToast(R.string.sinvie_upgrade_conf_success);
                    }
                    break;

                // 静音会议结果
                case SinvieConfBroadcastConstants.MUTE_CONF_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_mute_conf_fail);
                    } else {
                        getView().showCustomToast(R.string.sinvie_mute_conf_success);
                    }
                    break;

                // 取消静音会议结果
                case SinvieConfBroadcastConstants.UN_MUTE_CONF_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_un_mute_conf_fail);
                    } else {
                        getView().showCustomToast(R.string.sinvie_un_mute_conf_success);
                    }
                    break;

                // 锁定会议结果
                case SinvieConfBroadcastConstants.LOCK_CONF_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_lock_conf_fail);
                    } else {
                        getView().showCustomToast(R.string.sinvie_lock_conf_success);
                    }
                    break;

                // 取消锁定会议结果
                case SinvieConfBroadcastConstants.UN_LOCK_CONF_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_un_lock_conf_fail);
                    } else {
                        getView().showCustomToast(R.string.sinvie_un_lock_conf_success);
                    }
                    break;

                // 邀请与会者结果
                case SinvieConfBroadcastConstants.ADD_ATTENDEE_RESULT:
                    result = (int) obj;
                    SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "add attendee result: " + result);
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_add_attendee_fail);
                        return;
                    }
                    break;

                // 删除与会者结果
                case SinvieConfBroadcastConstants.DEL_ATTENDEE_RESULT:
                    result = (int) obj;
                    SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "add attendee result: " + result);
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_del_attendee_fail);
                        return;
                    }
                    break;

                // 静音与会者结果
                case SinvieConfBroadcastConstants.MUTE_ATTENDEE_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_mute_attendee_fail);
                        return;
                    }
                    break;

                // 取消静音与会者结果
                case SinvieConfBroadcastConstants.UN_MUTE_ATTENDEE_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_un_mute_attendee_fail);
                        return;
                    }
                    break;

                // 举手结果
                case SinvieConfBroadcastConstants.HAND_UP_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_handup_fail);
                        return;
                    }
                    break;

                // 取消举手结果
                case SinvieConfBroadcastConstants.CANCEL_HAND_UP_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_cancel_handup_fail);
                        return;
                    }
                    break;

                // 设置会议视频模式结果
                case SinvieConfBroadcastConstants.SET_CONF_MODE_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_set_conf_mode_fail);
                        return;
                    }
                    break;

                // 选看会场结果
                case SinvieConfBroadcastConstants.WATCH_ATTENDEE_CONF_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_watch_conf_fail);
                        return;
                    }
                    break;

                // 广播与会者结果
                case SinvieConfBroadcastConstants.BROADCAST_ATTENDEE_CONF_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_broadcast_conf_fail);
                        return;
                    }
                    break;

                // 取消广播与会者结果
                case SinvieConfBroadcastConstants.CANCEL_BROADCAST_CONF_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_cancel_broadcast_fail);
                        return;
                    }
                    break;

                // 请求主席结果
                case SinvieConfBroadcastConstants.REQUEST_CHAIRMAN_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_request_chairman_fail);
                        return;
                    }
                    setSelfPresenter();
                    break;

                // 释放主席结果
                case SinvieConfBroadcastConstants.RELEASE_CHAIRMAN_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_release_chairman_fail);
                        return;
                    }
                    break;

                // 发言人通知
                case SinvieConfBroadcastConstants.SPEAKER_LIST_IND:
                    if (!"ConfManagerActivity".equals(getCurrentActivity(SinvieLocContext.getContext()))) {
                        return;
                    }
                    int speakerNum = (int) obj;
                    if (0 == speakerNum) {
                        return;
                    }
                    String[] speakerName = SinvieMeetingMgr.getInstance().getSpeakers();
                    getView().showMessage(speakerName[0] + " is speaking.");
                    break;

                // 申请主讲人结果
                case SinvieConfBroadcastConstants.REQUEST_PRESENTER_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_request_presenter_fail);
                        return;
                    }
                    break;

                default:
                    break;
            }

            onBroadcastReceive(broadcastName, obj);
        }
    };

    protected abstract void onBroadcastReceive(String broadcastName, Object obj);


    @Override
    public void registerBroadcast() {
        SinvieLocBroadcast.getInstance().registerBroadcast(receiver, broadcastNames);
    }

    @Override
    public void unregisterBroadcast() {
        SinvieLocBroadcast.getInstance().unRegisterBroadcast(receiver, broadcastNames);
    }

    @Override
    public void setConfID(String confID) {
        this.confID = confID;
    }

    public String getSubject() {
        if (SinvieMeetingMgr.getInstance().getCurrentConferenceBaseInfo() == null) {
            return null;
        }
        return SinvieMeetingMgr.getInstance().getCurrentConferenceBaseInfo().getSubject();
    }

    @Override
    public void closeConf() {
        int result = SinvieMeetingMgr.getInstance().leaveConf();
        if (result != 0) {
            getView().showCustomToast(R.string.sinvie_leave_conf_fail);
            return;
        }

        SinvieLocBroadcast.getInstance().unRegisterBroadcast(receiver, broadcastNames);
    }

    @Override
    public void finishConf() {
        int result = SinvieMeetingMgr.getInstance().endConf();
        if (result != 0) {
            getView().showCustomToast(R.string.sinvie_end_audio_conf);
            return;
        }

        SinvieLocBroadcast.getInstance().unRegisterBroadcast(receiver, broadcastNames);
    }


    @Override
    public boolean muteSelf() {
        SinvieMember self = SinvieMeetingMgr.getInstance().getCurrentConferenceSelf();

        if (self == null) {
            return false;
        }
        int result = SinvieMeetingMgr.getInstance().muteAttendee(self, !self.isMute());
        if (result != 0) {
            return false;
        }
        return true;
    }

    @Override
    public int switchLoudSpeaker() {
        return SinvieCallMgr.getInstance().switchAudioRoute();
    }

    @Override
    public boolean isChairMan() {
        SinvieMember self = SinvieMeetingMgr.getInstance().getCurrentConferenceSelf();

        if (null == self) {
            return false;
        }
        return (self.getRole() == TsdkConfRole.TSDK_E_CONF_ROLE_ATTENDEE ? false : true);
    }

    @Override
    public List<SinvieMember> getMemberList() {
        return SinvieMeetingMgr.getInstance().getCurrentConferenceMemberList();
    }

    @Override
    public void setSelfPresenter() {
        SinvieConfBaseInfo sinvieConfBaseInfo = SinvieMeetingMgr.getInstance().getCurrentConferenceBaseInfo();
        if (null == sinvieConfBaseInfo) {
            return;
        }

        if (sinvieConfBaseInfo.getMediaType() == TsdkConfMediaType.TSDK_E_CONF_MEDIA_VIDEO
                || sinvieConfBaseInfo.getMediaType() == TsdkConfMediaType.TSDK_E_CONF_MEDIA_VOICE) {
            return;
        }

        int result = 0;
        SinvieMember self = SinvieMeetingMgr.getInstance().getCurrentConferenceSelf();
        if (null == self) {
            return;
        }

        if (self.getRole() == TsdkConfRole.TSDK_E_CONF_ROLE_CHAIRMAN && !self.isPresent()) {
//            result = MeetingMgr.getInstance().setPresenter(self);
        }

        if (0 != result) {
            getView().showCustomToast(R.string.sinvie_set_presenter_failed);
        }
    }


    @Override
    public String getConfBaseInfoSubject() {
        if (null == SinvieMeetingMgr.getInstance().getConfBaseInfo()) {
            return "";
        }
        return SinvieMeetingMgr.getInstance().getConfBaseInfo().getSubject();
    }

    /**
     * 获取当前显示的activity名称
     *
     * @param context
     * @return
     */
    private String getCurrentActivity(Context context) {
        ActivityManager manager =
                (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
        ComponentName componentName = manager.getRunningTasks(1).get(0).topActivity;
        String className = componentName.getClassName();
        if (null == className) {
            return null;
        }

        if (!className.contains(".")) {
            return className;
        }

        String[] str = className.split("\\.");
        return str[str.length - 1];
    }
}
