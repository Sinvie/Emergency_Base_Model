package com.sinvie.emergency_base.logic.report_manager.report_manager_detail;

import com.sinvie.emergency_base.ui.base.SinvieMVPBasePresenter;

/**
 * @author: JonsonBob
 * @date: 2021/8/23
 * @Des: 类描述
 */
public class SinvieReportManagerDetailPresenter extends SinvieMVPBasePresenter<SinvieReportManagerDetailContact.ReportManagerDetailBaseView>
        implements SinvieReportManagerDetailContact.ReportManagerDetailBasePresenter {

}
