package com.sinvie.emergency_base.logic.report.add_report_detail;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.sinvie.emergency_conf.CommonService.util.SinvieDeviceManager;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieReportDetailBean;
import com.sinvie.emergency_base.bean.SinvieSaveFileBean;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.http.SinvieNetContact;
import com.sinvie.emergency_base.http.base.SinvieBaseObserver;
import com.sinvie.emergency_base.http.common.SinvieCommonNetUtils;
import com.sinvie.emergency_base.http.repository.SinvieBaseURLRepository;
import com.sinvie.emergency_base.ui.base.SinvieMVPBasePresenter;
import com.sinvie.emergency_base.utils.SinvieDemoLog;
import com.sinvie.emergency_base.utils.SinvieFileUtil;
import com.sinvie.emergency_base.utils.SinvieTimeUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/11
 * @Des: 类描述
 */
public class SinvieAddReportDetailPresenter extends SinvieMVPBasePresenter<SinvieAddReportDetailContact.AddReportDetailBaseView> implements SinvieAddReportDetailContact.AddReportDetailBasePresenter {

    private static final String TAG = SinvieAddReportDetailPresenter.class.getSimpleName();
    private Context mContext;

    public SinvieAddReportDetailPresenter(Context context) {
        mContext = context;
    }

    @Override
    public void submitReportDetail(int intentType, String title, String content,
                                   SinvieReportDetailBean.ReportSupplementsRoomListBean bean,
                                   List<String> attachmentIds) {
        //判断网络
        if (!SinvieDeviceManager.isNetworkAvailable(mContext)) {
            SinvieDemoLog.e(TAG, "network has been disconnected");
            if (isViewAtteached()) {
                getView().submitReportDetailFail(SinvieNetContact.FAIL_EMPTY,
                        mContext.getString(R.string.sinvie_network_be_disconnected));
            }
            return;
        }

        if (content.isEmpty()) {
            if (isViewAtteached()) {
                getView().submitReportDetailFail(SinvieNetContact.FAIL_EMPTY,
                        mContext.getString(R.string.sinvie_report_content_is_empty));
            }
            return;
        }
        HashMap<String, String> map = new HashMap<>();
        if (intentType == SinvieUIConstants.REPORT_TYPE_EDIT) {
            map.put("id", bean.getId() + "");
        }
        map.put("title", title);
        map.put("eventInfoId", bean.getEventInfoId() + "");
        map.put("address", bean.getAddress());
        map.put("latitude", bean.getLatitude());
        map.put("longitude", bean.getLongitude());
        map.put("content", content);
        map.put("attachmentIds", new Gson().toJson(attachmentIds));
        map.put("reportTime", SinvieTimeUtil.getTimeData());


//        Log.d(TAG, "onIntentParams: " + new Gson().toJson(attachmentIds));
        SinvieCommonNetUtils.execute(SinvieCommonNetUtils.covert(new SinvieBaseURLRepository().saveReportFollowUp(map)), new SinvieBaseObserver<String>() {
            @Override
            public void onSuccess(@Nullable String s) {
                if (isViewAtteached()) {
                    getView().submitReportDetailSuccess();
                }
            }

            @Override
            public void onFailure(int errCode, @NotNull String errMsg) {
                if (isViewAtteached()) {
                    getView().submitReportDetailFail(errCode, errMsg);
                }
            }
        });
    }


    @Override
    public void upLoadFile(List<String> paths) {
        //判断网络
        if (!SinvieDeviceManager.isNetworkAvailable(mContext)) {
            SinvieDemoLog.e(TAG, "network has been disconnected");
            if (isViewAtteached()) {
                getView().upLoadFileFail(SinvieNetContact.ERROR_NET, mContext.getString(R.string.sinvie_network_be_disconnected));
            }
            return;
        }
        startUpLoadFile(paths);
    }

    //记录上传的次数
//    private int mIndex = 0;

    private void startUpLoadFile(List<String> paths) {
        String type;
        Log.d(TAG, "startUpLoadFile: " + paths.size() + " : " + 0);
        if (SinvieFileUtil.getSuffixFormat(paths.get(0)) == SinvieFileUtil.SUFFIX_FORMAT_IMG) {
            type = "image/*";
        } else if (SinvieFileUtil.getSuffixFormat(paths.get(0)) == SinvieFileUtil.SUFFIX_FORMAT_VIDEO) {
            type = "video/*";
        } else {
            //格式上传错误
            if (isViewAtteached()) {
                getView().upLoadFileFail(SinvieNetContact.ERROR_FILE_SUFFIX, mContext.getString(R.string.sinvie_file_suffix_error));
            }
            return;
        }
        //开始上传
        SinvieCommonNetUtils.execute(SinvieCommonNetUtils.covert(new SinvieBaseURLRepository().saveFile(paths.get(0), type, "1")), new SinvieBaseObserver<SinvieSaveFileBean>() {
            @Override
            public void onFailure(int errCode, @NotNull String errMsg) {
                if (isViewAtteached()) {
                    getView().upLoadFileFail(errCode, errMsg);
                }
            }

            @Override
            public void onSuccess(@Nullable SinvieSaveFileBean sinvieSaveFileBean) {
                //判断文件是否上传完毕
//                mIndex++;
//                if (mIndex == paths.size()) {
//                    //文件上传完毕
//                    mIndex = 0;
//                    if (isViewAtteached()) {
//                        getView().upLoadFileSuccess(saveFileBean,null, true);
//                    }
//                } else {
//                    getView().upLoadFileSuccess(saveFileBean,paths.get(mIndex-1), false);
//                }
                getView().upLoadFileSuccess(sinvieSaveFileBean, null, paths.size() == 1);
            }
        });
    }
}
