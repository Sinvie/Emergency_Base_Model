package com.sinvie.emergency_base.logic.conference;

import android.content.Context;

import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieConfBroadcastConstants;


/**
 * @author: JonsonBob
 * @date: 2021/7/8
 * @Des: 类描述
 */
public class SinvieConfPresenter extends SinvieConfBasePresenter {


    public SinvieConfPresenter(Context context) {
        mContext = context;
        mSinvieConfModel = new SinvieConfModel();
        broadcastNames = new String[]{SinvieConfBroadcastConstants.CONF_INFO_PARAM,

                SinvieConfBroadcastConstants.GET_CONF_LIST_FAILED_RESULT,
                SinvieConfBroadcastConstants.GET_CONF_LIST_RESULT,
                SinvieConfBroadcastConstants.CANCEL_CONF_MSG,

                SinvieConfBroadcastConstants.CONF_STATE_UPDATE,

                //会议连接
                SinvieConfBroadcastConstants.CONF_CALL_CONNECTED,

                //会场结束
                SinvieConfBroadcastConstants.GET_CONF_END,
                SinvieConfBroadcastConstants.ACTION_CALL_END
        };
    }

}
