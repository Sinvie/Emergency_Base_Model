package com.sinvie.emergency_base.logic.report;

import com.sinvie.emergency_base.bean.SinvieReportBean;

/**
 * @author: JonsonBob
 * @date: 2021/7/11
 * @Des: 类描述
 */
public interface SinvieReportContact {
    interface ReportBaseView{

        void getReportDataSuccess(SinvieReportBean beans);

        void getReportDataFail(int errCode,String errMsg);

        void dismissLoadingDialog( );
    }

    interface ReportBasePresenter{

        void  getReportData( int pageIndex,String keyword );
    }
}
