package com.sinvie.emergency_base.logic.conference;

import android.content.Context;

import androidx.annotation.NonNull;

import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieConfBroadcastConstants;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcast;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcastReceiver;
import com.sinvie.emergency_conf.ConferenceService.SinvieConfBaseInfo;
import com.sinvie.emergency_conf.ConferenceService.SinvieMeetingMgr;
import com.sinvie.emergency_base.ui.base.SinvieMVPBasePresenter;
import com.sinvie.emergency_base.utils.SinvieDateUtil;
import com.sinvie.emergency_base.utils.SinvieObjectToListUtils;
import com.sinvie.emergency_base.utils.SinvieTimeUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/10
 * @Des: 类描述
 */
public abstract class SinvieConfBasePresenter extends SinvieMVPBasePresenter<SinvieConferenceContact.ConferenceSinvieBaseView>
        implements SinvieConferenceContact.ConferencePresenter {

    private static final String TAG = SinvieConfBasePresenter.class.getSimpleName();

    protected Context mContext;
    protected SinvieConfModel mSinvieConfModel;

    private List<SinvieConfBaseInfo> mSinvieConfBaseInfoList;
    public static final int MEET_PAGE_SIZE = 10;


    protected String[] broadcastNames;

    @Override
    public void registerBroadcast() {
        SinvieLocBroadcast.getInstance().registerBroadcast(receiver, broadcastNames);
    }

    @Override
    public void unregisterBroadcast() {
        SinvieLocBroadcast.getInstance().unRegisterBroadcast(receiver, broadcastNames);
    }

    @Override
    public void getConferenceList(int pageIndex) {
        SinvieMeetingMgr.getInstance().queryMyConfList(pageIndex, MEET_PAGE_SIZE, SinvieTimeUtil.getTimeData());
    }

    @NonNull
    private Comparator<SinvieConfBaseInfo> getComparator() {
        return new Comparator<SinvieConfBaseInfo>() {
            @Override
            public int compare(SinvieConfBaseInfo lhs, SinvieConfBaseInfo rhs) {
                long time1 = SinvieDateUtil.getInstance().parseDateStr(lhs.getStartTime(), SinvieDateUtil.UTC
                        , SinvieDateUtil.FMT_YMDHM).getTime();
                long time2 = SinvieDateUtil.getInstance().parseDateStr(rhs.getStartTime(), SinvieDateUtil.UTC
                        , SinvieDateUtil.FMT_YMDHM).getTime();
                if (time1 < time2) {
                    return 1;
                }
                return -1;
            }
        };
    }

    /**
     * ----------------------------------------华为会议返回操作----------------------------------------
     */

    private final SinvieLocBroadcastReceiver receiver = (broadcastName, obj) -> {
        switch (broadcastName) {
            case SinvieConfBroadcastConstants.CONF_INFO_PARAM:
                break;
            case SinvieConfBroadcastConstants.GET_CONF_LIST_FAILED_RESULT:
                if (obj != null) {
                    mSinvieConfBaseInfoList = new ArrayList<>();
                    SinvieMeetingMgr.getInstance().setConfBaseInfoList(mSinvieConfBaseInfoList);
                }
                break;
            case SinvieConfBroadcastConstants.GET_CONF_LIST_RESULT:
                if (obj != null) {

                    List<SinvieConfBaseInfo> sinvieConfBaseInfoList = SinvieObjectToListUtils.castList(obj,
                            SinvieConfBaseInfo.class);
                    if (sinvieConfBaseInfoList.size() == 0) {
                        if (isViewAtteached()) {
                            getView().refreshConfListEmpty();
                        }
                    } else {
                        if (isViewAtteached()) {
                            getView().refreshConfList(sinvieConfBaseInfoList);
                        }
                    }

                    //按照时间排序
                    Collections.sort(sinvieConfBaseInfoList, getComparator());
                    SinvieMeetingMgr.getInstance().setConfBaseInfoList(sinvieConfBaseInfoList);
                }
                break;
            case SinvieConfBroadcastConstants.CONF_CALL_CONNECTED:
                if (obj instanceof String) {
                    String confID = (String) obj;
                    if (isViewAtteached()) {
                        getView().joinConfSuccess(confID);
                    }
                }
                break;
            case SinvieConfBroadcastConstants.GET_CONF_END:
            case SinvieConfBroadcastConstants.ACTION_CALL_END:
                if (isViewAtteached()) {
                    getView().leaveConf();
                }
                break;
            default:
                break;
        }
    };
}
