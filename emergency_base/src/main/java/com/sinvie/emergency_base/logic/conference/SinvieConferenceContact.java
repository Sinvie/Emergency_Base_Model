package com.sinvie.emergency_base.logic.conference;

import com.sinvie.emergency_conf.ConferenceService.SinvieConfBaseInfo;
import com.sinvie.emergency_base.logic.SinvieBaseView;

import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/8
 * @Des: 类描述
 */
public interface SinvieConferenceContact {

    interface ConferenceSinvieBaseView extends SinvieBaseView {

        void refreshConfList(List<SinvieConfBaseInfo> tupConfInfoList);

        void refreshConfListEmpty();

        void joinConfSuccess(String confID);

        void leaveConf();

    }

    interface ConferencePresenter {

        void registerBroadcast();

        void unregisterBroadcast();

//       void  receiveBroadcast(String broadcastName, Object obj);

        void getConferenceList(int pageIndex);
    }

}
