package com.sinvie.emergency_base.logic.live_room;

import com.sinvie.emergency_base.ui.scene.adapter.SinvieRoomDifficultListAdapter;

import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/6
 * @Des: 类描述
 */
public interface SinvieLiveRoomContact {

    interface LiveRoomBaseView{
        void getScenesRoomListSuccess(List<SinvieRoomDifficultListAdapter.ScenesRoomInfo> scenesRoomInfos);

        void getScenesRoomListFail(int errCode,String errMsg);
    }

    interface LiveRoomBasePresenter{
        void getScenesRoomList();
    }
}
