package com.sinvie.emergency_base.logic.live_room.live_room_anchor;

import android.content.Context;

import com.sinvie.emergency_base.http.common.SinvieCommonNetUtils;
import com.sinvie.emergency_conf.CommonService.util.SinvieLogUtil;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieRoomBean;
import com.sinvie.emergency_base.bean.SinvieRoomInfo;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.http.SinvieNetContact;
import com.sinvie.emergency_base.http.base.SinvieBaseObserver;
import com.sinvie.emergency_base.http.repository.SinvieBaseURLRepository;
import com.sinvie.emergency_base.ui.base.SinvieMVPBasePresenter;
import com.sinvie.emergency_base.ui.scene.net.SinvieHeartbeatManager;
import com.sinvie.emergency_base.utils.SinvieDemoLog;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * @author: JonsonBob
 * @date: 2021/6/30
 * @Des: 类描述
 */
public class SinvieLiveRoomAnchorPresenter extends SinvieMVPBasePresenter<SinvieLiveRoomAnchorContact.LiveRoomAnchorBaseView> implements SinvieLiveRoomAnchorContact.LiveRoomAnchorBasePresenter {
    private Context mContext;
    private SiniveLiveRoomAnchorModel mSiniveLiveRoomAnchorModel;

    private static final String TAG = SinvieLiveRoomAnchorPresenter.class.getSimpleName();

    public SinvieLiveRoomAnchorPresenter(Context context) {
        mContext = context;
        mSiniveLiveRoomAnchorModel = new SiniveLiveRoomAnchorModel();
    }


    @Override
    public void createRoom(SinvieRoomInfo info) {
        RequestBody formBody = new MultipartBody.Builder()
                .addFormDataPart(SinvieNetContact.REQUEST_PARAM_USER_ID, info.getUserId())
                .addFormDataPart(SinvieNetContact.REQUEST_PARAM_ROOM_ID, info.getRoomId())
                .addFormDataPart(SinvieNetContact.REQUEST_PARAM_LIVE_TYPE, info.getLiveType() + "")
                .addFormDataPart(SinvieNetContact.REQUEST_PARAM_LIVE_ADDRESS, info.getLiveAddress() + "")
                .addFormDataPart(SinvieNetContact.REQUEST_PARAM_LIVE_START_TIME, info.getStartTime())
                .addFormDataPart(SinvieNetContact.REQUEST_PARAM_LIVE_STATUS, info.getLiveStatus() + "")
                .build();
        SinvieCommonNetUtils.execute(SinvieCommonNetUtils.covert(new SinvieBaseURLRepository().saveRoom(formBody)), new SinvieBaseObserver<SinvieRoomBean>() {
            @Override
            public void onSuccess(@Nullable SinvieRoomBean sinvieRoomBean) {
                if (isViewAtteached()) {
                    info.setId(sinvieRoomBean.getId() + "");
                    SinvieHeartbeatManager.getInstance().start(info);
                }
            }

            @Override
            public void onFailure(int errCode, @NotNull String errMsg) {
                if (isViewAtteached()) {
                    SinvieDemoLog.d(TAG, "createRoom: errorCode --> " + errCode + " , errorMsg -->" + errMsg);
                    getView().destroyRoomFail(errCode, errMsg);
                }
            }

        });
    }

    @Override
    public void destroyRoom(SinvieRoomInfo info) {
        if (info.getId() == null) {
            getView().destroyRoomFail(SinvieUIConstants.LIVE_ROOM_DESTROY_ERROR, mContext.getString(R.string.sinvie_live_room_error_service_suspended));
            return;
        }
        MultipartBody.Builder builder = new MultipartBody.Builder()
                .addFormDataPart(SinvieNetContact.REQUEST_PARAM_ID, info.getId())
                .addFormDataPart(SinvieNetContact.REQUEST_PARAM_USER_ID,info.getUserId())
                .addFormDataPart(SinvieNetContact.REQUEST_PARAM_ROOM_ID, info.getRoomId())
                .addFormDataPart(SinvieNetContact.REQUEST_PARAM_LIVE_TYPE, info.getLiveType() + "")
                .addFormDataPart(SinvieNetContact.REQUEST_PARAM_LIVE_ADDRESS, info.getLiveAddress() + "")
                .addFormDataPart(SinvieNetContact.REQUEST_PARAM_LIVE_STATUS, info.getLiveStatus() + "");
        if (info.getEndTime() != null) {
            builder.addFormDataPart(SinvieNetContact.REQUEST_PARAM_LIVE_END_TIME, info.getEndTime());
        }
        if (info.getStartTime() != null) {
            builder.addFormDataPart(SinvieNetContact.REQUEST_PARAM_LIVE_START_TIME, info.getStartTime());
        }
        RequestBody formBody = builder.build();

        SinvieCommonNetUtils.execute(SinvieCommonNetUtils.covert(new SinvieBaseURLRepository().updateRoom(formBody)), new SinvieBaseObserver<SinvieRoomBean>() {
            @Override
            public void onSuccess(@Nullable SinvieRoomBean sinvieRoomBean) {
                if (isViewAtteached()) {
                    SinvieHeartbeatManager.getInstance().stop();
                }
            }

            @Override
            public void onFailure(int errCode, @NotNull String errMsg) {
                if (isViewAtteached()) {
                    SinvieLogUtil.d(TAG, "destroyRoom: errorCode --> " + errCode + " , errorMsg -->" + errMsg);
                    getView().destroyRoomFail(errCode, errMsg);
                }
            }

        });
    }
}
