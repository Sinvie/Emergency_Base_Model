package com.sinvie.emergency_base.logic.live_room.live_room_audience;

/**
 * @author: JonsonBob
 * @date: 2021/6/30
 * @Des: 类描述
 */
public interface SinvieLiveRoomAudienceContact {

    interface LiveRoomAudienceBaseView{

    }

    interface LiveRoomAudienceBasePresenter{

    }
}
