package com.sinvie.emergency_base.logic.conference.data_conf;


import android.content.Context;
import android.view.SurfaceView;
import android.view.ViewGroup;

import com.huawei.ecterminalsdk.base.TsdkConfChatMsgInfo;
import com.huawei.ecterminalsdk.base.TsdkConfRole;
import com.sinvie.emergency_conf.CallService.SinvieCallMgr;
import com.sinvie.emergency_conf.CallService.SinvieVideoMgr;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieConfBroadcastConstants;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcast;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcastReceiver;
import com.sinvie.emergency_conf.ConferenceService.SinvieMeetingMgr;
import com.sinvie.emergency_conf.ConferenceService.SinvieMember;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.ui.base.SinvieMVPBasePresenter;


public class SinvieDataConfPresenter extends SinvieMVPBasePresenter<SinvieIDataConfContract.DataConfViewSinvie>
        implements  SinvieIDataConfContract.IDataConfPresenter {
    private String confID;

    private String[] broadcastNames = new String[]{
            SinvieConfBroadcastConstants.DATE_CONFERENCE_START_SHARE_STATUS,
            SinvieConfBroadcastConstants.DATE_CONFERENCE_END_SHARE_STATUS,
            SinvieConfBroadcastConstants.DATE_CONFERENCE_AUX_DATA_STATE,
            SinvieConfBroadcastConstants.DATE_CONFERENCE_CHAT_MSG,
            SinvieConfBroadcastConstants.GET_CONF_END};

    private SinvieLocBroadcastReceiver receiver = new SinvieLocBroadcastReceiver() {
        @Override
        public void onReceive(String broadcastName, Object obj) {
            switch (broadcastName) {
                case SinvieConfBroadcastConstants.DATE_CONFERENCE_START_SHARE_STATUS:
                    getView().startAsShare(true);
                    break;

                case SinvieConfBroadcastConstants.DATE_CONFERENCE_END_SHARE_STATUS:
                    getView().startAsShare(false);
                    getView().showCustomToast(R.string.sinvie_share_end);
                    break;

                case SinvieConfBroadcastConstants.DATE_CONFERENCE_AUX_DATA_STATE:
                    boolean auxDataState = (boolean) obj;
                    getView().setAuxDataState(auxDataState);
                    break;

                case SinvieConfBroadcastConstants.GET_CONF_END:
                    getView().finishActivity();
                    break;

                case SinvieConfBroadcastConstants.DATE_CONFERENCE_CHAT_MSG:
                    TsdkConfChatMsgInfo chatMsgInfo = (TsdkConfChatMsgInfo) obj;
                    String msgInfo = chatMsgInfo.getChatMsg();
                    String userName = chatMsgInfo.getSenderDisplayName();
                    String userNumber = chatMsgInfo.getSenderNumber();
                    boolean isSelfMsg = false;

                    SinvieMember self = SinvieMeetingMgr.getInstance().getCurrentConferenceSelf();
                    if (null != self) {
                        if (self.getDisplayName().equals(userName) || self.getNumber().equals(userNumber)) {
                            isSelfMsg = true;
                        }
                    }

                    if (null == userName || "".equals(userName)) {
                        if (null == userNumber || "".equals(userNumber)) {
                            getView().displayConfChatMag(isSelfMsg, "The sender's name was not " +
                                    "obtained.");
                        } else {
                            getView().displayConfChatMag(isSelfMsg, userNumber + ": " + msgInfo);
                        }
                        return;
                    }
                    getView().displayConfChatMag(isSelfMsg, userName + ": " + msgInfo);
                    break;

                default:
                    break;
            }
        }
    };

    @Override
    public void attachSurfaceView(ViewGroup container, Context context) {
//        MeetingMgr.getInstance().attachSurfaceView(container, context);
    }

    @Override
    public void sendChatMsg(String content) {
//        MeetingMgr.getInstance().sendConfMessage(content);
    }

    public void sendConfData(int dataType, String content) {
        if ("".equals(dataType)) {
            dataType = 0;
        }
//        MeetingMgr.getInstance().sendConfData(dataType,content);
    }

    @Override
    public void setConfID(String confID) {
        this.confID = confID;
    }

    public String getSubject() {
        return SinvieMeetingMgr.getInstance().getCurrentConferenceBaseInfo().getSubject();
    }

    @Override
    public void closeConf() {
        int result = SinvieMeetingMgr.getInstance().leaveConf();
        if (result != 0) {
            getView().showCustomToast(R.string.sinvie_leave_conf_fail);
            return;
        }
    }

    @Override
    public void finishConf() {
        int result = SinvieMeetingMgr.getInstance().endConf();
        if (result != 0) {
            getView().showCustomToast(R.string.sinvie_end_audio_conf);
            return;
        }
    }


    @Override
    public boolean muteSelf() {
        SinvieMember self = SinvieMeetingMgr.getInstance().getCurrentConferenceSelf();

        if (self == null) {
            return false;
        }
        int result = SinvieMeetingMgr.getInstance().muteAttendee(self, !self.isMute());
        if (result != 0) {
            return false;
        }
        return true;
    }

    @Override
    public int switchLoudSpeaker() {
        return SinvieCallMgr.getInstance().switchAudioRoute();
    }

    @Override
    public boolean isChairMan() {
        SinvieMember self = SinvieMeetingMgr.getInstance().getCurrentConferenceSelf();

        return (self.getRole() == TsdkConfRole.TSDK_E_CONF_ROLE_ATTENDEE ? false : true);
    }

    @Override
    public void registerBroadcast() {
        SinvieLocBroadcast.getInstance().registerBroadcast(receiver, broadcastNames);
    }

    @Override
    public void unregisterBroadcast() {
        SinvieLocBroadcast.getInstance().unRegisterBroadcast(receiver, broadcastNames);
    }

    @Override
    public SurfaceView getHideVideoView() {
        return SinvieVideoMgr.getInstance().getLocalHideView();
    }

    @Override
    public SurfaceView getLocalVideoView() {
        return SinvieVideoMgr.getInstance().getLocalVideoView();
    }

    @Override
    public SurfaceView getAuxDataView() {
        return SinvieVideoMgr.getInstance().getAuxDataView();
    }

    @Override
    public void setVideoContainer(Context context, ViewGroup smallLayout, ViewGroup hideLayout,
                                  ViewGroup auxDataLayout) {
        if (smallLayout != null) {
            addSurfaceView(smallLayout, getLocalVideoView());
        }

        if (hideLayout != null) {
            addSurfaceView(hideLayout, getHideVideoView());
        }

        if (auxDataLayout != null) {
            addSurfaceView(auxDataLayout, getAuxDataView());
        }
    }

    private void addSurfaceView(ViewGroup container, SurfaceView child) {
        if (child == null) {
            return;
        }
        if (child.getParent() != null) {
            ViewGroup vGroup = (ViewGroup) child.getParent();
            vGroup.removeAllViews();
        }
        container.addView(child);
    }
}
