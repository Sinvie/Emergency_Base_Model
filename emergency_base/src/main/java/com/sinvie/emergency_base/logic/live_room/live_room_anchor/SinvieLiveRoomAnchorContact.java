package com.sinvie.emergency_base.logic.live_room.live_room_anchor;

import com.sinvie.emergency_base.bean.SinvieRoomInfo;

/**
 * @author: JonsonBob
 * @date: 2021/6/30
 * @Des: 类描述
 */
public interface SinvieLiveRoomAnchorContact {
    interface LiveRoomAnchorBaseView {

        void createRoomFail(int errCode,String errMsg);

        void destroyRoomSuccess(String msg);

        void destroyRoomFail(int errCode,String errMsg);
    }

    interface LiveRoomAnchorBasePresenter {
        void createRoom(SinvieRoomInfo info );

        void destroyRoom(SinvieRoomInfo info );
    }
}
