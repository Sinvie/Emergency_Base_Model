package com.sinvie.emergency_base.logic.report_manager;

import com.sinvie.emergency_base.bean.SinvieReportBean;

/**
 * @author: JonsonBob
 * @date: 2021/8/23
 * @Des: 类描述
 */
public class SinvieReportManagerContact {
    public interface ReportManagerBaseView {
        void getReportManagerSuccess(SinvieReportBean beans);

        void getReportManagerFail(int errCode, String errMsg);

        void dismissLoadingDialog();
    }

    public interface ReportManagerBasePresenter {
        void getReportManagerList(int index,String keyWorld);
    }
}
