package com.sinvie.emergency_base.logic.login;

/**
 * @author: JonsonBob
 * @date: 2021/6/30
 * @Des: 类描述
 */
public interface SinvieLoginFragToActListener {

    void onSentCode();

    void onLockLoginBtn(boolean isLockLogin);
}
