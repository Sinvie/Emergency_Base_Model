package com.sinvie.emergency_base.logic.report.report_detail;

import com.sinvie.emergency_base.bean.SinvieReportDetailBean;

/**
 * @author: JonsonBob
 * @date: 2021/7/11
 * @Des: 类描述
 */
public interface SinvieReportDetailContact {

    interface ReportDetailBaseView{

        void getReportDetailInfoSuccess(SinvieReportDetailBean bean);

        void getReportDetailInfoFail(int errCode,String errMsg);

        void delReportDetailInfoSuccess();

        void delReportDetailInfoFail(int errCode,String errMsg);

        void deleteReportFollowUpSuccess(int id);

        void deleteReportFollowUpFail(int errCode,String errMsg);

    }

    interface ReportDetailBasePresenter{

        /**
         * 获取事报详情
         * @param id 事报id
         */
        void getReportDetailInfo(int id);

        /**
         * 删除事报
         * @param id 事报id
         */
        void delReportDetailInfo(int id);

        /**
         * 删除续报
         * @param id 续报id
         */
        void deleteReportFollowUp(int id);
    }
}
