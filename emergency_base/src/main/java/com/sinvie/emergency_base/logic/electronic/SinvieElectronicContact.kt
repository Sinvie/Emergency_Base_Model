package com.sinvie.emergency_base.logic.electronic

import com.sinvie.emergency_base.bean.electronic.*

interface SinvieElectronicContact {
    interface ElectronicBaseView {
        fun getReportDataSuccess(beans: List<ResourceTypeBean>?)
        fun getReportDataFail(errCode: Int, errMsg: String?)
        fun getResourceFail(errMsg: String?)
        fun getDeviceInfoSuccess(device: DeviceInfoBean)
        fun getRiskInfoSuccess(risk: RiskInfo)
        fun getEquipLibInfoSuccess(equipLib: String)
        fun getProtectTargetSuccess(protect: ProtectBean)
        fun getRefugeSuccess(refuge: String)
    }

    interface ElectronicBasePresenter {
        val electronicData: Unit
    }
}