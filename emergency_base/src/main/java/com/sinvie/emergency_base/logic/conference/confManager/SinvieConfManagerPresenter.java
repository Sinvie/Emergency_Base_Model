package com.sinvie.emergency_base.logic.conference.confManager;


import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import com.huawei.ecterminalsdk.base.TsdkConfRole;
import com.sinvie.emergency_conf.CallService.SinvieCallConstant;
import com.sinvie.emergency_conf.CallService.SinvieCallMgr;
import com.sinvie.emergency_conf.CallService.SinvieVideoMgr;
import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieConfBroadcastConstants;
import com.sinvie.emergency_conf.ConferenceService.SinvieConfBaseInfo;
import com.sinvie.emergency_conf.ConferenceService.SinvieConfConstant;
import com.sinvie.emergency_conf.ConferenceService.SinvieMeetingMgr;
import com.sinvie.emergency_conf.ConferenceService.SinvieMember;
import com.sinvie.emergency_base.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class SinvieConfManagerPresenter extends SinvieConfManagerBasePresenter {
    private static final int ADD_LOCAL_VIEW = 101;

    private boolean is_refresh_view = false;
    private int mCameraIndex = SinvieCallConstant.FRONT_CAMERA;

    private MyHandler mHandler;

    private class MyHandler extends Handler {

        private WeakReference<SinvieConfManagerPresenter> mReference;

        public MyHandler(Looper looper,  SinvieConfManagerPresenter presenter) {
            super(looper);
            mReference = new WeakReference<SinvieConfManagerPresenter>(presenter);
        }

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == ADD_LOCAL_VIEW) {
                if (getView() != null) {
                    getView().updateLocalVideo();
                }
            }
        }
    }

    public SinvieConfManagerPresenter() {
        broadcastNames = new String[]{SinvieConfBroadcastConstants.CONF_STATE_UPDATE,
                SinvieConfBroadcastConstants.GET_DATA_CONF_PARAM_RESULT,
                SinvieConfBroadcastConstants.DATA_CONFERENCE_JOIN_RESULT,
                SinvieConfBroadcastConstants.ADD_LOCAL_VIEW,
                SinvieConfBroadcastConstants.DEL_LOCAL_VIEW,
                SinvieConfBroadcastConstants.DATE_CONFERENCE_START_SHARE_STATUS,
                SinvieConfBroadcastConstants.DATE_CONFERENCE_END_SHARE_STATUS,
                SinvieConfBroadcastConstants.DATE_CONFERENCE_AUX_DATA_STATE,
                SinvieConfBroadcastConstants.UPGRADE_CONF_RESULT,
                SinvieConfBroadcastConstants.UN_MUTE_CONF_RESULT,
                SinvieConfBroadcastConstants.MUTE_CONF_RESULT,
                SinvieConfBroadcastConstants.LOCK_CONF_RESULT,
                SinvieConfBroadcastConstants.UN_LOCK_CONF_RESULT,
                SinvieConfBroadcastConstants.ADD_ATTENDEE_RESULT,
                SinvieConfBroadcastConstants.DEL_ATTENDEE_RESULT,
                SinvieConfBroadcastConstants.MUTE_ATTENDEE_RESULT,
                SinvieConfBroadcastConstants.UN_MUTE_ATTENDEE_RESULT,
                SinvieConfBroadcastConstants.HAND_UP_RESULT,
                SinvieConfBroadcastConstants.CANCEL_HAND_UP_RESULT,
                SinvieConfBroadcastConstants.SET_CONF_MODE_RESULT,
                SinvieConfBroadcastConstants.WATCH_ATTENDEE_CONF_RESULT,
                SinvieConfBroadcastConstants.BROADCAST_ATTENDEE_CONF_RESULT,
                SinvieConfBroadcastConstants.CANCEL_BROADCAST_CONF_RESULT,
                SinvieConfBroadcastConstants.REQUEST_CHAIRMAN_RESULT,
                SinvieConfBroadcastConstants.RELEASE_CHAIRMAN_RESULT,
                SinvieConfBroadcastConstants.SPEAKER_LIST_IND,
                SinvieConfBroadcastConstants.GET_CONF_END};
        mHandler = new MyHandler(Looper.myLooper(),this);
    }

    @Override
    protected void onBroadcastReceive(String broadcastName, Object obj) {
        switch (broadcastName) {
            case SinvieConfBroadcastConstants.CONF_STATE_UPDATE:
                if (!is_refresh_view && mHandler != null) {
                    mHandler.sendEmptyMessage(ADD_LOCAL_VIEW);
                    is_refresh_view = true;
                }
                break;

            case SinvieConfBroadcastConstants.ADD_LOCAL_VIEW:
                if (mHandler != null) {
                    mHandler.sendEmptyMessage(ADD_LOCAL_VIEW);
                }
                break;

            case SinvieConfBroadcastConstants.DEL_LOCAL_VIEW:
                break;

            default:
                break;
        }
    }

    @Override
    public void switchCamera() {
        int callID = SinvieMeetingMgr.getInstance().getCurrentConferenceCallID();
        if (callID == 0) {
            return;
        }

        mCameraIndex = SinvieCallConstant.FRONT_CAMERA == mCameraIndex ?
                SinvieCallConstant.BACK_CAMERA : SinvieCallConstant.FRONT_CAMERA;

        SinvieCallMgr.getInstance().switchCamera(callID, mCameraIndex);
    }

    @Override
    public void setVideoContainer(Context context, ViewGroup smallLayout, ViewGroup bigLayout,
                                  ViewGroup hideLayout) {
        if (bigLayout != null) {
            addSurfaceView(bigLayout, getRemoteVideoView());
        }

        if (smallLayout != null) {
            addSurfaceView(smallLayout, getLocalVideoView());
        }

        if (hideLayout != null) {
            addSurfaceView(hideLayout, getHideVideoView());
        }
    }

    @Override
    public void setVideoContainer(Context context, ViewGroup smallLayout, ViewGroup bigLayoutA,
                                  ViewGroup bigLayoutB, ViewGroup bigLayoutC,
                                  ViewGroup bigLayoutD, ViewGroup hideLayout) {

        ArrayList<SurfaceView> svcRemoteVideoView = getSvcRemoteVideoView();
        if (svcRemoteVideoView != null && svcRemoteVideoView.size() > 0) {
            for (int i = 0; i < svcRemoteVideoView.size(); i++) {
                switch (i) {
                    case 0:
                        if (bigLayoutA != null) {
                            addSurfaceView(bigLayoutA, svcRemoteVideoView.get(i));
                        }
                        break;
                    case 1:
                        if (bigLayoutB != null) {
                            addSurfaceView(bigLayoutB, svcRemoteVideoView.get(i));
                        }
                        break;
                    case 2:
                        if (bigLayoutC != null) {
                            addSurfaceView(bigLayoutC, svcRemoteVideoView.get(i));
                        }
                        break;
                    case 3:
                        if (bigLayoutD != null) {
                            addSurfaceView(bigLayoutD, svcRemoteVideoView.get(i));
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        if (smallLayout != null) {
            addSurfaceView(smallLayout, getLocalVideoView());
        }

        if (hideLayout != null) {
            addSurfaceView(hideLayout, getHideVideoView());
        }
    }

    @Override
    public void setAutoRotation(Object object, boolean isOpen, int orientation) {
        SinvieVideoMgr.getInstance().setAutoRotation(object, isOpen, orientation);
    }

    @Override
    public void attachRemoteVideo(long userID, long deviceID) {
        //do nothing
    }

    @Override
    public void watchAttendee(SinvieMember sinvieMember) {
        int result = SinvieMeetingMgr.getInstance().watchAttendee(sinvieMember);
        if (0 != result) {
            getView().showCustomToast(R.string.sinvie_watch_conf_fail);
        }
    }

    @Override
    public void setConfMode(SinvieConfConstant.ConfVideoMode confVideoMode) {
        getView().showCustomToast(R.string.sinvie_unsupport_function);
    }

    @Override
    public void shareSelfVideo(long deviceID) {
        //do nothing
    }

    @Override
    public void leaveVideo() {
        //do nothing
    }

    @Override
    public void changeLocalVideoVisible(boolean visible) {
        if (visible) {
            //重新显示本地窗口，无需再打开本地视频
            getLocalVideoView().setVisibility(View.VISIBLE);
        } else {
            //只隐藏本地窗口，并不关闭本地视频
            getLocalVideoView().setVisibility(View.GONE);
        }
    }

    @Override
    public boolean closeOrOpenLocalVideo(boolean close) {
        int callID = SinvieMeetingMgr.getInstance().getCurrentConferenceCallID();
        if (callID == 0) {
            return false;
        }

        if (close) {
            SinvieCallMgr.getInstance().closeCamera(callID);
        } else {
            SinvieCallMgr.getInstance().openCamera(callID);
            SinvieVideoMgr.getInstance().setVideoOrient(callID, SinvieCallConstant.FRONT_CAMERA);
        }

        return true;
    }

    @Override
    public SurfaceView getHideVideoView() {
        return SinvieVideoMgr.getInstance().getLocalHideView();
    }

    @Override
    public SurfaceView getLocalVideoView() {
        return SinvieVideoMgr.getInstance().getLocalVideoView();
    }

    @Override
    public SurfaceView getRemoteVideoView() {
        return SinvieVideoMgr.getInstance().getRemoteVideoView();
    }

    @Override
    public ArrayList<SurfaceView> getSvcRemoteVideoView() {
        return SinvieVideoMgr.getInstance().getSvcViewList();
    }

    @Override
    public void onItemClick(int position) {
        List<Object> items = new ArrayList<>();
        addLabel(items, position);
        if (!items.isEmpty()) {
            getView().showItemClickDialog(items,
                    SinvieMeetingMgr.getInstance().getCurrentConferenceMemberList().get(position));
        }
    }

    @Override
    public void onItemDetailClick(String clickedItem, SinvieMember sinvieConfMemberEntity) {
        if (SinvieLocContext.getString(R.string.sinvie_watch_contact).equals(clickedItem)) {
            watchAttendee(sinvieConfMemberEntity);
        } else if (SinvieLocContext.getString(R.string.sinvie_broadcast_mode).equals(clickedItem)) {
            setConfMode(SinvieConfConstant.ConfVideoMode.CONF_VIDEO_BROADCAST);
        } else if (SinvieLocContext.getString(R.string.sinvie_voice_control_mode).equals(clickedItem)) {
            setConfMode(SinvieConfConstant.ConfVideoMode.CONF_VIDEO_VAS);
        } else if (SinvieLocContext.getString(R.string.sinvie_free_mode).equals(clickedItem)) {
            setConfMode(SinvieConfConstant.ConfVideoMode.CONF_VIDEO_FREE);
        }
    }

    @Override
    public void requestChairman(String chairmanPassword) {
        int result = SinvieMeetingMgr.getInstance().requestChairman(chairmanPassword);
        if (result != 0) {
            getView().showCustomToast(R.string.sinvie_request_chairman_fail);
            return;
        }
    }

    @Override
    public void releaseChairman() {
        int result = SinvieMeetingMgr.getInstance().releaseChairman();
        if (result != 0) {
            getView().showCustomToast(R.string.sinvie_release_chairman_fail);
            return;
        }
    }

    @Override
    public void muteConf(boolean isMute) {
        int result = SinvieMeetingMgr.getInstance().muteConf(isMute);
        if (result != 0) {
            if (isMute) {
                getView().showCustomToast(R.string.sinvie_mute_conf_fail);
            } else {
                getView().showCustomToast(R.string.sinvie_un_mute_conf_fail);
            }
        }
    }

    @Override
    public void lockConf(boolean isLock) {
        int result = SinvieMeetingMgr.getInstance().lockConf(isLock);
        if (result != 0) {
            if (isLock) {
                getView().showCustomToast(R.string.sinvie_lock_conf_fail);
            } else {
                getView().showCustomToast(R.string.sinvie_un_lock_conf_fail);
            }
        }
    }

    @Override
    public void recordConf(boolean isRecord) {
        int result = SinvieMeetingMgr.getInstance().recordConf(isRecord);
        if (result != 0) {
            if (isRecord) {
                getView().showCustomToast(R.string.sinvie_start_record_fail);
            } else {
                getView().showCustomToast(R.string.sinvie_stop_record_fail);
            }
        }
    }

    @Override
    public void handUpSelf() {
        SinvieMember self = getSelf();
        if (self == null) {
            return;
        }

        boolean isHandUp = !self.isHandUp();
        int result = SinvieMeetingMgr.getInstance().handup(isHandUp, self);
        if (result != 0) {
            if (isHandUp) {
                getView().showCustomToast(R.string.sinvie_handup_fail);

            } else {
                getView().showCustomToast(R.string.sinvie_cancel_handup_fail);
            }
        }
    }

    @Override
    public boolean isHandUp() {
        SinvieMember self = getSelf();
        if (self == null) {
            return false;
        }
        return self.isHandUp();
    }

    @Override
    public boolean isReqTalk() {
        SinvieMember self = getSelf();
        if (self == null) {
            return false;
        }
        return self.isReqTalk();
    }

    @Override
    public boolean isConfMute() {
        SinvieConfBaseInfo sinvieConfBaseInfo = SinvieMeetingMgr.getInstance().getCurrentConferenceBaseInfo();
        if (sinvieConfBaseInfo == null) {
            return false;
        }
        return sinvieConfBaseInfo.isMuteAll();
    }

    @Override
    public boolean isConfLock() {
        SinvieConfBaseInfo sinvieConfBaseInfo = SinvieMeetingMgr.getInstance().getCurrentConferenceBaseInfo();
        if (sinvieConfBaseInfo == null) {
            return false;
        }
        return sinvieConfBaseInfo.isLock();
    }

    @Override
    public boolean isRecord() {
        SinvieConfBaseInfo sinvieConfBaseInfo = SinvieMeetingMgr.getInstance().getCurrentConferenceBaseInfo();
        if (sinvieConfBaseInfo == null) {
            return false;
        }
        return sinvieConfBaseInfo.isRecord();
    }

    @Override
    public boolean isSupportRecord() {
        SinvieConfBaseInfo sinvieConfBaseInfo = SinvieMeetingMgr.getInstance().getCurrentConferenceBaseInfo();
        if (sinvieConfBaseInfo == null) {
            return false;
        }
        return sinvieConfBaseInfo.isSupportRecord();
    }

    @Override
    public void updateConf() {
        int result = SinvieMeetingMgr.getInstance().upgradeConf();
        if (result != 0) {
            getView().showCustomToast(R.string.sinvie_upgrade_conf_fail);
            return;
        }
    }

    @Override
    public void addMember(String name, String number, String account) {
        SinvieMember sinvieMember = new SinvieMember();
        sinvieMember.setNumber(number);
        sinvieMember.setDisplayName(name);
        sinvieMember.setAccountId(account);
        sinvieMember.setRole(TsdkConfRole.TSDK_E_CONF_ROLE_ATTENDEE);

        int result = SinvieMeetingMgr.getInstance().addAttendee(sinvieMember);
        if (result != 0) {
            getView().showCustomToast(R.string.sinvie_add_attendee_fail);
        }
    }

    private SinvieMember getSelf() {
        return SinvieMeetingMgr.getInstance().getCurrentConferenceSelf();
    }

    private void addSurfaceView(ViewGroup container, SurfaceView child) {
        if (child == null) {
            return;
        }
        if (child.getParent() != null) {
            ViewGroup vGroup = (ViewGroup) child.getParent();
            vGroup.removeAllViews();
        }
        container.addView(child);
    }

    private void addLabel(List<Object> items, int position) {
        SinvieMember sinvieMember = SinvieMeetingMgr.getInstance().getCurrentConferenceMemberList().get(position);

        switch (sinvieMember.getStatus()) {
            case IN_CONF:
                if (isChairMan()) {
                    items.add(SinvieLocContext.getString(R.string.sinvie_broadcast_contact));
                    items.add(SinvieLocContext.getString(R.string.sinvie_cancel_broadcast_contact));
                    items.add(SinvieLocContext.getString(R.string.sinvie_watch_contact));
                } else {
                    items.add(SinvieLocContext.getString(R.string.sinvie_watch_contact));
                }
                break;

            default:
                break;
        }
    }

}
