package com.sinvie.emergency_base.logic.login;

/**
 * @author: JonsonBob
 * @date: 2021/6/29
 * @Des: 登录 MVP 中心控制类
 */
public interface SinvieILoginContact {

    interface LoginBaseView {

        void getMobileCodeSuccess( );

        void getMobileCodeFail(int errCode,String errMsg);

        void onLoginSuccess();

        void onLoginFail(int errCode,String errDes);

        void getUserSigSuccess();

        void getUserSigFail(int errCode,String errDes);

        void onHWLoginSuccess(String msg);

        void onHWLoginFail(int errCode,String errDes);

        void onIMLoginSuccess();

        void onIMLoginFail(int errCode, String errDes);

        void dismissLoginDialog(boolean isSuccess);

        void showToast(int resId);



    }

    interface LoginPresenter {

        /**
         * 登录到服务后台
         * @param account 用户账号
         * @param pwd 密码
         */
        void doLogin(String account,String pwd,int loginType);

        void getUserSig();

        void getMobileCode(String mobile);

        /**
         * ----------------------------------华为  -------------------------------------
         */

        void registerBroadcast();

        void unRegisterBroadcast();
        /**
         * 执行华为音视频登录操作
         */
        void doHWVideoCallLogin( );


        /**
         * ---------------------------------腾讯 -----------------------------------------
         */
        /**
         * 执行腾讯 IM音视频登录操作
         */
        void doIMVideoCallLogin( );

        /**
         * 执行华为音视频拨打匿名电话
         *
         * @param callNumber
         * @param isVideo
         * @param primaryIp
         * @param primaryPort
         * @param backupIp1
         * @param backupPort1
         * @param sipPort
         * @param isVpn
         */
        void doAnonymousCall(String callNumber, boolean isVideo, String primaryIp,
                             String primaryPort,
                             String backupIp1, String backupPort1, String sipPort, boolean isVpn);

        void destroyPresenter();
    }
}
