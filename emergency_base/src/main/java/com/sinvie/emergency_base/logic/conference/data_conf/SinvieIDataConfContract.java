package com.sinvie.emergency_base.logic.conference.data_conf;

import android.content.Context;
import android.view.SurfaceView;
import android.view.ViewGroup;

import com.sinvie.emergency_base.logic.SinvieBaseView;


public interface SinvieIDataConfContract
{
    interface DataConfViewSinvie extends SinvieBaseView
    {
        void finishActivity();
        void startAsShare(boolean isShare);
        void setAuxDataState(boolean auxDataState);
        void displayConfChatMag(boolean isSelf, String msg);
    }

    interface IDataConfPresenter
    {
        void setConfID(String confID);

        String getSubject();

        boolean muteSelf();

        int switchLoudSpeaker();

        boolean isChairMan();

        void closeConf();

        void finishConf();

        void attachSurfaceView(ViewGroup container, Context context);

        void sendChatMsg(String content);

        void registerBroadcast();

        void unregisterBroadcast();

        SurfaceView getHideVideoView();

        SurfaceView getLocalVideoView();

        SurfaceView getAuxDataView();

        void setVideoContainer(Context context, ViewGroup smallLayout, ViewGroup hideLayout, ViewGroup auxDataLayout);
    }
}
