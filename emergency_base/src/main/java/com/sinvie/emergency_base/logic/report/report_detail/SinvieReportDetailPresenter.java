package com.sinvie.emergency_base.logic.report.report_detail;

import android.content.Context;

import com.sinvie.emergency_conf.CommonService.util.SinvieDeviceManager;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieReportDetailBean;
import com.sinvie.emergency_base.http.SinvieNetContact;
import com.sinvie.emergency_base.http.base.SinvieBaseObserver;
import com.sinvie.emergency_base.http.common.SinvieCommonNetUtils;
import com.sinvie.emergency_base.http.repository.SinvieBaseURLRepository;
import com.sinvie.emergency_base.ui.base.SinvieMVPBasePresenter;
import com.sinvie.emergency_base.utils.SinvieDemoLog;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * @author: JonsonBob
 * @date: 2021/7/11
 * @Des: 类描述
 */
public class SinvieReportDetailPresenter extends SinvieMVPBasePresenter<SinvieReportDetailContact.ReportDetailBaseView> implements SinvieReportDetailContact.ReportDetailBasePresenter {

    private static final String TAG = SinvieReportDetailPresenter.class.getSimpleName();
    private Context mContext;

    public SinvieReportDetailPresenter(Context context) {
        mContext = context;
    }

    @Override
    public void getReportDetailInfo(int id) {
        //判断网络
        if (!SinvieDeviceManager.isNetworkAvailable(mContext)) {
            SinvieDemoLog.e(TAG, "network has been disconnected");
            if (isViewAtteached()) {
                getView().getReportDetailInfoFail(SinvieNetContact.ERROR_NET,
                        mContext.getString(R.string.sinvie_network_be_disconnected));
            }
            return;
        }

        SinvieCommonNetUtils.execute(SinvieCommonNetUtils.covert(new SinvieBaseURLRepository().getReportDetailInfo(id + "")), new SinvieBaseObserver<SinvieReportDetailBean>() {
            @Override
            public void onFailure(int errCode, @NotNull String errMsg) {
                if (isViewAtteached()) {
                    getView().getReportDetailInfoFail(errCode, errMsg);
                }
            }

            @Override
            public void onSuccess(@Nullable SinvieReportDetailBean bean) {
                if (isViewAtteached()) {
                    getView().getReportDetailInfoSuccess(bean);
                }
            }
        });
    }

    @Override
    public void delReportDetailInfo(int id) {
        //判断网络
        if (!SinvieDeviceManager.isNetworkAvailable(mContext)) {
            SinvieDemoLog.e(TAG, "network has been disconnected");
            if (isViewAtteached()) {
                getView().delReportDetailInfoFail(SinvieNetContact.ERROR_NET,
                        mContext.getString(R.string.sinvie_network_be_disconnected));
            }
            return;
        }

        SinvieCommonNetUtils.execute(SinvieCommonNetUtils.covert(new SinvieBaseURLRepository().deleteReportEvent(id + "")), new SinvieBaseObserver<String>() {
            @Override
            public void onFailure(int errCode, @NotNull String errMsg) {
                if (isViewAtteached()) {
                    getView().delReportDetailInfoFail(errCode, errMsg);
                }
            }

            @Override
            public void onSuccess(@Nullable String bean) {
                if (isViewAtteached()) {
                    getView().delReportDetailInfoSuccess( );
                }
            }
        });
    }

    @Override
    public void deleteReportFollowUp(int id) {
        //判断网络
        if (!SinvieDeviceManager.isNetworkAvailable(mContext)) {
            SinvieDemoLog.e(TAG, "network has been disconnected");
            if (isViewAtteached()) {
                getView().getReportDetailInfoFail(SinvieNetContact.ERROR_NET,
                        mContext.getString(R.string.sinvie_network_be_disconnected));
            }
            return;
        }
        SinvieCommonNetUtils.execute(SinvieCommonNetUtils.covert(new SinvieBaseURLRepository().deleteReportFollowUp(id + "")), new SinvieBaseObserver<String>() {
            @Override
            public void onFailure(int errCode, @NotNull String errMsg) {
                if (isViewAtteached()) {
                    getView().deleteReportFollowUpFail(errCode, errMsg);
                }
            }

            @Override
            public void onSuccess(@Nullable String bean) {
                if (isViewAtteached()) {
                    getView().deleteReportFollowUpSuccess(id);
                }
            }
        });
    }
}
