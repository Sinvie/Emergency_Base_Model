package com.sinvie.emergency_base.logic.call;

import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.Toast;

import com.google.gson.Gson;
import com.huawei.ecterminalsdk.base.TsdkCallInfo;
import com.huawei.ecterminalsdk.models.conference.TsdkConference;
import com.sinvie.emergency_conf.CallService.SinvieCallConstant;
import com.sinvie.emergency_conf.CallService.SinvieCallInfo;
import com.sinvie.emergency_conf.CallService.SinvieCallMgr;
import com.sinvie.emergency_conf.CallService.SinvieICallNotification;
import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieConfBroadcastConstants;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcast;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcastReceiver;
import com.sinvie.emergency_conf.CommonService.util.SinvieLogUtil;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.ui.SinvieIntentConstant;
import com.sinvie.emergency_base.ui.base.SinvieActivityStack;
import com.sinvie.emergency_base.ui.call.SinvieVideoActivity;
import com.sinvie.emergency_base.utils.SinvieActivityUtil;
import com.sinvie.emergency_base.utils.SinvieDemoLog;
import com.sinvie.emergency_base.utils.SinvieFileUtil;

import java.io.File;

/**
 * @param
 * @author JonsonBob
 * @description 呼叫管理 ICallNotification: 呼叫接口实现 接通时的操作判断
 * @return
 * @time 2021/7/9 13:47
 */
public class SinvieCallFunc implements SinvieICallNotification {

    private static final String TAG = SinvieCallFunc.class.getSimpleName();
    private static final int UPGRADE_FAILED = 100;
    private static final int CTD_FAILED = 101;
    private static final int CTD_SUCCESS = 102;
    private static final String RINGING_FILE = "ringing.wav";
    private static final String RING_BACK_FILE = "ring_back.wav";

    private boolean mMuteStatus;
    private String mFilePath;

    private static SinvieCallFunc mInstance = new SinvieCallFunc();
    private String[] broadcastNames =
            new String[]{SinvieConfBroadcastConstants.CONF_INCOMING_TO_CALL_INCOMING};

    private SinvieCallFunc() {
        SinvieLocBroadcast.getInstance().registerBroadcast(receiver, broadcastNames);
    }

    private SinvieLocBroadcastReceiver receiver = new SinvieLocBroadcastReceiver() {
        @Override
        public void onReceive(String broadcastName, Object obj) {
            if (SinvieConfBroadcastConstants.CONF_INCOMING_TO_CALL_INCOMING.equals(broadcastName)) {
                if (obj instanceof TsdkConference) {
                    TsdkConference tsdkConference = (TsdkConference) obj;
                    TsdkCallInfo tsdkcallInfo = tsdkConference.getCall().getCallInfo();

                    SinvieCallInfo sinvieCallInfo = new SinvieCallInfo();
                    sinvieCallInfo.setCallID(tsdkcallInfo.getCallId());
                    sinvieCallInfo.setFocus(true);
                    sinvieCallInfo.setCaller(false);
                    sinvieCallInfo.setPeerNumber(tsdkcallInfo.getPeerNumber());
                    sinvieCallInfo.setPeerDisplayName(tsdkcallInfo.getPeerDisplayName());
                    sinvieCallInfo.setVideoCall(tsdkcallInfo.getIsVideoCall() == 0 ? false : true);
                    sinvieCallInfo.setConfID(tsdkConference.getHandle() + "");

                    mFilePath = Environment.getExternalStorageDirectory() + File.separator + RINGING_FILE;
                    SinvieCallMgr.getInstance().startPlayRingingTone(mFilePath);

                    SinvieLogUtil.e(SinvieUIConstants.DEMO_TAG, "First --> " + new Gson().toJson(sinvieCallInfo));
                    Intent intent = new Intent(SinvieIntentConstant.CALL_IN_ACTIVITY_ACTION);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addCategory(SinvieIntentConstant.DEFAULT_CATEGORY);
                    intent.putExtra(SinvieUIConstants.CALL_INFO, sinvieCallInfo);
                    SinvieActivityUtil.startActivity(SinvieLocContext.getContext(), intent);
                }
            }
        }
    };

    private Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case UPGRADE_FAILED:
                    Toast.makeText(SinvieLocContext.getContext(),
                            SinvieLocContext.getContext().getString(R.string.sinvie_video_be_refused),
                            Toast.LENGTH_LONG).show();
                    SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.CALL_UPDATE_AUDIO_TO_VIDEO, false);
                    break;
                case CTD_FAILED:
                    Toast.makeText(SinvieLocContext.getContext(), SinvieLocContext.getContext().getString(R.string.sinvie_ctd_failed), Toast.LENGTH_SHORT).show();
                    break;
                case CTD_SUCCESS:
                    Toast.makeText(SinvieLocContext.getContext(), SinvieLocContext.getContext().getString(R.string.sinvie_ctd_success), Toast.LENGTH_SHORT).show();
                    break;
                default:
                    break;
            }
        }
    };

    public static SinvieCallFunc getInstance() {
        return mInstance;
    }

    @Override
    public void onCallEventNotify(SinvieCallConstant.CallEvent event, Object obj) {
        SinvieDemoLog.i(TAG, "CallConstant.CallEvent: " + event + ", object: " + obj);
        switch (event) {
            //来电
            case CALL_COMING:
                SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "call coming!");
                if (obj instanceof SinvieCallInfo) {
                    SinvieCallInfo sinvieCallInfo = (SinvieCallInfo) obj;


                    mFilePath = Environment.getExternalStorageDirectory() + File.separator + RINGING_FILE;
                    SinvieCallMgr.getInstance().startPlayRingingTone(mFilePath);

                    Intent intent = new Intent(SinvieIntentConstant.CALL_IN_ACTIVITY_ACTION);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addCategory(SinvieIntentConstant.DEFAULT_CATEGORY);

                    SinvieLogUtil.e(SinvieUIConstants.DEMO_TAG, "Second --> " + new Gson().toJson(sinvieCallInfo));
                    intent.putExtra(SinvieUIConstants.CALL_INFO, sinvieCallInfo);
                    SinvieActivityUtil.startActivity(SinvieLocContext.getContext(), intent);
                }
                break;

            //去电
            case CALL_GOING:
                SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "call going!");

                if (obj instanceof SinvieCallInfo) {
                    SinvieCallInfo sinvieCallInfo = (SinvieCallInfo) obj;

                    //这里其实可以判断是否是主动接入会议的呼叫,若是会议，可以考虑跳转至会议界面

                    Intent intent = new Intent(SinvieIntentConstant.CALL_OUT_ACTIVITY_ACTION);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addCategory(SinvieIntentConstant.DEFAULT_CATEGORY);
                    SinvieLogUtil.e(SinvieUIConstants.DEMO_TAG, "Third --> " + new Gson().toJson(sinvieCallInfo));
                    intent.putExtra(SinvieUIConstants.CALL_INFO, sinvieCallInfo);
                    SinvieActivityUtil.startActivity(SinvieLocContext.getContext(), intent);
                }
                break;

            //播放回铃音
            case PLAY_RING_BACK_TONE:
                SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "play ring back!");

                if (SinvieFileUtil.isSdCardExist()) {
                    mFilePath =
                            Environment.getExternalStorageDirectory() + File.separator + RING_BACK_FILE;
                    SinvieCallMgr.getInstance().startPlayRingBackTone(mFilePath);
                }
                break;

            //媒体通道建立
            case RTP_CREATED:
                if (obj instanceof SinvieCallInfo) {
                    SinvieCallMgr.getInstance().stopPlayRingingTone();
                    SinvieCallMgr.getInstance().stopPlayRingBackTone();
                    SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.CALL_MEDIA_CONNECTED, obj);
                }

                break;

            //呼叫建立成功
            case CALL_CONNECTED:
                SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "call connected ");

                if (obj instanceof SinvieCallInfo) {
                    SinvieCallMgr.getInstance().stopPlayRingingTone();
                    SinvieCallMgr.getInstance().stopPlayRingBackTone();

                    SinvieCallInfo sinvieCallInfo = (SinvieCallInfo) obj;

//                    LocBroadcast.getInstance().sendBroadcast(CustomBroadcastConstants
//                    .ACTION_CALL_CONNECTED, callInfo);
                    if (sinvieCallInfo.isFocus()) {
//                        if (MeetingMgr.getInstance().isInConference()) {
                        SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.CONF_CALL_CONNECTED, sinvieCallInfo);
//                        } else {
                        //TODO
//                        }
                    } else {
                        SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.ACTION_CALL_CONNECTED, sinvieCallInfo);
                    }
                }
                break;

            //呼叫结束
            case CALL_ENDED:
                SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "call end!");

                if (obj instanceof SinvieCallInfo) {
                    //呼叫可能没有接通，结束时停止可能存在的振铃音和回铃音
                    SinvieCallMgr.getInstance().stopPlayRingingTone();
                    SinvieCallMgr.getInstance().stopPlayRingBackTone();

                    SinvieCallInfo params = (SinvieCallInfo) obj;
                    SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.ACTION_CALL_END, params);

                    resetData();
                }
                break;

            //语音呼叫保持成功
            case AUDIO_HOLD_SUCCESS:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.HOLD_CALL_RESULT, "HoldSuccess");
                break;

            //语音呼叫保持失败
            case AUDIO_HOLD_FAILED:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.HOLD_CALL_RESULT, "HoldFailed");
                break;

            //视频呼叫保持成功
            case VIDEO_HOLD_SUCCESS:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.HOLD_CALL_RESULT, "VideoHoldSuccess");
                break;

            //视频呼叫保持失败
            case VIDEO_HOLD_FAILED:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.HOLD_CALL_RESULT, "VideoHoldFailed");
                break;

            //取消保持(恢复)成功
            case UN_HOLD_SUCCESS:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.HOLD_CALL_RESULT, "UnHoldSuccess");
                break;

            //取消保持(恢复)失败
            case UN_HOLD_FAILED:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.HOLD_CALL_RESULT, "UnHoldFailed");
                break;

            //偏转失败
            case DIVERT_FAILED:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.DIVERT_RESULT,
                        "DivertFailed");
                break;

            //盲转成功
            case BLD_TRANSFER_SUCCESS:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.BLD_TRANSFER_RESULT, "BldTransferSuccess");
                break;

            //盲转失败
            case BLD_TRANSFER_FAILED:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.BLD_TRANSFER_RESULT, "BldTransferFailed");
                break;

            //关闭视频
            case CLOSE_VIDEO:
                SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "close video.");

                if (obj instanceof SinvieCallInfo) {
                    SinvieCallInfo sinvieCallInfo = (SinvieCallInfo) obj;

                    Intent intent = new Intent(SinvieIntentConstant.CALL_OUT_ACTIVITY_ACTION);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addCategory(SinvieIntentConstant.DEFAULT_CATEGORY);
                    intent.putExtra(SinvieUIConstants.CALL_INFO, sinvieCallInfo);

                    SinvieActivityStack.getIns().popup(SinvieActivityStack.getIns().getCurActivity());
                    SinvieActivityUtil.startActivity(SinvieLocContext.getContext(), intent);
                }
                break;

            //打开视频
            case OPEN_VIDEO:
                SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "open video.");
                if (obj instanceof SinvieCallInfo) {
                    SinvieCallInfo sinvieCallInfo = (SinvieCallInfo) obj;
                    boolean isFocus = sinvieCallInfo.isFocus();
                    if (!isFocus) {
                        String name = SinvieActivityStack.getIns().getCurActivity().getClass().getName();
                        if (name.equals(SinvieVideoActivity.class.getName())) {
                            break;
                        }
                        Intent intent = new Intent(SinvieIntentConstant.VIDEO_ACTIVITY_ACTION);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addCategory(SinvieIntentConstant.DEFAULT_CATEGORY);

                        SinvieLogUtil.e(SinvieUIConstants.DEMO_TAG, "Forth --> " + new Gson().toJson(sinvieCallInfo));
                        intent.putExtra(SinvieUIConstants.CALL_INFO, sinvieCallInfo);
                        SinvieActivityStack.getIns().popup(SinvieActivityStack.getIns().getCurActivity());
                        SinvieActivityUtil.startActivity(SinvieLocContext.getContext(), intent);
                    }
                }
                break;

            case ADD_LOCAL_VIEW:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.ADD_LOCAL_VIEW,
                        obj);
                break;

            case DEL_LOCAL_VIEW:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.DEL_LOCAL_VIEW,
                        obj);
                break;

            //远端拒绝增加视频请求
            case REMOTE_REFUSE_ADD_VIDEO_SREQUEST:
                SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "remote refuse upgrade video!");
                mHandler.sendEmptyMessage(UPGRADE_FAILED);
                break;

            //收到远端增加视频请求
            case RECEIVED_REMOTE_ADD_VIDEO_REQUEST:
                SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "Add video call!");
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.CALL_UPGRADE_ACTION, obj);
                break;


            case CONF_END:
                break;

            case SESSION_MODIFIED:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.SESSION_MODIFIED_RESULT, obj);
                break;

            case CALL_ENDED_FAILED:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.ACTION_CALL_END_FAILED, obj);
                break;

            case REFRESH_ROUTE:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.ACTION_CALL_ROUTE_CHANGE, obj);
                break;

            case DEVICE_CHANGED:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.ACTION_CALL_DEVICES_STATUS_CHANGE, obj);
                break;

            default:
                break;
        }
    }

    private void resetData() {
        //mPlayHandle = -1;
        mMuteStatus = false;
    }

    public boolean isMuteStatus() {
        return mMuteStatus;
    }

    public void setMuteStatus(boolean mMuteStatus) {
        this.mMuteStatus = mMuteStatus;
    }
}
