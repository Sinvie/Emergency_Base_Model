package com.sinvie.emergency_base.logic.live_room;

import android.content.Context;

import com.sinvie.emergency_base.bean.SinvieDataBean;
import com.sinvie.emergency_base.http.SinvieNetContact;
import com.sinvie.emergency_base.http.base.SinvieBaseObserver;
import com.sinvie.emergency_base.http.common.SinvieCommonNetUtils;
import com.sinvie.emergency_base.http.repository.SinvieBaseURLRepository;
import com.sinvie.emergency_base.ui.base.SinvieMVPBasePresenter;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/6
 * @Des: 类描述
 */
public class SinvieLiveRoomPresenter extends SinvieMVPBasePresenter<SinvieLiveRoomContact.LiveRoomBaseView> implements SinvieLiveRoomContact.LiveRoomBasePresenter {

    private Context mContext;
    private SinvieLiveRoomModel mSinvieLiveRoomModel;

    public SinvieLiveRoomPresenter(Context context) {
        mContext = context;
        mSinvieLiveRoomModel = new SinvieLiveRoomModel();
    }

    @Override
    public void getScenesRoomList() {
        SinvieCommonNetUtils.execute(SinvieCommonNetUtils.covert(new SinvieBaseURLRepository().getRoomList()),
                new SinvieBaseObserver<List<SinvieDataBean>>() {
                    @Override
                    public void onSuccess(@Nullable List<SinvieDataBean> sinvieDataBeans) {
                        if (isViewAtteached()) {
                            //TODO 需要更换成解析类 bean 而不是使用自定义bean
//                            getView().getScenesRoomListSuccess(infoList);

                        }
                    }

                    @Override
                    public void onFailure(int errCode, @NotNull String errMsg) {
                        if (isViewAtteached()) {
                            getView().getScenesRoomListFail(SinvieNetContact.ERROR_CODE, errMsg);

                        }
                    }


                });
    }
}
