package com.sinvie.emergency_base.logic.add_more;

import android.content.Context;

import com.sinvie.emergency_base.ui.base.SinvieMVPBasePresenter;

/**
 * @author: JonsonBob
 * @date: 2021/6/30
 * @Des: 类描述
 */
public class SinvieAddMorePresenter extends SinvieMVPBasePresenter<SinvieAddMoreContact.AddMoreBaseView> implements SinvieAddMoreContact.AddMoreBasePresenter {

    private Context mContext;
    private SinvieAddMoreModel mSinvieAddMoreModel;

    public SinvieAddMorePresenter(Context context) {
        mContext = context;
        mSinvieAddMoreModel = new SinvieAddMoreModel();
    }
}
