package com.sinvie.emergency_base.logic.login;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.gson.Gson;
import com.huawei.ecterminalsdk.base.TsdkContactsInfo;
import com.huawei.ecterminalsdk.models.TsdkCommonResult;
import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieConfBroadcastConstants;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcast;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcastReceiver;
import com.sinvie.emergency_conf.LoginService.SinvieILoginEventNotifyUI;
import com.sinvie.emergency_conf.LoginService.SinvieLoginConstant;
import com.sinvie.emergency_conf.LoginService.SinvieLoginMgr;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.ui.base.SinvieActivityStack;
import com.sinvie.emergency_base.ui.login.SinvieLoginForDevActivity;
import com.sinvie.emergency_base.utils.SinvieDemoLog;

import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/6/29
 * @Des: 登录逻辑 ILoginEventNotifyUI : 登录接口返回
 */
public class SinvieLoginFunc implements
        SinvieILoginEventNotifyUI,
        SinvieLocBroadcastReceiver {
    private static final String TAG = SinvieLoginFunc.class.getSimpleName();

    private static final int VOIP_LOGIN_SUCCESS = 100;
    private static final int IM_LOGIN_SUCCESS = 101;
    private static final int LOGIN_FAILED = 102;
    private static final int LOGOUT = 103;
    private static final int FIREWALL_DETECT_FAILED = 104;
    private static final int BUILD_STG_FAILED = 105;
    private static final int CHANGE_PASSWORD = 106;
    private static final int FIRST_CHANGE_PASSWORD = 107;


    private static SinvieLoginFunc INSTANCE = new SinvieLoginFunc();

    private String[] broadcastNames =
            new String[]{SinvieConfBroadcastConstants.ACTION_ENTERPRISE_GET_SELF_RESULT};


    private Handler mMainHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            SinvieDemoLog.e(TAG, "what:" + msg.what);
            parallelHandleMessage(msg);
        }
    };

    private void sendHandlerMessage(int what, Object object) {
        if (mMainHandler == null) {
            return;
        }
        Message msg = mMainHandler.obtainMessage(what, object);
        mMainHandler.sendMessage(msg);
    }

    private SinvieLoginFunc() {
        SinvieLocBroadcast.getInstance().registerBroadcast(this, broadcastNames);
    }

    public static SinvieILoginEventNotifyUI getInstance() {
        return INSTANCE;
    }

    @Override
    public void onLoginEventNotify(SinvieLoginConstant.LoginUIEvent evt, int reason, String description) {
        SinvieDemoLog.e(TAG, "evt--->" + evt + " ,description" + description);
        if (evt == SinvieLoginConstant.LoginUIEvent.VOIP_LOGIN_SUCCESS) {
            SinvieDemoLog.e(TAG, "voip login success");
            sendHandlerMessage(VOIP_LOGIN_SUCCESS, description);
        } else if (evt == SinvieLoginConstant.LoginUIEvent.LOGIN_FAILED) {
            SinvieDemoLog.e(TAG, "login fail: " + reason);
            Bundle bundle = new Bundle();
            bundle.putInt(SinvieUIConstants.CONFERENCE_LOGIN_FAIL_CODE, reason);
            bundle.putString(SinvieUIConstants.CONFERENCE_LOGIN_FAIL_MSG, description);
            sendHandlerMessage(LOGIN_FAILED, bundle);
        } else if (evt == SinvieLoginConstant.LoginUIEvent.FIREWALL_DETECT_FAILED) {
            SinvieDemoLog.e(TAG, "firewall detect fail");
            sendHandlerMessage(FIREWALL_DETECT_FAILED, description);
        } else if (evt == SinvieLoginConstant.LoginUIEvent.BUILD_STG_FAILED) {
            SinvieDemoLog.e(TAG, "build stg fail");
            sendHandlerMessage(BUILD_STG_FAILED, description);
        } else if (evt == SinvieLoginConstant.LoginUIEvent.LOGOUT) {
            SinvieDemoLog.e(TAG, "logout");
            sendHandlerMessage(LOGOUT, description);
        } else if (evt == SinvieLoginConstant.LoginUIEvent.FIRST_CHANGE_PASSWORD) {
            SinvieDemoLog.e(TAG, "first login change password");
            sendHandlerMessage(FIRST_CHANGE_PASSWORD, description);
        }
    }

    @Override
    public void onLoginEventNotify(SinvieLoginConstant.LoginUIEvent evt, TsdkCommonResult result) {
        SinvieDemoLog.e(TAG, "evt--->" + evt + " , result --> " + new Gson().toJson(result));
        if (evt == SinvieLoginConstant.LoginUIEvent.CHANGE_PASSWORD) {
            SinvieDemoLog.e(TAG, "change password");
            sendHandlerMessage(CHANGE_PASSWORD, result);
        }
    }

    @Override
    public void onReceive(String broadcastName, Object obj) {
        if (SinvieConfBroadcastConstants.ACTION_ENTERPRISE_GET_SELF_RESULT.equals(broadcastName)) {
            List<TsdkContactsInfo> selfInfo = (List<TsdkContactsInfo>) obj;
            TsdkContactsInfo contactInfo = selfInfo.get(0);

            SinvieLoginMgr.getInstance().setSelfInfo(contactInfo);
            if (null != contactInfo.getTerminal() && !contactInfo.getTerminal().equals("")) {
                SinvieLoginMgr.getInstance().setTerminal(contactInfo.getTerminal());
            } else {
                SinvieLoginMgr.getInstance().setTerminal(contactInfo.getTerminal2());
            }
        }
    }

    /**
     * handle message 统一封装统一管理
     *
     * @param msg
     */
    private void parallelHandleMessage(Message msg) {
        switch (msg.what) {
            case VOIP_LOGIN_SUCCESS:
                //登录成功 应该跳转到首页 但是这里跳转到 conferenceActivity
                SinvieDemoLog.e(TAG, "voip login success,notify UI!");
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.LOGIN_SUCCESS, msg.obj);

                // SMC3.0密码过期提示
                int serverType = SinvieLoginMgr.getInstance().getServerType();
                if (serverType == SinvieUIConstants.SMC_TYPE_3_0) {
                    int passwordExpire = SinvieLoginMgr.getInstance().getPasswordExpire();
                    if (passwordExpire != SinvieUIConstants.PASSWORD_EXPIRE_NORMAL) {
                        Toast.makeText(SinvieLocContext.getContext(),
                                "Password Expire : " + passwordExpire, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case IM_LOGIN_SUCCESS:
                SinvieDemoLog.e(TAG, "im login success,notify UI!");
                Toast.makeText(SinvieLocContext.getContext(), (String) msg.obj, Toast.LENGTH_LONG).show();
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.ACTION_IM_LOGIN_SUCCESS, null);
                break;
            case LOGIN_FAILED:
                SinvieDemoLog.e(TAG, "login failed,notify UI!" + ((Bundle) msg.obj).getString(SinvieUIConstants.CONFERENCE_LOGIN_FAIL_MSG));
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.LOGIN_FAILED, ((Bundle) msg.obj));

                break;
            case LOGOUT:
                SinvieDemoLog.e(TAG, "logout success,notify UI!");
                //直接清除栈内activity 跳转到loginActivity
                SinvieActivityStack.getIns().popupAbove(SinvieLoginForDevActivity.class);
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.LOGOUT, null);
                Toast.makeText(SinvieLocContext.getContext(), ((String) msg.obj), Toast.LENGTH_SHORT).show();

                break;
            case FIREWALL_DETECT_FAILED:
                SinvieDemoLog.e(TAG, "firewall detect failed,notify UI!");
                Toast.makeText(SinvieLocContext.getContext(), ((String) msg.obj), Toast.LENGTH_SHORT).show();
                break;
            case BUILD_STG_FAILED:
                SinvieDemoLog.e(TAG, "build stg failed,notify UI!");
                Toast.makeText(SinvieLocContext.getContext(), ((String) msg.obj), Toast.LENGTH_SHORT).show();
                break;
            case CHANGE_PASSWORD:
                TsdkCommonResult result = (TsdkCommonResult) msg.obj;
                if (result != null) {
                    String reasonDescription = result.reasonDescription;
                    if (result.result == 0) {
                        reasonDescription = "change password success";
                    } else {
                        if (TextUtils.isEmpty(reasonDescription)) {
                            reasonDescription = "change password fail";
                        }
                    }
                    Toast.makeText(SinvieLocContext.getContext(), reasonDescription,
                            Toast.LENGTH_SHORT).show();
                }
                break;
            case FIRST_CHANGE_PASSWORD:
                SinvieDemoLog.e(TAG, "first login change password,notify UI!");
                Toast.makeText(SinvieLocContext.getContext(), ((String) msg.obj), Toast.LENGTH_SHORT).show();
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.FIRST_CHANGE_PASSWORD, null);
                break;
            default:

                break;
        }
    }
}
