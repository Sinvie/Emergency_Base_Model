package com.sinvie.emergency_base.logic.conference;


import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.Toast;

import com.huawei.ecterminalsdk.models.TsdkCommonResult;
import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieConfBroadcastConstants;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcast;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcastReceiver;
import com.sinvie.emergency_conf.ConferenceService.SinvieConfBaseInfo;
import com.sinvie.emergency_conf.ConferenceService.SinvieConfConstant;
import com.sinvie.emergency_conf.ConferenceService.SinvieIConfNotification;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.ui.SinvieIntentConstant;
import com.sinvie.emergency_base.utils.SinvieActivityUtil;

import java.util.List;

/**
 * @description 会议呼叫管理 IConfNotification: 呼叫管理 接通电话之后的操作判断
 * @param
 * @return
 * @author JonsonBob
 * @time 2021/7/9 13:47
 */
public class SinvieConfFunc implements SinvieIConfNotification {
    private static final int BOOK_CONF_SUCCESS = 100;
    private static final int BOOK_CONF_FAILED = 101;
    private static final int QUERY_CONF_LIST_SUCCESS = 102;
    private static final int QUERY_CONF_LIST_SUCCESS_EMPTY = 112;
    private static final int QUERY_CONF_LIST_FAILED = 103;
    private static final int QUERY_CONF_DETAIL_FAILED = 104;
    private static final int QUERY_CONF_DETAIL_SUCCESS = 105;
    private static final int GET_VMR_LIST_FAILED = 106;
    private static final int CHECKIN_CONF_CODE = 113;

    private static final int JOIN_VOICE_CONF_SUCCESS = 109;
    private static final int JOIN_VIDEO_CONF_SUCCESS = 110;
    private static final int JOIN_CONF_FAILED = 111;

    private static SinvieConfFunc mInstance = new SinvieConfFunc();
    private String[] broadcastNames = new String[]{SinvieConfBroadcastConstants.CONF_INFO_PARAM};

    private SinvieConfFunc() {
        SinvieLocBroadcast.getInstance().registerBroadcast(receiver, broadcastNames);
    }

    private Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case BOOK_CONF_SUCCESS:
                    Toast.makeText(SinvieLocContext.getContext(), "book conf success.",
                            Toast.LENGTH_SHORT).show();
                    break;
                case BOOK_CONF_FAILED:
                    Toast.makeText(SinvieLocContext.getContext(), "book conf failed.",
                            Toast.LENGTH_SHORT).show();
                    break;
                case QUERY_CONF_LIST_SUCCESS:
                    Toast.makeText(SinvieLocContext.getContext(), "query conf list success.", Toast.LENGTH_SHORT).show();
                    break;
                case QUERY_CONF_LIST_SUCCESS_EMPTY:
                    Toast.makeText(SinvieLocContext.getContext(), "query conf list is empty.",
                            Toast.LENGTH_SHORT).show();
                    break;
                case QUERY_CONF_LIST_FAILED:
                    Toast.makeText(SinvieLocContext.getContext(), "query conf list failed.",
                            Toast.LENGTH_SHORT).show();
                    break;
                case QUERY_CONF_DETAIL_FAILED:
                    Toast.makeText(SinvieLocContext.getContext(), "query conf detail failed.",
                            Toast.LENGTH_SHORT).show();
                    break;
                case JOIN_VOICE_CONF_SUCCESS:
                    if (msg.obj instanceof String) {
                        String confID = (String) msg.obj;
//                        Intent intent = new Intent(IntentConstant.CONF_MEMBER_LIST_ACTIVITY_ACTION);
                            //点击接通后 确定跳转的activity 如果注释 就会接听后返回原界面
//                        Intent intent = new Intent(IntentConstant.CONF_MANAGER_ACTIVITY_ACTION);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        intent.putExtra(UIConstants.CONF_ID, confID);
//                        intent.putExtra(UIConstants.IS_VIDEO_CONF, false);
//                        ActivityUtil.startActivity(LocContext.getContext(), intent);
                    }
                    break;

                case JOIN_VIDEO_CONF_SUCCESS:
                    if (msg.obj instanceof String) {
                        String confID = (String) msg.obj;

                        //点击接通后 确定跳转的activity 如果注释 就会接听后返回原界面
                        Intent intent = new Intent(SinvieIntentConstant.CONF_MANAGER_ACTIVITY_ACTION);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra(SinvieUIConstants.CONF_ID, confID);
                        intent.putExtra(SinvieUIConstants.IS_VIDEO_CONF, true);
                        SinvieActivityUtil.startActivity(SinvieLocContext.getContext(), intent);
                    }
                    break;

                case JOIN_CONF_FAILED:
                    Toast.makeText(SinvieLocContext.getContext(), "join conf failed.",
                            Toast.LENGTH_SHORT).show();
                    break;
                case CHECKIN_CONF_CODE:
                    TsdkCommonResult result = (TsdkCommonResult) msg.obj;
                    if (result.getResult() == 0) {
                        Toast.makeText(SinvieLocContext.getContext(), "join conf sign in success",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SinvieLocContext.getContext(), "join conf sign in failed",
                                Toast.LENGTH_SHORT).show();
                    }
                    break;
                default:
                    break;
            }
        }
    };


    private SinvieLocBroadcastReceiver receiver = new SinvieLocBroadcastReceiver() {
        @Override
        public void onReceive(String broadcastName, Object obj) {
//            switch (broadcastName)
//            {
//                default:
//                    break;
//            }
        }
    };


    public static SinvieConfFunc getInstance() {
        return mInstance;
    }

    @Override
    public void onConfEventNotify(SinvieConfConstant.CONF_EVENT confEvent, Object params) {
        switch (confEvent) {
            case BOOK_CONF_SUCCESS:
                mHandler.sendEmptyMessage(BOOK_CONF_SUCCESS);
                break;

            case BOOK_CONF_FAILED:
                mHandler.sendEmptyMessage(BOOK_CONF_FAILED);
                break;

            case QUERY_CONF_LIST_FAILED:
                mHandler.sendEmptyMessage(QUERY_CONF_LIST_FAILED);
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.GET_CONF_LIST_FAILED_RESULT, params);
                break;

            case QUERY_CONF_LIST_SUCCESS:
                List<SinvieConfBaseInfo> sinvieConfBaseInfoList = (List<SinvieConfBaseInfo>) params;
                if (sinvieConfBaseInfoList.size() == 0) {
                    mHandler.sendEmptyMessage(QUERY_CONF_LIST_SUCCESS_EMPTY);
                } else {
                    mHandler.sendEmptyMessage(QUERY_CONF_LIST_SUCCESS);
                }
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.GET_CONF_LIST_RESULT, params);
                break;

            case GET_VMR_LIST_FAILED:
                mHandler.sendEmptyMessage(GET_VMR_LIST_FAILED);
                break;

            case GET_VMR_LIST_SUCCESS:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.GET_VMR_LIST_RESULT, params);
                break;

            case QUERY_CONF_DETAIL_FAILED:
                mHandler.sendEmptyMessage(QUERY_CONF_DETAIL_FAILED);
                break;

            case QUERY_CONF_DETAIL_SUCCESS:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.GET_CONF_DETAIL_RESULT, params);
                break;

            case JOIN_VOICE_CONF_SUCCESS:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.CONF_CALL_CONNECTED, params);
                mHandler.sendMessage(mHandler.obtainMessage(JOIN_VOICE_CONF_SUCCESS, params));
                break;

            case JOIN_VIDEO_CONF_SUCCESS:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.CONF_CALL_CONNECTED, params);
                mHandler.sendMessage(mHandler.obtainMessage(JOIN_VIDEO_CONF_SUCCESS, params));
                break;

            case JOIN_CONF_FAILED:
                mHandler.sendEmptyMessage(JOIN_CONF_FAILED);
                break;

            case REQUEST_RIGHT_FAILED:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.REQUEST_CONF_RIGHT_RESULT, params);
                break;

            case STATE_UPDATE:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.CONF_STATE_UPDATE, params);
                break;

            case ADD_YOURSELF_FAILED:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.ADD_SELF_RESULT
                        , params);
                break;

            case ADD_ATTENDEE_RESULT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.ADD_ATTENDEE_RESULT, params);
                break;

            case DEL_ATTENDEE_RESULT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.DEL_ATTENDEE_RESULT, params);
                break;

            case MUTE_ATTENDEE_RESULT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.MUTE_ATTENDEE_RESULT, params);
                break;

            case UN_MUTE_ATTENDEE_RESULT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.UN_MUTE_ATTENDEE_RESULT, params);
                break;

            case MUTE_CONF_RESULT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.MUTE_CONF_RESULT, params);
                break;

            case UN_MUTE_CONF_RESULT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.UN_MUTE_CONF_RESULT, params);
                break;

            case LOCK_CONF_RESULT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.LOCK_CONF_RESULT, params);
                break;

            case UN_LOCK_CONF_RESULT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.UN_LOCK_CONF_RESULT, params);
                break;

            case HAND_UP_RESULT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.HAND_UP_RESULT,
                        params);
                break;

            case CANCEL_HAND_UP_RESULT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.CANCEL_HAND_UP_RESULT, params);
                break;

            case REQUEST_CHAIRMAN_RESULT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.REQUEST_CHAIRMAN_RESULT, params);
                break;

            case RELEASE_CHAIRMAN_RESULT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.RELEASE_CHAIRMAN_RESULT, params);
                break;

            case WILL_TIMEOUT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.WILL_TIMEOUT,
                        params);
                break;

            case POSTPONE_CONF_RESULT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.POSTPONE_CONF_RESULT, params);
                break;
            case REQUEST_PRESENTER_RESULT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.REQUEST_PRESENTER_RESULT, params);
                break;

            case SPEAKER_LIST_IND:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.SPEAKER_LIST_IND, params);
                break;

            case SET_CONF_MODE_RESULT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.SET_CONF_MODE_RESULT, params);
                break;

            case WATCH_ATTENDEE_RESULT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.WATCH_ATTENDEE_CONF_RESULT, params);
                break;

            case BROADCAST_ATTENDEE_RESULT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.BROADCAST_ATTENDEE_CONF_RESULT, params);
                break;

            case CANCEL_BROADCAST_RESULT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.CANCEL_BROADCAST_CONF_RESULT, params);
                break;

            case START_RECORD_RESULT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.START_RECORD_RESULT, params);
                break;

            case STOP_RECORD_RESULT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.STOP_RECORD_RESULT, params);
                break;

            case GET_DATA_CONF_PARAM_RESULT:
//                LocBroadcast.getInstance().sendBroadcast(CustomBroadcastConstants
//                .GET_DATA_CONF_PARAM_RESULT, params);
                int result = (int) params;
                if (result != 0) {
                    Toast.makeText(SinvieLocContext.getContext(), R.string.sinvie_get_data_conf_params_fail,
                            Toast.LENGTH_SHORT).show();
                    return;
                }
//                MeetingMgr.getInstance().joinDataConf();
                break;

            case UPGRADE_CONF_RESULT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.UPGRADE_CONF_RESULT, params);
                break;

            case JOIN_DATA_CONF_RESULT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.DATA_CONFERENCE_JOIN_RESULT, params);
                break;

            case CAMERA_STATUS_UPDATE:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.DATA_CONFERENCE_CAMERA_STATUS_UPDATE, params);
                break;

            case CONF_INCOMING_TO_CALL_INCOMING:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.CONF_INCOMING_TO_CALL_INCOMING, params);
                break;

            case START_DATA_CONF_SHARE:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.DATE_CONFERENCE_START_SHARE_STATUS, params);
                break;

            case END_DATA_CONF_SHARE:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.DATE_CONFERENCE_END_SHARE_STATUS, params);
                break;
            case AUX_DATA_STATE:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.DATE_CONFERENCE_AUX_DATA_STATE, params);
                break;
            case AUX_DATA_SEND:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.CONF_AUX_DATA_SEND, params);
                break;
            case AUX_DATA_FAILED:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.CONF_AUX_DATA_FAILED, params);
                break;
            case NO_STREAM_DURATION:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.VIDEO_NO_STREAM_DURATION_MSG, params);
                break;
            case CHECKIN_CONF:
                mHandler.sendMessage(mHandler.obtainMessage(CHECKIN_CONF_CODE, params));
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.CHECKIN_CONF_MSG, params);
                break;
            case CANCEL_CONF:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.CANCEL_CONF_MSG
                        , params);
                break;
            case LEAVE_CONF:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.GET_CONF_END,
                        params);
                break;

            case CONF_CHAT_MSG:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.DATE_CONFERENCE_CHAT_MSG, params);
                break;
            case GET_TEMP_USER_RESULT:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.GET_TEMP_USER_RESULT, params);
                break;

            case CALL_TRANSFER_TO_CONFERENCE:
                SinvieLocBroadcast.getInstance().sendBroadcast(SinvieConfBroadcastConstants.CALL_TRANSFER_TO_CONFERENCE, params);
                break;

            default:
                break;
        }
    }
}
