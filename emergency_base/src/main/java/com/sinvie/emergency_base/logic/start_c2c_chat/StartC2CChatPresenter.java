package com.sinvie.emergency_base.logic.start_c2c_chat;

import android.content.Context;

import com.sinvie.emergency_base.ui.base.SinvieMVPBasePresenter;

/**
 * @author: JonsonBob
 * @date: 2021/6/30
 * @Des: 类描述
 */
public class StartC2CChatPresenter extends SinvieMVPBasePresenter<SinvieStartC2CChatContact.StartC2CChatBaseView> implements SinvieStartC2CChatContact.StartC2CChatBasePresenter {

    private Context mContext;
    private SinvieStartC2CChatModel mSinvieStartC2CChatModel;

    public StartC2CChatPresenter(Context context) {
        mContext = context;
        mSinvieStartC2CChatModel = new SinvieStartC2CChatModel();
    }
}
