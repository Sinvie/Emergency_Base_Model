package com.sinvie.emergency_base.logic.login;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.huawei.ecterminalsdk.base.TsdkContactsInfo;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.ui.base.SinvieMVPBasePresenter;
import com.sinvie.emergency_base.ui.login.SinvieUserInfo;
import com.sinvie.emergency_base.utils.SinvieDemoLog;
import com.sinvie.emergency_base.utils.SinvieFileUtil;
import com.sinvie.emergency_conf.CallService.SinvieCallConstant;
import com.sinvie.emergency_conf.CallService.SinvieCallMgr;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieConfBroadcastConstants;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcast;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcastReceiver;
import com.sinvie.emergency_conf.CommonService.util.SinvieDeviceManager;
import com.sinvie.emergency_conf.LoginService.SinvieLoginConstant;
import com.sinvie.emergency_conf.LoginService.SinvieLoginMgr;
import com.sinvie.emergency_conf.LoginService.SinvieLoginParam;
import com.sinvie.emergency_live.tuikit.SinvieTUIKit;
import com.sinvie.emergency_live.tuikit.base.ISinvieUIKitCallBack;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executors;

/**
 * @author: JonsonBob
 * @date: 2021/7/10
 * @Des: 类描述
 */
public abstract class SinvieLoginBasePresenter extends SinvieMVPBasePresenter<SinvieILoginContact.LoginBaseView> implements
//        ILoginEventNotifyUI,
        SinvieILoginContact.LoginPresenter {

    private static final String TAG = SinvieLoginBasePresenter.class.getSimpleName();

    /**
     * 闹铃文件
     */
    private static final String RINGING_FILE = "ringing.wav";
    /**
     * 回铃文件
     */
    private static final String RING_BACK_FILE = "ring_back.wav";
    /**
     * BMP
     */
    private static final String BMP_FILE = "CameraBlack.BMP";
    /**
     * 注释文件
     */
    private static final String ANNOT_FILE = "annotImages";
    private static final int VOIP_LOGIN_SUCCESS = 100;
    private static final int IM_LOGIN_SUCCESS = 101;
    private static final int LOGIN_FAILED = 102;
    private static final int LOGOUT = 103;
    private static final int FIREWALL_DETECT_FAILED = 104;
    private static final int BUILD_STG_FAILED = 105;
    private static final int CHANGE_PASSWORD = 106;
    private static final int FIRST_CHANGE_PASSWORD = 107;
    /**
     * 上下文对象
     */
    protected Context mContext;
    protected String[] broadcastNames;
    protected SinvieLocBroadcastReceiver mReceiver = (broadcastName, obj) -> {
        switch (broadcastName) {
            case SinvieConfBroadcastConstants.ACTION_ENTERPRISE_GET_SELF_RESULT:
                List<TsdkContactsInfo> selfInfo = (List<TsdkContactsInfo>) obj;
                TsdkContactsInfo contactInfo = selfInfo.get(0);

                SinvieLoginMgr.getInstance().setSelfInfo(contactInfo);
                if (null != contactInfo.getTerminal() && !contactInfo.getTerminal().equals("")) {
                    SinvieLoginMgr.getInstance().setTerminal(contactInfo.getTerminal());
                } else {
                    SinvieLoginMgr.getInstance().setTerminal(contactInfo.getTerminal2());
                }
            case SinvieConfBroadcastConstants.LOGIN_SUCCESS:
                if (obj instanceof String) {
                    //登录成功
                    if (isViewAtteached()) {
                        getView().onHWLoginSuccess(((String) obj));
                    }
                }
                break;
            case SinvieConfBroadcastConstants.ACTION_IM_LOGIN_SUCCESS:
                break;
            case SinvieConfBroadcastConstants.LOGIN_FAILED:
                Bundle bundle = ((Bundle) obj);
                //登录失败 33554441
                if (isViewAtteached()) {
                    getView().onHWLoginFail(bundle.getInt(SinvieUIConstants.CONFERENCE_LOGIN_FAIL_CODE)
                            , bundle.getString(SinvieUIConstants.CONFERENCE_LOGIN_FAIL_MSG));
                    getView().dismissLoginDialog(false);
                }
                break;
            default:
                break;
        }
    };
    /**
     * 网络请求辅助类
     */

    private MyHandler mMyHandler;
    /**
     * 当前 Android 设备唯一ID
     */
    private String anonymous = UUID.randomUUID().toString();
    private Runnable mRunnable;

    @Override
    public void registerBroadcast() {
        SinvieLocBroadcast.getInstance().registerBroadcast(mReceiver, broadcastNames);
    }

    @Override
    public void unRegisterBroadcast() {
        SinvieLocBroadcast.getInstance().registerBroadcast(mReceiver, broadcastNames);
    }

    /**
     * ***********************************************华为登录返回监听*************************************************
     */


    @Override
    public void doHWVideoCallLogin() {
        String userName = SinvieUserInfo.getInstance().getHwLoginUsername();
        String password = SinvieUserInfo.getInstance().getHwLoginPassword();

        if (!SinvieDeviceManager.isNetworkAvailable(mContext)) {
            getView().dismissLoginDialog(false);
            getView().showToast(R.string.sinvie_network_be_disconnected);
            return;
        }
        if (TextUtils.isEmpty(userName)) {
            getView().dismissLoginDialog(false);
            getView().showToast(R.string.sinvie_huawei_account_info_not_empty);
            return;
        }
        //TODO 取出登录需要的 地址 端口 和 uri 后面会更换成公网的 url
        String regServerAddress = SinvieUserInfo.getInstance().getTupRegisterServer();
        String serverPort = SinvieUserInfo.getInstance().getTupPort();
        String sipUri = SinvieUserInfo.getInstance().getTupSipUri();

        if (TextUtils.isEmpty(regServerAddress)) {
            getView().showToast(R.string.sinvie_reg_server_not_null);
            getView().dismissLoginDialog(false);
            return;
        }

        if (TextUtils.isEmpty(serverPort)) {
            getView().showToast(R.string.sinvie_server_port_not_null);
            getView().dismissLoginDialog(false);
            return;
        }

        if (null == Looper.myLooper()) {
            Looper.prepare();
        }

        SinvieLoginParam sinvieLoginParam = new SinvieLoginParam();

        sinvieLoginParam.setServerUrl(regServerAddress);
        sinvieLoginParam.setServerPort(Integer.parseInt(serverPort));
        sinvieLoginParam.setSipUri(sipUri);
        sinvieLoginParam.setUserName(userName);
        sinvieLoginParam.setPassword(password);

        sinvieLoginParam.setVPN(SinvieUserInfo.getInstance().isTupVpn());

        SinvieDemoLog.d(TAG, new Gson().toJson(sinvieLoginParam));

        HandlerThread thread = new HandlerThread("ConferenceLogin");
        thread.start();//创建一个HandlerThread并启动它

        mMyHandler = new MyHandler(thread.getLooper(), this);
        mRunnable = getRunnableData(sinvieLoginParam);
        mMyHandler.post(mRunnable);
    }

    private Runnable getRunnableData(SinvieLoginParam sinvieLoginParam) {
        mRunnable = () -> {
            //登录状态 此处需要异步
            int login = SinvieLoginMgr.getInstance().login(sinvieLoginParam);

            // 防止 登录接口调用 失败，还在Loading情况 (不是登录成功失败,是调用失败)
            if (login != 0) {
                getView().onHWLoginFail(login, "登录接口调用失败");
                getView().dismissLoginDialog(false);
            }
            // 设置默认的清晰度
            int result = SinvieLoginMgr.getInstance().setVideoDefinitionPolicy(SinvieCallConstant.VIDEO_DEFINITION_SD);
            SinvieDemoLog.e(TAG, "setVideoDefinitionPolicy result= " + result);
//            sharedPreferences.edit().putInt(LoginConstant.VIDEO_DEFINITION_POLICY,
//                    CallConstant.VIDEO_DEFINITION_SD).apply();

            SinvieUserInfo.getInstance().setHwLoginUsername(sinvieLoginParam.getUserName());
            SinvieUserInfo.getInstance().setHwLoginPassword(sinvieLoginParam.getPassword());
            //登录成功 导入媒体文件(来电铃声)
            importFile();

            //账号密码
//            UserInfo.getInstance().setAccount(loginParam.getUserName());
//            UserInfo.getInstance().setPwd(loginParam.getPassword());
        };
        return mRunnable;
    }

    @Override
    public void doIMVideoCallLogin() {
        //IM Login
        SinvieTUIKit.login(SinvieUserInfo.getInstance().getUserId(), SinvieUserInfo.getInstance().getUserSig(), new ISinvieUIKitCallBack() {
            @Override
            public void onError(String module, final int code, final String desc) {
                if (isViewAtteached()) {
                    getView().onIMLoginFail(code, desc);
                    getView().dismissLoginDialog(false);
                }
                SinvieDemoLog.i(TAG, "imLogin errorCode = " + code + ", errorInfo = " + desc);

            }

            @Override
            public void onSuccess(Object data) {
                SinvieUserInfo.getInstance().setAutoLogin(true);
                if (isViewAtteached()) {
                    getView().onIMLoginSuccess();
                }

            }
        });
    }

    @Override
    public void doAnonymousCall(String callNumber, boolean isVideo, String primaryIp,
                                String primaryPort, String backupIp1, String backupPort1,
                                String sipPort, boolean isVpn) {
        SinvieCallMgr.getInstance().startAnonymousCall(anonymous, callNumber, isVideo, primaryIp,
                TextUtils.isEmpty(primaryPort) ? 0 : Integer.parseInt(primaryPort),
                backupIp1, TextUtils.isEmpty(backupPort1) ? 0 : Integer.parseInt(backupPort1),
                "", 0,
                "", 0,
                TextUtils.isEmpty(backupPort1) ? 0 : Integer.parseInt(sipPort), isVpn);
    }

    /**
     * import file. 导入资源文件
     */
    private void importFile() {
        SinvieDemoLog.e(TAG, "import media file!~");
        Executors.newFixedThreadPool(SinvieLoginConstant.FIXED_NUMBER).execute(new Runnable() {
            @Override
            public void run() {
                importMediaFile();
                importBmpFile();
                importAnnotFile();
            }
        });
    }

    private void importBmpFile() {
        if (SinvieFileUtil.isSdCardExist()) {
            try {
                String bmpPath =
                        Environment.getExternalStorageDirectory() + File.separator + BMP_FILE;
                InputStream bmpInputStream = mContext.getAssets().open(BMP_FILE);
                SinvieFileUtil.copyFile(bmpInputStream, bmpPath);
            } catch (IOException e) {
                SinvieDemoLog.e(TAG, "importBmpFile->" + e.getMessage());
            }
        }
    }

    private void importAnnotFile() {
        if (SinvieFileUtil.isSdCardExist()) {
            try {
                String bmpPath =
                        Environment.getExternalStorageDirectory() + File.separator + ANNOT_FILE;
                File file = new File(bmpPath);
                if (!file.exists()) {
                    file.mkdir();
                }

                String[] bmpNames = new String[]{"check.bmp", "xcheck.bmp", "lpointer.bmp",
                        "rpointer.bmp", "upointer.bmp", "dpointer.bmp", "lp.bmp"};
                String[] paths = new String[bmpNames.length];

                for (int list = 0; list < paths.length; ++list) {
                    paths[list] = bmpPath + File.separator + bmpNames[list];
                    InputStream bmpInputStream = mContext.getAssets().open(bmpNames[list]);
                    SinvieFileUtil.copyFile(bmpInputStream, paths[list]);
                }

            } catch (IOException e) {
                SinvieDemoLog.e(TAG, "importAnnotFile->" + e.getMessage());
            }
        }

    }

    private void importMediaFile() {
        if (SinvieFileUtil.isSdCardExist()) {
            try {
                String mediaPath =
                        Environment.getExternalStorageDirectory() + File.separator + RINGING_FILE;
                InputStream mediaInputStream = mContext.getAssets().open(RINGING_FILE);
                SinvieFileUtil.copyFile(mediaInputStream, mediaPath);

                String ringBackPath =
                        Environment.getExternalStorageDirectory() + File.separator + RING_BACK_FILE;
                InputStream ringBackInputStream = mContext.getAssets().open(RING_BACK_FILE);
                SinvieFileUtil.copyFile(ringBackInputStream, ringBackPath);
            } catch (IOException e) {
                SinvieDemoLog.e(TAG, "importMediaFile->" + e.getMessage());
            }
        }
    }

    @Override
    public void destroyPresenter() {
        //清空所有的 消息
        if (mMyHandler != null) {
            mMyHandler.removeCallbacksAndMessages(null);
        }
    }

    private class MyHandler extends Handler {
        private WeakReference<SinvieLoginBasePresenter> mPresenterWeakReference;

        public MyHandler(@NonNull @NotNull Looper looper, SinvieLoginBasePresenter presenter) {
            super(looper);
            mPresenterWeakReference = new WeakReference<>(presenter);
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);

        }
    }
}
