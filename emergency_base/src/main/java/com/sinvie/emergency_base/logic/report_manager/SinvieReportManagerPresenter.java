package com.sinvie.emergency_base.logic.report_manager;

import android.content.Context;

import com.sinvie.emergency_conf.CommonService.util.SinvieDeviceManager;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.http.SinvieNetContact;
import com.sinvie.emergency_base.ui.base.SinvieMVPBasePresenter;

/**
 * @author: JonsonBob
 * @date: 2021/8/23
 * @Des: 类描述
 */
public class SinvieReportManagerPresenter extends SinvieMVPBasePresenter<SinvieReportManagerContact.ReportManagerBaseView>
        implements SinvieReportManagerContact.ReportManagerBasePresenter {

    private static final String TAG = SinvieReportManagerPresenter.class.getSimpleName();
    private Context mContext;

    public SinvieReportManagerPresenter(Context context) {
        mContext = context;
    }

    @Override
    public void getReportManagerList(int index, String keyWorld) {
        //判断网络
        if (!SinvieDeviceManager.isNetworkAvailable(mContext)) {
//            DemoLog.e(TAG, "network has been disconnected");
            getView().getReportManagerFail(SinvieNetContact.ERROR_NET,  mContext.getString(R.string.sinvie_network_be_disconnected));
            getView().dismissLoadingDialog();
            return;
        }

        getView().getReportManagerSuccess(null);
    }
}
