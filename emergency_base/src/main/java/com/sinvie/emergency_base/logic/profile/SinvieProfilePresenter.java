package com.sinvie.emergency_base.logic.profile;

import android.content.Context;

import com.sinvie.emergency_conf.CommonService.util.SinvieDeviceManager;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieContactorBean;
import com.sinvie.emergency_base.http.SinvieNetContact;
import com.sinvie.emergency_base.http.base.SinvieBaseObserver;
import com.sinvie.emergency_base.http.common.SinvieCommonNetUtils;
import com.sinvie.emergency_base.http.repository.SinvieBaseURLRepository;
import com.sinvie.emergency_base.ui.base.SinvieMVPBasePresenter;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * @author: JonsonBob
 * @date: 2021/7/15
 * @Des: 类描述
 */
public class SinvieProfilePresenter extends SinvieMVPBasePresenter<SinvieProfileContact.ProfileBaseView> implements SinvieProfileContact.ProfileBasePresenter {

    private static final String TAG = SinvieProfilePresenter.class.getSimpleName();

    private Context mContext;

    public SinvieProfilePresenter(Context context) {
        mContext = context;
    }

    @Override
    public void getContactorData(int pageIndex, String contactorId) {
        //判断网络
        if (!SinvieDeviceManager.isNetworkAvailable(mContext)) {
            getView().getContactorDataFail(SinvieNetContact.ERROR_NET,
                    mContext.getString(R.string.sinvie_network_be_disconnected));
            return;
        }
        SinvieCommonNetUtils.execute(SinvieCommonNetUtils.covertEvent(new SinvieBaseURLRepository().getContactData(pageIndex, contactorId)), new SinvieBaseObserver<SinvieContactorBean>() {
            @Override
            public void onSuccess(@Nullable SinvieContactorBean sinvieContactorBean) {
                if (isViewAtteached()) {
                    getView().getContactorDataSuccess(sinvieContactorBean);
                }
            }

            @Override
            public void onFailure(int errCode, @NotNull String errMsg) {
                if (isViewAtteached()) {
                    getView().getContactorDataFail(errCode, errMsg);
                }
            }

        });
    }
}
