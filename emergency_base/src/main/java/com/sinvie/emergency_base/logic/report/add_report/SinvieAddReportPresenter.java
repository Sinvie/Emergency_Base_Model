package com.sinvie.emergency_base.logic.report.add_report;

import android.content.Context;

import com.sinvie.emergency_conf.CommonService.util.SinvieDeviceManager;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieReportEventInfo;
import com.sinvie.emergency_base.bean.SinvieReportTypeBean;
import com.sinvie.emergency_base.bean.SinvieSaveFileBean;
import com.sinvie.emergency_base.http.SinvieNetContact;
import com.sinvie.emergency_base.http.base.SinvieBaseObserver;
import com.sinvie.emergency_base.http.common.SinvieCommonNetUtils;
import com.sinvie.emergency_base.http.repository.SinvieBaseURLRepository;
import com.sinvie.emergency_base.ui.base.SinvieMVPBasePresenter;
import com.sinvie.emergency_base.utils.SinvieDemoLog;
import com.sinvie.emergency_base.utils.SinvieFileUtil;
import com.sinvie.emergency_base.utils.SinvieTimeUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/11
 * @Des: 类描述
 */
public class SinvieAddReportPresenter extends SinvieMVPBasePresenter<SinvieAddReportContact.AddReportBaseView> implements SinvieAddReportContact.AddReportBasePresenter {

    private static final String TAG = SinvieAddReportPresenter.class.getSimpleName();

    private Context mContext;

    public SinvieAddReportPresenter(Context context) {
        mContext = context;
    }

    @Override
    public void submitReport(int id, String title, String address, int caseClassId, String latitude,
                             String longitude, String content, List<String> attachmentIds) {
        //判断网络
        if (!SinvieDeviceManager.isNetworkAvailable(mContext)) {
            SinvieDemoLog.e(TAG, "network has been disconnected");
            if (isViewAtteached()) {
                getView().submitReportFail(mContext.getString(R.string.sinvie_network_be_disconnected));
            }
            return;
        }
        if (title.isEmpty()) {
            if (isViewAtteached()) {
                getView().submitReportFail(mContext.getString(R.string.sinvie_report_title_is_empty));
            }
            return;
        }
        if (address.isEmpty() || latitude.isEmpty() || longitude.isEmpty()) {
            if (isViewAtteached()) {
                getView().submitReportFail(mContext.getString(R.string.sinvie_report_address_is_empty));
            }
            return;
        }
        if (content.isEmpty()) {
            if (isViewAtteached()) {
                getView().submitReportFail(mContext.getString(R.string.sinvie_report_content_is_empty));
            }
            return;
        }
        if (caseClassId == -1) {
            if (isViewAtteached()) {
                getView().submitReportFail(mContext.getString(R.string.sinvie_report_case_class_is_empty));
            }
            return;
        }
        SinvieReportEventInfo info = new SinvieReportEventInfo();
        if (id != -1) {
            info.setId(id);
        }
        info.setTitle(title);
        info.setAddress(address);
        info.setLatitude(latitude);
        info.setLongitude(longitude);
        info.setContent(content);
        info.setCaseClassId(caseClassId);
        info.setAttachmentIds(attachmentIds);
        info.setReportTime(SinvieTimeUtil.getTimeData());

        SinvieCommonNetUtils.execute(SinvieCommonNetUtils.covert(new SinvieBaseURLRepository().saveReportEvent(info)), new SinvieBaseObserver<String>() {
            @Override
            public void onSuccess(@Nullable String s) {
                if (isViewAtteached()) {
                    getView().submitReportSuccess();
                }
            }

            @Override
            public void onFailure(int errCode, @NotNull String errMsg) {
                if (isViewAtteached()) {
                    getView().submitReportFail(errMsg);
                }
            }
        });
    }

    @Override
    public void getEventType() {
        //判断网络
        if (!SinvieDeviceManager.isNetworkAvailable(mContext)) {
            SinvieDemoLog.e(TAG, "network has been disconnected");
            if (isViewAtteached()) {
                getView().submitReportFail(mContext.getString(R.string.sinvie_network_be_disconnected));
            }
            return;
        }
        SinvieCommonNetUtils.execute(SinvieCommonNetUtils.covert(new SinvieBaseURLRepository().getEventType()),
                new SinvieBaseObserver<List<SinvieReportTypeBean>>() {
                    @Override
                    public void onSuccess(@Nullable List<SinvieReportTypeBean> beanList) {
                        //这里需要按照自己的要求解析
                        if (isViewAtteached()) {
                            getView().getEventTypeSuccess(beanList);
                        }
                    }

                    @Override
                    public void onFailure(int errCode, @NotNull String errMsg) {
                        if (isViewAtteached()) {
                            getView().getEventTypeFail(errMsg);
                        }
                    }
                });
    }

    @Override
    public void upLoadFile(List<String> paths) {
        //判断网络
        if (!SinvieDeviceManager.isNetworkAvailable(mContext)) {
            SinvieDemoLog.e(TAG, "network has been disconnected");
            if (isViewAtteached()) {
                getView().upLoadFileFail(SinvieNetContact.ERROR_NET, mContext.getString(R.string.sinvie_network_be_disconnected));
            }
            return;
        }
        startUpLoadFile(paths);
    }

    //记录上传的次数
//    private int mIndex = 0;

    private void startUpLoadFile(List<String> paths) {
        String type;
        if (SinvieFileUtil.getSuffixFormat(paths.get(0)) == SinvieFileUtil.SUFFIX_FORMAT_IMG) {
            type = "image/*";
        } else if (SinvieFileUtil.getSuffixFormat(paths.get(0)) == SinvieFileUtil.SUFFIX_FORMAT_VIDEO) {
            type = "video/*";
        } else {
            //格式上传错误
            if (isViewAtteached()) {
                getView().upLoadFileFail(SinvieNetContact.ERROR_FILE_SUFFIX, mContext.getString(R.string.sinvie_file_suffix_error));
            }
            return;
        }
        //开始上传
        SinvieCommonNetUtils.execute(SinvieCommonNetUtils.covert(new SinvieBaseURLRepository().saveFile(paths.get(0), type, "1")), new SinvieBaseObserver<SinvieSaveFileBean>() {
            @Override
            public void onFailure(int errCode, @NotNull String errMsg) {
                if (isViewAtteached()) {
                    getView().upLoadFileFail(errCode, errMsg);
                }
            }

            @Override
            public void onSuccess(@Nullable SinvieSaveFileBean sinvieSaveFileBean) {
                //判断文件是否上传完毕
//                mIndex++;
//                if (mIndex == paths.size()) {
//                    //文件上传完毕
//                    mIndex = 0;
//                    if (isViewAtteached()) {
//                        getView().upLoadFileSuccess(saveFileBean, true);
//                    }
//                } else {
//                    getView().upLoadFileSuccess(saveFileBean, false);
//                    startUpLoadFile(paths);
//                }
                getView().upLoadFileSuccess(sinvieSaveFileBean, paths.size() == 1);
            }
        });
    }

}
