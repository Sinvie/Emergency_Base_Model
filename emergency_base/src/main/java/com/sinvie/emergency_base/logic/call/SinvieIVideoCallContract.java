package com.sinvie.emergency_base.logic.call;

import android.view.SurfaceView;

import com.sinvie.emergency_conf.CallService.SinvieCallInfo;


public interface SinvieIVideoCallContract {
    interface VideoCallBaseView {
        void switchMuteBtn(boolean currentMuteStatus);

    }

    interface VideoCallBaserPresenter {
        void setCurrentCallInfo(SinvieCallInfo sinvieCallInfo);

        void endCall();

        void transferToConference();

        String getOppositeNumber();

        boolean getIsSpeakerRoute();

        int getCallType();

        String getOppositeName();

        void muteCall();

        int switchAudioRoute();

        void videoToAudio();

        void holdVideo();

        void videoDestroy();

        void switchLocalView(boolean isClose);

        void switchCamera();

        void switchCameraStatus(boolean isCameraClose);

        SurfaceView getHideVideoView();

        SurfaceView getLocalVideoView();

        SurfaceView getRemoteVideoView();

        void setAutoRotation(Object object, boolean isOpen);
    }
}
