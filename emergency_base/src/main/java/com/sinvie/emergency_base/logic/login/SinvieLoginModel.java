package com.sinvie.emergency_base.logic.login;

import com.sinvie.emergency_conf.ServiceMgr.SinvieServiceMgr;
import com.sinvie.emergency_base.bean.SinvieCodeLoginBean;
import com.sinvie.emergency_base.bean.SinvieLoginBean;
import com.sinvie.emergency_base.ui.login.SinvieUserInfo;
import com.sinvie.emergency_live.tuikit.component.gatherimage.SinvieUserIconView;

/**
 * @author: JonsonBob
 * @date: 2021/6/29
 * @Des: 华为登录所要保存的信息
 */
public class SinvieLoginModel {

    public SinvieLoginModel() {
    }

    /**
     * 存储 账号随机码登录返回信息
     *
     * @param sinvieLoginBean
     */
    public void saveLoginBean(SinvieLoginBean sinvieLoginBean) {
        //交接数据 传递回来的数据使用userinfo 缓存 而不是继续使用loginBean
        //是否自动登录 默认自动登录 可以在 我的 界面设置
        SinvieUserInfo.getInstance().setAutoLogin(true);
        //登录姓名
        SinvieUserInfo.getInstance().setName(sinvieLoginBean.getUsername());
        //userId
        SinvieUserInfo.getInstance().setUserId(sinvieLoginBean.getUserId() + "");
        //token
        SinvieUserInfo.getInstance().setToken(sinvieLoginBean.getToken());
        //平台ID
        SinvieUserInfo.getInstance().setPlatformId(sinvieLoginBean.getPlatformId() + "");

    }

    /**
     * 存储验证码登录返回 信息
     *
     * @param loginBean
     */
    public void saveLoginBean(SinvieCodeLoginBean loginBean) {
        //是否自动登录 默认自动登录 可以在 我的 界面设置
        SinvieUserInfo.getInstance().setAutoLogin(true);
        //登录姓名
        SinvieUserInfo.getInstance().setName(loginBean.getUsername() + "");
        //userId
        SinvieUserInfo.getInstance().setUserId(loginBean.getUserId() + "");
        //token
        SinvieUserInfo.getInstance().setToken(loginBean.getToken());
        //平台ID
        SinvieUserInfo.getInstance().setPlatformId(loginBean.getPlatformId() + "");

        if (loginBean.getAppDevice() != null) {

            //存储华为的登录账号密码
            SinvieUserInfo.getInstance().setHwLoginUsername(loginBean.getAppDevice().getLoginUsername());
            SinvieUserInfo.getInstance().setHwLoginPassword(loginBean.getAppDevice().getLoginPassword());

            //记录地址 端口
            SinvieUserInfo.getInstance().setTupRegisterServer(loginBean.getAppDevice().getLoginUrl());
            SinvieUserInfo.getInstance().setTupPort(loginBean.getAppDevice().getLoginPort());

            //必须重新初始化一次 登录服务
            //初始化与去初始化业务组件类 设置网络配置
            SinvieServiceMgr.getServiceMgr().networkParam(
                    SinvieUserInfo.getInstance().getTupPort(),
                    SinvieUserInfo.getInstance().getTupRegisterServer(),
                    SinvieUserInfo.getInstance().getSipTransport());

            SinvieUserInfo.getInstance().setLoginHuaWeiConf(true);
        }else{

            //如果华为账号为空 不需要登录华为账号
            SinvieUserInfo.getInstance().setLoginHuaWeiConf(false);
            //如果返回为空 清空华为登录地址 信息
            SinvieUserInfo.getInstance().setHwLoginUsername("");
            SinvieUserInfo.getInstance().setHwLoginPassword("");
            SinvieUserInfo.getInstance().setTupRegisterServer("");
            SinvieUserInfo.getInstance().setTupPort("");
        }

    }

}
