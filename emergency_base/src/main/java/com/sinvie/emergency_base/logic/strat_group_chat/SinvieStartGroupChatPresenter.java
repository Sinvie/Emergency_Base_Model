package com.sinvie.emergency_base.logic.strat_group_chat;

import android.content.Context;

import com.sinvie.emergency_base.ui.base.SinvieMVPBasePresenter;

/**
 * @author: JonsonBob
 * @date: 2021/6/30
 * @Des: 类描述
 */
public class SinvieStartGroupChatPresenter extends SinvieMVPBasePresenter<SinvieStartGroupChatContact.StartGroupChatBaseView> implements SinvieStartGroupChatContact.StartGroupChatBasePresenter {
    private Context mContext;
    private SinvieStartGroupChatModel mSinvieStartGroupChatModel;

    public SinvieStartGroupChatPresenter(Context context) {
        mContext = context;
        mSinvieStartGroupChatModel = new SinvieStartGroupChatModel();
    }
}
