package com.sinvie.emergency_base.logic.report.add_report;

import com.sinvie.emergency_base.bean.SinvieReportTypeBean;
import com.sinvie.emergency_base.bean.SinvieSaveFileBean;

import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/11
 * @Des: 类描述
 */
public interface SinvieAddReportContact {

    interface AddReportBaseView {

        void submitReportSuccess( );

        void submitReportFail( String errMsg);

        void getEventTypeSuccess(List<SinvieReportTypeBean> beanList);

        void getEventTypeFail(String errMsg);

        void upLoadFileSuccess(SinvieSaveFileBean sinvieSaveFileBean, boolean isAll);

        void upLoadFileFail(int errCode,  String errMsg);


        void showLoadingDialog(int str,int success,int fail);

        void dismissLoadingDialog();


    }

    interface AddReportBasePresenter {

        void submitReport(int id ,String title, String address, int caseClassId, String latitude, String longitude, String content, List<String> attachmentIds );

        void getEventType();
        
        void upLoadFile(List<String> paths);
    }
}

