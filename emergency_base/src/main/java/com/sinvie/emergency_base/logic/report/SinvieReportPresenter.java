package com.sinvie.emergency_base.logic.report;

import android.content.Context;

import com.sinvie.emergency_conf.CommonService.util.SinvieDeviceManager;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.bean.SinvieReportBean;
import com.sinvie.emergency_base.http.SinvieNetContact;
import com.sinvie.emergency_base.http.base.SinvieBaseObserver;
import com.sinvie.emergency_base.http.common.SinvieCommonNetUtils;
import com.sinvie.emergency_base.http.repository.SinvieBaseURLRepository;
import com.sinvie.emergency_base.ui.base.SinvieMVPBasePresenter;
import com.sinvie.emergency_base.utils.SinvieDemoLog;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * @author: JonsonBob
 * @date: 2021/7/11
 * @Des: 类描述
 */
public class SinvieReportPresenter extends SinvieMVPBasePresenter<SinvieReportContact.ReportBaseView> implements SinvieReportContact.ReportBasePresenter {
    private static final String TAG = SinvieReportPresenter.class.getSimpleName();
    private Context mContext;

    public SinvieReportPresenter(Context context) {
        mContext = context;
    }

    @Override
    public void getReportData(int pageIndex, String keyword) {
        //判断网络
        if (!SinvieDeviceManager.isNetworkAvailable(mContext)) {
            SinvieDemoLog.e(TAG, "network has been disconnected");
            getView().getReportDataFail(SinvieNetContact.ERROR_NET,
                    mContext.getString(R.string.sinvie_network_be_disconnected));
            getView().dismissLoadingDialog();
            return;
        }
        SinvieCommonNetUtils.execute(SinvieCommonNetUtils.covert(new SinvieBaseURLRepository().
                        getReportList(pageIndex, keyword)),
                new SinvieBaseObserver<SinvieReportBean>() {
                    @Override
                    public void onSuccess(@Nullable SinvieReportBean beans) {
                        if (isViewAtteached()) {
                            getView().getReportDataSuccess(beans);
                            getView().dismissLoadingDialog();
                        }
                    }

                    @Override
                    public void onFailure(int errCode, @NotNull String errMsg) {
                        SinvieDemoLog.d(TAG, errCode + " : " + errMsg);
                        if (isViewAtteached()) {
                            getView().getReportDataFail(errCode, errMsg);
                            getView().dismissLoadingDialog();
                        }
                    }
                });
    }
}
