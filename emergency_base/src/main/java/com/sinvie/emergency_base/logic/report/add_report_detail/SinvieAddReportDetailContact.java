package com.sinvie.emergency_base.logic.report.add_report_detail;

import com.sinvie.emergency_base.bean.SinvieReportDetailBean;
import com.sinvie.emergency_base.bean.SinvieSaveFileBean;

import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/11
 * @Des: 类描述
 */
public interface SinvieAddReportDetailContact {

    interface AddReportDetailBaseView {

        void submitReportDetailSuccess();

        void submitReportDetailFail(int errCod, String errMsg);

        void upLoadFileSuccess(SinvieSaveFileBean sinvieSaveFileBean, String path, boolean isAll);

        void upLoadFileFail(int errCode,  String errMsg);
        
        void dismissLoadingDialog();
    }

    interface AddReportDetailBasePresenter {
        void submitReportDetail(int intentType ,String title, String content,
                                SinvieReportDetailBean.ReportSupplementsRoomListBean bean,
                                List<String> attachmentIds);


        void upLoadFile(List<String> paths);
    }
}
