package com.sinvie.emergency_base.logic.call;

import android.content.Context;
import android.text.TextUtils;
import android.view.SurfaceView;

import com.sinvie.emergency_conf.CallService.SinvieCallConstant;
import com.sinvie.emergency_conf.CallService.SinvieCallInfo;
import com.sinvie.emergency_conf.CallService.SinvieCallMgr;
import com.sinvie.emergency_conf.CallService.SinvieVideoMgr;
import com.sinvie.emergency_conf.CommonService.util.SinvieLogUtil;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.ui.base.SinvieMVPBasePresenter;

public class SinvieVideoCallPresenter extends SinvieMVPBasePresenter<SinvieIVideoCallContract.VideoCallBaseView>
        implements SinvieIVideoCallContract.VideoCallBaserPresenter {
    private final SinvieCallMgr mCallMgr;
    private final Context mContext;
    private final SinvieCallFunc mSinvieCallFunc;

    private int mCallID;

    private int mAudioRoute;
    private int mCallType;
    private String mOppositeName;
    private String mOppositeNumber;

    private int mCameraIndex = SinvieCallConstant.FRONT_CAMERA;

    public SinvieVideoCallPresenter(Context context) {
        this.mContext = context;
        mCallMgr = SinvieCallMgr.getInstance();
        mSinvieCallFunc = com.sinvie.emergency_base.logic.call.SinvieCallFunc.getInstance();
    }

    public void switchLocalView(boolean visible) {
    }

    /**
     * formatTimeFString
     * 格式化时间字符
     * @param longTime
     * @return String
     */
    private String formatTimeFString(long longTime) {
        String time = "%2d:%2d:%2d";
        int hour = parseLongToInt(longTime / (60 * 60));
        int min = parseLongToInt((longTime - hour * (60L * 60)) / 60);
        int sec = parseLongToInt(longTime % 60);
        time = String.format(time, hour, min, sec);

        return time.replace(' ', '0');
    }

    private int parseLongToInt(long value) {
        return Long.valueOf(value).intValue();
    }

    @Override
    public void setCurrentCallInfo(SinvieCallInfo sinvieCallInfo) {
        mOppositeName = sinvieCallInfo.getPeerDisplayName();
        mOppositeNumber = sinvieCallInfo.getPeerNumber();
        mAudioRoute = mCallMgr.getCurrentAudioRoute();
        mCallID = sinvieCallInfo.getCallID();
    }

    @Override
    public void endCall() {
        mCallMgr.endCall(mCallID);
    }

    @Override
    public void transferToConference() {
//        MeetingMgr.getInstance().callTransferToConference(mCallID);
    }

    @Override
    public String getOppositeNumber() {
        if (TextUtils.isEmpty(mOppositeNumber)) {
            return mContext.getString(R.string.sinvie_unknown);
        }
        return mOppositeNumber;
    }

    @Override
    public boolean getIsSpeakerRoute() {
        return mAudioRoute == SinvieCallConstant.TYPE_LOUD_SPEAKER;
    }

    @Override
    public int getCallType() {
        return mCallType;
    }

    @Override
    public String getOppositeName() {
        if (TextUtils.isEmpty(mOppositeName)) {
            return mContext.getString(R.string.sinvie_unknown);
        }
        return mOppositeName;
    }

    @Override
    public void muteCall() {
        boolean currentMuteStatus = mSinvieCallFunc.isMuteStatus();
        if (SinvieCallMgr.getInstance().muteMic(mCallID, !currentMuteStatus)) {
            mSinvieCallFunc.setMuteStatus(!currentMuteStatus);
            getView().switchMuteBtn(currentMuteStatus);
        }
    }

    @Override
    public int switchAudioRoute() {
        return SinvieCallMgr.getInstance().switchAudioRoute();
    }

    @Override
    public void videoToAudio() {
        SinvieCallMgr.getInstance().delVideo(mCallID);
    }

    @Override
    public void holdVideo() {
        SinvieCallMgr.getInstance().holdVideoCall(mCallID);
    }

    @Override
    public void videoDestroy() {
        if (null != SinvieCallMgr.getInstance().getVideoDevice()) {
            SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "onCallClosed destroy.");
            SinvieCallMgr.getInstance().videoDestroy();
        }
    }

    @Override
    public void switchCamera() {
        mCameraIndex = SinvieCallConstant.FRONT_CAMERA == mCameraIndex ?
                SinvieCallConstant.BACK_CAMERA : SinvieCallConstant.FRONT_CAMERA;
        SinvieCallMgr.getInstance().switchCamera(mCallID, mCameraIndex);
    }

    @Override
    public void switchCameraStatus(boolean isCameraClose) {
        if (isCameraClose) {
            SinvieCallMgr.getInstance().closeCamera(mCallID);
        } else {
            SinvieCallMgr.getInstance().openCamera(mCallID);
        }
    }

    @Override
    public SurfaceView getHideVideoView() {
        return SinvieVideoMgr.getInstance().getLocalHideView();
    }

    @Override
    public SurfaceView getLocalVideoView() {
        return SinvieVideoMgr.getInstance().getLocalVideoView();
    }

    @Override
    public SurfaceView getRemoteVideoView() {
        return SinvieVideoMgr.getInstance().getRemoteVideoView();
    }

    @Override
    public void setAutoRotation(Object object, boolean isOpen) {
        SinvieVideoMgr.getInstance().setAutoRotation(object, isOpen, 1);
    }
}
