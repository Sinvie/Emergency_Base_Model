package com.sinvie.emergency_base.logic.conference.attendee_list;

import android.content.Context;
import android.view.SurfaceView;
import android.view.ViewGroup;

import com.sinvie.emergency_conf.ConferenceService.SinvieConfBaseInfo;
import com.sinvie.emergency_conf.ConferenceService.SinvieMember;
import com.sinvie.emergency_base.logic.SinvieBaseView;

import java.util.List;


public interface SinvieIAttendeeListContract
{
    interface IAttendeeListViewSinvie extends SinvieBaseView
    {
        void refreshMemberList(List<SinvieMember> list);

        void updateAddAttendeeButton(boolean isChairman);

        void updateMuteButton(boolean isMute);

        void updateLoudSpeakerButton(int type);

        void updateTitle(String title);

        void showItemClickDialog(List<Object> items, SinvieMember sinvieMember);

        void finishActivity();

        void updateConfTypeIcon(SinvieConfBaseInfo sinvieConfBaseInfo);

        void updateVideoBtn(boolean show);

        void updateUpgradeConfBtn(boolean isInDataConf);

        void showMessage(String message);

        void updateSpeaker(String[] speakers, boolean noSpeaker);

    }

    interface IAttendeeListPresenter
    {
        void registerBroadcast();

        void unregisterBroadcast();

        String getConfID();

        void setConfID(String confID);

        SinvieConfBaseInfo getConfBaseInfo();

        void leaveConf();

        void endConf();

        void addMember(String name, String number, String account);

        void delMember(SinvieMember sinvieMember);

        void muteSelf();

        void muteMember(SinvieMember sinvieMember, boolean isMute);

        void muteConf(boolean isMute);

        void lockConf(boolean islock);

        void recordConf(boolean isRecord);

        void handUpSelf();

        void cancelMemberHandUp(SinvieMember sinvieMember);

        void requestChairman(String chairmanPassword);

        void releaseChairman();

        void postponeConf(int time);

        void updateConf();

        void switchConfMode();

        void broadcastMember(SinvieMember sinvieMember);

        void setPresenter(SinvieMember sinvieMember);

        void setHost(SinvieMember sinvieMember);

        void switchLoudSpeaker();

        void onItemClick(int position);

        void onItemDetailClick(String clickedItem, SinvieMember sinvieConfMemberEntity);

        boolean isChairMan();

        boolean isHandUp();

        boolean isInDataConf();

        boolean isPresenter();

        boolean isReqTalk();

        boolean isHost();

        boolean isConfMute();

        boolean isConfLock();

        boolean isRecord();

        boolean isSupportRecord();

        List<SinvieMember> updateAttendeeList();

        SinvieConfBaseInfo updateConfBaseInfo();

        SinvieMember selfInfo();

        boolean isMuteSelf();

        void setVideoContainer(Context context, ViewGroup smallLayout, ViewGroup hideLayout);

        SurfaceView getHideVideoView();

        SurfaceView getLocalVideoView();

        void broadcastAttendee(SinvieMember sinvieMember, boolean isBroad);

        void requestPresenter();

        void applySpeak();

        void rollCall(SinvieMember sinvieMember, boolean isRollCall);

    }
}
