package com.sinvie.emergency_base.logic.main;

import android.content.Context;
import android.content.SharedPreferences;

import com.sinvie.emergency_base.ui.base.SinvieMVPBasePresenter;

/**
 * @author: JonsonBob
 * @date: 2021/6/30
 * @Des: 类描述
 */
public class SinvieMainPresenter extends SinvieMVPBasePresenter<SinvieIMainContact.MainBaseView> implements SinvieIMainContact.MainBasePresenter {
    /**
     * 上下文对象
     */
    private final Context mContext;
    /**
     * 轻量级存储 这里用来存储user登录信息
     */
    private SharedPreferences sharedPreferences;
    /**
     * login model
     */
    private SinvieMainModel mLoginModel;

    public SinvieMainPresenter(Context context ) {
        mContext = context;
//        this.sharedPreferences = sharedPreferences;
        mLoginModel = new SinvieMainModel();
    }
}
