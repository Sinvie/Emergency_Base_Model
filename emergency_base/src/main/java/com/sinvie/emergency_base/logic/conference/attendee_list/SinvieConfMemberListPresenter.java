package com.sinvie.emergency_base.logic.conference.attendee_list;

import android.content.Context;
import android.view.SurfaceView;
import android.view.ViewGroup;

import com.huawei.ecterminalsdk.base.TsdkConfMediaType;
import com.huawei.ecterminalsdk.base.TsdkConfRole;
import com.sinvie.emergency_conf.CallService.SinvieCallMgr;
import com.sinvie.emergency_conf.CallService.SinvieVideoMgr;
import com.sinvie.emergency_conf.CommonService.common.SinvieLocContext;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieConfBroadcastConstants;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcast;
import com.sinvie.emergency_conf.CommonService.localbroadcast.SinvieLocBroadcastReceiver;
import com.sinvie.emergency_conf.CommonService.util.SinvieLogUtil;
import com.sinvie.emergency_conf.ConferenceService.SinvieConfBaseInfo;
import com.sinvie.emergency_conf.ConferenceService.SinvieMeetingMgr;
import com.sinvie.emergency_conf.ConferenceService.SinvieMember;
import com.sinvie.emergency_base.R;
import com.sinvie.emergency_base.common.SinvieUIConstants;
import com.sinvie.emergency_base.ui.base.SinvieMVPBasePresenter;

import java.util.ArrayList;
import java.util.List;

import static com.huawei.ecterminalsdk.base.TsdkConfRole.TSDK_E_CONF_ROLE_CHAIRMAN;
import static com.sinvie.emergency_conf.ConferenceService.SinvieConfConstant.ParticipantStatus.LEAVED;


public class SinvieConfMemberListPresenter extends SinvieMVPBasePresenter<SinvieIAttendeeListContract.IAttendeeListViewSinvie> implements SinvieIAttendeeListContract.IAttendeeListPresenter {
    private String confID;

    private static final String TAG = SinvieConfMemberListPresenter.class.getSimpleName();

    private static boolean flag = true;

    private String number = "";

    private String[] broadcastNames = new String[]{SinvieConfBroadcastConstants.CONF_STATE_UPDATE,
            SinvieConfBroadcastConstants.REQUEST_CONF_RIGHT_RESULT,
            SinvieConfBroadcastConstants.GET_CONF_SUBSCRIBE_RESULT,
            SinvieConfBroadcastConstants.SPEAKER_LIST_IND,
            SinvieConfBroadcastConstants.WATCH_ATTENDEE_CONF_RESULT,
            SinvieConfBroadcastConstants.BROADCAST_ATTENDEE_CONF_RESULT,
            SinvieConfBroadcastConstants.CANCEL_BROADCAST_CONF_RESULT,
            SinvieConfBroadcastConstants.CONF_INFO_PARAM,
            SinvieConfBroadcastConstants.UPDATE_HOST_INFO,
            SinvieConfBroadcastConstants.DATA_CONF_USER_LEAVE,
            SinvieConfBroadcastConstants.DATA_CONFERENCE_USER_JOIN,
            SinvieConfBroadcastConstants.DATA_CONFERENCE_PRESENTER_CHANGE_IND,
            SinvieConfBroadcastConstants.DATA_CONFERENCE_HOST_CHANGE_IND,
            //CustomBroadcastConstants.CONF_CALL_CONNECTED,
            SinvieConfBroadcastConstants.DATA_CONFERENCE_GET_DEVICE_INFO_RESULT,
            SinvieConfBroadcastConstants.DATA_CONFERENCE_EXTEND_DEVICE_INFO,
            SinvieConfBroadcastConstants.DATA_CONFERENCE_CAMERA_STATUS_UPDATE,
            SinvieConfBroadcastConstants.GET_CONF_END};

    private SinvieLocBroadcastReceiver receiver = new SinvieLocBroadcastReceiver() {
        @Override
        public void onReceive(String broadcastName, Object obj) {
            int result;
            switch (broadcastName) {
                //申请会控权限结果，仅在失败时提示用户
                case SinvieConfBroadcastConstants.REQUEST_CONF_RIGHT_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_request_conf_ctrl_fail);
                        return;
                    }
                    break;

                case SinvieConfBroadcastConstants.CONF_STATE_UPDATE:
                    String conferenceID = (String) obj;
                    if (!conferenceID.equals(confID)) {
                        return;
                    }

                    SinvieConfBaseInfo sinvieConfBaseInfo = SinvieMeetingMgr.getInstance().getCurrentConferenceBaseInfo();
                    int temp = 0;
                    if (sinvieConfBaseInfo.getRemainTime() <= 10 && sinvieConfBaseInfo.getRemainTime() != 0 && temp == 0) {
                        temp = 1;
                        SinvieLogUtil.i(SinvieUIConstants.DEMO_TAG, "showConfRestTimeDialog ");
                        getView().showMessage("Conf Time Less Than 10 Min,please post pone");

                    }

                    if (null != sinvieConfBaseInfo) {
                        //更新会议类型图标
                        getView().updateConfTypeIcon(sinvieConfBaseInfo);
                        getView().updateUpgradeConfBtn(sinvieConfBaseInfo.getMediaType() == TsdkConfMediaType.TSDK_E_CONF_MEDIA_VIDEO_DATA
                                || sinvieConfBaseInfo.getMediaType() == TsdkConfMediaType.TSDK_E_CONF_MEDIA_VOICE_DATA);
                    }

                    SinvieMember selfEntity = getSelf();
                    if (selfEntity != null) {
                        getView().updateAddAttendeeButton(selfEntity.getRole() == TSDK_E_CONF_ROLE_CHAIRMAN);
                        getView().updateMuteButton(selfEntity.isMute());
                    }

                    List<SinvieMember> sinvieMemberList =  SinvieMeetingMgr.getInstance().getCurrentConferenceMemberList();
                    if (sinvieMemberList.isEmpty()) {
                        return;
                    }
                    boolean rollCallIsLeave = isContainRollCall(sinvieMemberList);
                    if (rollCallIsLeave) {
                        flag = true;
                        number = "";
                    }
                    getView().refreshMemberList(sinvieMemberList);

                    break;

                // 邀请自己结果
                case SinvieConfBroadcastConstants.ADD_SELF_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_add_self_fail);
                        return;
                    }
                    break;

                // 会议即将结束通知
                case SinvieConfBroadcastConstants.WILL_TIMEOUT:
                    // 暂不支持
                    break;

                // 延长会议结果
                case SinvieConfBroadcastConstants.POSTPONE_CONF_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_post_pone_fail);
                        return;
                    }
                    getView().showCustomToast(R.string.sinvie_post_pone_succ);
                    break;

                // 发言人通知
                case SinvieConfBroadcastConstants.SPEAKER_LIST_IND:
                    int speakerNum = (int) obj;
                    if (0 == speakerNum) {
                        getView().updateSpeaker(new String[0], true);
                        return;
                    }
                    String[] speakerName = SinvieMeetingMgr.getInstance().getSpeakers();
                    getView().updateSpeaker(speakerName, false);
                    break;

                case SinvieConfBroadcastConstants.WATCH_ATTENDEE_CONF_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_watch_conf_fail);
                        return;
                    }
                    break;

                case SinvieConfBroadcastConstants.BROADCAST_ATTENDEE_CONF_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_broadcast_conf_fail);
                        return;
                    }
                    break;

                case SinvieConfBroadcastConstants.CANCEL_BROADCAST_CONF_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_cancel_broadcast_fail);
                        return;
                    }
                    break;

                case SinvieConfBroadcastConstants.UPDATE_HOST_INFO:
                    break;

                case SinvieConfBroadcastConstants.DATA_CONFERENCE_CAMERA_STATUS_UPDATE:
                    break;

                case SinvieConfBroadcastConstants.GET_CONF_END:
                    getView().finishActivity();
                    break;

                // 开始录制会议结果
                case SinvieConfBroadcastConstants.START_RECORD_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_start_record_fail);
                    } else {
                        getView().showCustomToast(R.string.sinvie_start_record_success);
                    }
                    break;

                // 停止录制会议结果
                case SinvieConfBroadcastConstants.STOP_RECORD_RESULT:
                    result = (int) obj;
                    if (result != 0) {
                        getView().showCustomToast(R.string.sinvie_stop_record_fail);
                    } else {
                        getView().showCustomToast(R.string.sinvie_stop_record_success);
                    }
                    break;

                default:
                    break;
            }
        }
    };

    private boolean isContainRollCall(List<SinvieMember> sinvieMemberList) {
        for (SinvieMember sinvieMember : sinvieMemberList) {
            if (number.equals(sinvieMember.getNumber())) {
                if (sinvieMember.getStatus() == LEAVED) {
                    return true;
                }
                return false;
            }
        }
        return true;
    }

    private SinvieMember getSelf() {
        return SinvieMeetingMgr.getInstance().getCurrentConferenceSelf();
    }


    @Override
    public void registerBroadcast() {
        SinvieLocBroadcast.getInstance().registerBroadcast(receiver, broadcastNames);
    }

    @Override
    public void unregisterBroadcast() {
        SinvieLocBroadcast.getInstance().unRegisterBroadcast(receiver, broadcastNames);
    }

    @Override
    public String getConfID() {
        return confID;
    }

    @Override
    public void setConfID(String confID) {
        this.confID = confID;
    }

    @Override
    public SinvieConfBaseInfo getConfBaseInfo() {
        return SinvieMeetingMgr.getInstance().getCurrentConferenceBaseInfo();
    }

    @Override
    public void leaveConf() {
        int result = SinvieMeetingMgr.getInstance().leaveConf();
        if (result != 0) {
            getView().showCustomToast(R.string.sinvie_leave_conf_fail);
            return;
        }
        SinvieLocBroadcast.getInstance().unRegisterBroadcast(receiver, broadcastNames);
    }

    @Override
    public void endConf() {
        int result = SinvieMeetingMgr.getInstance().endConf();
        if (result != 0) {
            getView().showCustomToast(R.string.sinvie_end_conf_fail);
            return;
        }
        SinvieLocBroadcast.getInstance().unRegisterBroadcast(receiver, broadcastNames);
    }

    @Override
    public void addMember(String name, String number, String account) {
        SinvieMember sinvieMember = new SinvieMember();
        sinvieMember.setNumber(number);
        sinvieMember.setDisplayName(name);
        sinvieMember.setAccountId(account);
        sinvieMember.setRole(TsdkConfRole.TSDK_E_CONF_ROLE_ATTENDEE);

        int result = SinvieMeetingMgr.getInstance().addAttendee(sinvieMember);
        if (result != 0) {
            getView().showCustomToast(R.string.sinvie_add_attendee_fail);
        }
    }

    @Override
    public void delMember(SinvieMember sinvieMember) {
        int result = SinvieMeetingMgr.getInstance().removeAttendee(sinvieMember);
        if (result != 0) {
            getView().showCustomToast(R.string.sinvie_del_attendee_fail);
        } else {
            if (number.equals(sinvieMember.getNumber())) {
                number = "";
            }
        }
    }

    @Override
    public void muteSelf() {
        SinvieMember self = getSelf();
        if (self == null) {
            return;
        }

        boolean isMute = !self.isMute();
        int result = SinvieMeetingMgr.getInstance().muteAttendee(self, isMute);
        if (result != 0) {
            if (isMute) {
                getView().showCustomToast(R.string.sinvie_mute_attendee_fail);
            } else {
                getView().showCustomToast(R.string.sinvie_un_mute_attendee_fail);
            }
        }
    }

    @Override
    public void muteMember(SinvieMember sinvieMember, boolean isMute) {
        int result = SinvieMeetingMgr.getInstance().muteAttendee(sinvieMember, isMute);
        if (result != 0) {
            if (isMute) {
                getView().showCustomToast(R.string.sinvie_mute_attendee_fail);
            } else {
                getView().showCustomToast(R.string.sinvie_un_mute_attendee_fail);
            }
        }
    }

    @Override
    public void muteConf(boolean isMute) {
        int result = SinvieMeetingMgr.getInstance().muteConf(isMute);
        if (result != 0) {
            if (isMute) {
                getView().showCustomToast(R.string.sinvie_mute_conf_fail);
            } else {
                getView().showCustomToast(R.string.sinvie_un_mute_conf_fail);
            }
        }
    }

    @Override
    public void lockConf(boolean islock) {
        int result = SinvieMeetingMgr.getInstance().lockConf(islock);
        if (result != 0) {
            if (islock) {
                getView().showCustomToast(R.string.sinvie_lock_conf_fail);
            } else {
                getView().showCustomToast(R.string.sinvie_un_lock_conf_fail);
            }
        }
    }

    @Override
    public void recordConf(boolean isRecord) {
        int result = SinvieMeetingMgr.getInstance().recordConf(isRecord);
        if (result != 0) {
            if (isRecord) {
                getView().showCustomToast(R.string.sinvie_start_record_fail);
            } else {
                getView().showCustomToast(R.string.sinvie_stop_record_fail);
            }
        }
    }

    @Override
    public void switchLoudSpeaker() {
        int type = SinvieCallMgr.getInstance().switchAudioRoute();
        getView().updateLoudSpeakerButton(type);
    }

    @Override
    public void updateConf() {
        int result = SinvieMeetingMgr.getInstance().upgradeConf();
        if (result != 0) {
            getView().showCustomToast(R.string.sinvie_upgrade_conf_fail);
            return;
        }
    }

    @Override
    public void switchConfMode() {

    }

    @Override
    public void broadcastMember(SinvieMember sinvieMember) {

    }

    @Override
    public void setPresenter(SinvieMember sinvieMember) {
//        int result = MeetingMgr.getInstance().setPresenter(member);
//        if (result != 0)
//        {
//            getView().showCustomToast(R.string.set_presenter_failed);
//        }
    }

    /**
     * 挂断与会者
     *
     * @param sinvieMember
     */
    public void hangUpAttendee(SinvieMember sinvieMember) {
        int result = SinvieMeetingMgr.getInstance().hangupAttendee(sinvieMember);
        SinvieLogUtil.i(TAG, "hangUpAttendee. result =" + result);
        if (result != 0) {
            getView().showCustomToast(R.string.sinvie_hang_up_attendee_failed);
        } else {
            if (number.equals(sinvieMember.getNumber())) {
                number = "";
            }
        }
    }

    /**
     * 重拨与会者
     *
     * @param sinvieMember
     */
    public void redialAttendee(SinvieMember sinvieMember) {
        int result = SinvieMeetingMgr.getInstance().redialAttendee(sinvieMember);
        SinvieLogUtil.i(TAG, "redialAttendee. result =" + result);
        if (result != 0) {
            getView().showCustomToast(R.string.sinvie_call_attendee_failed);
        }
    }

    @Override
    public void rollCall(SinvieMember sinvieMember, boolean isRollCall) {
        int result = SinvieMeetingMgr.getInstance().rollCall(sinvieMember, isRollCall);
        if (result != 0) {
            if (isRollCall == true) {
                getView().showCustomToast(R.string.sinvie_roll_call_failed);
            } else {
                getView().showCustomToast(R.string.sinvie_cancel_roll_call_failed);
            }
        } else {
            if (isRollCall == true) {
                flag = false;
                number = sinvieMember.getNumber();
            } else {
                flag = true;
                number = "";
            }
        }
    }

    @Override
    public void setHost(SinvieMember sinvieMember) {
        int result = SinvieMeetingMgr.getInstance().setHost(sinvieMember);
        if (result != 0) {
//            getView().showCustomToast(R.string.mute_attendee_fail);
        }
    }

    @Override
    public void handUpSelf() {
        SinvieMember self = getSelf();
        if (self == null) {
            return;
        }

        boolean isHandUp = !self.isHandUp();
        int result = SinvieMeetingMgr.getInstance().handup(isHandUp, self);
        if (result != 0) {
            if (isHandUp) {
                getView().showCustomToast(R.string.sinvie_handup_fail);
            } else {
                getView().showCustomToast(R.string.sinvie_cancel_handup_fail);
            }
        }
    }

    @Override
    public void cancelMemberHandUp(SinvieMember sinvieMember) {
        int result = SinvieMeetingMgr.getInstance().handup(false, sinvieMember);
        if (result != 0) {
            getView().showCustomToast(R.string.sinvie_cancel_handup_fail);
            return;
        }
    }

    @Override
    public void releaseChairman() {
        int result = SinvieMeetingMgr.getInstance().releaseChairman();
        if (result != 0) {
            getView().showCustomToast(R.string.sinvie_release_chairman_fail);
            return;
        }
    }

    @Override
    public void requestChairman(String chairmanPassword) {
        int result = SinvieMeetingMgr.getInstance().requestChairman(chairmanPassword);
        if (result != 0) {
            getView().showCustomToast(R.string.sinvie_request_chairman_fail);
            return;
        }
    }

    @Override
    public void postponeConf(int time) {
        int result = SinvieMeetingMgr.getInstance().postpone(time);
        if (result != 0) {
            getView().showCustomToast(R.string.sinvie_post_pone_fail);
            return;
        }
    }

    @Override
    public void requestPresenter() {
//        int result = MeetingMgr.getInstance().requestPresenter();
//        if (result != 0) {
//            getView().showCustomToast(R.string.request_presenter_fail);
//            return;
//        }
    }

    @Override
    public void applySpeak() {
        SinvieMember self = getSelf();
        if (self == null) {
            return;
        }
        //boolean isCancel = self.isReqTalk();//预留取消申请发言接口，先传默认值false
        int result = SinvieMeetingMgr.getInstance().applySpeak(false);
        if (result != 0) {
            getView().showCustomToast(R.string.sinvie_apply_speak_fail);
            return;
        }
    }

    @Override
    public void onItemClick(int position) {
        List<Object> items = new ArrayList<>();
        addLabel(items, position);
        if (!items.isEmpty()) {
            getView().showItemClickDialog(items,
                    SinvieMeetingMgr.getInstance().getCurrentConferenceMemberList().get(position));
        }
    }

    @Override
    public void onItemDetailClick(String clickedItem, SinvieMember sinvieMemberEntity) {
        if (SinvieLocContext.getString(R.string.sinvie_permit).equals(clickedItem)) {
            muteMember(sinvieMemberEntity, false);
        } else if (SinvieLocContext.getString(R.string.sinvie_forbid).equals(clickedItem)) {
            muteMember(sinvieMemberEntity, true);
        } else if (SinvieLocContext.getString(R.string.sinvie_cancel_hand_up).equals(clickedItem)) {
            cancelMemberHandUp(sinvieMemberEntity);
        } else if (SinvieLocContext.getString(R.string.sinvie_hangup).equals(clickedItem)) {
            delMember(sinvieMemberEntity);
        } else if (SinvieLocContext.getString(R.string.sinvie_reinvite).equals(clickedItem)) {
            addMember(sinvieMemberEntity.getDisplayName(), sinvieMemberEntity.getNumber(),
                    sinvieMemberEntity.getAccountId());
        } else if (SinvieLocContext.getString(R.string.sinvie_set_presenter).equals(clickedItem)) {
            setPresenter(sinvieMemberEntity);
        } else if (SinvieLocContext.getString(R.string.sinvie_set_host).equals(clickedItem)) {
            setHost(sinvieMemberEntity);
        } else if (SinvieLocContext.getString(R.string.sinvie_broadcast_contact).equals(clickedItem)) {
            broadcastAttendee(sinvieMemberEntity, true);
        } else if (SinvieLocContext.getString(R.string.sinvie_cancel_broadcast_contact).equals(clickedItem)) {
            broadcastAttendee(sinvieMemberEntity, false);
        } else if (SinvieLocContext.getString(R.string.sinvie_rollcall).equals(clickedItem)) {
            rollCall(sinvieMemberEntity, true);
        } else if (SinvieLocContext.getString(R.string.sinvie_cancel_roll_call).equals(clickedItem)) {
            rollCall(null, false);
        } else if (SinvieLocContext.getString(R.string.sinvie_hang_up_attendee).equals(clickedItem)) {
            hangUpAttendee(sinvieMemberEntity);
        } else if (SinvieLocContext.getString(R.string.sinvie_call_attendee).equals(clickedItem)) {
            redialAttendee(sinvieMemberEntity);
        } else if (SinvieLocContext.getString(R.string.sinvie_remove_attendee).equals(clickedItem)) {
            delMember(sinvieMemberEntity);
        }
    }

    @Override
    public boolean isConfMute() {
        SinvieConfBaseInfo sinvieConfBaseInfo = SinvieMeetingMgr.getInstance().getCurrentConferenceBaseInfo();
        if (sinvieConfBaseInfo == null) {
            return false;
        }
        return sinvieConfBaseInfo.isMuteAll();
    }

    @Override
    public boolean isConfLock() {
        SinvieConfBaseInfo sinvieConfBaseInfo = SinvieMeetingMgr.getInstance().getCurrentConferenceBaseInfo();
        if (sinvieConfBaseInfo == null) {
            return false;
        }
        return sinvieConfBaseInfo.isLock();
    }

    @Override
    public boolean isRecord() {
        SinvieConfBaseInfo sinvieConfBaseInfo = SinvieMeetingMgr.getInstance().getCurrentConferenceBaseInfo();
        if (sinvieConfBaseInfo == null) {
            return false;
        }
        return sinvieConfBaseInfo.isRecord();
    }


    @Override
    public boolean isSupportRecord() {
        SinvieConfBaseInfo sinvieConfBaseInfo = SinvieMeetingMgr.getInstance().getCurrentConferenceBaseInfo();
        if (sinvieConfBaseInfo == null) {
            return false;
        }
        return sinvieConfBaseInfo.isSupportRecord();
    }


    @Override
    public List<SinvieMember> updateAttendeeList() {
        List<SinvieMember> sinvieMemberList = SinvieMeetingMgr.getInstance().getCurrentConferenceMemberList();
        return sinvieMemberList;
    }

    @Override
    public SinvieConfBaseInfo updateConfBaseInfo() {
        SinvieConfBaseInfo sinvieConfBaseInfo = SinvieMeetingMgr.getInstance().getCurrentConferenceBaseInfo();

        if (null != sinvieConfBaseInfo) {
            getView().updateConfTypeIcon(sinvieConfBaseInfo);
            getView().updateUpgradeConfBtn(sinvieConfBaseInfo.getMediaType() == TsdkConfMediaType.TSDK_E_CONF_MEDIA_VIDEO_DATA
                    || sinvieConfBaseInfo.getMediaType() == TsdkConfMediaType.TSDK_E_CONF_MEDIA_VOICE_DATA);
        }

        return sinvieConfBaseInfo;
    }

    @Override
    public SinvieMember selfInfo() {
        SinvieMember self = getSelf();
        if (null == self) {
            return null;
        }
        return self;
    }

    @Override
    public boolean isMuteSelf() {
        SinvieMember self = getSelf();
        if (self == null) {
            return false;
        }
        return self.isMute();
    }

    @Override
    public void setVideoContainer(Context context, ViewGroup smallLayout, ViewGroup hideLayout) {
        if (smallLayout != null) {
            addSurfaceView(smallLayout, getLocalVideoView());
        }

        if (hideLayout != null) {
            addSurfaceView(hideLayout, getHideVideoView());
        }
    }

    @Override
    public SurfaceView getHideVideoView() {
        return SinvieVideoMgr.getInstance().getLocalHideView();
    }

    @Override
    public SurfaceView getLocalVideoView() {
        return SinvieVideoMgr.getInstance().getLocalVideoView();
    }

    @Override
    public void broadcastAttendee(SinvieMember sinvieMember, boolean isBroad) {
        int result = SinvieMeetingMgr.getInstance().broadcastAttendee(sinvieMember, isBroad);
        if (0 != result) {
            if (isBroad) {
                getView().showCustomToast(R.string.sinvie_broadcast_conf_fail);
            } else {
                getView().showCustomToast(R.string.sinvie_cancel_broadcast_fail);
            }
        }
    }

    @Override
    public boolean isChairMan() {
        SinvieMember self = getSelf();
        if (self == null) {
            return false;
        }
        return self.getRole() == TSDK_E_CONF_ROLE_CHAIRMAN ? true : false;
    }

    @Override
    public boolean isHandUp() {
        SinvieMember self = getSelf();
        if (self == null) {
            return false;
        }
        return self.isHandUp();
    }

    @Override
    public boolean isInDataConf() {
        SinvieMember self = getSelf();
        if (self == null) {
            return false;
        }
        return self.isInDataConference();
    }

    @Override
    public boolean isPresenter() {
        SinvieMember self = getSelf();
        if (self == null) {
            return false;
        }
        return self.isPresent();
    }

    @Override
    public boolean isReqTalk() {
        SinvieMember self = getSelf();
        if (self == null) {
            return false;
        }
        return self.isReqTalk();
    }

    @Override
    public boolean isHost() {
        SinvieMember self = getSelf();
        if (self == null) {
            return false;
        }
        return self.isHost();
    }


    private void addLabel(List<Object> items, int position) {
        SinvieMember sinvieMember = SinvieMeetingMgr.getInstance().getCurrentConferenceMemberList().get(position);

        //设置主讲人按钮
        if (isSupportSetPresenter(sinvieMember)) {
            if (sinvieMember.getStatus() != LEAVED) {
                items.add(SinvieLocContext.getString(R.string.sinvie_set_presenter));
            }
        }

        //设置主持人按钮
        if (isSupportSetHost(sinvieMember)) {
            items.add(SinvieLocContext.getString(R.string.sinvie_set_host));
        }

        if (!isChairMan()) {
            return;
        }
        switch (sinvieMember.getStatus()) {
            case IN_CONF:
                if (sinvieMember.isMute()) {
                    items.add(SinvieLocContext.getString(R.string.sinvie_permit));
                } else {
                    items.add(SinvieLocContext.getString(R.string.sinvie_forbid));
                }

                if (sinvieMember.isHandUp()) {
                    items.add(SinvieLocContext.getString(R.string.sinvie_cancel_hand_up));
                }

                items.add(SinvieLocContext.getString(R.string.sinvie_hang_up_attendee));
                items.add(SinvieLocContext.getString(R.string.sinvie_hangup));
                if (sinvieMember.getRole() != TSDK_E_CONF_ROLE_CHAIRMAN) {
                    if (flag && "".equals(sinvieMember.getNumber())) {
                        items.add(SinvieLocContext.getString(R.string.sinvie_rollcall));
                    } else {
                        if (!number.equals(sinvieMember.getNumber())) {
                            items.add(SinvieLocContext.getString(R.string.sinvie_rollcall));
                        } else {
                            items.add(SinvieLocContext.getString(R.string.sinvie_cancel_roll_call));
                        }

                    }
                }

                // 取消判断视频会议中才能广播，DTS单：DTS2020080511052
                /*
                if (!isVideoConf())
                {
                    return;
                }*/
                if (sinvieMember.isBroadcastSelf()) {
                    items.add(SinvieLocContext.getString(R.string.sinvie_cancel_broadcast_contact));
                } else {
                    items.add(SinvieLocContext.getString(R.string.sinvie_broadcast_contact));
                }
                break;

            case LEAVED:
                items.add(SinvieLocContext.getString(R.string.sinvie_reinvite));
                items.add(SinvieLocContext.getString(R.string.sinvie_call_attendee));
                items.add(SinvieLocContext.getString(R.string.sinvie_remove_attendee));
                break;

            case CALLING:
                items.add(SinvieLocContext.getString(R.string.sinvie_hangup));
                break;

            default:
                break;
        }
    }

    private boolean isSupportSetPresenter(SinvieMember sinvieMember) {
        if (isInDataConf() && (isChairMan() || isPresenter())) {
            if ((sinvieMember.isInDataConference()) && (!sinvieMember.isPresent())) {
                return true;
            }
        }
        return false;
    }

    private boolean isSupportSetHost(SinvieMember sinvieMember) {
        return false;
    }

    private void addSurfaceView(ViewGroup container, SurfaceView child) {
        if (child == null) {
            return;
        }
        if (child.getParent() != null) {
            ViewGroup vGroup = (ViewGroup) child.getParent();
            vGroup.removeAllViews();
        }
        container.addView(child);
    }

    private boolean isVideoConf() {
        SinvieConfBaseInfo sinvieConfBaseInfo = SinvieMeetingMgr.getInstance().getCurrentConferenceBaseInfo();
        if (null == sinvieConfBaseInfo) {
            return false;
        }

        if (sinvieConfBaseInfo.getMediaType() == TsdkConfMediaType.TSDK_E_CONF_MEDIA_VOICE
                || sinvieConfBaseInfo.getMediaType() == TsdkConfMediaType.TSDK_E_CONF_MEDIA_VOICE_DATA) {
            return false;
        } else {
            return true;
        }
    }
}
