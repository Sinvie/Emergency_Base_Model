package com.sinvie.emergency_base.logic.profile;

import com.sinvie.emergency_base.bean.SinvieContactorBean;

/**
 * @author: JonsonBob
 * @date: 2021/7/15
 * @Des: 类描述
 */
public interface SinvieProfileContact {
    interface ProfileBaseView {
        void getContactorDataSuccess(SinvieContactorBean sinvieContactorBean);

        void getContactorDataFail(int errCode, String errMsg);
    }

    interface ProfileBasePresenter {

        void getContactorData(int pageIndex, String contactorId);
    }
}
