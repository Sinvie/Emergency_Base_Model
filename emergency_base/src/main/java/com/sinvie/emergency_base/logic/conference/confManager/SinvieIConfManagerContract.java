package com.sinvie.emergency_base.logic.conference.confManager;

import android.content.Context;
import android.view.SurfaceView;
import android.view.ViewGroup;

import com.sinvie.emergency_conf.ConferenceService.SinvieConfBaseInfo;
import com.sinvie.emergency_conf.ConferenceService.SinvieConfConstant;
import com.sinvie.emergency_conf.ConferenceService.SinvieMember;
import com.sinvie.emergency_base.logic.SinvieBaseView;

import java.util.ArrayList;
import java.util.List;


public interface SinvieIConfManagerContract {
    interface ConfManagerViewSinvie extends SinvieBaseView {
        void finishActivity();

        void updateMuteButton(boolean isMute);

        void updateAttendeeButton(SinvieMember sinvieMember);

        void updateIsCheckedButton(boolean isChecked);

        void updateLocalVideo();

        void refreshMemberList(final List<SinvieMember> list);

        void showItemClickDialog(List<Object> items, SinvieMember sinvieMember);


        void updateUpgradeConfBtn(boolean isInDataConf);

        void updateConfTypeIcon(SinvieConfBaseInfo sinvieConfBaseInfo);

        void showMessage(String message);

        void startAsShare(boolean isShare);

        void setAuxDataState(boolean auxDataState);
    }

    interface ConfManagerPresenter {
        void registerBroadcast();

        void unregisterBroadcast();

        void setConfID(String confID);

        boolean muteSelf();

        int switchLoudSpeaker();

        void switchCamera();

        boolean isChairMan();

        void setVideoContainer(Context context, ViewGroup smallLayout, ViewGroup bigLayout,
                               ViewGroup hideLayout);

        void setVideoContainer(Context context, ViewGroup smallLayout, ViewGroup bigLayout_a,
                               ViewGroup bigLayout_b, ViewGroup bigLayout_c,
                               ViewGroup bigLayout_d, ViewGroup hideLayout);

        void setAutoRotation(Object object, boolean isOpen, int orientation);

        /**
         * 打开指定与会者的视频
         *
         * @param userID
         */
        void attachRemoteVideo(long userID, long deviceID);

        void watchAttendee(SinvieMember sinvieMember);

        void setConfMode(SinvieConfConstant.ConfVideoMode confVideoMode);

        /**
         * 共享自己的视频
         */
        void shareSelfVideo(long deviceID);

        void closeConf();

        void finishConf();

        void leaveVideo();

        List<SinvieMember> getMemberList();

        void changeLocalVideoVisible(boolean visible);

        boolean closeOrOpenLocalVideo(boolean close);

        SurfaceView getHideVideoView();

        SurfaceView getLocalVideoView();

        SurfaceView getRemoteVideoView();

        ArrayList<SurfaceView> getSvcRemoteVideoView();

        void onItemClick(int position);

        void onItemDetailClick(String clickedItem, SinvieMember sinvieConfMemberEntity);

        void requestChairman(String chairmanPassword);

        void releaseChairman();

        void muteConf(boolean isMute);

        void lockConf(boolean isLock);

        void recordConf(boolean isRecord);

        void handUpSelf();

        boolean isHandUp();

        boolean isConfMute();

        boolean isConfLock();

        boolean isRecord();

        boolean isSupportRecord();

        void updateConf();

        void addMember(String name, String number, String account);

        void setSelfPresenter();

        boolean isReqTalk();

        String getConfBaseInfoSubject();
    }
}
