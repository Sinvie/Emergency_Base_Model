package com.sinvie.emergency_base.logic.electronic

import android.content.Context
import android.util.Log
import com.amap.api.maps.model.LatLng
import com.sinvie.emergency_conf.CommonService.util.SinvieDeviceManager
import com.sinvie.emergency_base.R
import com.sinvie.emergency_base.bean.electronic.*
import com.sinvie.emergency_base.http.base.SinvieBaseObserver
import com.sinvie.emergency_base.http.common.convertProtect
import com.sinvie.emergency_base.http.common.convertRisk
import com.sinvie.emergency_base.http.common.covert
import com.sinvie.emergency_base.http.common.execute
import com.sinvie.emergency_base.http.nearby.convert
import com.sinvie.emergency_base.http.repository.*
import com.sinvie.emergency_base.logic.electronic.SinvieElectronicContact.ElectronicBasePresenter
import com.sinvie.emergency_base.logic.electronic.SinvieElectronicContact.ElectronicBaseView
import com.sinvie.emergency_base.ui.base.SinvieMVPBasePresenter

class SinvieElectronicPresenter(context: Context) : SinvieMVPBasePresenter<ElectronicBaseView>(),
    ElectronicBasePresenter {
    private val TAG = SinvieElectronicPresenter::class.java.simpleName
    private val mContext = context

    override val electronicData: Unit
        get() = TODO("Not yet implemented")

    /**
     * 周边检索，需先统计资源情况，确定可以用于检索的类型，以在周边检索的顶部栏显示
     */
    fun getResources(position: LatLng, searchRadius: Float, resourceList: List<Int>? = null) {
        //判断网络
        if (SinvieDeviceManager.isNetworkAvailable(mContext)) {
            val searchParam = NearbySearchParam(1).apply {
                currentPage = 1
                keywords = ""
                pageSize = 10
                eventId = -1
                if (resourceList != null) {
                    searchAllType = 0
                    resoureTypeList = resourceList
                } else {
                    searchAllType = 1
                }
                longitude = position.longitude
                latitude = position.latitude
                radius = (searchRadius / 1000).toInt()
                resoureTypeList = mutableListOf()
                otherParam = OtherParam(0, "")
            }

            nearbySearch(searchParam).convert()
                .execute(object : SinvieBaseObserver<List<ResourceTypeBean>>() {
                    override fun onFailure(errCode: Int, errMsg: String) {
                        if (isViewAtteached) Log.e(TAG, "search errCode:$errCode, errMsg:$errMsg")
                    }

                    override fun onSuccess(t: List<ResourceTypeBean>?) {
                        Log.d(TAG, "nearby search result:$t")
                        view.getReportDataSuccess(t)
                    }
                })
        } else {
            Log.e(TAG, "network has been disconnected")
            if (isViewAtteached) view.getResourceFail(mContext.getString(R.string.sinvie_network_be_disconnected))
        }
    }

    /**
     * 设备详情
     */
    fun getDeviceDetail(deviceId: String) {
        getDeviceInfo(deviceId).covert().execute(object : SinvieBaseObserver<DeviceInfoBean>() {
            override fun onFailure(errCode: Int, errMsg: String) {
                if (isViewAtteached) Log.e(TAG, "search errCode:$errCode, errMsg:$errMsg")
            }

            override fun onSuccess(t: DeviceInfoBean?) {
                t?.let { view.getDeviceInfoSuccess(it) }
            }
        })
    }

    fun getRiskDetail(id: String) {
        getRiskInfo(id).convertRisk().execute(object : SinvieBaseObserver<RiskInfo>() {
            override fun onFailure(errCode: Int, errMsg: String) {
                if (isViewAtteached) Log.e(TAG, "risk errCode:$errCode, errMsg:$errMsg")
            }

            override fun onSuccess(t: RiskInfo?) {
                Log.d(TAG, "风险隐患详情：$t")
                t?.let { view.getRiskInfoSuccess(it) }
            }
        })
    }

    fun getEquipLibDetail(id: String) {
        getEquipLibInfo(id).covert().execute(object : SinvieBaseObserver<String>() {
            override fun onFailure(errCode: Int, errMsg: String) {
                if (isViewAtteached) Log.e(TAG, "equip errCode:$errCode, errMsg:$errMsg")
            }

            override fun onSuccess(t: String?) {
                Log.d(TAG, "装备库详情：$t")
                t?.let { view.getEquipLibInfoSuccess(it) }
            }
        })
    }

    fun getTeamDetail(id: String) {
        getTeamInfo(id).covert().execute(object : SinvieBaseObserver<String>() {
            override fun onFailure(errCode: Int, errMsg: String) {
                if (isViewAtteached) Log.e(TAG, "equip errCode:$errCode, errMsg:$errMsg")
            }

            override fun onSuccess(t: String?) {
                Log.d(TAG, "装备库详情：$t")
                t?.let { view.getEquipLibInfoSuccess(it) }
            }
        })
    }

    fun getProtectTarget(id: String) {
        getProtectTargetInfo(id).convertProtect().execute(object : SinvieBaseObserver<ProtectBean>() {
            override fun onFailure(errCode: Int, errMsg: String) {
                if (isViewAtteached) Log.e(TAG, "equip errCode:$errCode, errMsg:$errMsg")
            }

            override fun onSuccess(t: ProtectBean?) {
                Log.d(TAG, "防护目标详情：$t")
                t?.let { view.getProtectTargetSuccess(it) }
            }
        })
    }

    fun getRefugeInfo(id: String) {
        getRefuge(id).covert().execute(object : SinvieBaseObserver<String>() {
            override fun onFailure(errCode: Int, errMsg: String) {
                if (isViewAtteached) Log.e(TAG, "refuge errCode:$errCode, errMsg:$errMsg")
            }

            override fun onSuccess(t: String?) {
                Log.d(TAG, "装备库详情：$t")
                t?.let { view.getRefugeSuccess(it) }
            }
        })
    }

    fun getResourceLibDetail(id: String) {
        getResourceLib(id).covert().execute(object : SinvieBaseObserver<String>() {
            override fun onFailure(errCode: Int, errMsg: String) {
                if (isViewAtteached) Log.e(TAG, "equip errCode:$errCode, errMsg:$errMsg")
            }

            override fun onSuccess(t: String?) {
                Log.d(TAG, "物资库详情：$t")
                t?.let { view.getEquipLibInfoSuccess(it) }
            }
        })
    }

    fun getMaterialDetail(id: String) {
        getMaterial(id).covert().execute(object : SinvieBaseObserver<String>() {
            override fun onFailure(errCode: Int, errMsg: String) {
                if (isViewAtteached) Log.e(TAG, "equip errCode:$errCode, errMsg:$errMsg")
            }

            override fun onSuccess(t: String?) {
                Log.d(TAG, "物资详情：$t")
                t?.let { view.getEquipLibInfoSuccess(it) }
            }
        })
    }
}